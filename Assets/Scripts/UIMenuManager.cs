﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class UIMenuManager : MonoBehaviour {

    public GameObject settingsMenu;
    public Toggle musicToggle;
    private bool isSettingsMenuOpen = false;

	public const string CombatScene = "BattleTime";
	public const string PlaypenScene = "Playpen";

	[Header("Counters")]
	public Transform counterParent; //where counter spawn from and return to
	public float counterDistance = 1.8f;
	public float counterLifeTime = 1.8f;
	public float counterAlphaDelay = 1.2f;
	public Ease counterEase = Ease.OutCubic;
	public BaseSlime currentSlime;
	private BaseSlime monitoredSlime;
	public delegate void Delegate();
	public Delegate OnClickOffMenu;
	public Delegate OnSlimeChanged;
	public Delegate OnStatsChanged;
	public Delegate OnCombatChanged;
	public Delegate OnUpdateHud;
	// Use this for initialization
	private List<TextMesh> counters = new List<TextMesh>();
	private int lastCounterPos = 0;
	private void OnDisable()
	{
		OnClickOffMenu = null;
		OnStatsChanged = null;
		OnSlimeChanged = null;
		OnUpdateHud = null;
	}
	private void Awake()
	{
		GameManager._Instance.uiManager = this;
		transform.GetComponent<Canvas>().worldCamera = Camera.main;
	}
	void Start ()
	{
		counterParent = GameManager._Instance.garbageCan;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (GameManager._Instance.ourSlimes.Count == 0)
			return;
		if (GameManager._Instance.playPenManager != null)
		{
			currentSlime = GameManager._Instance.GetSelectedSlime();
			//UpdatePlaypenOnlyHUD();
		}
		else if (GameManager._Instance.combatManager != null)
		{
			currentSlime = GameManager._Instance.combatManager.playerSlime;
			//UpdateCombatOnlyHUD();
		}
		if (currentSlime == null)
			return;
		if (monitoredSlime != currentSlime)
		{
			//Debug.Log("Monitoring of OnStatChange Initialized for" + slime.gameObject.name);
			if (monitoredSlime != null && monitoredSlime.OnStatChange != null)
			{
				//Debug.Log("Monitoring of OnStatChange Stopped for" + monitoredSlime.gameObject.name);
				monitoredSlime.OnStatChange -= InvokeStatChange;
			}
			monitoredSlime = currentSlime;
			//Debug.Log("Monitoring of OnStatChange Started for" + monitoredSlime.gameObject.name);
			monitoredSlime.OnStatChange += InvokeStatChange;
			InvokeSlimeChange();
			InvokeStatChange();
		}
	}
	private void LateUpdate()
	{
		if (GameManager._Instance.ourSlimes.Count == 0 || currentSlime == null)
			return;
		OnUpdateHud();
	}
	public void InvokeSlimeChange()
	{
		OnSlimeChanged();
	}
	public void InvokeStatChange()
	{
		OnStatsChanged();
	}
	public void ResetEverything()
	{
		GameManager._Instance.messageManager.InitializeMessage("RESETEVERYTHING");
	}
	public void GoToCombat()
	{
		GoToScene(2);

	}
	public void GoToScene(int _sceneID)
	{
		string sceneName = string.Empty;
		switch (_sceneID)
		{

			case 0:

				break;
			case 1:
				sceneName = PlaypenScene;
				break;
			case 2:
				sceneName = CombatScene;
				break;
		}
		StartCoroutine(InitializeLoadScreen(sceneName));
	}
	public void UpdateCombatMap()//Called in update
	{
		if(OnCombatChanged != null)
			OnCombatChanged();
	}
	private GraphicRaycaster gr;
	public GraphicRaycaster GetRayCaster
	{
		get
		{
			if(gr == null)
			{
				gr = transform.GetComponentInParent<GraphicRaycaster>();
			}
			return gr;
		}
		set
		{
			gr = value;
		}
	}

    public void OpenSettingMenu() {
        if(isSettingsMenuOpen == false) {
            settingsMenu.transform.DOScale(1, 0.25f);
            isSettingsMenuOpen = true;
        } else {
            CloseSettingMenu();
        }
    }
    public void CloseSettingMenu() {
        if (isSettingsMenuOpen == true) {
            settingsMenu.transform.DOScale(0, 0.25f);
            isSettingsMenuOpen = false;
        }
    }

    public void MuteSound() {
        if (musicToggle.isOn == true) {
            GameManager._Instance.soundManager.gameObject.SetActive(false);
        } else {
            GameManager._Instance.soundManager.gameObject.SetActive(true);
            GameManager._Instance.soundManager.enabled = true;
        }
    }

	IEnumerator InitializeLoadScreen(string sceneName)
	{
		GameManager._Instance.GameSave();
		//GameManager._Instance.GameCleanUp();
		if (LoadingScreen._Instance == null)
		{
			Instantiate(Resources.Load("GameObjects/LoadingScreen"));
			while (LoadingScreen._Instance == null)
			{
				yield return new WaitForSeconds(0.1f);
			}
		}

		LoadingScreen._Instance.GoToScene(sceneName);
	}
	
	public void LaunchCounter(Vector3 _pos, int _amount, Color _color, string flavorText = "")
	{
		TextMesh counter = null;
		if(counters.Count > 0)
		{
			for(int i = 0; i < counters.Count; ++i)
			{
				if(!counters[i].gameObject.activeInHierarchy)
				{
					counter = counters[i];
					break;
				}
			}
		} 
		if(counter == null){
			GameObject textMesh = (GameObject)Instantiate(Resources.Load("GameObjects/Counter"), counterParent);
			counter = textMesh.transform.GetComponent<TextMesh>();
			textMesh.transform.position = Vector3.zero;
			counters.Add(textMesh.GetComponent<TextMesh>());
			textMesh.gameObject.SetActive(false);
		}
		if (counter != null)
		{
			DOTween.Kill(counterParent);
			counter.text = flavorText + " " + _amount ;		
			counter.color = _color;
			counter.gameObject.SetActive(true);
			counter.transform.SetParent(GameManager._Instance.garbageCan);
			counter.transform.localScale = new Vector3(1, 1, 1);
			counter.transform.position = _pos;
			Vector3 movePos = _pos;
			lastCounterPos = -lastCounterPos;
			Vector2 addMovePos = new Vector2(counterDistance * lastCounterPos, counterDistance);
			Vector2 addRandomPos = (Random.insideUnitCircle * counterDistance * 0.7f);
			movePos += (Vector3)addMovePos + (Vector3)addRandomPos;
			counter.transform.DOMove(movePos, counterLifeTime, false).SetEase(counterEase).SetId(counterParent);
			float alpha = counter.color.a;
			
			StartCoroutine(KillCounter(counter));
			
		}
	}
	IEnumerator KillCounter(TextMesh counter)
	{
		while (counter.color.a > 0)
		{
			Color setCol = counter.color;
			setCol.a -= 0.025f;
			counter.color = setCol;
			yield return new WaitForSeconds(0.01f);

		}
		counter.transform.SetParent(counterParent);
		counter.transform.localPosition = Vector3.zero;
		counter.gameObject.SetActive(false);
	}
	public void CopySprite(SpriteRenderer renderFrom = null, Image imageFrom = null, SpriteRenderer renderTo = null, Image imageTo = null, Sprite overrideSprite = null)
	{
		Transform setImage;
		Transform getImage;

		if (renderTo != null)
			setImage = renderTo.transform;
		else if (imageTo != null)
			setImage = imageTo.transform;
		else
			return;

		if (renderFrom != null)
		{
			if (renderTo != null)
				renderTo.sprite = renderFrom.sprite;
			else if (imageTo != null)
				imageTo.sprite = renderFrom.sprite;
			getImage = renderFrom.transform;
		}
		else if (imageFrom != null)
		{
			if (renderTo != null)
				renderTo.sprite = imageFrom.sprite;
			else if (imageTo != null)
				imageTo.sprite = imageFrom.sprite;
			getImage = imageFrom.transform;
		}
		else
			return;
		if (overrideSprite != null)
		{
			if (renderTo != null)
				renderTo.sprite = overrideSprite;
			else if (imageTo != null)
				imageTo.sprite = overrideSprite;
		}
		_2dxFX_ColorChange getColorChanger = getImage.GetComponent<_2dxFX_ColorChange>();
		if (getColorChanger != null && getColorChanger.enabled)
		{
			_2dxFX_ColorChange setColorChanger = setImage.gameObject.GetComponent<_2dxFX_ColorChange>();
			if (setColorChanger == null)
				setColorChanger = setImage.gameObject.AddComponent<_2dxFX_ColorChange>();

			setColorChanger._HueShift = getColorChanger._HueShift;
			setColorChanger._Saturation = getColorChanger._Saturation;
			setColorChanger._ValueBrightness = getColorChanger._ValueBrightness;
			setColorChanger._Tolerance = getColorChanger._Tolerance;
			setColorChanger._Color = getColorChanger._Color;
			setColorChanger.enabled = true;
		}
		else if (getColorChanger != null && !getColorChanger.enabled || getColorChanger == null)
		{
			_2dxFX_ColorChange setColorChanger = setImage.gameObject.GetComponent<_2dxFX_ColorChange>();
			if (setColorChanger != null)
				setColorChanger.enabled = false;
		}
	}
	public int FormatedPercent(float tenthFloat)
	{
		return (int)(tenthFloat * 100);
	}
	public float FormatedFloat(float numberFloat)
	{
		float start = numberFloat;
		float finish = (int)(numberFloat * 100);
		finish = (float)(finish * 0.01f);
		return finish;
	}
	public string FormatedNumber(int nI, bool useDecimal = false)
	{
		string nS = nI.ToString();
		if (nI > 999)
		{
			char[] nC = nS.ToCharArray();
			int count = 0;
			nS = string.Empty;
			for (int i = 0; i < nC.Length; i++)
			{
				nS += nC[i].ToString();
				count++;
				if (count == 3)
				{
					count = 0;
					nS.Insert(i - 2, ",");
				}
			}
		}
		


		//string numberStr = string.Empty;
		//if (numberInt > 999999)
		//{
		//	int length = 1;
		//	if (numberInt > 9999999)
		//	{
		//		length++;
		//		if (numberInt > 99999999)
		//		{
		//			length++;
		//		}
		//	}
		//	string num = numberInt.ToString().Substring(0, length);
		//	string dec = numberInt.ToString().Substring(length, 1);

		//	string add = string.Empty;
		//	if (numberInt < 100000000)
		//		add += "0";
		//	if (numberInt < 10000000)
		//		add += "0";
		//	numberStr += add + num + (useDecimal ? ("." + dec) : "") + "M";
		//}
		//else if (numberInt > 999)
		//{
		//	int length = 1;
		//	if (numberInt > 9999)
		//	{
		//		length++;
		//		if (numberInt > 99999)
		//		{
		//			length++;
		//		}
		//	}
		//	string num = numberInt.ToString().Substring(0, length);
		//	string dec = numberInt.ToString().Substring(length, 1);
		//	string add = string.Empty;
		//	if (numberInt < 100000)
		//		add += "0";
		//	if (numberInt < 10000)
		//		add += "0";
		//	numberStr += add + num + (useDecimal ? ("." + dec) : "") + "K";
		//}
		//else
		//{
		//	if (numberInt < 100)
		//		numberStr += "0";
		//	if (numberInt < 10)
		//		numberStr += "0";

		//	numberStr += numberInt;

		//}
		return nS;

	}
	public void ShakeCamera()
	{
		Shake.Instance.StartShake(0.005f, 0.0008f, 0.001f, true);
	}
}
