﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleFlipper : MonoBehaviour {
	private bool flipSet = false;
	public int flipDir = 1;
	private void OnEnable()
	{
		flipSet = false;
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void LateUpdate ()
	{
		if(!flipSet)
		{
			flipSet = true;
			transform.localScale = new Vector3(flipDir, 1, 1);
		}
	}
}
