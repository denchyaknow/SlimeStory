﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEditor;
using DG.Tweening;
using Denchyaknow;
public class BaseAttack : MonoBehaviour
{
	public static string TweenID = "BASE_ATTACK_";
	public float baseForce = 1f;
	public float modForce = 1;

	[HideInInspector] public bool flipAttack = true;
	[HideInInspector] public bool rotateAttack = false;
	[HideInInspector] public PathType pathType;
	[HideInInspector] public float lobHeight = 0.5f;
	[HideInInspector] public int lobJumpCount = 1;
	[HideInInspector] public float lobJumpHeightMultiplier = 1f;
	[HideInInspector] public float lobDuration = 0.5f;
	[HideInInspector] public float lobDurationMultiplier = 1f;
	[HideInInspector] public float enableDelay = 0f;

	[HideInInspector] public float disableDelay = 0.5f;
	[HideInInspector] public bool speedBased = false;

	[HideInInspector] public Ease lobEase = Ease.Linear;
	[HideInInspector] public Ease lobJumpEase = Ease.Linear;
	private UniMob_Base myMob;
	private BaseSlime mySlime;
	private AreaEffector2D myArea
	{
		get
		{
			if (mA == null)
				mA = transform.GetComponent<AreaEffector2D>();
			return mA;
		}
	}
	private AreaEffector2D mA;
	private PointEffector2D myEffector
	{
		get
		{
			if (mE == null)
				mE = transform.GetComponent<PointEffector2D>();
			return mE;
		}
	}
	private PointEffector2D mE;
	private CircleCollider2D myCollider
	{
		get
		{
			if (mC == null)
				mC = transform.GetComponent<CircleCollider2D>();
			return mC;
		}
	}
	private CircleCollider2D mC;
	private Rigidbody2D myBody
	{
		get
		{
			if(mB == null)
				mB = transform.GetComponent<Rigidbody2D>();
			return mB;
		}
	}
	private Rigidbody2D mB;
	private Transform currentTarget;
	private Vector3 currentTargetPos;
	private int attackType;
	private int damage;
	private int hitCount = 1;
	private float attackRadius
	{
		get
		{
			if (mySlime != null)
				return mySlime.slimeStats.attackRadius;
			else if (myMob != null)
				return myMob.attackRange;
			else
				return 1;
		}
	}
	private float hitRate;
	private float attackTime;
	private bool isPlayerProjectile;
	[HideInInspector] public bool isDisabled = false;
	private LayerMask whatIsTarget;
	private List<int> lastHitMobs = new List<int>();
	public void Setup()
	{

	}
	public void Setup(BaseSlime slime)//Setup for slimes
	{
		mySlime = slime;
		if (slime.currentTargetMob != null)
			currentTarget = slime.currentTargetMob; 
		if (myArea != null)
		{
			myArea.colliderMask = GameManager._Instance.inputManager.whatIsEnemy;
			myArea.forceAngle = slime.attackRotation.z;
		}
		attackType = slime.slimeStats.attackType;
		enableDelay = slime.slimeStats.attackDelay;
		damage = Mathf.CeilToInt(slime.slimeStats.damage);
		attackTime = slime.slimeStats.attackDuration;
		hitCount = slime.slimeStats.attackHitCount;
		hitRate = slime.slimeStats.attackHitRate;

		whatIsTarget = GameManager._Instance.inputManager.whatIsEnemy;
		isPlayerProjectile = true;
		timesHit = 0;
		if (flipAttack)
		{
			foreach (ParticleFlipper flipper in transform.GetComponentsInChildren<ParticleFlipper>())
			{
				flipper.flipDir = slime.attackDirection;
			}
		}
		if (rotateAttack)
		{
			transform.GetChild(0).transform.localEulerAngles = slime.attackRotation.normalized;
		}
		foreach (ParticleTriggers triggers in transform.GetComponentsInChildren<ParticleTriggers>())
		{
			triggers.baseAttack = this;
			triggers.layer = whatIsTarget;
			triggers.force = baseForce + (modForce * (attackType == 0 ? (slime.slimeStats.strength + slime.slimeStats.strengthAddative) : (slime.slimeStats.magic + slime.slimeStats.magicAddative)));
			triggers.damage = damage;
			triggers.attackType = attackType;
			triggers.hitRate = hitRate;
			triggers.hitCount = hitCount;
			if (triggers.myArea != null)
			{
				triggers.myArea.colliderMask = whatIsTarget;
				triggers.myArea.forceAngle = slime.attackRotation.z;
			}
		}

	}
	public void Setup(MobData mob)//null code
	{
		
	}
	public void Setup(UniMob_Attacks mob)//Setup for mobs
	{
		myMob = mob;
		transform.localEulerAngles = mob.attackRotation;
		attackType = mob.attackID;
		damage = Mathf.CeilToInt(mob.mobDamage);
		attackTime = mob.attackTime;
		lobDuration = mob.attackTime;
		enableDelay = mob.attackDelay;
		hitCount = 1;
		hitRate = 1f;
		whatIsTarget = mob.whatIsPlayer;
		isPlayerProjectile = false;

		mob.OnDeath += OnParentDeath;

		foreach (ParticleFlipper flipper in transform.GetComponentsInChildren<ParticleFlipper>())
		{
			flipper.flipDir = mob.agroDirection;
		}
		foreach (ParticleTriggers triggers in transform.GetComponentsInChildren<ParticleTriggers>())
		{
			triggers.baseAttack = this;
			triggers.layer = mob.whatIsPlayer;
			triggers.force = (float)mob.attackHitForce;
			triggers.damage = damage;
			triggers.attackType = attackType;
			triggers.hitRate = hitRate;
			triggers.hitCount = hitCount;
			if (triggers.myArea != null)
			{
				triggers.myArea.colliderMask = whatIsTarget;
				triggers.myArea.forceAngle = mob.attackRotation.z;
			}
		}
	}
	private void OnDisable()
	{
		if (myMob != null)
		{
			myMob.OnDeath -= OnParentDeath;
		}
	}

	public void Fire(Vector3 target)
	{
		currentTargetPos = target;
		if (currentTarget != null)
			currentTargetPos = currentTarget.position;
		
		//transform.SetParent(GameManager._Instance.garbageCan);
		float dis = Vector3.Distance(currentTargetPos, transform.position);
		float dur = lobDuration;
		float height = lobHeight;
		Vector2 dir = (currentTargetPos - transform.position).normalized;
		switch (pathType)
		{
			case PathType.LobRigidbody:
				if (myBody == null)
				{
					pathType = PathType.LobTransform;
					Fire(currentTargetPos);
					return;

				}

				if (speedBased)
				{
					height *= dis * lobJumpHeightMultiplier;
					dur *= dis * lobDurationMultiplier;
				}
				transform.GetChild(0).transform.DOLocalJump(Vector3.zero, height, lobJumpCount, dur, false).SetId(BaseAttack.TweenID + gameObject.GetInstanceID()).SetEase(lobJumpEase);
				myBody.DOMove(currentTargetPos, dur, false).SetDelay(enableDelay).SetId(BaseAttack.TweenID + gameObject.GetInstanceID()).SetEase(lobEase)
			  .OnStart(() =>
			  {
				  transform.SetParent(GameManager._Instance.garbageCan);
			  }).OnComplete(() =>
			  {
				  DisableAttack();
			  });
				break;
			case PathType.LobTransform:

				if (speedBased)
				{
					height *= dis * lobJumpHeightMultiplier;
					dur *= dis * lobDurationMultiplier;
				}
				transform.GetChild(0).transform.DOLocalJump(Vector3.zero, height, lobJumpCount, dur, false).SetEase(lobJumpEase);
				transform.DOMove(currentTargetPos, dur, false).SetDelay(enableDelay).SetId(BaseAttack.TweenID + gameObject.GetInstanceID()).SetEase(lobEase)
					.OnStart(() =>
					{
						transform.SetParent(GameManager._Instance.garbageCan);
					})
					.OnComplete(() =>
					{
						DisableAttack();
					});
				break;
			case PathType.MoveRigibody:
				if (myBody == null)
				{
					pathType = PathType.MoveTransform;
					Fire(currentTargetPos);
					return;
				}
				if (enableDelay > 0)
				{
					float timer = 0;
					DOTween.To(() => timer, x => timer = x, 1, enableDelay)
						.OnComplete(()=>
						{
							transform.SetParent(GameManager._Instance != null ? GameManager._Instance.garbageCan : null);
							dir = ((currentTarget != null?currentTarget.position:currentTargetPos)- transform.position).normalized;
							myBody.velocity = dir * lobDuration;

						});
				}
				else
				{
					transform.SetParent(GameManager._Instance != null ? GameManager._Instance.garbageCan : null);
					dir = ((currentTarget != null ? currentTarget.position : currentTargetPos) - transform.position).normalized;
					myBody.velocity = dir * lobDuration;
				}
				
				break;
			case PathType.MoveTransform:
				
				transform.DOMove((currentTarget != null?currentTarget.position:currentTargetPos) + (Vector3)(dir * lobDuration), lobDuration, false).SetDelay(enableDelay).SetId(BaseAttack.TweenID + gameObject.GetInstanceID()).SetSpeedBased(speedBased).SetEase(lobEase)
					.OnStart(() =>
					{
						transform.SetParent(GameManager._Instance != null ? GameManager._Instance.garbageCan : null);
					}).OnComplete(() =>
					{
						DisableAttack();
					});
				break;
			case PathType.None:
				
				float time = 0;
				float timeTo = disableDelay == 0 ? attackTime : disableDelay;
				int flipDir = 1;
				if (dir.x > 0)
					flipDir = 1;
				else
					flipDir = -1;
				if (flipAttack)
					foreach (ParticleSystem sys in transform.GetComponentsInChildren<ParticleSystem>())
						sys.transform.localScale = new Vector3(flipDir, 1, 1);
				DOTween.To(() => time, x => time = x, 1, timeTo).SetDelay(enableDelay).SetId(BaseAttack.TweenID + gameObject.GetInstanceID())
			  .OnUpdate(() =>
			  {
				  transform.position = parent.transform.position;
			  }).OnComplete(() =>
			  {
				  DisableAttack();
			  });

				break;
		}
	}
	int timesHit = 0;

	
	public enum PathType
	{
		LobRigidbody,
		LobTransform,
		MoveRigibody,
		MoveTransform,
		None
	}
	public Transform parent;
	public Transform fxParent;
	public Transform fxOnHitParent;
	private bool isActive = false;
	private float timeActivated;
	private ParticleSystem[] fxParticles = new ParticleSystem[0];
	private ParticleSystem[] fxOnHitParticles = new ParticleSystem[0];

	void Awake()
	{
		ResizeLocalArray();
		ResizeOnHitArray();
	}
	public void Start()
	{
		isActive = true;
		timeActivated = Time.timeSinceLevelLoad;
		PlayLocalFX();
	}
	public void OnEnable()
	{
		isActive = true;
		timeActivated = Time.timeSinceLevelLoad;
		PlayLocalFX();


	}
	
	public void PlayLocalFX()
	{
		ResizeLocalArray();
		if (fxParticles.Length > 0)
		{
			for (int i = 0; i < fxParticles.Length; ++i)
			{
				ParticleSystem.MainModule main = fxParticles[i].main;
				main.loop = true;
				fxParticles[i].Play(true);
			}
		}
	}
	public void StopLocalFX()
	{
		ResizeLocalArray();
		if (fxParticles.Length > 0)
		{
			for (int i = 0; i < fxParticles.Length; ++i)
			{
				ParticleSystem.MainModule main = fxParticles[i].main;
				main.loop = false;
				fxParticles[i].Stop(true);
			}
		}
	}
	void ResizeLocalArray()
	{
		if (fxParticles.Length != fxParent.GetComponentsInChildren<ParticleSystem>().Length)
		{
			fxParticles = new ParticleSystem[fxParent.GetComponentsInChildren<ParticleSystem>().Length];
			if (fxParticles.Length > 0)
			{
				for (int i = 0; i < fxParticles.Length; ++i)
				{
					fxParticles[i] = fxParent.GetComponentsInChildren<ParticleSystem>()[i];
				}
			}
		}
	}
	void ResizeOnHitArray()
	{
		if (fxOnHitParticles.Length != fxOnHitParent.GetComponentsInChildren<ParticleSystem>().Length)
		{
			fxOnHitParticles = new ParticleSystem[fxOnHitParent.GetComponentsInChildren<ParticleSystem>().Length];
			if (fxOnHitParticles.Length > 0)
			{
				for (int i = 0; i < fxOnHitParticles.Length; ++i)
				{
					fxOnHitParticles[i] = fxOnHitParent.GetComponentsInChildren<ParticleSystem>()[i];
				}
			}
		}
	}
	public void PlayOnHitFX()
	{
		ResizeOnHitArray();
		if (fxOnHitParticles.Length > 0)
		{
			for (int i = 0; i < fxOnHitParticles.Length; ++i)
			{
				fxOnHitParticles[i].time = 0;
				fxOnHitParticles[i].Play(true);
			}
		}
	}
	public void DisableAttack()
	{
		if (isDisabled)
			return;
		DisableTweens();
		isDisabled = true;
		float timer0 = 1;
		PlayOnHitFX();
		if (myBody != null)
			myBody.simulated = false;
		DOTween.To(() => timer0, x => timer0 = x, 0, disableDelay).SetId(BaseAttack.TweenID + gameObject.GetInstanceID())
			.OnComplete(() =>
			{
				if (myEffector != null && myEffector.enabled)
					myEffector.enabled = false;
				StopLocalFX();
			});
	}
	public void DisableTweens()
	{
		DOTween.Kill(BaseAttack.TweenID + gameObject.GetInstanceID());
	}
	public void OnParentDeath()
	{
		DisableTweens();
		isDisabled = true;
		if (myEffector != null && myEffector.enabled)
			myEffector.enabled = false;
		StopLocalFX();
	}
	public void OnHitTarget(Transform target)
	{
		DisableTweens();
		//isActive = false;
		if (GameManager._Instance.Debugging)
			//Debug.Log("Attack "+ gameObject.GetInstanceID() + " Hit " + target.name);
			StopLocalFX();
		fxOnHitParent.SetParent(GameManager._Instance.garbageCan);
		fxOnHitParent.position = target.position;
		PlayOnHitFX();
	}
	
	// Update is called once per frame
	void Update()
	{
		if (Time.timeSinceLevelLoad - timeActivated > 5 && isActive)
		{
			isActive = false;
			if (GameManager._Instance != null ? GameManager._Instance.Debugging : false)
				//Debug.Log("Attack being disabled onUpdate " + gameObject.GetInstanceID());
				transform.SetParent(parent);
			transform.localPosition = Vector3.zero;
			fxOnHitParent.SetParent(transform);
			fxOnHitParent.transform.localPosition = Vector3.zero;
			if (GameManager._Instance != null ? !GameManager._Instance.DoNotPool : false)
				gameObject.SetActive(false);
			else
				Destroy(gameObject);
		}
	}
}
[System.Serializable]
public struct HitID
{
	public int id;
	public float time;
	public HitID(int _id, float _time)
	{
		id = _id;
		time = _time;
	}
}
