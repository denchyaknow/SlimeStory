﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public static class Sprite_Database
{
    public static Texture2D[] AllSlimes = Resources.LoadAll<Texture2D>("Sprites/Slimes").OrderBy(tex => tex.name).ToArray();
    public static Texture2D[] AllMobs = Resources.LoadAll<Texture2D>("Sprites/Mobs").OrderBy(tex => tex.name).ToArray();
    public static Texture2D[] AllEnemyWalks = Resources.LoadAll<Texture2D>("Sprites/Enemies/Walk").OrderBy(tex => tex.name).ToArray();
    public static Texture2D[] AllEnemyBattles = Resources.LoadAll<Texture2D>("Sprites/Enemies/Battle").OrderBy(tex => tex.name).ToArray();
    public static Sprite[] AllEnemyFaces = Resources.LoadAll<Sprite>("Sprites/Enemies/Face").OrderBy(tex => tex.name).ToArray();
    public static Texture2D[] AllPoop = Resources.LoadAll<Texture2D>("Sprites/Poop").OrderBy(tex => tex.name).ToArray();
	public static Texture2D[] AllDecorations = Resources.LoadAll<Texture2D>("Sprites/Decorations").OrderBy(tex => tex.name).ToArray();
	public static Sprite[] Backgrounds = Resources.LoadAll<Sprite>("Sprites/Backgrounds").OrderBy(tex => tex.name).ToArray();

    public static Sprite[] GetMobSprites(string _Name)
	{
		Sprite[] _TempSheet = Resources.LoadAll<Sprite>("Sprites/Mobs/" + _Name);
		//if (_TempSheet == null || _TempSheet.Length < 1)
			//Debug.Log("OI WTF THERES NO SLICED SPRITES FOUND AT: Resources/Sprites/Mobs/" + _Name + " Ai plse fix :( ");
		return _TempSheet;
		//if (AllMobs.Length > 0)
		//{
		//	for (int i = 0; i < AllMobs.Length; ++i)
		//	{
		//		if (AllMobs[i].name == _Name)
		//		{
		//			_TempSheet = AllMobs[i]
		//			////break;

		//		}
		//	}
		//}
		//Resources.LoadAll<Sprite>("")

	}
    public static Sprite[] GetEnemyWalkSprites(int _ID)
    {
        string name = AllEnemyWalks[_ID].name;

        Sprite[] _TempSheet = Resources.LoadAll<Sprite>("Sprites/Enemies/Walk/" + name);
        //if (_TempSheet == null || _TempSheet.Length < 1)
            //Debug.Log("OI WTF THERES NO SLICED SPRITES FOUND AT: Resources/Sprites/Mobs/Walk" + name + " Ai plse fix :( "+_ID);

        return _TempSheet;
    }
    public static Sprite[] GetEnemyBattleSprites(int _ID)
    {
        string name = AllEnemyBattles[_ID].name;

        Sprite[] _TempSheet = Resources.LoadAll<Sprite>("Sprites/Enemies/Battle/" + name);
        //if (_TempSheet == null || _TempSheet.Length < 1)
            //Debug.Log("OI WTF THERES NO SLICED SPRITES FOUND AT: Resources/Sprites/Mobs/Battle" + name + " Ai plse fix :( " + _ID);

        return _TempSheet;
    }
    public static Sprite[] GetSlimeSprites(int _ID)
	{
        string _IDHash = _ID.ToString("D3");
        int _IDHashed = 0;
        for(int i = 0; i < AllSlimes.Length; ++i)
        {
             if (AllSlimes[i].name.Substring(0, 3) == _IDHash)
            {
                _IDHashed = i;
                break;
            }
        }
		Sprite[] _TempSheet = Resources.LoadAll("Sprites/Slimes/" + AllSlimes[_IDHashed].name).OfType<Sprite>().ToArray();
		//if(_TempSheet == null || _TempSheet.Length < 1)
			//Debug.Log("OI WTF THERES NO SLICED SPRITES FOUND AT: Resources/Sprites/Slimes/" + _ID + " Ai plse fix :( ");
		return _TempSheet;
	}
    public static Sprite[] GetFoodSprites(string _Name)
    {
        Sprite[] _TempSheet = Resources.LoadAll<Sprite>("Sprites/Food/" + _Name).OfType<Sprite>().ToArray();
        //if (_TempSheet == null || _TempSheet.Length < 1)
            //Debug.Log("OI WTF THERES NO SLICED SPRITES FOUND AT: Resources/Sprites/Food/" + _Name + " Ai plse fix :( ");
        return _TempSheet;
    }
	public static Sprite GetDecorSprites(int _ID)
	{
		string _IDHash = _ID.ToString("D3");
		int _IDHashed = 0;
		for(int i = 0; i < AllPoop.Length; ++i)
		{
			if(AllPoop[i].name.Substring(0, 3) == _IDHash)
			{
				_IDHashed = i;
				break;
			}
		}
		Sprite _TempSheet = Resources.Load<Sprite>("Sprites/Decoration/" + AllDecorations[_IDHashed]);
		//if(_TempSheet == null)
			//Debug.Log("OI WTF THERES NO SLICED SPRITES FOUND AT: Resources/Sprites/Decorations/" + _ID + " Ai plse fix :( ");
		return _TempSheet;
	}
	public static Sprite[] GetPoopSprites(int _ID)
    {
        string _IDHash = _ID.ToString("D3");
        int _IDHashed = 0;
        for (int i = 0; i < AllPoop.Length; ++i)
        {
            if (AllPoop[i].name.Substring(0, 3) == _IDHash)
            {
                _IDHashed = i;
                break;
            }
        }
        Sprite[] _TempSheet = Resources.LoadAll<Sprite>("Sprites/Poop/" + AllPoop[_IDHashed]).OfType<Sprite>().ToArray();
        //if (_TempSheet == null || _TempSheet.Length < 1)
            //Debug.Log("OI WTF THERES NO SLICED SPRITES FOUND AT: Resources/Sprites/Poop/" + _IDHashed + " Ai plse fix :( ");
        return _TempSheet;
    }
}
