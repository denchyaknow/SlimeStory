﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class Telegraph : MonoBehaviour {
    [Header("ReticleTelegraph")]
    public bool useReticle = true;
    public float retFadeTimeStart = 0.5f;
    public float retFadeTimeEnd = 0.5f;
    public Ease retEase = Ease.InOutExpo;
    public Ease retFadeEase = Ease.InOutExpo;
    public GameObject reticlePrefab;
    private SpriteRenderer reticleSprite;
    private Vector3 retScale;
    private bool isFading = false;
    [Header("PointerTelegraph")]
    public bool useTrail = false;
    public float trailFadeTime = 1f;
    public Ease trailEase = Ease.InOutExpo;

    private List<TrailRenderer> trails = new List<TrailRenderer>();
    public GameObject trailPrefab;
	// Use this for initialization
	void Start ()
    {
		for(int i = 0; i < 5; i++)
        {
            if(trailPrefab != null)
            {
                TrailRenderer newTrail = Instantiate(trailPrefab, transform).GetComponent<TrailRenderer>();
                trails.Add(newTrail);
                newTrail.transform.SetParent(transform);
                newTrail.transform.localPosition = Vector3.zero;
                newTrail.gameObject.SetActive(false);
            }
        }
        if (reticlePrefab != null)
        {
            reticlePrefab.transform.SetParent(transform);
            reticlePrefab.transform.localPosition = Vector3.zero;
            reticleSprite = reticlePrefab.transform.GetComponentInChildren<SpriteRenderer>();
            Color newCol = reticleSprite.color;
            newCol.a = 0;
            reticleSprite.color = newCol;
            retScale = reticlePrefab.transform.localScale;
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
        //if (reticlePrefab.transform.localScale != retScale)
        //    reticlePrefab.transform.localScale = retScale;
	}
    private void OnDestroy()
    {
        DOTween.Kill(reticlePrefab);
        Destroy(reticlePrefab);
    }
    public void ShowTelegraph(Vector3 pos, float trailSpeed, float trailLife)
    {
        if(useReticle && reticlePrefab != null)
        {

            reticlePrefab.transform.DOMove(pos, trailSpeed / 2, false).SetEase(retEase).OnStart(() =>
            {
                reticlePrefab.transform.SetParent(GameManager._Instance.garbageCan);
                reticlePrefab.transform.eulerAngles = new Vector3(45, 0, 0);
            }).OnUpdate(() =>
            {
                //float dis = Vector3.Distance(pos, reticlePrefab.transform.position);
                //if(dis < 0.4f && !isFading)
                //{
                //    isFading = true;
                //}
                  
            }).OnComplete(()=>
            {

            });
            reticleSprite.DOFade(255, (trailSpeed/2) + (retFadeTimeStart)).SetDelay(trailSpeed/4).SetEase(retFadeEase);
            reticleSprite.DOFade(0, (trailSpeed/2) + (retFadeTimeEnd)).SetDelay(trailSpeed + trailLife).SetEase(retFadeEase).OnComplete(() =>
            {
                reticlePrefab.transform.SetParent(transform);
                reticlePrefab.transform.localPosition = Vector3.zero;
                //isFading = false;
            });
        }
        if(useTrail)
        {
            TrailRenderer trail = null;
            if (trails.Count < 1)
                return;
            for (int i = 0; i < trails.Count; i++)
            {
                if (!trails[i].gameObject.activeInHierarchy)
                {
                    trails[i].gameObject.SetActive(true);
                    //trails.Remove(trails[i]);
                    trails[i].time = trailFadeTime;
                    trail = trails[i];
                    break;

                }
            }
            if (trail != null)
            {
                trail.time = trailLife;
                trail.transform.SetParent(null);

                trail.transform.DOMove(pos, trailSpeed, false).SetEase(trailEase).OnComplete(() =>
                {
                    StartCoroutine("ReturnTrail", trail);
                });
            }
        }
        
    }
    IEnumerator ReturnTrail(TrailRenderer trail)
    {
        
        yield return new WaitForSeconds(trailFadeTime);
            
        
        trail.gameObject.SetActive(false);
        trail.transform.SetParent(transform);
        trail.transform.localPosition = Vector3.zero;
    }
}
