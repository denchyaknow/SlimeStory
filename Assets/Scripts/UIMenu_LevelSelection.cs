﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class UIMenu_LevelSelection : MonoBehaviour {
	[Header("Open/Close Tween")]
	public float moveTime = 0.8f;
	public Ease moveEase = Ease.InExpo;
	[Header("Selection Tween")]
	public float selectionTime = 0.8f;
	public Ease selectionEase = Ease.InExpo;
	private bool showingInfo = false;
	public float infoTweenTime = 0.8f;
	public float infoTweenDelay = 0.4f;
	public Ease infoTweenEase = Ease.OutBounce;
	private UIMenuManager mg;
	private UIMenuManager manager
	{
		get
		{
			if(mg == null)
			{
				if(GameManager._Instance != null && GameManager._Instance.uiManager != null)
					mg = GameManager._Instance.uiManager;
				else
					mg = transform.GetComponentInParent<UIMenuManager>();
			}
			return mg;
		}
	}
	private UIRefs_LevelSelection refs;
	private bool isHidden = false;
	private bool isSelecting = false;
	private bool isShowingDeadMsg = false;
	private bool buttGoVisible = false;
	private Vector2 visiblePos = Vector2.zero;
	private Vector2 hiddenPos;

	private List<RectTransform> allLevels = new List<RectTransform>();
	private int curLvlID = 0;
	private void OnEnable()
	{
		refs = transform.GetComponent<UIRefs_LevelSelection>();
		//manager.OnClickOffMenu += CloseMenu;
	}
	private void Awake()
	{
		
	}
	void Start ()
	{
		visiblePos = new Vector2(refs.rootMenuPivot.rect.width / 2, 0);
		hiddenPos = new Vector2(-refs.rootMenuPivot.rect.width * 1.5f, 0);
		refs.rootMenuPivot.anchoredPosition = hiddenPos;
		isHidden = true;
		buttGoVisible = false;
		refs.mapGo.anchoredPosition = new Vector3(0, refs.mapGo.rect.height * 2, 0);
		refs.mapLocked.anchoredPosition = new Vector3(0, refs.mapGo.rect.height * 2, 0);
		StartLevelLoad();
		ResetSelectionInformation();
	}
	
	void Update ()
	{
		if (manager.currentSlime != null)
		{
			BaseSlime currentSlime = manager.currentSlime;
			bool slimeIsDead = currentSlime.isDead;
			if (slimeIsDead && buttGoVisible)
			{
				DisableGoButton();
				if (!isShowingDeadMsg)
				{
					isShowingDeadMsg = true;
					ShowDeadMessage();
				}
			} 
		}
	}

	void ShowDeadMessage()
	{
		MessageObj newMsg = MessageDatabase.GetMessage("YOURSLIMEDEAD");
		newMsg.msgTarget = refs.mapGo;
		newMsg.mainDelegate += () =>
		 {
			 isShowingDeadMsg = false;
			 CloseMenu();
		 };
		GameManager._Instance.messageManager.InitializeMessage(newMsg);
	}
	void GetHighMapLevel()
	{
		int highestSlimeLvl = 0;
		int highestMapLvl = 0;
		if (GameManager._Instance.ourSlimes.Count > 0)
		{
			for (int i = 0; i < GameManager._Instance.ourSlimes.Count; i++)
			{
				BaseSlime slime = GameManager._Instance.ourSlimes[i].GetComponent<BaseSlime>();
				if (slime.slimeStats.level > highestSlimeLvl)
					highestSlimeLvl = slime.slimeStats.level;
			}
		}
		
		for (int i = 0; i < GameManager._Instance.database.ZoneLvlRequirements.Length; i++)
		{
			if (highestSlimeLvl >= GameManager._Instance.database.ZoneLvlRequirements[i])
			{
				highestMapLvl = i;
			}
		}
			
		GameManager._Instance.playerData.combatMapHighest = highestMapLvl;
	}
	public void OpenMenu()
	{
		if (isHidden)
		{
			isHidden = false;
			manager.transform.GetComponentInChildren<UIMenu_Status>().moveAddDelay = moveTime * 0.6f;
			manager.transform.GetComponentInChildren<UIMenu_Status>().OnCloseMenu += CloseMenu;
			curLvlID = GameManager._Instance.playerData.combatMapIndex;
			DOTween.Kill(refs.rootMenuPivot);
			GetHighMapLevel();
			refs.rootMenuPivot.DOAnchorPosX(visiblePos.x, moveTime, true).SetId(refs.rootMenuPivot).SetEase(moveEase).OnStart(() =>
			{
				GetCurrentLevel();
				ShowSelectionInformation();
			});
		}

	}
	public void CloseMenu()
	{
		if (!isHidden)
		{
			isHidden = true;
			DOTween.Kill(refs.rootMenuPivot);
			refs.rootMenuPivot.DOAnchorPosX(hiddenPos.x, moveTime, true).SetId(refs.rootMenuPivot).SetEase(moveEase).OnComplete(() =>
			{
				manager.transform.GetComponentInChildren<UIMenu_Status>().moveAddDelay = 0;
				ResetSelectionInformation();
			});
		}
	}
	public void GoToSelectedLevel()
	{
		if(buttGoVisible)
			manager.GoToCombat();
	}
	public void ShowSelectionButtons()
	{

	}
	
	public void ShowSelectionInformation()
	{
		if (refs.mapInfoTitle != null && refs.mapInfoReq != null)
		{
			string title = SlimeDatabase.MapNames[curLvlID];
			string req = ("Lvl " + GameManager._Instance.database.ZoneLvlRequirements[curLvlID].ToString() + "+");
			float delay = infoTweenDelay;
			DOTween.Kill(refs.mapInfoTitle);
			if (showingInfo)
			{
				showingInfo = false;
				refs.mapInfoTitle.DOAnchorPosX(500, infoTweenTime/2, true).SetId(refs.mapInfoTitle).SetEase(Ease.Linear);
				refs.mapInfoReq.DOAnchorPosX(-250, infoTweenTime/2, true).SetId(refs.mapInfoTitle).SetEase(Ease.Linear);
				delay += infoTweenTime/2;
			}
			refs.mapInfoReq.DOAnchorPosX(-40, infoTweenTime, true).SetId(refs.mapInfoTitle).SetEase(infoTweenEase).SetDelay(delay);
			refs.mapInfoTitle.DOAnchorPosX(50, infoTweenTime, true).SetId(refs.mapInfoTitle).SetEase(infoTweenEase).SetDelay(delay).OnStart(() =>
			{
				UpdateSelectionInformation(title, req);
				showingInfo = true;
			});
	}
	}
	void UpdateSelectionInformation(string title, string req)
	{
		refs.mapInfoTitle.GetComponentInChildren<Text>().text = title;
		refs.mapInfoReq.GetComponentInChildren<Text>().text = req;
	}
	public void ResetSelectionInformation()
	{
		DOTween.Kill(refs.mapInfoTitle);
		refs.mapInfoTitle.DOAnchorPosX(500, infoTweenTime/2, true).SetId(refs.mapInfoTitle).SetEase(Ease.Linear);
		refs.mapInfoReq.DOAnchorPosX(-250, infoTweenTime/2, true).SetId(refs.mapInfoTitle).SetEase(Ease.Linear);
		showingInfo = false;
	}
	private void EnableGoButton()
	{
		if (!buttGoVisible)
		{
			buttGoVisible = true;
			DOTween.Kill(refs.mapGo.gameObject.GetInstanceID());
			DOTween.Kill(refs.mapLocked.gameObject.GetInstanceID());
			refs.mapGo.DOAnchorPosY(0, selectionTime, true)
				.SetId(refs.mapGo.gameObject.GetInstanceID())
				.SetEase(selectionEase)
				.SetDelay(selectionTime * 0.4f);
			refs.mapLocked.DOAnchorPosY(refs.mapGo.rect.height * 2f, selectionTime, true)
				.SetId(refs.mapLocked.gameObject.GetInstanceID())
				.SetEase(selectionEase)
				.SetDelay(selectionTime * 0.8f);
		}
	}
	private void DisableGoButton()
	{
		if (buttGoVisible)
		{
			buttGoVisible = false;
			DOTween.Kill(refs.mapGo.gameObject.GetInstanceID());
			DOTween.Kill(refs.mapLocked.gameObject.GetInstanceID());
			refs.mapGo.DOAnchorPosY(refs.mapGo.rect.height * 2f, selectionTime, true)
				.SetId(refs.mapGo.gameObject.GetInstanceID())
				.SetEase(selectionEase)
				.SetDelay(selectionTime * 0.8f);
			refs.mapLocked.DOAnchorPosY(0, selectionTime, true)
				.SetId(refs.mapLocked.gameObject.GetInstanceID())
				.SetEase(selectionEase)
				.SetDelay(selectionTime * 0.4f);

		}
	}
	public void GetLastLevel()
	{
		if (isSelecting|| GameManager._Instance.database == null)
			return;
		isSelecting = true;
		if (curLvlID == 0)
		{
			curLvlID = GameManager._Instance.database.Maps.Length - 1;
			Debug.Log("CurLVLID is 0 next level shown should be level " + (GameManager._Instance.database.Maps.Length - 1).ToString());
		}
		else
		{
			curLvlID--;
			Debug.Log("CurLVLID is "+curLvlID.ToString()+" next level shown should be level " + (GameManager._Instance.database.Maps.Length - 1).ToString());

		}
		RectTransform curLvl = allLevels[curLvlID];
		RectTransform lastLvl = refs.lvlCur.GetChild(0).transform.GetComponent<RectTransform>();
		curLvl.transform.parent = refs.lvlLast;
		curLvl.transform.localPosition = Vector3.zero;
		curLvl.transform.parent = refs.lvlCur;
		lastLvl.transform.parent = refs.lvlNext;
		if (curLvlID <= GameManager._Instance.playerData.combatMapHighest)
		{
			GameManager._Instance.playerData.combatMapIndex = curLvlID;
			EnableGoButton();
		}
		else
		{
			DisableGoButton();
		}
			ShowSelectionInformation();
		curLvl.DOAnchorPosX(0, selectionTime, true).SetId(refs.lvlCur).SetEase(selectionEase);
		lastLvl.DOAnchorPosX(0, selectionTime, true).SetId(refs.lvlCur).SetEase(selectionEase).OnComplete(() =>
		{
			isSelecting = false;
		});
		
	}
	public void GetNextLevel()
	{
		if (isSelecting || GameManager._Instance.database == null)
			return;
		isSelecting = true;
		if (curLvlID == GameManager._Instance.database.Maps.Length -1)
			curLvlID = 0;
		else
			curLvlID++;
		RectTransform curLvl = allLevels[curLvlID];
		RectTransform lastLvl = refs.lvlCur.GetChild(0).transform.GetComponent<RectTransform>();
		curLvl.transform.parent = refs.lvlNext;
		curLvl.transform.localPosition = Vector3.zero;
		curLvl.transform.parent = refs.lvlCur;
		lastLvl.transform.parent = refs.lvlLast;
		if (curLvlID <= GameManager._Instance.playerData.combatMapHighest)
		{
			GameManager._Instance.playerData.combatMapIndex = curLvlID;
			EnableGoButton();
		}
		else
			DisableGoButton();
			ShowSelectionInformation();
		lastLvl.DOAnchorPosX(0, selectionTime, true).SetId(refs.lvlCur).SetEase(selectionEase);
		curLvl.DOAnchorPosX(0, selectionTime, true).SetId(refs.lvlCur).SetEase(selectionEase).OnComplete(() =>
		{
			isSelecting = false;
		});
	}
	private void GetCurrentLevel()
	{
		int currentLvl = GameManager._Instance.playerData.combatMapIndex;
		curLvlID = currentLvl;
		EnableGoButton();
		for(int i = 0; i < allLevels.Count; i++)
		{
			if(i == currentLvl)
			{
				allLevels[i].transform.parent = refs.lvlCur;
				allLevels[i].anchoredPosition = Vector2.zero;
			} else if (i < currentLvl)
			{
				allLevels[i].transform.parent = refs.lvlLast;
				allLevels[i].anchoredPosition = Vector2.zero;
			} else if(i > currentLvl)
			{
				allLevels[i].transform.parent = refs.lvlNext;
				allLevels[i].anchoredPosition = Vector2.zero;
			}
		}
	}
	
	private void StartLevelLoad()
	{
		StartCoroutine(ContinueLevelLoad());
	}
	private void FinishLevelLoad()
	{
		int maxLvls = GameManager._Instance.database.Maps.Length;
		for (int i = 0; i < maxLvls; i++)
		{
			int index = i;
			GameObject newMap = (GameObject)Instantiate(Resources.Load("GameObjects/UI/LevelSelectionButton"), refs.lvlNext.transform);
			RectTransform newRect = newMap.transform.GetComponent<RectTransform>();
			Image imageTo = newRect.transform.GetChild(0).transform.GetChild(0).transform.GetComponent<Image>();
			SpriteRenderer renderFrom = GameManager._Instance.database.Maps[index].transform.GetComponent<BaseMap>().mapBackground.transform.GetComponent<SpriteRenderer>();
			newMap.name = index.ToString();
			manager.CopySprite(renderFrom, null, null, imageTo);
			newRect.anchoredPosition = Vector2.zero;
			allLevels.Add(newRect);
		}
	}

	IEnumerator ContinueLevelLoad()
	{
		while(GameManager._Instance.database == null)
		{
			yield return new WaitForSeconds(0.2f);
			Debug.Log("WAIT_FOR_DATABASE_LOAD");
		}
		while (GameManager._Instance.database.Maps.Length < 1)
		{
			yield return new WaitForSeconds(0.2f);
			Debug.Log("WAIT_FOR_DATABASE_LOAD_MAPS");
		}
		FinishLevelLoad();
	}
}
