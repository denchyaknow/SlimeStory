﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountModule : MonoBehaviour {

    public float cooldown = 1f;
    public int maxActive = 5;
    public int currentActive
    {
        get
        {
            return active.childCount;
        }
    }
    public Transform storage;
    public Transform active;
    public ParticleSystem timer;
    private List<ParticleSystem> counters = new List<ParticleSystem>();
    public ParticleSystem prefab;
    public bool test;
	// Use this for initialization
	void Start ()
    {
	    for(int i = 0; i < 10; i++)
        {
            ParticleSystem count = Instantiate(prefab, storage);
            count.transform.localPosition = Vector3.zero;
            counters.Add(count);
        }
        StartCoroutine(KickOffTimer());
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(test)
        {
            test = false;
            RemoveCounter();
        }
	}
    IEnumerator KickOffTimer()
    {
        yield return new WaitForSeconds(1f);
        StartCoroutine(DoTimer());
    }
    IEnumerator DoTimer()
    {
        yield return new WaitForSeconds(0.01f);
        if(currentActive != maxActive)
        {
            ParticleSystem.ShapeModule shape = timer.shape;
            shape.arcSpeedMultiplier = 1 / cooldown;           
            timer.Play(true);
            yield return new WaitForSeconds(cooldown);
            timer.Stop(true);

            if (currentActive < maxActive)
            {
                AddCounter();
            }
        }
        StartCoroutine(DoTimer());
    }
    public void AddCounter()
    {
        for(int i = 0; i < counters.Count; i++)
        {
            if(!counters[i].gameObject.activeInHierarchy)
            {
                Vector3 pos = counters[i].transform.position;
            
                counters[i].transform.SetParent(active);
                counters[i].gameObject.SetActive(true);
                counters[i].Play(true);
                break;
            }
        }
    }
    public void RemoveCounter()
    {
        if(currentActive > 0)
        {
            Transform count = active.GetChild(active.childCount - 1).transform;
            Vector3 pos = count.transform.position;
            count.gameObject.SetActive(false);
            count.SetParent(storage);
            count.transform.localPosition = Vector3.zero;
        }
    }
   
    public void ConnectModule(Transform target)
    {
        transform.SetParent(target);
        transform.localPosition = Vector3.zero;
    }
    public void DisconnectModule()
    {
        transform.SetParent(GameManager._Instance.garbageCan);
    }
}
