﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class BaseFood : BaseItem
{
	//public string foodName;
	//public int foodIndex;
	//public int foodHealthVal = 5;
	//public int foodHungerVal = 1;
	//public int foodPoopVal = 1;
	//public static ParticleSystem _InstanceFoodParticles;
	public static List<ParticleSystem> _InstanceFoodParticles = new List<ParticleSystem>();
	public bool isBeingEaten = false;
	private float timeLastBit = 0;
	public bool isDoneEaten = false;
    public bool isBeingHeld = false;
	public int currentBite = 0;
	private Sprite[] bites = null;

    public FoodData foodData;
	public ParticleSystem foodParticle
	{
		get
		{
			ParticleSystem food = null;
			if (_InstanceFoodParticles.Count > 0)
			{
				for (int i = 0; i < _InstanceFoodParticles.Count; i++)
				{
					if (_InstanceFoodParticles[i] != null)
					{
						if (!_InstanceFoodParticles[i].isPlaying)
						{
							food = _InstanceFoodParticles[i];
							break;
						}
					}
				}
			}
			if (_InstanceFoodParticles.Count == 0 || food == null)
			{
				GameObject foodParticle = (GameObject)Instantiate(Resources.Load("Effects/FoodParticles"));
				foodParticle.transform.SetParent(GameManager._Instance.garbageCan);
				food = foodParticle.GetComponent<ParticleSystem>();
				_InstanceFoodParticles.Add(food);
			}
			return food;
		}
	}
	public override void Start()
	{
		base.Start();
		
		isEdible = true;
	}

    public void OnEnable()
    {
       
    }
    public void OnDisable()
    {
        if (GameManager._Instance.ourFood.Contains(this))
            GameManager._Instance.ourFood.Remove(this);
    }
	public Color particleColor = Color.black;
	private List<Color> colList = new List<Color>();
	private List<int> colCounts = new List<int>();
	public override void Awake()
    {
		base.Awake();
		isBeingEaten = false;
		isBeingHeld = false;
		if (bites == null)
		{
			bites = Sprite_Database.GetFoodSprites(foodData.name);
			myRenderer.sprite = bites[0];
			currentBite = 0;
		}

		//Algorithm for getting average pixel color
		int sizeX = myRenderer.sprite.texture.width;
		int sizeY = myRenderer.sprite.texture.height;
		
		for (int i = 0; i < sizeX; i++)
		{
			for (int j = 0; j < sizeY; j++)
			{
				Color foodCol = myRenderer.sprite.texture.GetPixel(i, j);
				bool match = false;
				if (foodCol.a > 0.7f)
				{
				if (colList.Count > 0)
				{
					for (int k = 0; k < colList.Count; k++)
					{
						float alike = 0.005f;
						if (foodCol.b < colList[k].b + alike && foodCol.b > colList[k].b - alike)
							if (foodCol.g < colList[k].g + alike && foodCol.g > colList[k].g - alike)
								if (foodCol.r < colList[k].r + alike && foodCol.r > colList[k].r - alike)
									match = true;

						if (match)
						{
							colCounts[k]++;
							break;
						}
					}
				}
				
				if (!match)
				{
					colList.Add(foodCol);
					colCounts.Add(0);
				}
				}
			}
		}
		int maxCount = 0;
		if (colCounts.Count > 0)
		{
			for (int i = 0; i < colCounts.Count; i++)
			{
				if (i == 0)
				{
					maxCount = colCounts[i];
					particleColor = colList[i];
				}
				else if(colCounts[i] > maxCount)
				{
					maxCount = colCounts[i];
					particleColor = colList[i];
					particleColor.a = 255;
				}
			}
		}
		
		
		
	}
	public override void Update()
	{
		base.Update();
        isEdible = !isMoving() && !isBeingHeld && !isDoneEaten;
        
        Vector3 pos = transform.position;
        foodData.x = pos.x;
        foodData.y = pos.y;
        foodData.biteCount = currentBite;
		if (isBeingEaten)
		{
			mySortingGroup.sortingLayerName = "Player_Front";
			if (Time.timeSinceLevelLoad > timeLastBit)
			{
				isBeingEaten = false;
				mySortingGroup.sortingLayerName = "Player";
			}
		}
		if (isBeingHeld && GameManager._Instance.ourFood.Contains(this))
		{
			GameManager._Instance.ourFood.Remove(this);
		}
		else if (!isBeingHeld && !GameManager._Instance.ourFood.Contains(this))
		{
			GameManager._Instance.ourFood.Add(this);
		}
        //UpdateFoodData(transform.position, isDropped);

    }
	public void FocusSprites()
	{
		isBeingEaten = true;
		timeLastBit = Time.timeSinceLevelLoad + 4f;
	}
	public void MoveToAutoFeeder()
	{
		if(GameManager._Instance.ourFood.Contains(this))
			GameManager._Instance.ourFood.Remove(this);
        if (!GameManager._Instance.ourFoodStorage.Contains(this))
            GameManager._Instance.ourFoodStorage.Add(this);
        transform.DOScale(0.4f, 1);
		transform.DOMove(GameManager._Instance.autoFeeder.foodStorageSpawnPoint.position, 1).OnComplete(()=>
		{
            myGeneralCollider.enabled = false;
            myBody.gravityScale = 1;
			myBody.constraints = RigidbodyConstraints2D.None;
		});

	}
    public void MoveToPosition(Vector2 pos)
    {
        if (GameManager._Instance.ourFoodStorage.Contains(this))
            GameManager._Instance.ourFoodStorage.Remove(this);
        if (!GameManager._Instance.ourFood.Contains(this))
            GameManager._Instance.ourFood.Add(this);
        transform.DOScale(1f, 0.7f);
        transform.DOMove(pos, 20f).SetSpeedBased(true).OnStart(() =>
        {
            myGeneralCollider.enabled = true;
            myBody.gravityScale = 0;
            myBody.constraints = RigidbodyConstraints2D.FreezeRotation;
			  transform.localEulerAngles = Vector3.zero;
        });

    }
    
	public void ResetFood()
	{
		
        myGeneralCollider.enabled = true;
		myBody.gravityScale = 0;
		myBody.constraints = RigidbodyConstraints2D.FreezeRotation;
	}
    public override void TouchFunction(int _id, Vector3 _currentPos)
    {
        base.TouchFunction(_id, _currentPos);
        switch(_id)
        {
            case 0:
               
                isBeingHeld = true;
                break;
            case 1:

                break;
            case 2:
               
				isBeingHeld = false;
                break;
			case 5:
				if(!GameManager._Instance.ourFood.Contains(this))
					GameManager._Instance.ourFood.Remove(this);
				Destroy(gameObject);
				GameManager._Instance.GameSave();
				break;
        }

    }
    private List<BaseSlime> slimesEatingThis = new List<BaseSlime>();
	public void Bite(BaseSlime _slime)
	{
		bool slimeAlreadyReg = false;
		if(slimesEatingThis.Count > 0)
		{
			for(int i = 0; i < slimesEatingThis.Count; ++i)
			{
				if(_slime.slimeStats.index == slimesEatingThis[i].slimeStats.index)
				{
					slimeAlreadyReg = true;
				}
			}
		}
		if(!slimeAlreadyReg)
		{
			slimesEatingThis.Add(_slime);
		}
		currentBite++;
		if (currentBite <= 3)
		{
			myRenderer.sprite = bites[currentBite];
			if (foodParticle != null)
			{
				ParticleSystem.MainModule main = foodParticle.main;
				main.startColor = particleColor;
				Vector3 pos = transform.position;
				pos.y -= 0.4f;
				foodParticle.transform.position = pos;
				foodParticle.Play();
				ParticleSystemRenderer ren = foodParticle.GetComponent<ParticleSystemRenderer>();
				ren.sortingOrder = -(int)Camera.main.WorldToScreenPoint(pos).y;
			}
         _slime.HealDamage(foodData.healthPerBite);
			_slime.AddHunger(foodData.hungerPerBite);
		}

        if (currentBite > 3 && isDoneEaten == false)
        {
            isDoneEaten = true;
            GameManager._Instance.ourFood.Remove(this);
            GameManager._Instance.playerData.slimeFood.Remove(foodData);
            foreach (BaseSlime baseSlime in slimesEatingThis)
            {
                baseSlime.eatingFood = null;
            }
			StartCoroutine(DestroyFood());
        }
	}
	IEnumerator DestroyFood()
	{
		transform.GetComponentInChildren<Collider2D>().enabled = false;
		myRenderer.DOFade(0, 0.5f);
		yield return new WaitForSeconds(4);
		Destroy(this.gameObject);

	}
	/*
	void UpdateFoodData(Vector2 pos, bool dropped)
	{
		 bool contains = false;
		 int listIndex = -1;
		 if(GameManager._Instance.ourFood.Count > 0)
		 {
			  for(int i = 0; i < GameManager._Instance.ourFood.Count; ++i)
			  {
					if(GameManager._Instance.ourFood[i].gameObject.GetInstanceID() == GetInstanceID())
					{
						 listIndex = i;
						 break;
					}
			  }

		 }
		 if(listIndex > -1)
		 {
			  ///foodIndex = GameManager._Instance.ourFood.BinarySearch(this);
			  foodData.instanceID = listIndex;
			  //Debug.Log("oldFoodData=" + GameManager._Instance.ourFood[listIndex].foodData +
						 " newFoodData=" + foodData +
						 " Index=" + foodIndex + " oldInstanceID" +GameManager._Instance.ourFood[listIndex].foodData.instanceID + " newInstanceID=" + foodData.instanceID +
						 " oldIndex=" + GameManager._Instance.playerData.slimeFood[listIndex].index + " newIndex=" + foodData.index +
						 " dataMatches=" + (foodData == GameManager._Instance.playerData.slimeFood[listIndex])
						 );
			  foodData.x = pos.x;
			  foodData.y = pos.y;
			  foodData.biteCount = currentBite;
			  GameManager._Instance.playerData.slimeFood[listIndex] = foodData;

		 } else
		 {
			  //GameManager._Instance.ourFood.Add(this);
		 }
		 if(dropped)
		 {

		 } else
		 {

		 }
	}
	*/
}


