﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class MapData
{
	public int tileID = 0;
	public BaseMap.MapSize mapSize = BaseMap.MapSize.Medium;
	public List<MapData_Mobs> mobs = new List<MapData_Mobs>();
	public MapData()
	{
		mobs = new List<MapData_Mobs>();
		mobs.Add(new MapData_Mobs());
	}
	public MapData(List<MapData_Mobs> _mobs)
	{
		mobs = _mobs;
	}
}

[System.Serializable]
public class MapData_Mobs
{
	public int mobID = 0;
	public int spawnChance = 80;
	public int spawnCount = 10;
	public MapData_Mobs()
	{
		mobID = 0;
		spawnChance = 80;
		spawnCount = 10;

	}
	public MapData_Mobs(int mID, int sChance, int sCount)
	{
		mobID = mID;
		spawnChance = sChance;
		spawnCount = sCount;

	}

}
