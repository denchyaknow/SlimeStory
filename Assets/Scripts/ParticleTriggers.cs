﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Denchyaknow;
public class ParticleTriggers :MonoBehaviour
{

	public TriggerType triggerType = TriggerType.None;
	public ParticleSystem myParticles
	{
		get
		{
			if (mP == null)
				mP = transform.GetComponent<ParticleSystem>();
			return mP;
		}
		set
		{
			mP = value;
		}
	}
	private ParticleSystem mP;
	public AreaEffector2D myArea
	{
		get
		{
			if (mA == null)
				mA = transform.GetComponent<AreaEffector2D>();
			return mA;
		}
		set
		{
			mA = value;
		}
	}
	private AreaEffector2D mA;
	public PointEffector2D myEffector
	{
		get
		{
			if (mE == null)
				mE = transform.GetComponent<PointEffector2D>();
			return mE;
		}
		set
		{
			mE = value;
		}
	}
	private PointEffector2D mE;
	public CircleCollider2D myCollider
	{
		get
		{
			if (mC == null)
				mC = transform.GetComponent<CircleCollider2D>();
			return mC;
		}
		set
		{
			mC = value;
		}

	}
	private CircleCollider2D mC;
	public List<ParticleCollisionEvent> pEvents = new List<ParticleCollisionEvent>();
	public LayerMask layer
	{
		
		set
		{
			switch (triggerType)
			{
				case TriggerType.ColliderTrigger:
				case TriggerType.ColliderCollision:
					if (myArea != null)
					{
						myArea.colliderMask = value;
					}
					break;
				case TriggerType.ParticleCollision:
					if (myParticles != null)
					{
						ParticleSystem.CollisionModule col = myParticles.collision;
						col.collidesWith = value;
					}
					break;		
			}
		}
	}
	public float force
	{
		
		set
		{
			switch (triggerType)
			{
				case TriggerType.ColliderTrigger:
				case TriggerType.ColliderCollision:
					if (myArea != null)
					{
						myArea.forceMagnitude = value;
					}
					break;
				case TriggerType.ParticleCollision:
					if (myParticles != null)
					{
						ParticleSystem.CollisionModule col = myParticles.collision;
						col.colliderForce = value;
					}
					break;
			}
		}
	}
	public int damage;
	public int attackType;
	public float hitRate;
	public int hitCount = 1;

	public BaseAttack baseAttack
	{
		get
		{
			if (bA == null)
			{
				bA = transform.GetComponentInParent<BaseAttack>();
				if (bA == null)
					bA = transform.GetComponent<BaseAttack>();
			}
			return bA;
		}
		set
		{
			bA = value;
		}
	}
	private BaseAttack bA;
	public enum TriggerType
	{
		None,
		ColliderCollision,
		ParticleCollision,
		ColliderTrigger
	}
	[SerializeField] private List<HitID> hits = new List<HitID>();

	void Start()
	{
		myParticles = transform.GetComponent<ParticleSystem>();
		pEvents = new List<ParticleCollisionEvent>();
		baseAttack = transform.GetComponentInParent<BaseAttack>();

	}

	// Update is called once per frame
	void LateUpdate()
	{
	
	}
	
	void ProcessHit(Collider2D col)
	{
		BaseAttack bAttack = baseAttack;
		if (col.transform.root.gameObject.layer == bAttack.parent.transform.root.gameObject.layer || bAttack == null || bAttack.isDisabled)
			return;
		HitID hitID = new HitID(col.transform.root.gameObject.GetInstanceID(), Time.timeSinceLevelLoad + hitRate);
		bool applyHit = canBeHit(hitID);
		UniMob_Base uniMob = col.transform.root.GetComponent<UniMob_Base>();
		BaseSlime slime = col.transform.root.GetComponent<BaseSlime>();

	
		if (baseAttack != null)
			baseAttack.DisableAttack();
		if (damage > 0 && applyHit)
		{
			if (uniMob)
				uniMob.TakeDamage(damage);
			else if (slime)
				slime.TakeDamage(damage);
		}
	}
	void ProcessHit(Collision2D col)
	{
		BaseAttack bAttack = baseAttack;
		if (col.transform.root.gameObject.layer == bAttack.parent.transform.root.gameObject.layer || bAttack == null || bAttack.isDisabled)
			return;
		HitID hitID = new HitID(col.transform.root.gameObject.GetInstanceID(), Time.timeSinceLevelLoad + hitRate);
		bool applyHit = canBeHit(hitID);
		UniMob_Base uniMob = null;
		BaseSlime slime = null;

		uniMob = col.transform.root.GetComponent<UniMob_Base>();
		slime = col.transform.root.GetComponent<BaseSlime>();
		if (baseAttack != null)
			baseAttack.DisableAttack();
		if (damage > 0)
		{
			if (uniMob)
				uniMob.TakeDamage(damage);
			else if (slime)
				slime.TakeDamage(damage);
		}
		//Debug.Log("CollisionStay: " + parent.root.gameObject.name + " hit " + col.transform.root.gameObject.name);
	}
	private bool canBeHit(HitID curHit)
	{
		bool applyHit = true;
		if (hits.Count > 0)
		{
			for (int i = 0; i < hits.Count; i++)
			{
				HitID lastHit = hits[i];
				if (lastHit.id == curHit.id)
				{
					if (lastHit.time < Time.timeSinceLevelLoad)
					{
						lastHit.time = Time.timeSinceLevelLoad + hitRate;
					}
					else
					{
						hits.Add(curHit);
						applyHit = false;
					}
					break;
				}
			}
		}
		return applyHit;
	}
	private void OnCollisionStay2D(Collision2D col)
	{
		ProcessHit(col);
	}
	private void OnTriggerStay2D(Collider2D col)
	{
		ProcessHit(col);
	}
	private void OnParticleCollision(GameObject go)//Convert to Trigger || Collision || ParticleTrigger || ParticleCollision gentype void
	{
		////Debug.Log("Hit: " + go.name);
		if (baseAttack.parent == null)
			return;
		if (go.transform.root.gameObject.layer == baseAttack.parent.root.gameObject.layer || baseAttack.isDisabled)
			return;
		int numCollisionEvents = myParticles.GetCollisionEvents(go, pEvents);

		Rigidbody2D rb = go.transform.root.GetComponent<Rigidbody2D>();
		HitID hitID = new HitID(go.GetInstanceID(), Time.timeSinceLevelLoad + hitRate);
		UniMob_Base uniMob = go.transform.root.GetComponent<UniMob_Base>();
		BaseSlime slime = go.transform.root.GetComponent<BaseSlime>();
	
		if (baseAttack != null)
			baseAttack.DisableAttack();
		int i = 0;
		while (i < numCollisionEvents)
		{
			if (canBeHit(hitID))
			{
				if(uniMob || slime)
				{
					baseAttack.DisableAttack();
					if (DOTween.IsTweening(baseAttack.transform))
						DOTween.Kill(baseAttack, false);
				}
				if (damage > 0)
				{
					if (uniMob)
						uniMob.TakeDamage(damage);
					else if (slime)
						slime.TakeDamage(damage);
				}

			}
			i++;
		}
	}
}
