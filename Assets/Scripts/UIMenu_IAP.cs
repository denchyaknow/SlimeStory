﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Purchasing;
using DG.Tweening;
public class UIMenu_IAP : MonoBehaviour {
	public float tweenTime = 0.6f;
	public Ease tweenEase = Ease.OutBounce;
	private UIRefs_IAP refs;
	private UIManager_IAP mIAP;
	private void Awake()
	{
		refs = transform.GetComponent<UIRefs_IAP>();
		mIAP = transform.GetComponentInParent<UIManager_IAP>();
	}
	// Use this for initialization
	void Start ()
	{
		if (mIAP.products.Count > 0)
		{
			for (int i = 0; i < mIAP.products.Count; i++)
			{
				GameObject IAPButton = (GameObject)Instantiate(Resources.Load("GameObjects/UI/IAP_Button"));
				int index = i;
				IAPButton.transform.SetParent(refs.listParent);
				IAPButton.transform.localScale = new Vector3(1, 1, 1);
				IAPButton.name = mIAP.products[i].productID;
				IAPButton.transform.GetChild(0).GetComponent<Text>().text = mIAP.products[i].title;
				IAPButton.transform.GetChild(1).GetComponent<Text>().text = mIAP.products[i].desc;
				IAPButton.transform.GetChild(2).GetComponent<Text>().text = "$" + mIAP.products[i].price;
				IAPButton.transform.GetComponent<Button>().onClick.AddListener(()=>
				{
					mIAP.BuyConsumable(index);
				});

			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void OpenMenu()
	{
		DOTween.Kill(refs.rootMenuPivot);
		refs.rootMenuPivot.DOAnchorPosY(900f, tweenTime, true).SetEase(tweenEase).SetId(refs.rootMenuPivot);
	}
	public void CloseMenu()
	{
		DOTween.Kill(refs.rootMenuPivot);
		refs.rootMenuPivot.DOAnchorPosY(0f, tweenTime/2, true).SetEase(Ease.OutExpo).SetId(refs.rootMenuPivot);
	}
	
	
}
