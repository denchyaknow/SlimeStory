﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine.UI;
public class BaseMob : BaseSlime
{

	// Use this for initialization

	public MobData mobStats = new MobData();
	public SpriteRenderer mobHealth;
	public SpriteRenderer mobHealthBack;
	public _2dxFX_Clipping mobHealthFx;
	public Transform mobCoinPathParent;
	public int mobIndex = 0;
	public int mobGender = 0;//0=male , 1 = female
	public int mobRace = 0;//0=humans , 1 = elfs
	public int mobAccesories = 0;//0 = none , 1 = with
	public int mobValue = 5;
	public Vector3 mobIdleScale = new Vector3(0.5f, 0.5f, 0);
	public float mobIdleSpeed = 0.3f;
	public float mobIdleThreshold = 5;
	private float mobIdleStartPos;
	public float mobIdleOffset = 0.2f;
	public LoopType mobIdleLoopType = LoopType.Yoyo;
	public Ease mobIdleEase = Ease.Linear;
	public float itemSpawnSpeed = 0.6f;
	public Ease itemSpawnEase;

	private bool mobPlayWalk = false;
	private bool mobPlayIdle = false;

	public bool isMobDead = false;

	private bool isMovingRandomly = false;
	private float lastMoveTime;
	private Vector3 currentWaypointPos;

	private Sprite[] battleSheet;
	private Sprite faceSprite;

	public override void Start()
	{
		base.Start();
		//myRenderer.flipX = true;
		spriteSheet = GameManager._Instance.database.EnemyWalkSheets[mobStats.classType].sprites;
		battleSheet = GameManager._Instance.database.EnemyAttackSheets[mobStats.classType].sprites;
		faceSprite = GameManager._Instance.database.EnemyFaceSprites[mobStats.classType];
	}
	public override void Awake()
	{
		base.Awake();
		RandomizeMob();

		mobIdleStartPos = myRenderer.transform.localPosition.y;
		if(mobHealthFx == null)
		{
			mobHealthFx = transform.GetComponentInChildren<_2dxFX_Clipping>();
		}
		//attackedLast = Time.timeSinceLevelLoad + Random.Range(0, mobStats.attackCD + mobStats.attackDelay + mobStats.attackDuration) + Random.Range(0f, 4.44f);
		//lastMoveTime = Time.timeSinceLevelLoad + Random.Range(0f, 4.44f);

		//mobHealthBack = mobHealth.transform.GetComponentInChildren<SpriteRenderer>();
	}
	// Update is called once per frame
	public delegate void OnStateDel();
	public OnStateDel OnState;
	private bool freeWill = true;
	private bool orientation
	{

		get
		{
			bool orientate = false;
			Vector2 dir = GameManager._Instance.combatManager.playerSlime.transform.position - transform.position;
			if(dir.x > 0)
				orientate = true;
			else
				orientate = false;
			return orientate;
		}
	}
	//private bool inState = false;
	public void DoState()
	{
		if(freeWill && OnState == null)
		{
			OnState = OnIdle;
			float curSpeed = EstimatedSpeed(Time.deltaTime);
			////Debug.Log("mobSpeed =" + curSpeed);

			if(curSpeed > 0.6f)//if walking
			{
				OnState = OnWalk;
			}
		}
		////Debug.Log("State=" + OnState != null);
		if(OnState != null)
		{
			OnState();

		}

	}
	[HeaderAttribute("StateParams")]
	public float frameRate = 0.14f;
	private float lastFrameTime;
	private int walkFrame = 0;
	private int idleFrame = 0;
	private int attackFrame = 0;
	private int[] idleFrames = new int[4] { 0, 1, 2, 1 };
	private int[] attackPunchFrames = new int[5] { 2, 3, 4, 5, 2 };
	public void OnIdle()
	{
		if(idleFrame >= idleFrames.Length)
			idleFrame = 0;
		if(Time.timeSinceLevelLoad - frameRate > lastFrameTime)
		{
			//myRenderer.flipX = dir == 1 ? false : true;
			myRenderer.sprite = battleSheet[idleFrames[idleFrame]];
			lastFrameTime = Time.timeSinceLevelLoad;

			idleFrame++;
		}

		myRenderer.flipX = orientation;

		OnState = null;
	}
	public void OnWalk()
	{

		if(Time.timeSinceLevelLoad - frameRate > lastFrameTime)
		{
			if(dir == 0)
				return;
			int start = (dir == 1 ? 0 : 3);
			int end = (dir == 1 ? 2 : 5);
			if(walkFrame > end)
				walkFrame = start;
			lastFrameTime = Time.timeSinceLevelLoad;

			myRenderer.sprite = spriteSheet[walkFrame];

			walkFrame++;
			myRenderer.flipX = false;

		}
		OnState = null;
	}
	public void OnAttackPunch()
	{
		if(attackFrame >= attackPunchFrames.Length)
		{
			OnState = null;
			attackFrame = 0;
			return;
		}
		if(Time.timeSinceLevelLoad - frameRate > lastFrameTime)
		{
			//myRenderer.flipX = true;
			myRenderer.sprite = battleSheet[attackPunchFrames[attackFrame]];
			lastFrameTime = Time.timeSinceLevelLoad;

			attackFrame++;
		}
		myRenderer.flipX = orientation;

	}
	public override void LateUpdate()
	{
		//base.LateUpdate();

	}
	public override void Update()
	{
		mySortingGroup.sortingOrder = (int)(transform.position.y * -10);
		DoState();

		if(GameManager._Instance.combatManager == null)
			return;
		//base.Update();
		float speed = EstimatedSpeed(Time.deltaTime);
		float currentOverMax = ((float)mobStats.healthMax - (float)mobStats.healthCurrent) / (float)mobStats.healthMax;
		//currentOverMax = (currentOverMax / -1) * -1;
		//currentOverMax = currentOverMax * 0.1f;
		if(_Debug)
		{
			//Debug.Log("Mob velocity: " + speed + " Ismoving: " + (speed > mobIdleThreshold) + " Health: ");
		}
		//if (mobHealth.value != mobStats.healthCurrent)
		//{
		//    mobHealth.maxValue = mobStats.healthMax;
		//    mobHealth.value = mobStats.healthCurrent;
		//}
		if(mobStats.healthCurrent <= 0 && !isMobDead)
		{
			isMobDead = true;
			//PlayEffects(2);
			SpawnCoin();
			//GameManager._Instance.combatManager.AddExperience(mobStats);
			//GameManager._Instance.combatManager.enemies.Remove(this);
			StartCoroutine("DestroyMob");
		} else if(mobStats.healthCurrent > 0 && isMobDead)
		{
			isMobDead = false;
		}
		if(mobHealthFx)
		{
			mobHealthFx._ClipLeft = currentOverMax;
		}
		//      if (myRenderer.sprite != spriteSheet[(int)currentFrame])
		//	myRenderer.sprite = spriteSheet[(int)currentFrame];
		////isMoving = speed > mobIdleThreshold;
		//if (isMoving() && !mobPlayWalk)
		//{
		//	mobPlayWalk = true;
		//	StartCoroutine("MobWalk");
		//}
		//else if (!isMoving() && !mobPlayIdle)
		//{

		//	mobPlayIdle = true;
		//	previousPosition = transform.position;
		//	currentFrame = 7;
		//	myRenderer.transform.DOScale(mobIdleScale, mobIdleSpeed).SetEase(mobIdleEase).SetSpeedBased(true).OnComplete(() =>
		//	{
		//		myRenderer.transform.DOScale(new Vector3(1, 1, 0), mobIdleSpeed).SetEase(mobIdleEase).SetSpeedBased(true).OnComplete(() =>
		//		  {
		//			  mobPlayIdle = false;
		//		  });
		//	});
		//	//myRenderer.transform.DOLocalMoveY(mobIdleOffset, mobIdleSpeed).SetEase(mobIdleEase).SetSpeedBased(true).OnComplete(() =>
		//	//{
		//	//	myRenderer.transform.DOLocalMoveY(mobIdleStartPos, mobIdleSpeed).SetEase(mobIdleEase).SetSpeedBased(true);
		//	//});
		//}
		float distance = Vector3.Distance(GameManager._Instance.combatManager.playerSlime.transform.position, transform.position);
		if(distance > mobStats.moveRadius)
		{
			Attack(null, GameManager._Instance.combatManager.playerSlime);
		}
		if((Time.timeSinceLevelLoad) > lastMoveTime && !isMovingRandomly && !isAttacking)
		{
			lastMoveTime = Time.timeSinceLevelLoad + mobStats.moveCD;
			int mrRandom = Random.Range(0, 500);
			int difficulty = 100;
			int mapLvl = GameManager._Instance.combatManager.mapLevel;
			mapLvl = Random.Range((int)(mapLvl * -0.5f), (mapLvl * 4));
			difficulty += mapLvl;
			if(mrRandom < difficulty && !isMovingRandomly)
			{
				isMovingRandomly = true;
				lastMoveTime = Time.timeSinceLevelLoad + Random.Range(mobStats.moveCD, mobStats.moveCD * 3);
				MoveRandomly();

			}

		}
	}
	public float moveTime = 1;
	public float moveForce = 50;
	public float moveDrag = -50;
	Vector2 movePoint;
	Vector2 moveRandom;
	Transform moveTarget = null;
	public bool testMove = false;
	void MoveRandomly()
	{
		if((Time.timeSinceLevelLoad - lastMoveTime) < moveTime)
		{
			return;
			//lastMoveTime = Time.timeSinceLevelLoad;
		}
		lastMoveTime = Time.timeSinceLevelLoad;
		Vector2 moveRandom = (Random.insideUnitCircle * moveRange);
		Vector2 moveRandomPivot = (Random.insideUnitCircle * moveRange);
		Vector2 mobPos = transform.position;
		Vector2 moveDir = ((moveRandom + mobPos) - mobPos).normalized;
		Vector2 moveDirPivot = ((moveRandomPivot + mobPos) - mobPos).normalized;
		if(testMove)
		{
			Vector2 pos = transform.position;
			pos.x = 0;
			transform.position = pos;
			moveDir = Vector2.right;
			moveDirPivot = Vector2.right;
		}
		while(Physics2D.Raycast(mobPos, moveDir, moveRange, whatIsWall))
		{
			mobPos = transform.position;
			moveDir = ((moveRandom + mobPos) - mobPos).normalized;
		}
		float timer = moveSpeed;

		DOTween.To(() => timer, x => timer = x, moveDrag, moveTime).SetEase(moveEase).OnUpdate(() =>
		{
			myBody.AddForce(timer * (timer > 0 ? moveDir : moveDirPivot));
		}).OnComplete(() =>
		{
			isMovingRandomly = false;
		});
	}
#region Old movement method
	//IEnumerator DoRandomMovement()
 //   {
 //       moveRandom = (Random.insideUnitCircle * (mobStats.moveRadius * 1.5f));
	//	moveTarget = GameManager._Instance.combatManager.playerSlime.transform;
	//	movePoint = transform.position.x > moveTarget.position.x ? (moveTarget.position + (Vector3.right * (mobStats.moveRadius * 1.25f))) : (moveTarget.position + (Vector3.left * (mobStats.moveRadius * 1.25f)));
		
	//	while(Vector2.Distance(movePoint + moveRandom, moveTarget.position) < mobStats.moveRadius)
 //       {
 //           yield return new WaitForEndOfFrame();
	//		moveRandom = (Random.insideUnitCircle * (mobStats.moveRadius * 1.5f));
 //       }
	//	Vector3 newWayPoint = movePoint + moveRandom;

 //       currentWaypointPos = newWayPoint;
 //       float min = (mobStats.moveSpeed - (mobStats.moveSpeed / 3));
 //       float max = (mobStats.moveSpeed + (mobStats.moveSpeed / 3));
 //       float moveDistance = Random.Range(min, max);
 //       float dis = Vector3.Distance(transform.position, newWayPoint);
 //       myBody.velocity = (currentWaypointPos - transform.position).normalized * moveDistance;
        
 //       isMovingRandomly = false;
 //   }
#endregion
	public void SpawnCoin()
	{
		int coinValue = mobStats.coinValue;
		int low = Mathf.RoundToInt(coinValue * 0.6f);
		int high = Mathf.RoundToInt(coinValue * 1.2f);
        GameManager._Instance.coinManager.SpawnCoin(transform.position, Random.Range(low,high));
        /*
		GameObject newCoin = (GameObject)Instantiate(Resources.Load("GameObjects/Coin"), mobCoinPathParent.GetChild(0).transform.position, Quaternion.identity);
		newCoin.transform.SetParent(GameManager._Instance.transform.GetChild(GameManager._Instance.transform.childCount -1).transform);
		BaseItem baseCoin = newCoin.GetComponent<BaseItem>();
		baseCoin.itemType = BaseItem.ItemType.coin;
		baseCoin.itemValue = mobStats.coinValue;

		if(mobCoinPathParent != null && mobCoinPathParent.childCount > 0)
		{
			Vector3[] coinPath = new Vector3[mobCoinPathParent.childCount];
			float mrRandom = Random.Range(-0.6f, 0.6f);
			for(int i = 0; i < mobCoinPathParent.childCount; ++i)
			{

				coinPath[i] = mobCoinPathParent.GetChild(i).transform.position;
				if(i != 0)
				{
					coinPath[i].x += mrRandom * i;
				}
			}
			newCoin.transform.DOPath(coinPath, itemSpawnSpeed, PathType.CatmullRom, PathMode.Full3D, 10, Color.red).SetEase(itemSpawnEase);
		}*/
	}
	public override void MoveToSpawn(Vector2 _pos, float _delay)
	{
		if(GameManager._Instance.Debugging)
		{
			//Debug.Log("MOBMOVING" + _pos);
		}
		isMovingToSpawn = true;
		myCollider.enabled = false;
		myBody.DOMove(_pos, moveSpeed).SetSpeedBased(true).SetEase(moveEase).OnComplete(() =>
		{
			myCollider.enabled = true;
			isMovingToSpawn = false;
			previousPosition = _pos;
			attackedLast = Time.timeSinceLevelLoad;
		});
		//transform.DOPath(new Vector3[2] { transform.position, _pos }, moveSpeed, PathType.CatmullRom, PathMode.Full3D, 10, Color.red)
		//	.SetSpeedBased(true)
		//	.SetEase(moveEase).OnStart(() => {
		//		isMovingToSpawn = true;
		//	}).OnComplete(() => {
		//		isMovingToSpawn = false;
		//        previousPosition = _pos;
		//	});


	}
    public override bool CanAttack()
    {
        bool attack = false;
        float cd = Time.timeSinceLevelLoad - attackedLast;
        int count = 1;
        float velocity = 0;
        float stam = 1;
        if (myBody != null)
            velocity = myBody.velocity.magnitude;
        if (attackCounter != null && attackCounter.gameObject.activeInHierarchy)
            count = attackCounter.currentActive;
        if (stamina != null && stamina.gameObject.activeInHierarchy)
            stam = stamina.count;
        if (cd > mobStats.attackCD + mobStats.attackDelay && !isAttacking && !isMovingToSpawn && !isDead && velocity < 0.25f && count != 0 && stam > slimeStats.attackCost)
            attack = true;

        return attack;
    }
	private BaseSlime currentTargetSlime;
    public  void Attack(Vector3 pos)
    {
        attackPos = pos;
        Attack();
    }
	public virtual void Attack(BaseMob targetMob = null, BaseSlime targetSlime = null)
	{
        if(targetSlime != null)
        {
            if (targetSlime.isDead || targetSlime.isAttacking || targetSlime.CheckIfSpawning())
                return;
        }
        if (!CanAttack() || isMoving() || isMobDead)
			return;
		isAttacking = true;
		attackedLast = Time.timeSinceLevelLoad + mobStats.attackCD + Random.Range(0,mobStats.attackCD + mobStats.attackDelay + mobStats.attackDuration);
        currentTargetSlime = targetSlime;


        StartCoroutine(AttackTheTarget());
	}
    public Ease rotationEase;
    public float rotation = -560f;
    public float rotMulti = 0.6f;
	public float meleeThreshold = 0.75f;
    IEnumerator AttackTheTarget()
	{
        Vector3 targetPos;
        Vector2 _direction;
        previousPosition = transform.position;
        if (currentTargetSlime != null)
        {
            _direction = ( previousPosition - currentTargetSlime.previousPosition).normalized;
            targetPos = currentTargetSlime.transform.position;
        }
        else
        {
            _direction = (previousPosition - attackPos).normalized;
            targetPos = attackPos;

        }
        //float _angle = Mathf.Atan2(_direction.y, _direction.x) * Mathf.Rad2Deg;
		//Vector2 rot = Quaternion.AngleAxis(_angle, Vector3.forward).eulerAngles;//rotate transform towards target
																		
		//myRenderer.transform.DOLocalRotate(rot, mobStats.attackDelay * rotMulti);
        lastHitMobs.Clear();
        switch (mobStats.attackType)
		{
			case 0://melee
                if (attackTelegraph != null)
                    attackTelegraph.ShowTelegraph(targetPos,mobStats.attackDelay,mobStats.attackDuration);
				//Vector3[] _path = new Vector3[3] { previousPosition, targetPos, previousPosition };

                yield return new WaitForSeconds(mobStats.attackDelay);

                GameManager._Instance.soundManager.PlayMobAttack(mobStats.classType);
                Vector3 hitCenter = myCollider.bounds.center + (Vector3.right * -transform.localScale.x);
                Vector2 hitSize = myCollider.bounds.size;


                BaseAttack baseAtt0 = ReloadedAttack();
                string tweenID = "mobMovement_" + gameObject.GetInstanceID();
                //myCollider.enabled = false;
                //Vector3 localRot = new Vector3(0, 0, rotation);
				Vector2 moveRandom = (Random.insideUnitCircle * mobStats.attackRadius);
				Vector2 moveRandomPivot = (Random.insideUnitCircle * mobStats.attackRadius);
				Vector2 mobPos = transform.position;
				Vector2 moveDir = ((moveRandom + mobPos) - mobPos).normalized;
				Vector2 moveDirPivot = ((moveRandomPivot + mobPos) - mobPos).normalized;
				//float attackDurAdd = attackPunchFrames.Length * frameRate * 1.2f;
				float delay = mobStats.attackDelay;// + attackDurAdd;
				float duration = mobStats.attackDuration;// - attackDurAdd;
				float timer = moveForce;
				float timerEnd = moveDrag;

				DOTween.To(()=>timer, x=> timer = x, timerEnd, duration)
					.SetId(tweenID)
					.SetEase(attackInEase)
					.OnUpdate(() =>
					{
						
						myBody.AddForce(timer * _direction);

						float dis =	Vector3.Distance(transform.position, targetPos);
						if(dis < meleeThreshold)
						{
							OnState = OnAttackPunch;
							baseAtt0.Fire(targetPos);
						}
						if(isKnockedBack)
						{
							DOTween.Kill(tweenID);
							isAttacking = false;
						}
					})
					.OnComplete(() =>
					{
						myRenderer.transform.localEulerAngles = Vector3.zero;
						isAttacking = false;
						lastHitMobs.Clear();
						myCollider.enabled = true;
					});
				//transform.DOMove(targetPos, mobStats.attackDuration * 0.8f, false).SetEase(attackEase).OnUpdate(() =>
				//myBody.DOMove(targetPos, duration, false)
    //                .SetId(tweenID)
    //                .SetEase(attackInEase)
    //                .OnUpdate(() =>
				//    {
    //                    if(isKnockedBack)
    //                    {
    //                        DOTween.Kill(tweenID);
    //                        isAttacking = false;
    //                    }
    //                })
    //                .OnComplete(()=>
    //                {
    //                    myRenderer.transform.localEulerAngles = Vector3.zero;
    //                    isAttacking = false;
    //                    lastHitMobs.Clear();
    //                    myCollider.enabled = true;
    //                });
                //myRenderer.transform.DOLocalJump(Vector3.zero, attackJumpHeight, attackJumpCount, (mobStats.attackDuration * attackCutoff))
                //    .SetDelay(delay)
                //    .SetEase(attackOutEase);
                //myRenderer.transform.DOLocalRotate(localRot, mobStats.attackDuration * rotMulti)
                //    .SetDelay(delay)
                //    .SetEase(rotationEase);
                //myBody.DOMove(previousPosition, mobStats.attackDuration * attackCutoff)
                //    .SetDelay(delay)
                //    .SetEase(attackOutEase)
                //    .OnUpdate(()=>
                //    {
                //    })
                //    .OnComplete(() =>
                //    {
                //        myRenderer.transform.localEulerAngles = Vector3.zero;
                //        isAttacking = false;
                //        lastHitMobs.Clear();
                //        myCollider.enabled = true;
                //    }); 
                //yield return new WaitForSeconds(duration);
                //OnState = OnAttackPunch;
                //baseAtt0.Fire(targetPos);

                //yield return new WaitForSeconds(attackDurAdd);
                break;
			case 1://spell
                BaseAttack baseAtt = ReloadedAttack();
                yield return new WaitForSeconds(mobStats.attackDelay);
                GameManager._Instance.soundManager.PlayMobAttack(mobStats.classType);

                baseAtt.Fire(targetPos);
                if (myRendererAttfx != null)
                    myRendererAttfx.enabled = false;
                yield return new WaitForSeconds(mobStats.attackDuration);
                isAttacking = false;
                if (stamina && stamina.gameObject.activeInHierarchy)
                    stamina.ConnectModule(transform);
            
                break;
		}
	}
    public void TakeDamage(int _amount)
	{
        isKnockedBack = true;
		mobStats.healthCurrent -= _amount;
		//PlayEffects(1);
        GameManager._Instance.soundManager.PlayMobOnHit();
		GameManager._Instance.uiManager.LaunchCounter( transform.position, _amount, Color.red);
		GameManager._Instance.uiManager.ShakeCamera();
		
	}
	public IEnumerator DestroyMob()
	{
        DOTween.Pause(transform);
		foreach(SpriteRenderer sprite in transform.GetComponentsInChildren<SpriteRenderer>())
		{
			sprite.enabled = false;

		}
		myCollider.enabled = false;
		
		yield return new WaitForSeconds(5);
		Destroy(gameObject);
	}
	public void RandomizeMob()
	{
		string mobR = "";
		switch (mobRace)
		{
			case 0:
				mobR = "Human";
				break;
			case 1:
				mobR = "Elf";
				break;

		}
		string mobLoadPath = ((mobGender == 0 ? "Male" : "Female") + "_" + mobR + "_" + mobAccesories.ToString() + "_" + Random.Range(0, 149).ToString());
		spriteSheet = Sprite_Database.GetMobSprites(mobLoadPath);
	}
    public override void SetStats()
    {
        mobStats.healthMax = GetCurrentHealth();
        mobStats.healthCurrent = mobStats.healthMax;
        mobStats.damage = GetCurrentDamage();
        mobStats.expValue = GetCurrentExperience();
        mobStats.coinValue = GetCurrentCoin();
        mobStats.currentScale = GetCurrentScale();
    }
    public int GetCurrentHealth()
    {
        int hp = mobStats.baseHealth;
        for (int i = 0; i < GameManager._Instance.playerData.combatMapIndex; ++i)
        {
            hp += GameManager._Instance.ScaledHP(mobStats.baseHealth, mobStats.lvlHealthRate, mobStats.lvlHealthFlat, GameManager._Instance.playerData.combatMapIndex);
        }
        return hp;

    }
    public int GetCurrentExperience()
    {
        int exp = mobStats.baseExp;
        for (int i = 0; i < GameManager._Instance.playerData.combatMapIndex; ++i)
        {
            exp += GameManager._Instance.ScaledEXP(mobStats.baseExp, mobStats.lvlExpRate, mobStats.lvlExpFlat, GameManager._Instance.playerData.combatMapIndex);
        }
        return exp;
    }
    public int GetCurrentDamage()
    {
        int damage = mobStats.baseDamage;
        for (int i = 0; i < GameManager._Instance.playerData.combatMapIndex; ++i)
        {
            damage += (GameManager._Instance.ScaledDamage(mobStats.baseDamage, mobStats.lvlDamageRate, mobStats.lvlDamageFlat, GameManager._Instance.playerData.combatMapIndex));
        }
        return damage;
    }
    public int GetCurrentCoin()
    {
        int coin = GameManager._Instance.ScaledCoin(mobStats.healthCurrent, mobStats.lvlCoinRate, mobStats.lvlCoinFlat, GameManager._Instance.playerData.combatMapIndex, GameManager._Instance.playerData.coinMobBonusMulti);
        for(int i = 0; i < GameManager._Instance.playerData.combatMapIndex; ++i)
        {
            coin += GameManager._Instance.ScaledCoin(mobStats.healthCurrent, mobStats.lvlCoinRate, mobStats.lvlCoinFlat, GameManager._Instance.playerData.combatMapIndex, GameManager._Instance.playerData.coinMobBonusMulti);
        }
        return coin;
    }
    public float GetCurrentScale()
    {
        float scale = mobStats.baseScale;
        for (int i = 0; i < GameManager._Instance.playerData.combatMapIndex; ++i)
        {
            scale += 0.01f * GameManager._Instance.ScaledTransform(GameManager._Instance.playerData.combatMapIndex, mobStats.baseScale, mobStats.lvlScaleRate, mobStats.lvlScaleFlat);
        }
        if (scale > 4f)
            scale = 4f;
        return scale;
    }
    IEnumerator MobWalk()
	{
        GameManager._Instance.soundManager.PlayMobWalk();

        currentFrame = 6;
		yield return new WaitForSeconds(0.15f);
		currentFrame = 7;
		yield return new WaitForSeconds(0.15f);
		currentFrame = 8;
		yield return new WaitForSeconds(0.15f);
		mobPlayWalk = false;
	}
	//public override float EstimatedSpeed(float timePassed)
	//{
	//	// For completeness, we'll get the velocity vector itself, which is the distance travelled
	//	// in each direction divided by the time passed.
	//	Vector3 velocityVector = (transform.position - previousPosition) / timePassed;

	//	// Get the magnitude of thie vector
	//	float velocityMagnitude = velocityVector.magnitude;

	//	// Mark this position for next time
	//	previousPosition = transform.position;

	//	// Then just return what we calculated
	//	return velocityMagnitude;
	//}
	public override void OnDrawGizmosSelected()
	{
        //base.OnDrawGizmosSelected();
        Gizmos.color = Color.red;
		if(moveTarget)
		{
			Gizmos.color = Color.magenta;

			Gizmos.DrawWireSphere(movePoint, 0.15f);
			Gizmos.DrawWireSphere(movePoint, mobStats.moveRadius * 1.5f);
			if(Vector2.Distance(movePoint + moveRandom, moveTarget.position) < mobStats.moveRadius * 1.25f)
				Gizmos.color = Color.grey;
			else Gizmos.color = Color.green;
			Gizmos.DrawWireSphere(movePoint + moveRandom, 0.15f);
			Gizmos.DrawLine(moveTarget.position, movePoint);
			if(Vector3.Distance(moveTarget.position, transform.position) > mobStats.moveRadius)
				Gizmos.color = Color.cyan;
			else
				Gizmos.color = Color.magenta;
				Gizmos.DrawLine(movePoint + moveRandom, movePoint);
		}
		Gizmos.DrawWireSphere(transform.position, mobStats.attackRadius);
		//Gizmos.DrawLine(transform.position, attackPos);
       
	}
}

