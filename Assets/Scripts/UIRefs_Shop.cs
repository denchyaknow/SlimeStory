﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIRefs_Shop : MonoBehaviour {

	public RectTransform rootMenuPivot;//The rect that moves the whole menu
	public RectTransform rootButtonPivot;//The rect tjat moves the whole button list
	public Transform[] rootMenuPath;//The path the rootMenuPivot takes when moving.
	public Transform[] rootButtonPath;//The path the rootbuttonpivot takes when moving, layed out in transforms

	//0=Decor 1=Food 2=Buffs 3=Slimes
	public RectTransform[] tabButtons;//Array of all tab buttons
	public RectTransform[] tabMenus;//Array of all menus
	public Button openButton;
	public Button closeButton;
	public Scrollbar scrollBar;
}
