﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlimeEffectsEditor : MonoBehaviour {
   public Transform effectsPrefab;
   public string effectsRoot = "Effects_Parent";
   public List<Transform> parents;
   public  GameObject root
    {
        get { return gameObject; }
    }
}
