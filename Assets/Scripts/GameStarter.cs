﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using DG.Tweening;
public class GameStarter : EventTrigger {

	//public UnityEvent OnGameStart;
	// Use this for initialization
	private bool gameStarted = false;
	public delegate void Delegate();
	public Delegate OnGameStart;
	//public Delegate OnGameLoaded;
	//public Image fadingImage;
	public override void OnPointerDown(PointerEventData data)
	{
		//if(!gameStarted)
		//{
		//	gameStarted = true;
		//	////Debug.Log("GameStart!");
		//	ProcessPointerDown();
		//	//transform.GetComponent<MainMenu>().OnGameStart();
		//}
	}
	public void ProcessPointerDown(Image fadingImage)
	{
		if(fadingImage)
		{
			fadingImage.DOFade(0, 1).OnComplete(() => OnGameStart());
		} else
		{
			OnGameStart();
		}
	}
	
	void Update ()
	{
		
	}
	
	
}
