﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;
using UnityEngine.Playables;

public class PlayPenManager : MonoBehaviour
{
    public PlayableDirector cutscenePlayable;

	public GameManager gameManager
	{
		get
		{
			if (gM == null && GameManager._Instance != null)
				gM = GameManager._Instance;
			else if (gM == null)
				gM = FindObjectOfType<GameManager>();
			return gM;
		}
	}
	private GameManager gM;
	public AutoFeeder autoFeeder
	{
		get
		{
			if (aF == null)
				aF = transform.GetComponentInChildren<AutoFeeder>();
			return aF;
		}
	}
	private AutoFeeder aF;
	public float spawnTime = 2f;

	public float spawnRadius = 2f;
	public Ease spawnEase;
	public Transform spawnPath;
	public GameObject spawnFXPrefab;
	public GameObject trashCan;//set from trashcan
	private List<Transform> spawnWayPoints = new List<Transform>();

	public Color objectDisabledColor;

	public float testAutoTime = 0;
	private float testTime = 0;
	public bool testSpawn = false;
	public BaseObject testObject;
	// Use this for initialization
	private void OnEnable()
	{


		if (GameManager._Instance.OnOneShotDisable != null)
			GameManager._Instance.OnOneShotDisable();
		GameManager._Instance.playPenManager = this;
		GameManager._Instance.GameLoad();
		GameManager._Instance.GameLoadMap();
		GameManager._Instance.soundManager.PlayBackgroundMusic();
		GameManager._Instance.cameraManager.ResetCamera();
		//GameManager._Instance.OnOneShotDisable += GameManager._Instance.GameSave;
		GameManager._Instance.OnOneShotDisable += GameManager._Instance.inputManager.PrepForNextScene;
		GameManager._Instance.OnOneShotDisable += GameManager._Instance.GameUnload;
		GameManager._Instance.inputManager.touchJoyStickLeft.gameObject.SetActive(false);

	}
	private void OnDisable()
	{
		
		//GameManager._Instance.GameUnload();
	}
	public IEnumerator Start()
	{
		while (GameManager._Instance == null)
		{
			yield return new WaitForSeconds(0.1f);
		}
		if (LoadingScreen._Instance != null)
		{
			while (!LoadingScreen._Instance.isDone)
			{
				yield return new WaitForSeconds(0.1f);
                //cutscenePlayable.Play();
			}
		}
		if(MainMenu._Instance != null)
		{
			while(MainMenu._Instance != null)
			{
				yield return new WaitForSeconds(0.1f);
			}
		}

		//GameManager._Instance.GameLoad();
		//GameManager._Instance.GameLoadMap();
		//GameManager._Instance.uiManager.StartCoroutine(GameManager._Instance.uiManager.SceneLoaded(1));
		//GameManager._Instance.soundManager.PlayBackgroundMusic();
		//GameManager._Instance.autoFeeder.gameObject.SetActive(true);
		//if(LoadingScreen._Instance != null)
		//	LoadingScreen._Instance.OnActiveSceneChange += SetupTrashCan;
		//else
		//	Debug.Log("Trashcan Failed: SetupTrashCanDelagate");
		for (int i = 0; i < spawnPath.childCount; i++)
		{
			spawnWayPoints.Add(spawnPath.GetChild(i).transform);
		}
		yield return new WaitForSeconds(0.1f);
		//Camera.main.GetComponent<CameraManager>().enabled = false;
		//Camera.main.transform.parent.transform.position = Vector3.zero;
		
	}

	// Update is called once per frame
	void Update()
	{
		//foreach (GameObject slime in GameManager._Instance.ourSlimes)
		//{
		//	BaseSlime stat = slime.GetComponent<BaseSlime>();
		//	stat.slimeStats.penPosX = slime.transform.position.x;
		//	stat.slimeStats.penPosY = slime.transform.position.y;

		//}
		if (gameManager != null && autoFeeder != null)
		{
			if (gameManager.playerData.playPenDeco.Contains(autoFeeder.decoData) && !autoFeeder.gameObject.activeInHierarchy)
			{
				autoFeeder.gameObject.SetActive(true);
			}
			else if (!gameManager.playerData.playPenDeco.Contains(autoFeeder.decoData) && autoFeeder.gameObject.activeInHierarchy)
			{
				autoFeeder.gameObject.SetActive(false);
			}
		}
		if (testAutoTime > 0)
		{
			testTime += Time.deltaTime;
			if (testTime > testAutoTime)
			{
				testTime = 0;
				testSpawn = true;

			}
		}
		if (testSpawn && testObject != null)
		{
			testSpawn = false;
			GameObject testObj = (GameObject)Instantiate(testObject.gameObject);
			SpawnObject(testObj.GetComponent<BaseObject>());
			if (testAutoTime > 0)
				StartCoroutine(DeleteTests(testObj));
		}

	}
	//public void SetupTrashCan()
	//{
	//	int scene = SceneManager.GetActiveScene().buildIndex;
	//	if(scene != 1)
	//	{
	//		gameObject.SetActive(false);
	//	} else
	//	{
	//		gameObject.SetActive(true);
	//	}
	//}
	//public void OnEnable()
	//{
	//	int scene = SceneManager.GetActiveScene().buildIndex;
	//	if(scene != 1)
	//	{
	//		gameObject.SetActive(false);
	//	} else
	//	{
	//		gameObject.SetActive(true);
	//	}
	//}
	IEnumerator DeleteTests(GameObject obj)
	{
		yield return new WaitForSeconds(5);
		Destroy(obj);
	}
	public void SpawnObject(BaseObject obj)
	{
		Vector3[] path = new Vector3[spawnWayPoints.Count];
		for (int i = 0; i < spawnWayPoints.Count; i++)
		{
			if (i == spawnWayPoints.Count - 1)
				path[i] = (Vector2)spawnWayPoints[i].position + (Random.insideUnitCircle * spawnRadius);
			else
				path[i] = spawnWayPoints[i].position;
		}
		obj.transform.position = path[0];
		obj.isMovingToSpawn = true;
		GameObject fx = Instantiate(spawnFXPrefab, obj.transform);
		ImageAnimator anim = fx.GetComponentInChildren<ImageAnimator>();
		if (anim != null)
		{
			anim.delay = spawnTime;
			anim.StartAnimation();
		}
		fx.SetActive(true);
		fx.transform.localPosition = Vector3.zero;
		BaseDeco deco = obj.GetComponent<BaseDeco>();
		if (deco != null)
			fx.transform.localScale = new Vector3(deco.spawnfxScale, deco.spawnfxScale, deco.spawnfxScale);

		StartCoroutine(DisableSpawnFX(fx));
		obj.transform.DOPath(path, spawnTime, PathType.CatmullRom, PathMode.Full3D, 10, Color.white).SetEase(spawnEase).OnComplete(() =>
		{
			obj.isMovingToSpawn = false;
			obj.myBody.velocity = Vector2.zero;
			fx.GetComponentInChildren<TrailRenderer>().enabled = false;
		});

	}

	IEnumerator DisableSpawnFX(GameObject fx)
	{
		yield return new WaitForSeconds(spawnTime);
		fx.GetComponentInChildren<ParticleSystem>().Play(true);
		yield return new WaitForSeconds(spawnTime * 1.5f);
		fx.GetComponentInChildren<SpriteRenderer>().DOFade(0, 1).SetDelay(spawnTime).OnComplete(() =>
		{
			fx.gameObject.SetActive(false);

		});
	}
	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.magenta;
		Color col = Gizmos.color;
		if (spawnPath != null)
		{
			if (spawnPath.childCount > 0)
			{
				for (int i = 0; i < spawnPath.childCount; i++)
				{
					Transform child = spawnPath.GetChild(i).transform;
					col.b += 0.05f;
					col.r -= 0.025f;
					col.g -= 0.025f;
					Gizmos.color = col;
					if (i == 0)
						Gizmos.DrawWireSphere(child.position, 0.5f);
					else if (i == spawnPath.childCount - 1)
					{
						Gizmos.DrawLine(spawnPath.GetChild(i - 1).transform.position, (Vector2)child.position + (Random.insideUnitCircle * spawnRadius));
						Gizmos.DrawWireSphere(child.position, spawnRadius);
					}
					else
						Gizmos.DrawLine(spawnPath.GetChild(i - 1).transform.position, child.position);
				}
			}
		}
	}
}
