﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UnityEngine.EventSystems;
public class MainMenu : MonoBehaviour {
	private const string playPenScene = "Playpen";
	public static MainMenu _Instance;
	public Text progressText;
	private float loadTime = 0;
	private int loadProg = 0;
	public float lerpTime = 2f;
	private Camera menuCam;
	private static string[] loadTexts =
	{
		"Pixie dust and happiness",
		"Stuff that smells good",
		"Players pending doom",
		"Epic tale that's so good",
		"Friendship and power",
		"Goodies and more goodies",
		"All the bad things",
		"Some good stuff",
		"Enough mobs",
		"Done"
	};
	void Awake ()
	{
		menuCam = Camera.main;
	}

	IEnumerator Start()
	{
		//loadingParent.gameObject.SetActive(false);
		yield return new WaitForSeconds(0.5f);
		DontDestroyOnLoad(gameObject);
		DontDestroyOnLoad(menuCam.gameObject);
		StartCoroutine(GameStart());
	}

	private void UpdateLoadingText(int progress)
	{
		//string beg = "Loading: ";
		string mid = loadTexts[progress];
		string end = ".";
		int ran = Random.Range(0, 4);
		for (int i = 0; i < ran; i++)
			end += ".";

		progressText.text =   mid + end;
	}
	IEnumerator GameStart()
	{
		loadTime = 0;
		loadProg = 0;
		string load = string.Empty;
		yield return new WaitForSeconds(0.5f);
		AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(playPenScene,LoadSceneMode.Single);
		asyncLoad.allowSceneActivation = false;
		while (loadProg < 8)
		{
			//if (asyncLoad.progress >= 0.9f)
			//{
			//	SceneManager.SetActiveScene(SceneManager.GetSceneByName(playPenScene));
			//}
			loadProg = Mathf.CeilToInt(loadTime * 10);
			loadTime =  Mathf.Lerp(loadTime, asyncLoad.progress, Time.deltaTime * lerpTime);
			UpdateLoadingText(loadProg);
			if (loadProg == 8)
				break;
			yield return new WaitForEndOfFrame();
			
		}
				asyncLoad.allowSceneActivation = true;
		//Debug.Log(load);
		yield return new WaitForEndOfFrame();
		yield return new WaitForEndOfFrame();

		if (menuCam != null)
		{
			Destroy(menuCam.gameObject);
		}
		yield return new WaitForEndOfFrame();
		Destroy(this.gameObject);

	}
}
