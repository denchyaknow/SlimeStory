﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;
[RequireComponent(typeof(AudioSource))]
public class ParticleWriter : MonoBehaviour {
    public bool writerStart = false;
    public bool writerPlaying = false;
    public string writerText;
    public float writerStepSpeed;
    public Transform pathParent;


    public TextMesh myTextPrefab;
    private AudioSource myAudioSource;

    private AudioSource textStepSound;

    public ParticleSystem textParticle;
    public Font textFont;
    public FontStyle textStyle;
    public int textSize = 5;
    public float textTabSize = 4;
    public Color textColor = Color.cyan;
    public Ease textEase;
    public ScrambleMode textScramble;
    public string textScrambleSeed = "";
    public bool useParticleColor = false;
    public Gradient textParticleColor;
    private List<GameObject> textChars = new List<GameObject>();
    private List<TextMesh> textMeshes = new List<TextMesh>();
    private List<AudioSource> textSounds = new List<AudioSource>();
    private List<ParticleSystem> textParticles = new List<ParticleSystem>();
    private List<float> textTimers = new List<float>();
    public int letterCount = 24;

    public float letterLifeTime = 2f;
    public float letterFlavorDuration = 1f;
    public Vector3 letterDirection = new Vector2(0, 0);

    public Material letterTextMaterial;
    public Material letterFlavorMaterial;

    private List<Tween> myTweens = new List<Tween>();
    public bool _Debug = false;
    public bool _DebugReload = false;
    private bool wroteSpace = false;
    // Use this for initialization
    void Start() {
        myAudioSource = transform.GetComponent<AudioSource>();
        InitializeMeshes();
    }

    // Update is called once per frame
    void Update() {

        if (writerStart && !writerPlaying) {
            writerPlaying = true;
            writerStart = false;
            InitiateSwaggerExplosion();
        }
        if (_DebugReload) {
            InitializeMeshes();
        }
    }
    int lastChar = 0;
    void InitiateSwaggerExplosion() {
        InvokeRepeating("PrayToTheUnityGod", 0, writerStepSpeed);
    }
    void PrayToTheUnityGod() {
        char[] _text = writerText.ToCharArray();
        if (_Debug) {
            Debug.Log("Writer: MainStringLength=" + _text.Length);
        }
        if (_text == null || _text.Length < 1) {
            CancelInvoke("PrayToTheUnityGod");

            return;
        }
        if (lastChar < _text.Length) {
            DropItLikeItsHot(_text[lastChar].ToString());
            if (_text[lastChar].ToString() == " ")
                wroteSpace = true;
            lastChar++;
        } else {
            lastChar = 0;
            CancelInvoke("PrayToTheUnityGod");
        }
    }
    float maxParticleDurLife = 0;
    void DropItLikeItsHot(string _char) {
        maxParticleDurLife = 0;
        for (int i = 0; i < textChars.Count; i++) {
            float _particleDurLife = 0;
            if (!textChars[i].activeInHierarchy) {
                GameObject newChar = textChars[i].gameObject;
                ParticleSystem newSystem = textChars[i].transform.GetComponent<ParticleSystem>();
                TextMesh newText = textChars[i].transform.GetComponent<TextMesh>();
                ParticleSystem.MainModule _main = newSystem.main;
                newChar.SetActive(true);

                newChar.transform.SetParent(null);
                newChar.transform.position = transform.position;
                //PerParticle Text settings
                newText.text = _char;
                newText.font = textFont;
                newText.color = textColor;
                newText.fontStyle = textStyle;
                newText.fontSize = textSize;
                newText.tabSize = textTabSize;
                //Per-Particle settings
                newSystem.Clear(true);
                newSystem.Stop();

                _main.playOnAwake = true;
                _main.loop = false;
                _main.startLifetime = letterLifeTime;

                _main.duration = letterFlavorDuration;
                if (useParticleColor)
                    _main.startColor = textParticleColor;
                newSystem.time = 0;
                newSystem.Play(true);

                //Tween settings
                DOTween.To(() => newText.text, x => newText.text = x, _char, letterLifeTime / 3)
                        .SetOptions(true, textScramble, textScrambleSeed == "" ? null : textScrambleSeed)
                        .SetId(newChar.transform)
                        .SetLoops(letterLifeTime == 0 ? -1 : 0, LoopType.Yoyo)
                        .SetEase(textEase)
                        .SetAutoKill(true)
                        ;
                Vector3[] _path = GetThatJuiciness();
                newText.transform.DOPath(_path, letterLifeTime / 2, PathType.CatmullRom, PathMode.Full3D, 10, Color.red)
                        .SetId(newText.transform)
                        .SetLoops(letterLifeTime == 0 ? -1 : 0, LoopType.Yoyo)
                        .SetAutoKill(true)
                        .SetEase(textEase)
                        ;
                Color fadedColor = textColor;
                fadedColor.a = 0;
                DOTween.To(() => newText.color, x => newText.color = x, fadedColor, letterLifeTime * 2)
                    .SetEase(textEase)
                    .SetId(newChar.transform)
                    .SetAutoKill(true)
                    .SetLoops(letterLifeTime == 0 ? -1 : 0, LoopType.Yoyo)
                    .OnComplete(() => {
                        Invoke("DisableThatAss", letterLifeTime * 2);
                    })
                    ;



                //Finalize



                if (_Debug) {
                    Debug.Log("Writer: LaunchedChar=" + _char.ToString());
                }
                break;
            }
        }
    }
    void DisableThatAss(GameObject _char) {
        _char.SetActive(false);
    }
    Vector3[] GetThatJuiciness() {
        Vector3[] _path = new Vector3[pathParent.childCount];
        for (int i = 0; i < pathParent.childCount; i++) {
            _path[i] = pathParent.GetChild(i).transform.position;
        }
        return _path;
    }
    void InitializeMeshes() {
        if (textChars.Count > 0) {
            for (int i = 0; i < textChars.Count; i++) {
                Destroy(textChars[i].gameObject);
            }
            textChars.Clear();
        }
        maxParticleDurLife = 0;
        for (int i = 0; i < letterCount; i++) {
            GameObject newChar = (GameObject)Instantiate(myTextPrefab.gameObject, transform);
            textChars.Add(newChar);
            ParticleSystem newSystem = newChar.transform.GetComponent<ParticleSystem>();
            TextMesh newText = newChar.transform.GetComponent<TextMesh>();
            ParticleSystem.MainModule _main = newSystem.main;

            newText.transform.DOMove(transform.position + letterDirection, letterLifeTime, false)
                        .SetId(newText.transform)
                        .SetLoops(letterLifeTime == 0 ? -1 : 0, LoopType.Yoyo)
                        .SetAutoKill(false)
                        .SetEase(textEase)
                        ;

            Color fadedColor = textColor;
            fadedColor.a = 255;
            letterTextMaterial.DOFade(0, letterLifeTime)
                .SetEase(textEase)
                .SetId(newChar.transform)
                .SetAutoKill(false)
                .SetLoops(letterLifeTime == 0 ? -1 : 0, LoopType.Yoyo)
                .OnComplete(() => {
                    newChar.SetActive(false);
                })
                ;

            ParticleSystem[] newSystems = newChar.GetComponentsInChildren<ParticleSystem>();
            if (newSystems != null && newSystems.Length > 0) {
                for (int t = 0; t < newSystems.Length; t++) {
                    ParticleSystem.MainModule _NewMain = newSystems[t].main;
                    float _particleDurLife = _NewMain.duration + _NewMain.startLifetime.constant;

                    if (_particleDurLife > maxParticleDurLife) {
                        maxParticleDurLife = _particleDurLife;
                    }
                }
            }
        }
    }
}
