﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class TrashCan : EventTrigger {
	private bool trashActive = false;
	public override void OnPointerEnter(PointerEventData eventData)
	{
		base.OnPointerEnter(eventData);
		if(!GameManager._Instance.inputManager.draggingOBject)
			return;
		DOTween.Kill(transform);

		if(!trashActive)
		{
			trashActive = true;
			transform.DOLocalRotate(new Vector3(0, 0, 90), 1, RotateMode.Fast).SetEase(Ease.OutExpo).SetId(transform)
				.OnStart(()=>
				{
					GameManager._Instance.inputManager.trashCanActive = true;
				});
		//Debug.Log("TrashcanEnter");
		}
	}
	public override void OnPointerExit(PointerEventData eventData)
	{
		base.OnPointerExit(eventData);
		DOTween.Kill(transform);

		if(trashActive)
		{
			trashActive = false;
			GameManager._Instance.inputManager.trashCanActive = false;
			transform.DOLocalRotate(Vector3.zero, 1, RotateMode.Fast).SetEase(Ease.OutExpo).SetId(transform);
		//Debug.Log("TrashcanExit");
		}
	}
	private void Awake()
	{
		if(GameManager._Instance.playPenManager != null)
		{
			GameManager._Instance.playPenManager.trashCan = gameObject;
		}
	}
	void Start () {
	
		
	}
	
	void Update () {
	
	}
}
