﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.SceneManagement;
using UnityEngine.UI.Extensions;



public class UIManager : MonoBehaviour
{

    //Settings Menu
    public GameObject settingsMenu;
    public Toggle musicToggle;
    public Toggle sfxToggle;
    private bool isSettingsOpen = false;

	public const string CombatScene = "BattleTime";
	public const string PlaypenScene = "Playpen";
	[Header("StatRangeSettings")]
	//public float statMaxCutOFF = 0.75f;// the max stat will be maxStat * cutoff to alow OPness
	//public float statMaxGeneral = 255;//stats are out of 255?
	//public float statMaxAgility = 3f;//affects cooldowns((cd - max * -1) / (max * cutoff)) and moveSpeed/Range(range /((max * multi) * cutoff))
	//public float statMaxDefense = 4f;//sums up hp(hpcurrentMax / (GetScaledHP(base, rate, flat, max) * cutoff)) and maybe damageTaken(def /((max * multi) * cutoff)) FormulaForDamageReduc(damage -= damage * (def * multi))
	//public float statMaxIntelligence = 5f;//attackType == 1? 
	[Header("HUDRefferences")]

	private GraphicRaycaster gr;
	public GraphicRaycaster GetRayCaster
	{
		get
		{
			if(gr == null)
			{
				gr = transform.GetComponentInParent<GraphicRaycaster>();
			}
			return gr;
		}
		set
		{
			gr = value;
		}
	}

	[Header("Slime Tab")]
	public Image slimeTabImage;
	public Image slimeIsNullTabImage;
	public UIRadarStats playerStats;
	private SlimeData lastStats;

	private bool showingStats = false;
	public Text slimeTabNameText;
	public Text slimeTabLvlText;
	public Text slimeTabHealthText;
	public Text slimeTabHappinessText;
	public Text slimeTabStrengthText;
	public Text slimeTabAgilityText;
	public Text slimeTabMagicText;
	public Text slimeTabSpiritText;
	public Text slimeTabDefenseText;
	public Slider slimeTabHealthBar;
	public Slider slimeTabHappinessBar;

	public float visiblePosX = 9;
	public float hiddenPosX = -9;
	public Vector3 startPos;

	//public RectTransform rootCloseButton;
	public RectTransform rootMenu;
	public RectTransform rootTabButtons;
	private int currentTab = 0;
	public Ease rootMenuEase = Ease.OutExpo;
	public float rootMenuEndPosX = -600f;
	public float rootfxDuration = 0.8f;
	public Ease tabButtonsEase;
	public float tabButtonsEndPosY = -480;
	public float tabButtonsfxDuration = 1.2f;
	public GameObject[] tabButtons;// 0-decor | 1-food | 2-buff | 3-slimes | 4-main
	public GameObject[] tabMenus;// 0-decor | 1-food | 2-buff | 3-slimes | 4-main

	public GameObject stageList;
	public GameObject toolTip;
	public bool isHidden = true;
	[Header("Gold Text")]
	public Text curGoldText;
	private int curGoldCur;
	[Header("Slime Hud")]
	public Image curSlimeIcon;
	public Image curSlimeIsNullIcon;
	public Text curSlimeName;
	private string curSlimeNameCache;
	public Text curSlimeLv;
	private int curSlimeLvCache;

	[Header("Tooltip")]
	public Image tooltipPreviewImage;
	public Vector2 tooltipOffset = new Vector2();
	private Vector2 tooltipPreviewExtents
	{
		get
		{
			Vector2 extents = new Vector2();
			if(tooltipPreviewImage != null)
			{
				extents = tooltipPreviewImage.sprite.bounds.extents;
			}
			return extents;
		}
	}
	[Header("Combat Hud")]
	public RectTransform combatHudRoot;
	public Text combatMapTitle;
	public Text combatMapWave;

	public Slider combatHealthSlider;
	public Text combatHealthText;
	private int combatHealthCur;
	private int combatHealthMax;

	[Header("Counters")]
	public Transform counterParent; //where counter spawn from and return to
	public float counterDistance = 1.8f;
	public float counterLifeTime = 1.8f;
	public float counterAlphaDelay = 1.2f;
	public Ease counterEase = Ease.OutCubic;

	public delegate void OnSwitchMenuTabDel();
	public OnSwitchMenuTabDel OnSwitchMenuTab;

	[Header("Shake_Settings")]
	public float shakeIntensity = 0.005f;
	public float shakeDecay = 0.0008f;
	public float shakeLimit = 0.001f;
	public bool shakeAddative = true;
	public Shake.ShakeType shakeType = Shake.ShakeType.standard;


	public Image gameSelectMenu;

	private void OnDisable()
	{
		if(OnSwitchMenuTab != null)
		{
			OnSwitchMenuTab = null;
		}
	}
	private IEnumerator Start()
	{

		while(GameManager._Instance == null)
		{
			yield return new WaitForSeconds(0.1f);
		}
		//GameManager._Instance.uiManager = this;
		GetRayCaster = transform.GetComponentInParent<GraphicRaycaster>();
		yield return new WaitForSeconds(0.5f);
		RegisterButtons();
		RegisterStageButtons();
		ToggleChildMenu(-1);
		InitializeCounters();
		//loadingScreen.gameObject.SetActive(true);
		//faderBackground.gameObject.SetActive(true);
		//faderForground.gameObject.SetActive(false);
		//isLoading = true;
		//StartCoroutine(SceneLoaded(0));
		//HideToolTip();
		//So that money sound doesnt play annoyingly when scene loads
		curGoldCur = GameManager._Instance.playerData.coinCount;
		string coinCount = FormatedNumber(curGoldCur, true);

		curGoldText.text = coinCount;

	}
    public void OpenSettingsMenu() {
        if (isSettingsOpen == false) {
            settingsMenu.transform.DOScale(1, 0.3f);
            isSettingsOpen = true;
        } else {
            CloseSettingsMenu();
        }
    }
    public void CloseSettingsMenu() {
        if (isSettingsOpen == true) {
            settingsMenu.transform.DOScale(0, 0.3f);
            isSettingsOpen = false;
        }
    }

	private void Update()
	{
		//if(testFadeOut)
		//{
		//    testFadeOut = false;
		//    FadeOut(testLoad);
		//}
		//if (testFadeIn)
		//{
		//    testFadeIn = false;
		//    FadeIn();
		//}
		if(Input.GetKeyDown(KeyCode.Space))
		{
			GameManager._Instance.GetSelectedSlime().LevelUp();
		}
		//Show/Hide combat hud
		if(GameManager._Instance.combatManager != null && !combatHudRoot.gameObject.activeInHierarchy)
		{
			combatHudRoot.gameObject.SetActive(true);
			rootMenu.gameObject.SetActive(false);
			rootTabButtons.gameObject.SetActive(false);
		} else if(GameManager._Instance.combatManager == null && combatHudRoot.gameObject.activeInHierarchy)
		{
			combatHudRoot.gameObject.SetActive(false);
			rootMenu.gameObject.SetActive(true);
			rootTabButtons.gameObject.SetActive(true);
		}
		UpdateHUD();
		//CoinCounter();
	}
	private BaseSlime monitoredSlime;
	public void OnStatChanged()
	{
		Debug.Log("Monitoring of OnStatChange Triggered for" + monitoredSlime.gameObject.name);
		lastStats = monitoredSlime.slimeStats;
		showingStats = true;
		if(showingStats)
		{
			showingStats = false;
			if(!isHidden)
			{

				playerStats.ShowStats(lastStats);
				//ShowToolTip(monitoredSlime);
			}

		}

		slimeTabNameText.text = lastStats.name;
		slimeTabLvlText.text = "Lv." + lastStats.level.ToString();
		double healthValue = lastStats.currentHealth;
		double healthValueMax = lastStats.maxHealth + lastStats.maxHealthAddative;
		string healthText = lastStats.currentHealth.ToString();
		string healthTextMax = lastStats.maxHealth.ToString();
		healthText = healthText + " / " + healthTextMax;
		slimeTabHealthText.text = healthText;

		float happinessValue = lastStats.GetHappinessPercent();
		happinessValue = FormatedPercent(happinessValue);
		string happiness = happinessValue.ToString() + "%";
		slimeTabHappinessText.text = happiness;
		slimeTabHealthBar.minValue = 0;
		slimeTabHealthBar.maxValue = (int)healthValueMax;
		slimeTabHealthBar.value = (int)healthValue;
		slimeTabHappinessBar.minValue = 0;
		slimeTabHappinessBar.maxValue = 100;
		slimeTabHappinessBar.value = (int)happinessValue;

		slimeTabStrengthText.text = (lastStats.strength + lastStats.strengthAddative).ToString();
		slimeTabAgilityText.text = (lastStats.agility + lastStats.agilityAddative).ToString();
		slimeTabMagicText.text = (lastStats.magic + lastStats.magicAddative).ToString();
		slimeTabSpiritText.text = (lastStats.spirit + lastStats.spiritAddative).ToString();
		slimeTabDefenseText.text = (lastStats.defense + lastStats.defenseAddative).ToString();
	}
	BaseSlime slime;
	void UpdateCombatOnlyHUD()
	{
		if(slime == null)
			return;
		if(curSlimeIcon.sprite != slime.myRenderer.sprite)
		{
			CopySprite(slime.myRenderer, null, null, curSlimeIcon);
			//curSlimeIcon.sprite = slime.myRenderer.sprite;
		}
		combatHealthSlider.minValue = 0;
		combatHealthSlider.maxValue = slime.slimeStats.maxHealth + slime.slimeStats.maxHealthAddative;
		combatHealthSlider.value = slime.slimeStats.currentHealth;
		if(combatHealthMax != slime.slimeStats.maxHealth + slime.slimeStats.maxHealthAddative ||
			combatHealthCur != slime.slimeStats.currentHealth)
		{
			combatHealthMax = slime.slimeStats.maxHealth + slime.slimeStats.maxHealthAddative;
			combatHealthCur = slime.slimeStats.currentHealth;
			string healthCur = combatHealthCur.ToString();
			string healthMax = " | " + combatHealthMax.ToString();
			combatHealthText.DOText(healthCur, 0.8F, false, ScrambleMode.Numerals, null).OnUpdate(() =>
			{
				combatHealthText.text = combatHealthText.text + healthMax;
			});
		}
	}
	void UpdatePlaypenOnlyHUD()
	{
		if(slime == null && curSlimeIcon.enabled)
		{
			curSlimeIcon.enabled = false;
			slimeTabImage.enabled = false;
			curSlimeIsNullIcon.enabled = true;
			slimeIsNullTabImage.enabled = true;
		} else if(slime != null && !curSlimeIcon.enabled)
		{
			curSlimeIcon.enabled = true;
			slimeTabImage.enabled = true;
			curSlimeIsNullIcon.enabled = false;
			slimeIsNullTabImage.enabled = false;
		}
		if(slime == null)
			return;
		if(slimeTabImage != null && slimeTabImage.sprite != slime.myRenderer.sprite)
		{
			CopySprite(slime.myRenderer, null, null, slimeTabImage);
			CopySprite(slime.myRenderer, null, null, curSlimeIcon);
			//slimeTabImage.sprite = slime.myRenderer.sprite;

		}

		if(isHidden)
			return;

		if(showingStats && monitoredSlime != null)
		{
			//playerStats.ShowStats(lastStats);
			OnStatChanged();
		}
	}
	void UpdateHUD()
	{
		if(GameManager._Instance.ourSlimes.Count == 0)
			return;
		if(GameManager._Instance.playPenManager != null)
		{
			slime = GameManager._Instance.GetSelectedSlime();
			UpdatePlaypenOnlyHUD();
		} else if(GameManager._Instance.combatManager != null)
		{
			slime = GameManager._Instance.combatManager.playerSlime;
			UpdateCombatOnlyHUD();
		}
		if(slime == null)
			return;
		if(monitoredSlime != slime)
		{
			//Debug.Log("Monitoring of OnStatChange Initialized for" + slime.gameObject.name);
			if(monitoredSlime != null && monitoredSlime.OnStatChange != null)
			{
				//Debug.Log("Monitoring of OnStatChange Stopped for" + monitoredSlime.gameObject.name);
				monitoredSlime.OnStatChange -= OnStatChanged;
			}
			monitoredSlime = slime;
			//Debug.Log("Monitoring of OnStatChange Started for" + monitoredSlime.gameObject.name);
			monitoredSlime.OnStatChange += OnStatChanged;
			OnStatChanged();
		}

		if(curSlimeNameCache != slime.slimeStats.name || curSlimeLvCache != slime.slimeStats.level)
		{
			curSlimeNameCache = slime.slimeStats.name;
			curSlimeLvCache = slime.slimeStats.level;
			curSlimeLv.text = "Lv." + curSlimeLvCache.ToString();
			curSlimeName.text = curSlimeNameCache;
		}
		if(curGoldCur != GameManager._Instance.playerData.coinCount)
		{
			curGoldCur = GameManager._Instance.playerData.coinCount;
			string coinCount = FormatedNumber(curGoldCur, true);
			string add = string.Empty;
			string rem = string.Empty;
			char[] lng = coinCount.ToCharArray();
			int cnt = 0;

			DOTween.Kill(curGoldText);
			curGoldText.DOText(coinCount, 0.8f, false, ScrambleMode.Numerals, null).OnStart(() =>
			 {
				 GameManager._Instance.soundManager.PlayUIMoney();

			 }).OnUpdate(() =>
			 {
				 //curGoldText.text = add + curGoldText.text;
			 }).SetId(curGoldText);
		}
	}

	public void UpdateMapWave()
	{
		//combatMapWave.text = (/*"Wave:"+ (GameManager._Instance.combatManager.waveCur.ToString() + "/" + GameManager._Instance.combatManager.waveMax.ToString() + */"Lv:" + GameManager._Instance.combatManager.subLvlCur.ToString() + "/" + GameManager._Instance.combatManager.subLvlMax.ToString());
	}

	public void UpdateMapTitle()
	{
		int mapID = GameManager._Instance.combatManager.mapLevel;
		if(mapID >= SlimeDatabase.MapNames.Length)
			mapID = 0;
		combatMapTitle.text = ("Zone " + (GameManager._Instance.combatManager.mapLevel).ToString() + ": " + SlimeDatabase.MapNames[mapID]);
	}
	private List<TextMesh> counters = new List<TextMesh>();
	private void InitializeCounters()
	{
		if(counterParent != null)
		{
			for(int i = 0; i < 50; ++i)
			{
				GameObject textMesh = (GameObject)Instantiate(Resources.Load("GameObjects/Counter"), counterParent);
				textMesh.transform.position = Vector3.zero;
				counters.Add(textMesh.GetComponent<TextMesh>());
				textMesh.gameObject.SetActive(false);
			}
		}
	}
	private float lastCounterPos = 1f;
	public void LaunchCounter(Vector3 _pos, int _amount, Color _color, string flavorText = "")
	{

		if(counters.Count > 0)
		{
			for(int i = 0; i < counters.Count; ++i)
			{
				if(counters[i] == null)
				{
					counters.Remove(counters[i]);
				} else if(!counters[i].gameObject.activeInHierarchy)
				{
					TextMesh counter = counters[i];
					counter.text = flavorText + (_amount == 0 ? (string.Empty) : (_amount.ToString()));
					counter.color = _color;

					counter.gameObject.SetActive(true);
					counter.transform.SetParent(GameManager._Instance.garbageCan);
					counter.transform.localScale = new Vector3(1, 1, 1);
					counter.transform.position = _pos;
					Vector3 movePos = _pos;
					lastCounterPos = -lastCounterPos;
					Vector2 addMovePos = new Vector2(Random.Range(counterDistance, counterDistance * 1.5f), Random.Range(counterDistance / 2, counterDistance * 1.5f));
					addMovePos.x *= lastCounterPos;
					if(_amount != 0)
					{
						if(_amount < 0 && addMovePos.x > 0 || _amount > 0 && addMovePos.x < 0)
							addMovePos.x = -addMovePos.x;
					}
					movePos += (Vector3)addMovePos;


					counter.transform.DOMove(movePos, counterLifeTime, false).SetEase(counterEase).OnStart(() =>
					{

					});
					DOTween.ToAlpha(() => counter.color, x => counter.color = x, 0, counterLifeTime).SetDelay(counterLifeTime * 0.5f).OnComplete(() =>
					{
						counter.transform.SetParent(counterParent);
						counter.transform.localPosition = Vector3.zero;
						counter.gameObject.SetActive(false);
					});
					///Debug.Log("Counter Launched");
					break;
				}
			}
		}
	}

	/*----------------------------------------
                  MenuLogic 
    ---------------------------------------*/
	#region Menu Logic

	//Game Select Logic

	public void OpeGameSelectnMenu()
	{
		if(gameSelectMenu.transform.localScale.x == 0)
		{
			gameSelectMenu.transform.DOScale(1, 0.5f);
		} else
		{
			gameSelectMenu.transform.DOScale(0, 0.5f);
		}
	}


	public void GoToFallingFruitGame()
	{
		SceneManager.LoadScene("FallingFruitGame");
	}

	public void GoToCombat()
	{
		if(GameManager._Instance.playerData.combatMapHighest > 0)
		{
			stageList.gameObject.SetActive(true);
		} else
		{
			GoToScene(2);
		}
	}
	public void GoToScene(int _sceneID)
	{
		string sceneName = string.Empty;
		switch(_sceneID)
		{

			case 0:

				break;
			case 1:
				sceneName = PlaypenScene;
				break;
			case 2:
				sceneName = CombatScene;
				break;
		}
		StartCoroutine(InitializeLoadScreen(sceneName));
	}
	IEnumerator InitializeLoadScreen(string sceneName)
	{
		//GameManager._Instance.GameSave();
		GameManager._Instance.GameCleanUp();
		if(LoadingScreen._Instance == null)
		{
			Instantiate(Resources.Load("GameObjects/LoadingScreen"));
			while(LoadingScreen._Instance == null)
			{
				yield return new WaitForSeconds(0.1f);
			}
		}
		LoadingScreen._Instance.GoToScene(sceneName);
	}

	public void ContolHelperOpenStatMenu()
	{
		if(isHidden)
		{
			OpenRootMenu(tabMenus.Length - 1);
			showingStats = false;
		}

	}
	public void OpenRootMenu(int _ID)
	{
		if(isHidden && !DOTween.IsTweening(rootMenu))
		{
			rootTabButtons.DOAnchorPosY(0, tabButtonsfxDuration, false).SetEase(tabButtonsEase);
			rootMenu.DOAnchorPosX(rootMenuEndPosX, rootfxDuration, false).SetId(rootMenu).SetEase(rootMenuEase).OnComplete(() =>
				{
					isHidden = false;
					showingStats = true;
				});
			ToggleChildMenu(_ID);
		} else if(currentTab == _ID)
		{
			CloseRootMenu();
		} else
		{
			ToggleChildMenu(_ID);
		}
	}
	public void CloseRootMenu()
	{
		if(!isHidden)
		{
			isHidden = true;
			rootTabButtons.DOAnchorPosY(tabButtonsEndPosY, tabButtonsfxDuration, false).SetEase(tabButtonsEase);
			rootMenu.DOAnchorPosX(0, rootfxDuration, false).SetEase(rootMenuEase).OnComplete(() =>
		{
			ToggleChildMenu(-1);

		});
			showingStats = false;
		}
	}
	public void ToggleChildMenu(int _ID)
	{
		if(OnSwitchMenuTab != null)
			OnSwitchMenuTab();
		lastButtonToolTip = 0;
		currentTab = _ID;
		if(tabMenus.Length > 0)
		{
			for(int i = 0; i < tabMenus.Length; ++i)
			{
				if(i != _ID)
				{
					tabMenus[i].SetActive(false);
				} else
				{
					tabMenus[i].SetActive(true);
				}
			}
		}
		if(_ID == tabMenus.Length - 1)
		{
			if(!isHidden)
				showingStats = true;
			//GameManager._Instance.inputManager.SelectDefaultSlime();
		}
		if(stageList != null)
			stageList.SetActive(false);
		//HideToolTip();
	}

	#endregion

	/*----------------------------------------
                      Shop 
    ---------------------------------------*/
	//public void ShowToolTip(string text)
	//{


	//}
	//public void HideToolTip()
	//{
	//	tooltipPreviewImage.enabled = false;
	//	GameManager._Instance.tooltipManager.HideUITooltip();

	//}
	private int FormatedPercent(float tenthFloat)
	{
		return (int)(tenthFloat * 100);
	}
	private float FormatedFloat(float numberFloat)
	{
		float start = numberFloat;
		float finish = (int)(numberFloat * 100);
		finish = (float)(finish * 0.01f);
		return finish;
	}
	private string FormatedNumber(int numberInt, bool useDecimal = false)
	{
		string numberStr = string.Empty;
		if(numberInt > 999999)
		{
			int length = 1;
			if(numberInt > 9999999)
			{
				length++;
				if(numberInt > 99999999)
				{
					length++;
				}
			}
			string num = numberInt.ToString().Substring(0, length);
			string dec = numberInt.ToString().Substring(length, 3);

			string add = string.Empty;
			if(numberInt < 100000000)
				add += "0";
			if(numberInt < 10000000)
				add += "0";
			numberStr += add + num + (useDecimal ? ("." + dec) : "") + "M";
		} else if(numberInt > 999)
		{
			int length = 1;
			if(numberInt > 9999)
			{
				length++;
				if(numberInt > 99999)
				{
					length++;
				}
			}
			string num = numberInt.ToString().Substring(0, length);
			string dec = numberInt.ToString().Substring(length, 3);
			string add = string.Empty;
			if(numberInt < 100000)
				add += "0";
			if(numberInt < 10000)
				add += "0";
			numberStr += add + num + (useDecimal ? ("." + dec) : "") + "K";
		} else
		{
			if(numberInt < 100)
				numberStr += "0";
			if(numberInt < 10)
				numberStr += "0";

			numberStr += numberInt;

		}
		return numberStr;

	}
	//public void ShowToolTip(BaseSlime slime)
	//{
	//	SlimeData stats = slime.slimeStats;
	//	string str = GetStatRating(stats.growth[0]);
	//	string mag = GetStatRating(stats.growth[1]);
	//	string agi = GetStatRating(stats.growth[2]);
	//	string spr = GetStatRating(stats.growth[3]);
	//	string def = GetStatRating(stats.growth[4]);
	//	float dmgReduction = slime.GetCurrentDamageReduction();
	//	dmgReduction = FormatedFloat(dmgReduction);
	//	float cdReduc = slime.GetCurrentAttackCDReduction();
	//	cdReduc = FormatedFloat(cdReduc);
	//	int damage = slime.GetCurrentDamage();
	//	string name = "Attribute Ratings";
	//	string val0 = "STR- \t" + str + "\n" + "AGI- \t" + agi;
	//	string val1 = "MAG- \t" + mag + "\n" + "SPI- \t" + spr;
	//	string val2 = "DEF- \t" + def;
	//	//string val4 = "Damage " + slime.GetCurrentDamage() + " Rate" + FormatedFloat(stats.attackCD - cdReduc) + "(-" + cdReduc + ") /s";
	//	//ShowTooltip(4, name, val0, val1, val2, null, stats.description);
	//}
	//public void ShowTooltip(int menuID, string name, string val0, string val1, string val2, string val3, string desc0)
	//{
	//	int limit = GameManager._Instance.tooltipManager.uiMaxCharsPerLine;

	//	string value0 = string.Empty;
	//	string value1 = string.Empty;
	//	string value2 = string.Empty;
	//	string value3 = string.Empty;
	//	string flavorText = string.Empty;
	//	switch(menuID)
	//	{
	//		case 0://Deco

	//			value0 += name + "\t\t" + "$ " + val0 + "\n\n";
	//			value1 += "Happiness: " + "" + val1 + "\n";

	//			break;
	//		case 1://Food
	//			value0 += name + "\t\t" + "$ " + val0 + "\n\n";
	//			value1 += "Health: " + "" + val1 + "\n";
	//			value2 += "Hunger: " + "" + val2 + "\n\n";
	//			break;
	//		case 2://Upgrades
	//			value0 += name + "\t\t" + "$ " + val0 + "\n\n";
	//			value1 += val1 + "\n";
	//			value2 += val2 + (val2 != string.Empty ? " / " : string.Empty) + val3 + "\n\n";
	//			break;
	//		case 3://Slimes		
	//			value0 += name + "\t\t" + "$ " + val0 + "\n";
	//			value1 += val1 + "\n";
	//			value2 += val2 + "\n\n";

	//			break;
	//		case 4://Status
	//			value0 += name + "\n";
	//			value1 += val0 + "\n";
	//			value2 += val1 + "\n";
	//			value3 += val2 + "\n";

	//			break;
	//	}
	//	flavorText += value0 + value1 + value2 + value3;
	//	//Debug.Log(desc0);
	//	if(desc0 == string.Empty)
	//	{
	//		desc0 = "No Description";
	//	}
	//	char[] desc = desc0.ToCharArray();
	//	int count = 0;
	//	bool addSpace = false;
	//	bool addAnotherSpace = false;
	//	if(desc.Length >= limit * 2)
	//	{
	//		limit = desc.Length / 3;
	//		limit -= 1;
	//	} else if(desc.Length >= limit)
	//	{
	//		limit = desc.Length / 2;
	//		limit -= 1;
	//		addSpace = true;
	//	} else
	//	{
	//		addSpace = true;
	//		addAnotherSpace = true;
	//	}
	//	for(int j = 0; j < desc.Length; j++)
	//	{
	//		flavorText += desc[j].ToString();
	//		if(count > limit && char.IsWhiteSpace(desc[j]))
	//		{
	//			count = 0;
	//			flavorText += "\n";
	//		}
	//		count++;
	//	}
	//	if(addAnotherSpace)
	//		flavorText += "\n";
	//	if(addSpace)
	//		flavorText += "\n";


	//	Vector2 tooltipPos = tooltipPreviewImage.transform.parent.position;
	//	tooltipPos += tooltipOffset;
	//	GameManager._Instance.tooltipManager.ShowUITooltip(flavorText, tooltipPos);

	//}
	public string GetStatRating(int statValue)
	{
		string val = string.Empty;
		switch(statValue)
		{
			case 0:
				val = "S";
				break;
			case 1:
				val = "A";
				break;
			case 2:
				val = "B";
				break;
			case 3:
				val = "C";
				break;
			case 4:
				val = "D";
				break;
		}
		return val;
	}
	public string GetEffectType(UpgradeData.EffectType effect, float effectValue)
	{
		string flavorText = string.Empty;
		switch(effect)
		{
			case UpgradeData.EffectType.ADD_STR:
				flavorText = "Adds " + Mathf.CeilToInt(effectValue).ToString() + " Strength";
				break;
			case UpgradeData.EffectType.ADD_MAG:
				flavorText = "Adds " + Mathf.CeilToInt(effectValue).ToString() + " Magic";

				break;
			case UpgradeData.EffectType.ADD_AGI:
				flavorText = "Adds " + Mathf.CeilToInt(effectValue).ToString() + " Agility";

				break;
			case UpgradeData.EffectType.ADD_SPR:
				flavorText = "Adds " + Mathf.CeilToInt(effectValue).ToString() + " Spirit";

				break;
			case UpgradeData.EffectType.ADD_DEF:
				flavorText = "Adds " + Mathf.CeilToInt(effectValue).ToString() + " Defense";

				break;
			case UpgradeData.EffectType.ADD_MAXHP:
				flavorText = "Adds " + Mathf.CeilToInt(effectValue).ToString() + " MaxHP";

				break;
		}

		return flavorText;
	}
	public void CopySprite(SpriteRenderer renderFrom = null, Image imageFrom = null, SpriteRenderer renderTo = null, Image imageTo = null, Sprite overrideSprite = null)
	{
		Transform setImage;
		Transform getImage;

		if(renderTo != null)
			setImage = renderTo.transform;
		else if(imageTo != null)
			setImage = imageTo.transform;
		else
			return;

		if(renderFrom != null)
		{
			if(renderTo != null)
				renderTo.sprite = renderFrom.sprite;
			else if(imageTo != null)
				imageTo.sprite = renderFrom.sprite;
			getImage = renderFrom.transform;
		} else if(imageFrom != null)
		{
			if(renderTo != null)
				renderTo.sprite = imageFrom.sprite;
			else if(imageTo != null)
				imageTo.sprite = imageFrom.sprite;
			getImage = imageFrom.transform;
		} else
			return;
		if(overrideSprite != null)
		{
			if(renderTo != null)
				renderTo.sprite = overrideSprite;
			else if(imageTo != null)
				imageTo.sprite = overrideSprite;
		}
		_2dxFX_ColorChange getColorChanger = getImage.GetComponent<_2dxFX_ColorChange>();
		if(getColorChanger != null && getColorChanger.enabled)
		{
			_2dxFX_ColorChange setColorChanger = setImage.gameObject.GetComponent<_2dxFX_ColorChange>();
			if(setColorChanger == null)
				setColorChanger = setImage.gameObject.AddComponent<_2dxFX_ColorChange>();

			setColorChanger._HueShift = getColorChanger._HueShift;
			setColorChanger._Saturation = getColorChanger._Saturation;
			setColorChanger._ValueBrightness = getColorChanger._ValueBrightness;
			setColorChanger._Tolerance = getColorChanger._Tolerance;
			setColorChanger._Color = getColorChanger._Color;
			setColorChanger.enabled = true;
		} else if(getColorChanger != null && !getColorChanger.enabled || getColorChanger == null)
		{
			_2dxFX_ColorChange setColorChanger = setImage.gameObject.GetComponent<_2dxFX_ColorChange>();
			if(setColorChanger != null)
				setColorChanger.enabled = false;
		}


	}
	private int lastButtonToolTip = 0;
	public void ClearButtons()
	{
		OnSwitchMenuTab = null;
		for(int i = 0; i < tabMenus.Length; i++)
		{
			Transform menu = tabMenus[i].transform.GetChild(0).transform;
			if(i != tabMenus.Length - 1)
			{
				for(int j = 0; j < menu.childCount; j++)
				{

					Destroy(menu.GetChild(j).gameObject);
				}

			}
		}
	}
	public void RegisterButtons()// 0-decor | 1-food | 2-buff | 3-slimes | 4-main
	{
		Debug.Log("LoadingShop...");
		if(SlimeDatabase.Upgrades.Length > 0)
		{
			for(int i = 0; i < SlimeDatabase.Upgrades.Length; i++)
			{
				GameObject butt = (GameObject)Instantiate(Resources.Load("GameObjects/UI/ShopItemButton"), tabMenus[2].transform.GetChild(0).transform);
				Button upgradeButtMain = butt.transform.GetComponent<UIStoreButton>().buttMain;
				Button upgradeButtCoin = butt.transform.GetComponent<UIStoreButton>().buttCoin;
				int index = i;
				OnSwitchMenuTab += butt.transform.GetComponent<UIStoreButton>().HideBuyButtons;

				upgradeButtMain.onClick.AddListener(() =>
		  {
			  upgradeButtMain.GetComponent<UIStoreButton>().ButtonClick();
			  int id = upgradeButtMain.gameObject.GetInstanceID();
			  if(lastButtonToolTip != id)
			  {
				  lastButtonToolTip = id;
				  if(tooltipPreviewImage != null)
				  {
					  tooltipPreviewImage.enabled = true;
					  Image previewImageTo = tooltipPreviewImage;
					  SpriteRenderer previewRenderFrom = SlimeDatabase.Upgrades[index].GetComponentInChildren<SpriteRenderer>();
					  CopySprite(previewRenderFrom, null, null, previewImageTo);
				  }
				  UpgradeData data = SlimeDatabase.Upgrades[index].GetComponent<BaseUpgrade>().upgradeData;
				  string name = data.name;
				  string val0 = FormatedNumber(data.goldCost);
				  string val1 = GetEffectType(data.upgradeEffect, data.upgradeValue);
				  string val2 = string.Empty;
				  if(data.happinessValue != 0)
				  {
					  if(data.happinessValue < 0)
						  val2 = "Depressant";
					  if(data.happinessValue > 0)
						  val2 = "Stimulant";
				  }
				  string val3 = string.Empty;
				  if(data.hungerValue != 0)
				  {
					  if(data.hungerValue < 0)
						  val3 = "Nauseating";
					  if(data.hungerValue > 0)
						  val3 = "Delectable";
				  }
				  string desc = data.description;
				  //ShowTooltip(2, name, val0, val1, val2, val3, desc);
			  }
		  });
				upgradeButtCoin.onClick.AddListener(() =>
				{
					int cost = SlimeDatabase.Upgrades[index].GetComponent<BaseUpgrade>().upgradeData.goldCost;
					if(GameManager._Instance.GameRemoveCoin(cost))
					{
						GameObject newBuff = Instantiate(SlimeDatabase.Upgrades[index], GameManager._Instance.garbageCan);
						newBuff.transform.position = (upgradeButtMain.transform.position);
						BaseUpgrade buff = newBuff.GetComponent<BaseUpgrade>();
						BaseSlime slime = GameManager._Instance.GetSelectedSlime();
						buff.UpgradeSlime(slime);
						GameManager._Instance.soundManager.PlayUIMoney();
					} else
					{
						GameManager._Instance.soundManager.PlayUIError();
						upgradeButtMain.GetComponent<UIStoreButton>().ButtonClick();
					}

				});
				Image imageTo = upgradeButtMain.transform.GetChild(0).transform.GetComponent<Image>();
				SpriteRenderer renderFrom = SlimeDatabase.Upgrades[i].GetComponentInChildren<SpriteRenderer>();
				CopySprite(renderFrom, null, null, imageTo);
				upgradeButtMain.transform.GetChild(1).transform.GetComponent<Text>().text = FormatedNumber(SlimeDatabase.Upgrades[i].GetComponent<BaseUpgrade>().upgradeData.goldCost);
			}
		}
		if(SlimeDatabase.Food.Length > 0)
		{
			for(int i = 0; i < SlimeDatabase.Food.Length; i++)
			{
				GameObject butt = (GameObject)Instantiate(Resources.Load("GameObjects/UI/ShopItemButton"), tabMenus[1].transform.GetChild(0).transform);
				Button foodButtMain = butt.transform.GetComponent<UIStoreButton>().buttMain;
				Button foodButtCoin = butt.transform.GetComponent<UIStoreButton>().buttCoin;
				int index = i;
				OnSwitchMenuTab += butt.transform.GetComponent<UIStoreButton>().HideBuyButtons;


				foodButtMain.onClick.AddListener(() =>
		  {
			  foodButtMain.GetComponent<UIStoreButton>().ButtonClick();
			  int id = foodButtMain.gameObject.GetInstanceID();
			  if(lastButtonToolTip != id)
			  {
				  lastButtonToolTip = id;
				  if(tooltipPreviewImage != null)
				  {
					  tooltipPreviewImage.enabled = true;
					  Image previewImageTo = tooltipPreviewImage;
					  SpriteRenderer previewRenderFrom = SlimeDatabase.Food[index].GetComponentInChildren<SpriteRenderer>();
					  CopySprite(previewRenderFrom, null, null, previewImageTo);
				  }
				  FoodData data = SlimeDatabase.Food[index].GetComponent<BaseFood>().foodData;
				  string name = data.name;
				  string val0 = FormatedNumber(data.goldCost);
				  string val1 = FormatedNumber(data.healthPerBite);
				  string val2 = FormatedNumber(data.hungerPerBite);
				  string description = data.description;
				  //ShowTooltip(1, name, val0, val1, val2, string.Empty, description);
			  }
		  });
				foodButtCoin.onClick.AddListener(() =>
				{
					int cost = SlimeDatabase.Food[index].GetComponent<BaseFood>().foodData.goldCost;
					if(GameManager._Instance.GameRemoveCoin(cost))
					{
						GameManager._Instance.soundManager.PlayUIMoney();
						Vector3 pos = foodButtMain.transform.position;
						GameManager._Instance.GameAddFood(index, pos);
					} else
					{
						GameManager._Instance.soundManager.PlayUIError();
						foodButtMain.GetComponent<UIStoreButton>().ButtonClick();
					}


				});
				Image imageTo = foodButtMain.transform.GetChild(0).transform.GetComponent<Image>();
				SpriteRenderer renderFrom = SlimeDatabase.Food[i].GetComponentInChildren<SpriteRenderer>();
				CopySprite(renderFrom, null, null, imageTo);
				foodButtMain.transform.GetChild(1).transform.GetComponent<Text>().text = FormatedNumber(SlimeDatabase.Food[i].GetComponent<BaseFood>().foodData.goldCost);
			}
		}
		if(SlimeDatabase.Maps.Length > 0)
		{
			for(int i = 0; i < SlimeDatabase.Maps.Length; i++)
			{
				GameObject butt = (GameObject)Instantiate(Resources.Load("GameObjects/UI/ShopItemButton"), tabMenus[0].transform.GetChild(0).transform);
				Button mapButtMain = butt.transform.GetComponent<UIStoreButton>().buttMain;
				Button mapButtCoin = butt.transform.GetComponent<UIStoreButton>().buttCoin;
				int index = i;
				OnSwitchMenuTab += butt.transform.GetComponent<UIStoreButton>().HideBuyButtons;
				bool purchasedBefore = GameManager._Instance.playerData.playPenMaps.Contains(index);
				if(purchasedBefore)
				{
					mapButtMain.transform.GetChild(1).transform.GetComponent<Text>().text = "Got";

				} else if(index == 0)
				{
					mapButtMain.transform.GetChild(1).transform.GetComponent<Text>().text = "Got";
					GameManager._Instance.playerData.playPenMaps.Add(index);
				} else
				{
					mapButtMain.transform.GetChild(1).transform.GetComponent<Text>().text = SlimeDatabase.Maps[index].GetComponent<BaseMap>().decoData.goldCost.ToString();
				}
				mapButtMain.onClick.AddListener(() =>
				{
					bool purchased = GameManager._Instance.playerData.playPenMaps.Contains(index);
					if(purchased)
					{
						GameManager._Instance.playerData.playPenMapIndex = index;
						GameManager._Instance.GameLoadMap();
					} else
					{
						mapButtMain.GetComponent<UIStoreButton>().ButtonClick();
					}
					
					int id = mapButtMain.gameObject.GetInstanceID();
					if(lastButtonToolTip != id)
					{
						lastButtonToolTip = id;
						if(tooltipPreviewImage != null)
						{
							tooltipPreviewImage.enabled = true;
							Image previewImageTo = tooltipPreviewImage;
							SpriteRenderer previewRenderFrom = SlimeDatabase.Maps[index].GetComponentInChildren<SpriteRenderer>();
							CopySprite(previewRenderFrom, null, null, previewImageTo);
						}
						DecoData data = SlimeDatabase.Maps[index].GetComponent<BaseDeco>().decoData;
						string name = data.name;
						string val0 = FormatedNumber(data.goldCost);
						string val1 = FormatedNumber(data.decoValue);
						string description = data.description;
						//ShowTooltip(0, name, val0, val1, string.Empty, string.Empty, description);
					}
				});
				mapButtCoin.onClick.AddListener(() =>
				{
					int cost = SlimeDatabase.Maps[index].GetComponent<BaseMap>().decoData.goldCost;
					if(GameManager._Instance.GameRemoveCoin(cost))
					{
						GameManager._Instance.GameAddMap(index);
						GameManager._Instance.soundManager.PlayUIAccept();
						GameManager._Instance.soundManager.PlayUIMoney();
						mapButtMain.transform.GetChild(1).transform.GetComponent<Text>().text = "Got";
					} 
			
					else
					{
						GameManager._Instance.soundManager.PlayUIError();
					}
					mapButtMain.GetComponent<UIStoreButton>().ButtonClick();
				});
				Image imageTo = mapButtMain.transform.GetChild(0).transform.GetComponent<Image>();
				SpriteRenderer renderFrom = SlimeDatabase.Maps[i].GetComponentInChildren<SpriteRenderer>();
				CopySprite(renderFrom, null, null, imageTo);
				
			}
		}
		if(SlimeDatabase.Decorations.Length > 0)
		{
			for(int i = 0; i < SlimeDatabase.Decorations.Length; i++)
			{
				GameObject butt = (GameObject)Instantiate(Resources.Load("GameObjects/UI/ShopItemButton"), tabMenus[0].transform.GetChild(0).transform);
				Button decoButtMain = butt.transform.GetComponent<UIStoreButton>().buttMain;
				Button decoButtCoin = butt.transform.GetComponent<UIStoreButton>().buttCoin;
				int index = i;
				int decoID = SlimeDatabase.Decorations[index].GetComponent<BaseDeco>().decoData.decoID;
				OnSwitchMenuTab += butt.transform.GetComponent<UIStoreButton>().HideBuyButtons;
				bool purchasedBefore = GameManager._Instance.playerData.playPenDecoPurchased.Contains(decoID);
				if(purchasedBefore)
				{
					decoButtMain.transform.GetChild(1).transform.GetComponent<Text>().text = "Got";
				} else
				{
					decoButtMain.transform.GetChild(1).transform.GetComponent<Text>().text = SlimeDatabase.Decorations[index].GetComponent<BaseDeco>().decoData.goldCost.ToString();
				}
				decoButtMain.onClick.AddListener(() =>
				 {
					 bool purchased = GameManager._Instance.playerData.playPenDecoPurchased.Contains(decoID);
					 bool isSpawned = false;
					 if(GameManager._Instance.ourDeco.Count > 0)
					 {
						 for(int j = 0; j < GameManager._Instance.ourDeco.Count; j++)
						 {
							 if(GameManager._Instance.ourDeco[j].decoData.decoID == decoID)
							 {
								 isSpawned = true;
								 break;
							 }
						 }
					 }
					 if(purchased && !isSpawned)
					 {
						 GameManager._Instance.soundManager.PlayUIAccept();
						 GameManager._Instance.GameAddDeco(index);
					 } else if(purchased && isSpawned)
					 {
						 GameManager._Instance.soundManager.PlayUIDecline();
					 } else if(!purchased)
					 {
						 decoButtMain.GetComponent<UIStoreButton>().ButtonClick();

					 }
		
					 int id = decoButtMain.gameObject.GetInstanceID();
					 if(lastButtonToolTip != id)
					 {
						 lastButtonToolTip = id;
						 if(tooltipPreviewImage != null)
						 {
							 tooltipPreviewImage.enabled = true;
							 Image previewImageTo = tooltipPreviewImage;
							 SpriteRenderer previewRenderFrom = SlimeDatabase.Decorations[index].GetComponentInChildren<SpriteRenderer>();
							 Sprite previewOverrideImage = SlimeDatabase.Decorations[index].GetComponent<BaseDeco>().decoIcon;

							 CopySprite(previewRenderFrom, null, null, previewImageTo, previewOverrideImage);
						 }
						 DecoData data = SlimeDatabase.Decorations[index].GetComponent<BaseDeco>().decoData;
						 string name = data.name;
						 string val0 = FormatedNumber(data.goldCost);
						 string val1 = FormatedNumber(data.decoValue);
						 string description = data.description;
						 //ShowTooltip(0, name, val0, val1, string.Empty, string.Empty, description);
					 }
				 });
				decoButtCoin.onClick.AddListener(() =>
				{
					int cost = SlimeDatabase.Decorations[index].GetComponent<BaseDeco>().decoData.goldCost;
					if(GameManager._Instance.GameRemoveCoin(cost) && decoButtMain.transform.GetChild(1).transform.GetComponent<Text>().enabled)
					{
						
						GameManager._Instance.GameAddDeco(index);
						decoButtMain.transform.GetChild(1).transform.GetComponent<Text>().text = "Got";

						GameManager._Instance.soundManager.PlayUIMoney();
						//decoButtMain.transform.GetChild(1).transform.GetComponent<Text>().enabled = false;
					} else if(!decoButtMain.transform.GetChild(1).transform.GetComponent<Text>().enabled)
					{
						GameManager._Instance.soundManager.PlayUIAccept();
						string desc = "You already bought this decoration";
						//ShowTooltip(0, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, desc);
					} else
					{
						GameManager._Instance.soundManager.PlayUIError();
					}
					decoButtMain.GetComponent<UIStoreButton>().ButtonClick();
				});
				Image imageTo = decoButtMain.transform.GetChild(0).transform.GetComponent<Image>();
				SpriteRenderer renderFrom = SlimeDatabase.Decorations[i].GetComponentInChildren<SpriteRenderer>();
				Sprite overrideImage = SlimeDatabase.Decorations[i].GetComponent<BaseDeco>().decoIcon;
				CopySprite(renderFrom, null, null, imageTo, overrideImage);
				
			}
		}
		if(SlimeDatabase.FriendlySlimes.Length > 0)
		{
			for(int i = 0; i < SlimeDatabase.FriendlySlimes.Length; ++i)
			{
				GameObject butt = (GameObject)Instantiate(Resources.Load("GameObjects/UI/ShopItemButton"), tabMenus[3].transform.GetChild(0).transform);
				butt.name = SlimeDatabase.FriendlySlimes[i].name;
				Button slimeButtMain = butt.transform.GetComponent<UIStoreButton>().buttMain;
				Button slimeButtCoin = butt.transform.GetComponent<UIStoreButton>().buttCoin;
				int index = i;
				OnSwitchMenuTab += butt.transform.GetComponent<UIStoreButton>().HideBuyButtons;

				int classIndex = SlimeDatabase.FriendlySlimes[i].transform.GetComponent<BaseSlime>().slimeStats.classType;
				Vector3 pos = butt.GetComponent<RectTransform>().anchoredPosition;
				slimeButtMain.onClick.AddListener(() =>
				{
					slimeButtMain.GetComponent<UIStoreButton>().ButtonClick();
					int id = slimeButtMain.gameObject.GetInstanceID();
					if(lastButtonToolTip != id)
					{
						lastButtonToolTip = id;
						if(tooltipPreviewImage != null)
						{
							tooltipPreviewImage.enabled = true;
							Image previewImageTo = tooltipPreviewImage;
							SpriteRenderer previewRenderFrom = SlimeDatabase.FriendlySlimes[index].GetComponentInChildren<SpriteRenderer>();
							CopySprite(previewRenderFrom, null, null, previewImageTo);
						}
						SlimeData data = SlimeDatabase.FriendlySlimes[index].GetComponent<BaseSlime>().slimeStats;
						string name = data.name;
						string val0 = FormatedNumber(data.goldCost);
						string str = "Str: " + GetStatRating(data.growth[0]) + " ";
						string agi = "Mag: " + GetStatRating(data.growth[1]) + " \n";
						string mag = "Agi: " + GetStatRating(data.growth[2]) + " ";
						string spr = "Spr: " + GetStatRating(data.growth[3]) + " ";
						string def = "Def: " + GetStatRating(data.growth[4]) + "";
						string val1 = str + "" + agi + "" + mag + "" + spr + "" + def;
						string val2 = "Dmg: " + "" + FormatedNumber(data.baseDamage) + " Spd: " + (data.attackCD + data.attackDelay) + "/s "
						+ "(" + (data.attackType == 0 ? "Melee" : "Ranged") + "" + (data.attackHitCount > 1 ? "/Area" : "/Single") + ")";
						string description = "\"" + data.description + "\"";
						//ShowTooltip(3, name, val0, val1, val2, string.Empty, description);
					}
				});
				slimeButtCoin.onClick.AddListener(() =>
					 {
						 int cost = SlimeDatabase.FriendlySlimes[index].GetComponent<BaseSlime>().slimeStats.goldCost;
						 if(GameManager._Instance.GameRemoveCoin(cost))
						 {
							 GameManager._Instance.GameAddSlime(classIndex);
							 GameManager._Instance.soundManager.PlayUIMoney();
						 } else
						 {
							 GameManager._Instance.soundManager.PlayUIError();
							 slimeButtMain.GetComponent<UIStoreButton>().ButtonClick();

						 }
					 });
				string attackType = string.Empty;
				switch(SlimeDatabase.FriendlySlimes[i].transform.GetComponent<BaseSlime>().slimeStats.attackType)
				{
					case 0:
						attackType = "Melee";
						break;
					case 1:
						attackType = "Ranged";
						break;
				}
				string growth = string.Empty;
				switch(SlimeDatabase.FriendlySlimes[i].transform.GetComponent<BaseSlime>().slimeStats.baseExp / 100)
				{
					case 1:
						growth = "Fast";
						break;
					case 2:
						growth = "Average";
						break;
					case 3:
						growth = "Slow";
						break;
				}
				string text =
				(
					"Health: " + SlimeDatabase.FriendlySlimes[i].transform.GetComponent<BaseSlime>().slimeStats.baseHealth.ToString()
					+ "\nGrowth: " + growth
					+ "\nDamage: " + SlimeDatabase.FriendlySlimes[i].transform.GetComponent<BaseSlime>().slimeStats.baseDamage.ToString()
					+ "\nAttackType: " + attackType
					+ "\nAttackCD: " + SlimeDatabase.FriendlySlimes[i].transform.GetComponent<BaseSlime>().slimeStats.attackCD
					+ "\nAutoCD: " + SlimeDatabase.FriendlySlimes[i].transform.GetComponent<BaseSlime>().slimeStats.attackAutoCD
				);
				
				Image imageTo = slimeButtMain.transform.GetChild(0).transform.GetComponent<Image>();
				SpriteRenderer renderFrom = SlimeDatabase.FriendlySlimes[i].GetComponentInChildren<SpriteRenderer>();
				CopySprite(renderFrom, null, null, imageTo);
				
				slimeButtMain.transform.GetChild(1).transform.GetComponent<Text>().text = FormatedNumber(SlimeDatabase.FriendlySlimes[i].GetComponent<BaseSlime>().slimeStats.goldCost);

			}
		}

	}
	public void RegisterStageButtons()
	{
		if(stageList == null)
			return;
		if(stageList.transform.GetChild(0).transform.childCount > 0)
		{
			for(int i = 0; i < stageList.transform.GetChild(0).transform.childCount; ++i)
			{
				Destroy(stageList.transform.GetChild(0).transform.GetChild(i).gameObject);
			}
		}
		if(GameManager._Instance.playerData.combatMapHighest > 0)
		{
			int count = GameManager._Instance.playerData.combatMapHighest;
			int nameID = 0;
			for(int i = 0; i < count; ++i)
			{
				GameObject butt = (GameObject)Instantiate(Resources.Load("GameObjects/UI/StageItemButton"), stageList.transform.GetChild(0).transform);
				butt.transform.SetParent(stageList.transform.GetChild(0).transform);
				if(nameID >= SlimeDatabase.MapNames.Length)
					nameID = 0;
				string _name = "(" + (i + 1).ToString() + "+) " + SlimeDatabase.MapNames[nameID];
				nameID++;
				butt.name = _name;
				butt.transform.GetComponentInChildren<Text>().text = _name;

				int index = i;
				butt.GetComponent<Button>().onClick.AddListener(() =>
				{
					GameManager._Instance.playerData.combatMapIndex = index;
					GameManager._Instance.uiManager.GoToScene(2);
				});
				butt.transform.localScale = new Vector3(1, 1, 1);
			}
		}
	}
	public void ShakeCamera()
	{
		Shake.Instance.StartShake(shakeIntensity, shakeDecay, shakeLimit, shakeAddative);
	}
}
