﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Rendering;

public class BaseObject :MonoBehaviour
{
	public static ParticleSystem _InstanceCoinParticles;
	public ParticleSystem coinParticle
	{
		get
		{
			if (_InstanceCoinParticles == null)
			{
				GameObject coin = (GameObject)Instantiate(Resources.Load("Effects/CoinParticles"));
				coin.transform.SetParent(GameManager._Instance.garbageCan);
				_InstanceCoinParticles = coin.GetComponent<ParticleSystem>();
			}
			return _InstanceCoinParticles;
		}
	}
	[HideInInspector] public Vector3 previousPosition = new Vector3();
	[HideInInspector] public int dir = 0;
	[HideInInspector] public float speed = 0;
	public int sortingOrder
	{
		get
		{
			int pos = -(int)Camera.main.WorldToScreenPoint(transform.position).y;
			return pos;
		}
	}
	public virtual float EstimatedSpeed(float timePassed)
	{
		Vector3 velocityVector = (transform.position - previousPosition) / timePassed;
		if (velocityVector.x > 0)
			dir = -1;
		else if (velocityVector.x < 0)
			dir = 1;
		float velocityMagnitude = velocityVector.magnitude;
		previousPosition = transform.position;
		return velocityMagnitude;
	}
	/// <summary>
	/// An object is selected when a tap comes down on object and remains selected while tap is held on object, Object is unselected on tap up
	/// For and object to be selected if much be under an selectable layer dictated in the playerControls=>whatisSelectable
	/// /// </summary>
	public bool isSelected = false;

	//public bool isMoving = false;
	public bool isKnockedBack = false;
	private bool resolvingKnockBack = false;
	IEnumerator ResolveKnockback()
	{
		yield return new WaitForSeconds(0.4f);
		isKnockedBack = false;
		resolvingKnockBack = false;
	}
	public bool isMoving()
	{
		bool moving = false;
		float vel = ((transform.position - lastFramePosition) / Time.deltaTime).magnitude;
		if (vel > 0.25f || isMovingToSpawn || myBody != null && myBody.velocity.magnitude > 0.25f)
		{
			moving = true;
		}
		lastFramePosition = transform.position;
		if (isKnockedBack && !resolvingKnockBack)
		{
			resolvingKnockBack = true;
			moving = true;
			StartCoroutine(ResolveKnockback());

		}
		if (GameManager._Instance.Debugging)
		{
			////Debug.Log(transform.gameObject.name + " TVel:" + vel + " RVel:" + (myBody != null ? myBody.velocity.magnitude : 0).ToString() + moving);
		}
		return moving;
	}
	public bool isLobbed = false;
	public bool isMovable = true;
	public bool isEdible = false;
	public bool isMovingToSpawn = false;
	private Vector3 target;
	private Vector3[] touch_Points = new Vector3[0];
	private Vector3 lastPos = new Vector3();
	[HideInInspector]
	public Vector3 lastFramePosition = new Vector3();
	private Vector2 direction = new Vector2();
	private float magnitude = 0;

	public float dragMagnituge = 0.5f;
	public float dragAngularVelocity = 0.2f;
	public bool fixedAngualarVelocity = false;// does item rotate to movement?

	public Ease lobEase;
	public bool _Debug;
	[HideInInspector] public SpriteRenderer myRenderer;
	[HideInInspector] public SpriteRenderer[] myRenderers;
	[HideInInspector] public SortingGroup mySortingGroup;
	[HideInInspector] public Rigidbody2D myBody;
	[HideInInspector] public Collider2D myGeneralCollider;
	[HideInInspector] public Collider2D[] myGeneralColliders;
	[HideInInspector] public Shake myShaker;
	[HideInInspector] public _2dxFX_Shiny_Reflect myRendererHitfx;
	[HideInInspector] public _2dxFX_Sepia myRendererAttfx;
	public virtual void Awake()
	{
		myRenderer = transform.GetComponentInChildren<SpriteRenderer>();
		myRenderers = transform.GetComponentsInChildren<SpriteRenderer>();
		if (transform.GetComponentInChildren<Shake>() != null)
			myShaker = transform.GetComponentInChildren<Shake>();
		if (transform.GetComponentInChildren<_2dxFX_Shiny_Reflect>())
			myRendererHitfx = transform.GetComponentInChildren<_2dxFX_Shiny_Reflect>();
		if (transform.GetComponentInChildren<_2dxFX_Sepia>())
			myRendererAttfx = transform.GetComponentInChildren<_2dxFX_Sepia>();
		if (myRenderer.sharedMaterial == null)
			myRenderer.sharedMaterial = new Material(Shader.Find("Sprites/Default"));
		if (myGeneralCollider == null || myGeneralColliders.Length == 0)
			myGeneralCollider = transform.GetComponent<Collider2D>();
		myGeneralColliders = transform.GetComponentsInChildren<Collider2D>();
		if (transform.GetComponent<SortingGroup>() == null)
		{
			//Debug.Log("No sortingGroup on " + gameObject.name + " be sure to add one later to cut back on load time");
			mySortingGroup = gameObject.AddComponent<SortingGroup>();
		}
		else
			mySortingGroup = transform.GetComponent<SortingGroup>();
		mySortingGroup.sortingLayerID = myRenderer.sortingLayerID;

		if (transform.GetComponent<Rigidbody2D>() != null)
		{
			myBody = transform.GetComponent<Rigidbody2D>();
		}
	}

	public virtual void Start()
	{
		dragMagnituge = 20;
		dragAngularVelocity = 20;
	}
	public virtual void Update()
	{
		speed = EstimatedSpeed(Time.deltaTime);
		if (transform.position.z != 0)
		{
			Vector3 newPos = transform.position;
			newPos.z = 0;
			transform.position = newPos;
		}

		if (fixedAngualarVelocity && !isMovingToSpawn)
		{
			Vector3 angle = myRenderer.transform.localEulerAngles;
			angle.z += (speed * dragAngularVelocity * dir * Time.deltaTime);
			myRenderer.transform.localEulerAngles = new Vector3(0, 0, angle.z);
			myBody.angularVelocity = speed * dragAngularVelocity * dir;

		}

		mySortingGroup.sortingOrder = sortingOrder;
		//mySortingGroup.sortingOrder = (int)(transform.position.y * -10);

	}
	public virtual void LateUpdate()
	{
		//lastPos = transform.position * Time.smoothDeltaTime;
		lastFramePosition = transform.position;
	}
	private void OnApplicationPause(bool pause)
	{

	}
	public void PickUpandDrag(Vector3 _Pos)
	{
		transform.position = _Pos;
	}

	Transform hungrySlime;

	public virtual void TouchFunction(int _id, Vector3 _currentPos)//0= Down, 1=Hold, 2=Up, 3=DoubleTap, 4+TotalTapCount
	{

		switch (_id)
		{
			case 0://pickup this object
				//if (isMovable)
				//{
				//	if (myBody != null)
				//	{
				//		Vector2 dir = (_currentPos - transform.position).normalized;
				//		float mag = Vector2.Distance(transform.position, _currentPos);
				//		myBody.velocity = dir * mag * dragMagnituge;
				//	}
				//	else
				//	{
				//		transform.position = _currentPos;

				//	}

				//}
				//foodLetGo = false;
				break;
			case 1://move
				if (isMovable)
				{
					if (myBody != null)
					{
						Vector2 dir = (_currentPos - transform.position).normalized;
						float mag = Vector2.Distance(transform.position, _currentPos);
						myBody.velocity = Vector2.Lerp(myBody.velocity, (dir * dragMagnituge), Time.deltaTime * mag);
					}
					else
					{
						transform.position = _currentPos;

					}
				}
				break;
			case 2:

				//if (isMovable)
				//{
				//	if (myBody != null)
				//	{
				//		Vector2 dir = (_currentPos - transform.position).normalized;
				//		float mag = Vector2.Distance(transform.position, _currentPos);
				//		myBody.velocity = dir * mag * dragMagnituge;
				//	}
				//	else
				//	{
				//		transform.position = _currentPos;

				//	}

				//}
				// foodLetGo = true;
				break;
			case 3:
				////Debug.Log("Touch-DoubleTap id:" + _id.ToString());

				break;//Pres enter here to add new case, then make int relative
			case 4:
				//Debug.Log("Touch-TapCount id:" + _id.ToString());

				break;

		}//when adding additional cases, match the logic of the below if()VVV
		if (_id > 4)
		{
			//Debug.Log("Touch-TapCount id:" + _id.ToString());

		}
	}

	public virtual void OnDrawGizmosSelected()
	{
		if (_Debug)
		{
			if (isMoving())
			{
				float _mag = magnitude;
				Gizmos.DrawSphere(transform.position, 0.4f);
				Gizmos.DrawSphere(lastPos, 0.2f);
				//direction * 0.8f * -magnitude
				Gizmos.DrawLine(lastPos, transform.position);
			}

		}
	}
}
