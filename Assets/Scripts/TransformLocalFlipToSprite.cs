﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformLocalFlipToSprite : MonoBehaviour {

	private SpriteRenderer sprite;
	public bool oppositeFlip = false;
	private bool flip;
	private Vector3 localOffset;
	// Use this for initialization
	void Start () {
		sprite = transform.GetComponentInParent<SpriteRenderer>();
		localOffset = transform.localPosition;
	}
	
	// Update is called once per frame
	void Update () {
		if(sprite != null)
		{
			
			if(flip != sprite.flipX)
			{
				flip = sprite.flipX;
				transform.localPosition = new Vector3(localOffset.x * (flip ? -1 : 1), localOffset.y, 0);
				if(flip)
				{

				}
			}
		}
	}
}
