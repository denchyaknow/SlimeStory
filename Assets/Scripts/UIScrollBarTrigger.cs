﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class UIScrollBarTrigger : EventTrigger
{
	public UIScrollBar scrollBar;
	private Vector2 dragStartPos = new Vector2();
	public override void OnBeginDrag(PointerEventData eventData)
	{
		base.OnBeginDrag(eventData);
		dragStartPos = eventData.position;
	}
	public override void OnEndDrag(PointerEventData eventData)
	{
		base.OnEndDrag(eventData);
	}
	public override void OnDrag(PointerEventData eventData)
	{
		base.OnDrag(eventData);
		Vector2 dir = (eventData.position - dragStartPos);
		float mag = dir.magnitude;
		mag *= scrollBar.scrollMagMultiplier;
		scrollBar.UpdateScrollRect(dir.normalized, mag);
	}


}
