﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
public class PlayerControls : MonoBehaviour
{
	public int touchCount;
	public float doubleTapDelay = 0.25f;
	private float lastTapTime = 0;
	private Transform selectorTransform;
	private bool selectorHighlighting = false;
	public Transform selectorParticle;
	public Transform touchParticleParent;
	public TouchJoyStick touchJoyStickLeft;
	private ParticleSystem[] touchParticles = new ParticleSystem[0];
	public float tRadius = 0.25f;
	public RaycastHit2D hitEnemy;
	public RaycastHit2D hitObject;
	public RaycastHit2D hitSelectable;
	private BaseObject dragableObject;
	public BaseObject selectableObject;
	public bool onButtDown;
	public bool onButt;
	public bool onButtUp;
	public bool onButtLeft;
	public bool onButtRight;

	public float timeHeldLeft = 1;
	[Tooltip("The time from Touch down to Touch up only triggers a swipe if its less than this")]
	public float timeHeldThresholdLeft = 1.0f;
	[Tooltip("The time till next left swipe is ready after performing one")]
	public float timeHeldLeftCd = 0.1f;
	private float timeHeldLastLeft = 0f;

	public float timeHeldRight = 1;
	public float timeHeldRightThresholdRight = 1f;
	public float timeHeldAddRateRight = 0.01f;

	public delegate void OnTouchLeftDelegate();
	public OnTouchLeftDelegate OnTouchLeft;
	public delegate void OnTouchRightDelegate();
	public OnTouchRightDelegate OnTouchRight;
	private BaseObject[] dragableObjects = new BaseObject[10];
	public bool draggingOBject = false;
	public bool trashCanActive = false;
	private int touchIndex = -1;
	private int touchIndexLeft = -1;
	private int touchIndexRight = -1;
	private float swipeLeftMag = 0;
	public float swipeLeftMagMax = 4f;
	public float swipeLeftThreshold = 0.2f;
	private float swipeRightMag = 0;
	public float swipeRightMagMax
	{
		get
		{
			float distance = 5f;
			if (slime != null)
				distance = slime.slimeStats.attackDistance;
			return distance;
		}
	}
	public float swipeRightThreshold = 0.2f;
	private Vector2 tPosSwipeLeftStart = new Vector3();
	private Vector2 tPosSwipeRightStart = new Vector3();
	public Vector2 tPosSwipeLeftDir = new Vector3();
	public Vector2 tPosSwipeRightDir = new Vector3();
	public bool UseMouse = false;
	public LayerMask whatIsEnemy;
	public LayerMask whatIsPlayer;
	public LayerMask whatIsDragable;
	public LayerMask whatIsSelectable;
	public LayerMask whatIsInteractable;
	private List<RaycastResult> rayHitsUI = new List<RaycastResult>();
	Touch fakeTouch = new Touch();
	private BaseSlime currentSlime;
	private BaseSlime slime
	{
		get
		{

			if (currentSlime == null && GameManager._Instance.combatManager != null)
				currentSlime = GameManager._Instance.combatManager.playerSlime;
			if (currentSlime == null && GameManager._Instance.playPenManager != null)
				currentSlime = GameManager._Instance.GetSelectedSlime();

			return currentSlime;
		}
	}
	public bool CheckForTarget(Vector3 pos)
	{
		Collider2D check = null;
		check = Physics2D.OverlapCircle(pos, tRadius, whatIsInteractable);
		return check != null;
	}
	public bool CheckForUI(Vector3 pos)
	{
		GraphicRaycaster gr = GameManager._Instance.uiManager.GetRayCaster;
		PointerEventData ped = new PointerEventData(null);
		ped.position = Camera.main.WorldToScreenPoint(pos);
		rayHitsUI.Clear();
		gr.Raycast(ped, rayHitsUI);
		return rayHitsUI.Count > 0;
	}
	Collider2D target;
	Collider2D attackTarget;
	public Collider2D GetClosestCollider(Vector2 tPos)
	{
		Collider2D closest = null;
		Collider2D[] allTargets = Physics2D.OverlapCircleAll(tPos, tRadius, whatIsInteractable);
		if (allTargets.Length > 0)
		{
			float dis = 0;
			for (int j = 0; j < allTargets.Length; j++)
			{
				float nextDis = Vector2.Distance(tPos, allTargets[j].bounds.center);
				if (nextDis < dis || j == 0)
				{
					dis = nextDis;
					closest = allTargets[j];
				}
			}
		}
		return closest;
	}
	public bool _Debug;
	private void OnEnable()
	{
		OnTouchDown += GameManager._Instance.soundManager.PlayUIClick;
		OnTouchDown += () =>
		{

		};
		OnTouchUpdate += () =>
		{

		};
		OnTouchUp += () =>
		{

		};
	}
	private void OnDisable()
	{
		OnTouchDown = null;
		OnTouchUpdate = null;
		OnTouchUp = null;
	}
	private IEnumerator Start()
	{
		if (touchParticleParent.childCount > 0)
		{
			touchParticles = new ParticleSystem[touchParticleParent.childCount];
			for (int i = 0; i < touchParticleParent.childCount; ++i)
			{
				touchParticles[i] = touchParticleParent.GetChild(i).GetComponentInChildren<ParticleSystem>();
				ParticleSystem.MainModule main = touchParticles[i].main;
				main.loop = false;
				main.playOnAwake = false;

			}
		}
		touchIndexLeft = -1;
		touchIndexRight = -1;
		SetControlScheme();
		while (LoadingScreen._Instance == null)
		{
			yield return new WaitForSeconds(0.1f);
		}
		//LoadingScreen._Instance.OnActiveSceneChange += PrepForNextScene;
		//LoadingScreen._Instance.OnActiveSceneChange += SetControlScheme;

	}
	private void LateUpdate()
	{
		if (selectorTransform == null && selectorHighlighting)
		{
			selectorHighlighting = false;
			UnHighlightObject();
		}
		if (selectorTransform != null && !selectorHighlighting)
		{
			selectorHighlighting = true;
			HighlightObject();
		}
		if (selectorTransform != null && selectorHighlighting)
		{
			selectorParticle.transform.position = selectorTransform.position;
		}
	}
	private void Update()
	{

		if (GameManager._Instance.gamePaused)
			return;

		//foreach(Touch touch in Input.touches)
		//{
		//	InputTouch(touch.fingerId, Camera.main.ScreenToWorldPoint(touch.position), touch.phase, touch);
		//}
		if (Input.touchCount == 0)
		{
			fakeTouch.position = Input.mousePosition;
			fakeTouch.fingerId = 0;
			if (Input.GetMouseButtonDown(0))
			{
				fakeTouch.phase = TouchPhase.Began;
				if (GameManager._Instance.combatManager != null)
					InputTouchCombat(fakeTouch);
				else
					InputTouchPlaypen(fakeTouch);
				//InputTouch(0, Camera.main.ScreenToWorldPoint(Input.mousePosition), TouchPhase.Began, fakeTouch);
			}
			if (Input.GetMouseButton(0))
			{
				fakeTouch.phase = TouchPhase.Moved;
				if (GameManager._Instance.combatManager != null)
					InputTouchCombat(fakeTouch);
				else
					InputTouchPlaypen(fakeTouch);
				//InputTouch(0, Camera.main.ScreenToWorldPoint(Input.mousePosition), TouchPhase.Moved, fakeTouch);
			}
			if (Input.GetMouseButtonUp(0))
			{
				fakeTouch.phase = TouchPhase.Ended;
				if (GameManager._Instance.combatManager != null)
					InputTouchCombat(fakeTouch);
				else
					InputTouchPlaypen(fakeTouch);
				//InputTouch(0, Camera.main.ScreenToWorldPoint(Input.mousePosition), TouchPhase.Ended, fakeTouch);
			}

		}
		else
		{
			for (int i = 0; i < Input.touchCount; i++)
			{
				//int index = i;
				//InputTouch(index, Camera.main.ScreenToWorldPoint(Input.touches[index].position), Input.touches[index].phase, Input.touches[index]);
				if (GameManager._Instance.combatManager != null)
					InputTouchCombat(Input.touches[i]);
				else
					InputTouchPlaypen(Input.touches[i]);
			}
		}
		touchCount = Input.touchCount;

		draggingOBject = dragableObject != null;

	}
	public void PrepForNextScene()
	{
		//selectorParticle.SetParent(transform);
		selectorParticle.transform.localPosition = Vector3.zero;
		selectorParticle.GetComponent<ParticleSystem>().Stop(true);
		selectorParticle.GetComponent<ParticleSystem>().Clear(true);
		touchIndex = -1;
		touchIndexLeft = -1;
		touchIndexRight = -1;

		onButtRight = false;
		onButtLeft = false;

	}
	public void SetControlScheme()
	{
		int sceneID = SceneManager.GetActiveScene().buildIndex;
		timeHeldLastLeft = 0;
		timeHeldLeft = 0;
		
		switch (sceneID)
		{
			case 0://menu
				OnTouchLeft = OnTouchLeftPlaypen;
				OnTouchRight = OnTouchRightPlaypen;
				Debug.Log("Controls switched to Playpen");
				break;
			case 1://pen
				OnTouchLeft = OnTouchLeftPlaypen;
				OnTouchRight = OnTouchRightPlaypen;
				Debug.Log("Controls switched to Playpen");
				break;
			case 2://combat
				OnTouchLeft = OnTouchLeftCombat;
				OnTouchRight = OnTouchRightCombat;
				Debug.Log("Controls switched to Combat");
				break;
		}
	}
	public void SelectDefaultSlime()
	{
		selectableObject = GameManager._Instance.GetSelectedSlime();
		if (selectableObject == null)
			return;
		selectorParticle.GetComponent<ParticleSystem>().Stop(true);
		selectorTransform = selectableObject.transform;
		//selectorParticle.SetParent(selectableObject.transform);
		selectorParticle.transform.localPosition = Vector3.zero;
		foreach (ParticleSystem sys in selectorParticle.transform.GetComponentsInChildren<ParticleSystem>())
		{
			ParticleSystem.MainModule main = sys.main;
			main.simulationSpace = ParticleSystemSimulationSpace.Local;
		}
		selectorParticle.GetComponent<ParticleSystem>().Play(true);
	}
	public void HighlightObject()
	{
		//BaseSlime slime = GameManager._Instance.GetSelectedSlime();
		selectorParticle.GetComponent<ParticleSystem>().Stop(true);
		//selectorParticle.SetParent(slime.transform);
		//selectorParticle.transform.localPosition = Vector3.zero;
		foreach (ParticleSystem sys in selectorParticle.transform.GetComponentsInChildren<ParticleSystem>())
		{
			ParticleSystem.MainModule main = sys.main;
			main.simulationSpace = ParticleSystemSimulationSpace.Local;
		}
		selectorParticle.GetComponent<ParticleSystem>().Play(true);
	}
	public void UnHighlightObject()
	{

		selectorParticle.GetComponent<ParticleSystem>().Stop(true);
		foreach (ParticleSystem sys in selectorParticle.transform.GetComponentsInChildren<ParticleSystem>())
		{
			ParticleSystem.MainModule main = sys.main;
			sys.time = 0;

			main.simulationSpace = ParticleSystemSimulationSpace.Local;
		}
		//selectorParticle.SetParent(transform);
		selectorParticle.transform.localPosition = Vector3.zero;
	}
	public delegate void Delegate();
	public Delegate OnTouchDown;//extra event hook interface for external code
	public Delegate OnTouchUpdate;//extra event hook interface for external code
	public Delegate OnTouchUp;//extra event hook interface for external code

	private void InputTouchPlaypen(Touch touch)
	{
		Vector2 tPos = Camera.main.ScreenToWorldPoint(touch.position);
		int i = touch.fingerId;
		bool clickInUI = CheckForUI(tPos);
		switch (touch.phase)
		{
			case TouchPhase.Began:
				if (OnTouchDown != null)
					OnTouchDown();
				touchParticles[i].transform.position = tPos;
				touchParticles[i].Play(true);
				if (touchIndex == -1 && !clickInUI)
				{
					StartGeneralTouch(touch);
				}
				break;
			case TouchPhase.Moved:
			case TouchPhase.Stationary:
				if (OnTouchUpdate != null)
					OnTouchUpdate();
				touchParticles[i].transform.position = tPos;

				if (touchIndex == i)
				{
					UpdateGeneralTouch(touch);
				}
				break;
			case TouchPhase.Ended:
			case TouchPhase.Canceled:
				if (OnTouchUp != null)
					OnTouchUp();
				touchParticles[i].GetComponentInChildren<ParticleSystem>().Stop(true);
				touchParticles[i].transform.position = Vector2.zero;
				if (touchIndex == i)
				{
					EndGeneralTouch(touch);
				}
				break;

		}
	}
	private void StartGeneralTouch(Touch touch)
	{
		touchIndex = touch.fingerId;
		
		target = GetClosestCollider(Camera.main.ScreenToWorldPoint(touch.position));
		if (target != null)
		{
			if (whatIsDragable == (whatIsDragable | 1 << target.gameObject.layer))
			{
				dragableObject = target.transform.GetComponent<BaseObject>();
				dragableObject.TouchFunction(0, dragableObject.transform.position);
			}
			if (whatIsSelectable == (whatIsSelectable | 1 << target.gameObject.layer))
			{
				selectableObject = target.transform.GetComponent<BaseObject>();
				selectableObject.isSelected = true;
				if (selectableObject.GetComponent<BaseSlime>())
				{
					GameManager._Instance.ResetSlimeIndex();
					GameManager._Instance.SetSelectedSlime(selectableObject.GetComponent<BaseSlime>());
					selectorTransform = selectableObject.transform;
				}
			}
		}
		else
			selectorTransform = null;
	}
	private void UpdateGeneralTouch(Touch touch)
	{
		Vector2 tPos = Camera.main.ScreenToWorldPoint(touch.position);
		if (dragableObject == null)
		{
			target = GetClosestCollider(Camera.main.ScreenToWorldPoint(touch.position));
			if (target != null)
			{
				if (whatIsInteractable == (whatIsInteractable | 1 << target.gameObject.layer))
				{
					target.transform.GetComponent<BaseObject>().isSelected = true;
				}
			}
		}
		else
		{
			BaseSlime slime = dragableObject.gameObject.GetComponent<BaseSlime>();
			if (slime != null)
			{
				if (slime.isBeingPet)
				{
					dragableObject = null;
					return;
				}
			}
			dragableObject.TouchFunction(1, tPos);
			//Check if trying to delete an object
			if (GameManager._Instance.trashCan != null)
			{
				TrashCanProperties trashCan = GameManager._Instance.trashCan;
				float dis = Vector2.Distance(trashCan.transform.position, tPos);
				if (dis < trashCan.trashCanActivationDistance)
				{
					trashCan.deletableObject = dragableObject.gameObject;
				}
				else if (trashCan.deletableObject != null)
				{
					trashCan.deletableObject = null;
				}
			}
		}
	}
	private void EndGeneralTouch(Touch touch)
	{
		int i = touch.fingerId;
		bool doubleTap = (Time.timeSinceLevelLoad - lastTapTime < doubleTapDelay);
		lastTapTime = Time.timeSinceLevelLoad;
		
		if (selectableObject != null)
		{
			//Highlight selected slime and pet it
			selectorTransform = selectableObject.transform;
			if (selectableObject.GetComponent<BaseSlime>() != null)
			{
				BaseSlime slime = selectableObject.GetComponent<BaseSlime>();
				if (doubleTap && !slime.isDead)
				{
					slime.TouchFunction(3, slime.transform.position);
					selectorTransform = null;
				}
				GameManager._Instance.SetSelectedSlime(slime.slimeStats.index);
			}
			selectableObject.isSelected = false;
			selectableObject = null;
		}
		//Finish deleting object
		if (GameManager._Instance.trashCan != null)
		{
			if (trashCanActive)
			{
				GameManager._Instance.trashCan.deletableObject = null;
				DeleteObject(dragableObject.transform);
			}
			else
			{
				GameManager._Instance.trashCan.deletableObject = null;
			}
		}
		if (dragableObject != null)
		{
			dragableObject.TouchFunction(2, dragableObject.transform.position);
			dragableObject = null;
		}
		touchIndex = -1;
	}
	void InputTouchCombat(Touch touch)
	{
		Vector2 tPos = Camera.main.ScreenToWorldPoint(touch.position);
		int i = touch.fingerId;
		//if we didnt already tap on right side, if touch is on right side, if touch is not already left touch index
		if (!onButtRight && tPos.x > GetScreenCenterPosition().x && i != touchIndexLeft)
		{
			onButtRight = true;
			touchIndexRight = i;

		}
		//if we didnt already tap on left side, if touch is on left side, if touch is not already right touch index
		if (!onButtLeft && tPos.x < GetScreenCenterPosition().x && i != touchIndexRight)
		{
			onButtLeft = true;
			touchIndexLeft = i;
		}

		switch (touch.phase)
		{
			case TouchPhase.Began:
				if (OnTouchDown != null)
					OnTouchDown();
				if (onButtLeft && touchIndexLeft == i)
				{
					StartLeftTouch(touch);
				}
				if (onButtRight && touchIndexRight == i)
				{
					StartRightTouch(touch);
				}
				break;
			case TouchPhase.Moved:
			case TouchPhase.Stationary:
				if (OnTouchUpdate != null)
					OnTouchUpdate();
				if (onButtLeft && touchIndexLeft == i)
				{
					UpdateLeftTouch(touch);
				}
				if (onButtRight && touchIndexRight == i)
				{
					UpdateRightTouch(touch);
				}
				break;
			case TouchPhase.Ended:
			case TouchPhase.Canceled:
				if (OnTouchUp != null)
					OnTouchUp();
				if (onButtLeft && touchIndexLeft == i)
				{
					EndLeftTouch(touch);
				}
				if (onButtRight && touchIndexRight == i)
				{
					EndRightTouch(touch);
				}
				break;

		}
	}
	void StartLeftTouch(Touch touch)
	{
		swipeLeftMag = 0;
		timeHeldLeft = 0;
		timeHeldLeft += Time.deltaTime;
		tPosSwipeLeftStart = Camera.main.ScreenToWorldPoint(touch.position);
		tPosSwipeLeftDir = Vector3.zero;
		touchJoyStickLeft.ActivateJoyStick(touchIndexLeft, touch.position);
	}
	void UpdateLeftTouch(Touch touch)
	{
		timeHeldLeft += Time.deltaTime;
		//Move slime relative to the joystick axis
		tPosSwipeLeftDir = touchJoyStickLeft.GetJoyStickDirection(touch.position).normalized;
		swipeLeftMag = touchJoyStickLeft.joyMagnitude;
		//Only move slime is joystick is not centered
		if (swipeLeftMag > 0.15f)
			slime.MoveBody(tPosSwipeLeftDir, swipeLeftMag);
	}
	void EndLeftTouch(Touch touch)
	{
		touchJoyStickLeft.DeactivateJoyStick();
		if (Time.timeSinceLevelLoad > timeHeldLastLeft)
		{
			if (timeHeldLeft < timeHeldThresholdLeft)
			{
				timeHeldLastLeft = Time.timeSinceLevelLoad + timeHeldLeftCd;
				slime.MoveBody(tPosSwipeLeftDir, slime.slimeStats.moveDistance, false);
				slime.SetNoClip(timeHeldLeftCd);
			}
		}
		touchIndexLeft = -1;
		onButtLeft = false;
	}
	void StartRightTouch(Touch touch)
	{
		swipeRightMag = 0;
		timeHeldRight = 1f;
		tPosSwipeRightStart = Camera.main.ScreenToWorldPoint(touch.position);
		tPosSwipeRightDir = Vector3.zero;
		attackTarget = null;
	}
	void UpdateRightTouch(Touch touch)
	{
		//Update directional targeting of attacks and debug it in the editor
		Vector2 tPos = Camera.main.ScreenToWorldPoint(touch.position);
		swipeRightMag = Vector3.Distance(tPos, tPosSwipeRightStart);
		tPosSwipeRightDir = (tPos - tPosSwipeRightStart).normalized;
		if (swipeRightMag > swipeRightThreshold)
		{
			Vector2 size = new Vector2(slime.slimeStats.attackRadius, slime.slimeStats.attackRadius);
			float angle = Mathf.Atan2(tPosSwipeRightDir.y, tPosSwipeRightDir.x) * Mathf.Rad2Deg;
			Quaternion orientation = Quaternion.AngleAxis(angle, Vector3.forward);

			RaycastHit2D[] hits = Physics2D.BoxCastAll(slime.transform.position, size, angle, tPosSwipeRightDir, slime.slimeStats.attackDistance, whatIsEnemy);
			ExtDebug.DrawBoxCast2DBox(slime.transform.position, size / 2, orientation, tPosSwipeRightDir, slime.slimeStats.attackDistance, Color.red);
			if (hits.Length > 0)
			{
				for (int j = 0; j < hits.Length; j++)
				{
					ExtDebug.DrawBoxCast2DOnHit(slime.transform.position, size / 2, orientation, tPosSwipeRightDir, hits[j].fraction * slime.slimeStats.attackDistance, Color.cyan);
				}
				attackTarget = hits[0].collider;
			}
			else
			{
				attackTarget = null;
			}
		}
	}
	void EndRightTouch(Touch touch)
	{
		swipeRightMag = 0;
		touchIndexRight = -1;
		tPosSwipeRightStart = Vector3.zero;
		tPosSwipeRightDir = Vector3.zero;
		onButtRight = false;
		Vector3 attackPos = Vector3.zero;
		Vector2 newDirection = tPosSwipeRightDir;
		float newDistance = slime.slimeStats.attackDistance;
		if (attackTarget)
		{
			newDirection = (attackTarget.transform.position - slime.transform.position).normalized;
			newDistance = Vector2.Distance(attackTarget.transform.position, slime.transform.position);
		}
		if (swipeRightMag < swipeRightThreshold && GameManager._Instance.combatManager.enemies.Count > 0)
		{
			Transform mob = GameManager._Instance.combatManager.enemies[0].transform;
			newDirection = (mob.position - slime.transform.position).normalized;
			newDistance = Vector2.Distance(mob.position, slime.transform.position);
		}
		if (newDistance > slime.slimeStats.attackDistance)
			newDistance = slime.slimeStats.attackDistance;

		attackPos = (slime.transform.position + (Vector3)(newDirection * newDistance));
		slime.Attack(attackPos);
	}
	
	void DeleteObject(Transform obj)
	{
		BaseSlime slime = obj.GetComponent<BaseSlime>();
		BaseFood food = obj.GetComponent<BaseFood>();
		BaseDeco deco = obj.GetComponent<BaseDeco>();
		bool delete = false;
		if (slime != null && GameManager._Instance.ourSlimes.Contains(slime.gameObject))
		{
			bool aSlimeAlive = false;
			for (int i = 0; i < GameManager._Instance.ourSlimes.Count; i++)
			{
				BaseSlime bSlime = GameManager._Instance.ourSlimes[i].GetComponent<BaseSlime>();
				if (bSlime != slime)
				{
					if (bSlime.slimeStats.currentHealth > 0)
					{
						aSlimeAlive = true;
						break;
					}
				}
			}
			if (aSlimeAlive)
			{
				//GameManager._Instance.ourSlimes.Remove(slime.gameObject);
				GameManager._Instance.ourSlimes.Remove(slime.gameObject);
				slime.enabled = false;
				delete = true;
			}
		}
		if (food != null)
		{
			if(GameManager._Instance.ourFood.Contains(food))
			{
				GameManager._Instance.ourFood.Remove(food);
			}
			delete = true;
		}
		if (deco != null && GameManager._Instance.ourDeco.Contains(deco))
		{
			GameManager._Instance.ourDeco.Remove(deco);
			delete = true;

		}
		if (delete)
		{
			Destroy(obj.gameObject);
			GameManager._Instance.ResetSlimeIndex();
			GameManager._Instance.GameSave();
		}
	}
	void UpdateJoystick(Touch touch)
	{
		tPosSwipeLeftDir = touchJoyStickLeft.GetJoyStickDirection(touch.position);
		swipeLeftMag = touchJoyStickLeft.joyMagnitude;
		if (swipeLeftMag > 0.2f)
			slime.MoveBody(tPosSwipeLeftDir, swipeLeftMag);
	}
	void OnTouchPlayPen()
	{

	}
	void OnTouchLeftCombat()
	{
		onButtLeft = false;
		touchIndexLeft = -1;
		slime.isSelected = false;
		//if(swipeLeftMag > swipeLeftThreshold)
		//{
		//    slime.MoveBody(tPosSwipeLeftDir, timeHeldLeft * swipeLeftMag);
		//}

	}
	void OnTouchLeftPlaypen()
	{

	}
	void OnTouchRightPlaypen()
	{

	}
	void OnTouchRightCombat()
	{
		onButtRight = false;
		touchIndexRight = -1;
		Vector3 attackPos = Vector3.zero;
		Vector2 newDirection = tPosSwipeRightDir;
		float newDistance = slime.slimeStats.attackDistance;
		if (attackTarget)
		{
			newDirection = (attackTarget.transform.position - slime.transform.position).normalized;
			newDistance = Vector2.Distance(attackTarget.transform.position, slime.transform.position);
		}
		if (swipeRightMag < swipeRightThreshold && GameManager._Instance.combatManager.enemies.Count > 0)
		{
			Transform mob = GameManager._Instance.combatManager.enemies[0].transform;
			newDirection = (mob.position - slime.transform.position).normalized;
			newDistance = Vector2.Distance(mob.position, slime.transform.position);
		}
		if (newDistance > slime.slimeStats.attackDistance)
			newDistance = slime.slimeStats.attackDistance;

		attackPos = (slime.transform.position + (Vector3)(newDirection * newDistance));
		slime.Attack(attackPos);
	}
	void DoButtUpRight(Vector2 pos)
	{
		onButtRight = false;
		touchIndexRight = -1;
		//slime.attackCharging = false;
		//slime.StopCharge();
		Vector3 attackPos = Vector3.zero;
		Vector2 newDirection = tPosSwipeRightDir;
		float newDistance = slime.slimeStats.attackDistance;
		if (target)
		{
			newDirection = (target.transform.position - slime.transform.position).normalized;
			newDistance = Vector2.Distance(target.transform.position, slime.transform.position);
		}
		if (swipeRightMag < swipeRightThreshold)
		{
			Transform mob = GameManager._Instance.combatManager.enemies[0].transform;
			newDirection = (mob.position - slime.transform.position).normalized;
			newDistance = Vector2.Distance(mob.position, slime.transform.position);
		}
		if (newDistance > slime.slimeStats.attackDistance)
			newDistance = slime.slimeStats.attackDistance;

		attackPos = (slime.transform.position + (Vector3)(newDirection * newDistance));
		slime.Attack(attackPos);
		//if (target && whatIsEnemy == (whatIsEnemy | 1 << target.gameObject.layer))//tap on right side and hit enemy
		//{
		//    slime.Attack(target.GetComponent<BaseMob>());
		//    //GameManager._Instance.combatManager.HitEnemy(2, target.transform.GetComponent<BaseMob>());
		//}
		//else if (swipeRightMag > swipeRightThreshold)
		//{
		//    Vector3 attackPos = (slime.transform.position + (Vector3)(tPosSwipeRightDir * slime.slimeStats.attackDistance));
		//}
		//else 
		//{
		//    //slime.attackPos = pos;
		//    slime.AttackAutoTarget();
		//}
	}
	void DoButtUpLeft()
	{
		onButtLeft = false;
		touchIndexLeft = -1;
		slime.isSelected = false;
		if (selectableObject != null && whatIsPlayer == (whatIsPlayer | 1 << selectableObject.gameObject.layer))
		{
			//GameManager._Instance.combatManager.HitSlime(2, selectableObject.GetComponent<BaseSlime>());
			selectableObject.GetComponent<BaseSlime>().MoveBody(tPosSwipeLeftDir, swipeLeftMag);
			selectableObject = null;
		}
	}
	public void OnDrawGizmosSelected()
	{
		if (Application.isPlaying)
		{
			if (GameManager._Instance.combatManager != null && slime != null)
			{
				if (onButtLeft)
				{
					Gizmos.color = Color.yellow;
					if (swipeLeftMag > swipeLeftThreshold)
						Gizmos.color = Color.green;
					float radius = (slime.slimeStats.moveDistance / 10) * swipeLeftMag * timeHeldLeft;
					Gizmos.DrawWireSphere(slime.transform.position, radius);
					Gizmos.DrawLine(slime.transform.position, slime.transform.position + (Vector3)(tPosSwipeLeftDir * (timeHeldLeft * swipeLeftMag * (slime.slimeStats.moveDistance / 10))));
					Gizmos.DrawWireSphere(slime.transform.position + (Vector3)(tPosSwipeLeftDir * timeHeldLeft * slime.slimeStats.moveDistance), 0.2f);
					Gizmos.color = Color.white;
					Gizmos.DrawWireSphere(slime.transform.position, swipeLeftThreshold);
				}
				Gizmos.color = Color.green;
				Gizmos.DrawWireSphere(slime.transform.position, swipeLeftMagMax);

			}
		}
	}
	public void OnDrawGizmos()
	{
		if (Application.isPlaying && _Debug)
		{
			if (touchParticles.Length > 0)
			{
				for (int i = 0; i < touchParticles.Length; ++i)
				{
					Gizmos.color = Color.yellow;
					Gizmos.DrawWireSphere(touchParticles[i].transform.position, 0.4f);
				}
			}
		}
	}
	public Vector3 GetScreenCenterPosition()
	{
		Vector2 center = new Vector2(Camera.main.pixelWidth / 2, Camera.main.pixelHeight / 2);
		return Camera.main.ScreenToWorldPoint(center);
	}
}