﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class CameraManager : MonoBehaviour {
	[HideInInspector]public bool followStart = false;
	[HideInInspector]public bool followStop = false;
	public bool isFollowing = false;
	private bool isStarting = false;
	private bool isStoping = false;
	//public float nodeTimeDefault = 1.4f;
	//public Ease nodeEase;
	//public Transform nodeCurrent = null;
	public float followLookAhead = 2f;
	//public float followThreshold = 0.5f;
	public float followSpeedDefault = 3f;
	private float followSpeed = 0f;
	public float tweenTime = 3f;
	public Ease tweenEase = Ease.InOutQuad;
	public Transform enemySpawns;
	public Transform[] GetEnemySpawnPoints()
	{
		Transform[] spawns = null;
		if (enemySpawns != null && enemySpawns.childCount > 0)
		{
			spawns = new Transform[enemySpawns.childCount];
			for (int i = 0; i < spawns.Length; i++)
				spawns[i] = enemySpawns.GetChild(i).transform;
		}
		return spawns;
	}
	public Transform followTarget
	{
		get
		{
			if(ft == null && GameManager._Instance.combatManager != null)
				ft = GameManager._Instance.combatManager.playerSlime.transform;
			else if(ft == null && GameManager._Instance.playPenManager != null)
				ft = GameManager._Instance.GetSelectedSlime().transform;
			return ft;
		}
	}
	private Transform ft;
	//private bool lookAheadSet = false;
	//private Vector3 lookAheadPos; 
	//private Vector3[] mapBounds = new Vector3[2];
	//private Vector3[] cameraThresholds = new Vector3[2];
	//private float[] camDistances = new float[2];
	//private int trackingDir = 0;
	//private float trackingStopTime = 1;
	void Start () {
		followSpeed = 0;
	}
	void Update ()
	{
		if(isFollowing)
		{
			TrackAheadPlayer(1);
		}
		if(followStart)
		{
			followStart = false;
			isFollowing = true;
		}
		if(followStop)
		{
			followStop = false;
			isFollowing = false;
		}
	}
	public void StartFollowing()
	{
		isStoping = false;
		if (!isStarting)
		{
			DOTween.Kill("CamSpeed");
			isStarting = true;
			DOTween.To(() => followSpeed, x => followSpeed = x, followSpeedDefault, tweenTime).SetEase(tweenEase).SetId("CamSpeed")
				.OnStart(()=>
				{
					isFollowing = true;
				});
		}
	}
	public void StopFollowing()
	{
		isStarting = false;
		if (!isStoping)
		{
			isStoping = true;
			DOTween.Kill("CamSpeed");
			DOTween.To(() => followSpeed, x => followSpeed = x, 0, tweenTime).SetEase(tweenEase).SetId("CamSpeed")
				.OnComplete(()=>
				{
					isFollowing = false;
				});
		}
	}
	public void ResetCamera()
	{
		isFollowing = false;
		isStoping = false;
		isStarting = false;
		transform.parent.position = Vector3.zero;
	}
	public void TrackAheadPlayer(int dir)
	{
		Vector3 byVector = followTarget.position + (Vector3.right * followLookAhead * dir);
		byVector.y = 0;
		byVector.z = 0;
		transform.parent.transform.position = Vector3.Lerp(transform.parent.transform.position, byVector, Time.deltaTime * followSpeed);
	}
}
