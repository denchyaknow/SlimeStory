﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(UIScrollBarTrigger))]
public class UIScrollBar : MonoBehaviour {
	public float scrollSpeed = 0.5f;
	public float scrollThreshold = 0.15f;
	public float scrollMagMultiplier = 0.001f;
	public ScrollRect scrollRect;
	public ScrollDirection scrollDirection;
	public enum ScrollDirection
	{
		Vertical,
		Horizontal
	}
	// Use this for initialization
	void Start () {
		transform.GetComponent<UIScrollBarTrigger>().scrollBar = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void UpdateScrollRect(Vector2 dir, float mag)
	{
		if (scrollRect != null)
		{
			switch (scrollDirection)
			{
				case ScrollDirection.Vertical:
					if (mag > scrollThreshold)
					{
						float newPos = scrollRect.verticalNormalizedPosition += (dir.y * (mag));
						float currentPos = scrollRect.verticalNormalizedPosition;
						
						currentPos = Mathf.Lerp(currentPos, newPos, Time.deltaTime * scrollSpeed);
						if (currentPos > 1f)
							currentPos = 1f;
						if (currentPos < 0f)
							currentPos = 0f;
						scrollRect.verticalNormalizedPosition = currentPos;
					}
					break;

			}
		}
	}
}
