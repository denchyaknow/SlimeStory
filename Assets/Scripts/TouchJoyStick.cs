﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TouchJoyStick : MonoBehaviour {
	public float joyRadius = 5f;//this max radius the joy nob can travel from the center
	public Vector2 joyDirection = Vector2.zero;
	public float joyMagnitude = 0;
	public int lastFinger = -1;
	private Vector2 lastTouchPos = Vector2.zero;
	public SpriteRenderer[] joySprites;
	private void OnDisable()
	{
		DeactivateJoyStick();
		foreach (SpriteRenderer sprite in joySprites)
		{
			sprite.enabled = false;
		}

		lastFinger = -1;
	}
	private void OnEnable()
	{
		foreach (SpriteRenderer sprite in joySprites)
		{
			sprite.enabled = false;
		}

		lastFinger = -1;
	}
	void Start ()
	{
		foreach(SpriteRenderer sprite in joySprites)
		{
			sprite.enabled = false;
		}

		lastFinger = -1;
	}
	public Vector2 GetJoyStickDirection(Vector2 touchPos)
	{
		Vector2 screenPosStart = lastTouchPos;
		Vector2 screenPosCurrent = touchPos;
		transform.position = (Vector2)Camera.main.ScreenToWorldPoint(screenPosStart);

		float knobDis = Vector2.Distance(touchPos, lastTouchPos);
		if(knobDis > joyRadius)
			knobDis = joyRadius;
		joyDirection = (screenPosCurrent - screenPosStart).normalized;
		Vector2 knobPos = lastTouchPos + (joyDirection * knobDis);
		knobPos = Camera.main.ScreenToWorldPoint(knobPos);
		joyMagnitude = Vector2.Distance(transform.position ,knobPos);
		joySprites[1].transform.position = knobPos;
		//joyImages[1].rectTransform.anchoredPosition = knobPos;

		return joyDirection;
	}
	public void ActivateJoyStick(int touchFinger,Vector2 touchPos)
	{
		lastFinger = touchFinger;
		lastTouchPos = touchPos;
		//joyImages[0].rectTransform.anchoredPosition = lastTouchPos;
		Vector3 startPos = Camera.main.ScreenToWorldPoint(lastTouchPos);
		startPos.z = 0;
		transform.position = startPos;

		joyMagnitude = 0;
		joyDirection = Vector2.zero;
		foreach(SpriteRenderer sprite in joySprites)
		{
			sprite.enabled = true;
		}
	}
	public void DeactivateJoyStick()
	{
		//joyImages[1].rectTransform.anchoredPosition = Vector3.zero;
		transform.localPosition = Vector3.zero;
		lastFinger = -1;

		foreach(SpriteRenderer sprite in joySprites)
		{
			sprite.enabled = false;
		}
	}
	void Update () {
		
	}
	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position, joyRadius);
	}
}
