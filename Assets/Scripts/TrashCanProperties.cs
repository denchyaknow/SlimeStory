﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class TrashCanProperties : MonoBehaviour
{
	public float trashCanActivationDistance = 5;
	public bool trashCanActive = false;

	public float trashCanTweenTime = 0.8f;
	public Ease trashCanTweenEase;
	public Transform[] trashCanPath;
	public SpriteRenderer deletionSprite;
	public SpriteRenderer trashCanSprite;
	public GameObject deletableObject;
	private Color spriteColor;
	private Color textColor;
	private Color alphaColor = new Color(0, 0, 0, 0);
	private TextMesh deletionText;
	// Use this for initialization
	IEnumerator Start()
	{
		spriteColor = trashCanSprite.color;
		trashCanSprite.transform.position = trashCanPath[0].position;
		deletionSprite.color = alphaColor;
		if(deletionSprite.transform.GetComponentInChildren<TextMesh>())
			deletionText = deletionSprite.transform.GetComponentInChildren<TextMesh>();
		if(deletionText != null)
		{
			textColor = deletionText.color;
			deletionText.color = alphaColor;
		}
		while(GameManager._Instance == null)
		{
			yield return new WaitForSeconds(0.1f);
		}
		GameManager._Instance.trashCan = this;
	}

	// Update is called once per frame
	void Update()
	{
		if(deletableObject != null)
		{
			if(!trashCanActive && trashCanPath.Length > 0)
			{
				trashCanActive = true;
				DOTween.Kill(trashCanSprite);
				trashCanSprite.transform.DOMove(trashCanPath[1].position, trashCanTweenTime, false).SetEase(trashCanTweenEase).SetId(trashCanSprite)
					.OnComplete(() =>
					{
						deletionSprite.transform.position = deletableObject.transform.position;
						deletionSprite.color = spriteColor;
						if(deletionText != null)
							deletionText.color = textColor;

						GameManager._Instance.inputManager.trashCanActive = true;
					});
			}
			if(GameManager._Instance.inputManager.trashCanActive)
			{
				deletionSprite.transform.position = deletableObject.transform.position;
			}
		} else
		{
			if(trashCanActive && trashCanPath.Length > 0)
			{
				trashCanActive = false;
				DOTween.Kill(trashCanSprite);
				GameManager._Instance.inputManager.trashCanActive = false;
				deletionSprite.transform.localPosition = Vector3.zero;

				deletionSprite.color = alphaColor;
				if(deletionText != null)
					deletionText.color = alphaColor;
				trashCanSprite.transform.DOMove(trashCanPath[0].position, trashCanTweenTime, false).SetEase(trashCanTweenEase).SetId(trashCanSprite)
					.OnComplete(() =>
					{

					});
			}
		}
	}
	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position, trashCanActivationDistance);
	}
}
