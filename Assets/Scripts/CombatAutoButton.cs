﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class CombatAutoButton : MonoBehaviour {

	public Text autoText;
	public Button autoButton;
	public Color autoOnColor;
	public Color autoOffColor;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (GameManager._Instance.combatManager != null)
		{
			if (autoText != null)
			{
				autoText.text = GameManager._Instance.combatManager.autoAttacks ? "Auto: On" : "Auto: Off";
			}
			if (autoButton != null)
			{
				ColorBlock newCols = autoButton.colors;
				newCols.normalColor = GameManager._Instance.combatManager.autoAttacks ? autoOnColor : autoOffColor;
				newCols.highlightedColor = GameManager._Instance.combatManager.autoAttacks ? autoOnColor : autoOffColor;

				autoButton.colors = newCols;
			}
		}
	}
	public void ToggleAutoButton()
	{
		GameManager._Instance.combatManager.autoAttacks = !GameManager._Instance.combatManager.autoAttacks;
		GameManager._Instance.combatManager.autoMoves = !GameManager._Instance.combatManager.autoMoves;

	}
}
