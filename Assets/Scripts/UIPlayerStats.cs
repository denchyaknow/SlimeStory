﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class UIPlayerStats : MonoBehaviour {
	public float animeTime = 0.6f;
	public Ease animeEase = Ease.OutElastic;
    public float statMaxGeneral = 255f;
	public float vertMax = 80f;
	public float widthMax = 60f;
    public float animeDelay = 0.6f;


    [Range(0, 80)]
	public float[] stats = new float[5];
	public RectTransform[] statRect = new RectTransform[5];
    public bool testShow = false;
	// Use this for initialization
	void Start () {
		
	}
	private void OnDisable()
	{
		HideStats();
	}
	// Update is called once per frame
	void Update ()
	{
		if(testShow)
        {
            testShow = false;
            
                ShowStats(GameManager._Instance.GetSelectedSlime().slimeStats);
        }
	}
	public void ShowStats(SlimeData _stats)
	{
		float statMulti = (vertMax /statMaxGeneral);
        float str = (int)((_stats.strength * (statMulti)) * 100);
        float mag = (int)((_stats.magic * (statMulti)) * 100);
        float agi = (int)((_stats.agility * (statMulti)) * 100);
        float spr = (int)((_stats.spirit * (statMulti)) * 100);
        float def = (int)((_stats.defense * (statMulti)) * 100);
        str = (str * 0.01f);
        mag = (mag * 0.01f);
        agi = (agi * 0.01f);
        spr = (spr * 0.01f);
        def = (def * 0.01f);
        float[] verts = new float[5]
        {
           str,mag,agi,spr,def
        };
        for (int i = 0; i < 5; ++i)
        {
			int index = i;
                DOTween.To(() => stats[index], x => stats[index] = x, verts[index], animeTime).SetDelay(animeDelay).SetEase(animeEase).OnUpdate(()=> 
                {
                    statRect[index].SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, stats[index]);
					float widthMulti = (widthMax / vertMax);
					float newWdith = stats[index] * widthMulti;
					statRect[index].offsetMax = new Vector2(statRect[index].offsetMax.x, newWdith);
					statRect[index].offsetMin = new Vector2(statRect[index].offsetMin.x, -newWdith);
				});
            
        }
    }
	public void HideStats()
	{
		stats = new float[5] { 0, 0, 0, 0, 0 };
		for(int i = 0; i < 5; ++i)
		{
			if(statRect != null)
			{

				statRect[i].SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, stats[i]);
				float widthMulti = (widthMax / vertMax);
				float newWdith = stats[i] * widthMulti;
				statRect[i].offsetMax = new Vector2(statRect[i].offsetMax.x, newWdith);
				statRect[i].offsetMin = new Vector2(statRect[i].offsetMin.x, -newWdith);
			}
		}
	}
	private void OnDrawGizmos()
	{
		if(!Application.isPlaying)
		{
			for(int i = 0; i < 5; ++i)
			{
				if(statRect != null)
				{
					
					statRect[i].SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal,stats[i]);
					float widthMulti = (widthMax / vertMax);
					float newWdith = stats[i] * widthMulti;
					statRect[i].offsetMax = new Vector2(statRect[i].offsetMax.x, newWdith);
					statRect[i].offsetMin = new Vector2(statRect[i].offsetMin.x, -newWdith);
				}
			}
		}
	}
}
