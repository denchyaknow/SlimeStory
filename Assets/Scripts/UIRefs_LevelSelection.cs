﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIRefs_LevelSelection : MonoBehaviour {

	public RectTransform rootMenuPivot;
	public RectTransform lvlLast;
	public RectTransform lvlCur;
	public RectTransform lvlNext;
	public Button arrowLeft;
	public Button arrowRight;
	public RectTransform menuOpen;
	public RectTransform mapGo;
	public RectTransform mapLocked;
	public RectTransform mapInfoTitle;
	public RectTransform mapInfoReq;


}
