﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class AutoFeeder : BaseDeco
{
	public List<BaseFood> foodStorage = new List<BaseFood>();
	public Transform foodStorageSpawnPoint;
	public Transform pivotNozzle;
	public Transform fireSpawnPoint;
	public Transform lastTarget;
	public float nozzleRotationSpeed = 5f;
	public float foodCheckRadius = 10f;
	public float foodAutoCooldown = 1f;
	private float lastAutoFeedTime;
    private float lastScanTime;
	public LayerMask whatIsFood;

    public ParticleSystem fireParticle;
    public ParticleSystem hitParticle;
	public ParticleSystem trailParticle;
	private Vector3 animScale0 = new Vector3(0.4f, 3f, 1);
	private float animTime0 = 0.25f;
	private float animDelay0 = 0f;
	private Ease animEase0 = Ease.OutCirc;
	private Vector3 animScale1 = new Vector3(1.8f, 1.2f, 1);
	private float animTime1 = 0.25f;
	private float animDelay1 = 0.25f;
	private Ease animEase1 = Ease.InOutExpo;
	private Vector3 animScale2 = new Vector3(1, 1, 1);
	private float animTime2 = 0.5f;
	private float animDelay2 = 0.5f;
	private Ease animEase2 = Ease.OutBack;

    public float fireLandRadius = 1f;
    public float fireTime = 1f;
    public float fireDelay = 0.4f;
	public Ease fireEase = Ease.OutExpo;
    
	public bool testAnim = false;
	public bool testReload = false;
	public bool testFire = false;
    private Vector3 randomPoint;
	// Use this for initialization
	public override void Start () {
        base.Start();
        mySortingGroup.enabled = false;
		lastAutoFeedTime = Time.timeSinceLevelLoad + foodAutoCooldown;
	}
    public override void Awake()
    {
		base.Awake();
        GameManager._Instance.autoFeeder = this;
		
    }
    private void OnDisable()
    {
        
    }
	// Update is called once per frame
	public IEnumerator RecallFood()
	{
		yield return new WaitForSeconds(0.5f);

	}
    public override void Update () {
		Vector3 myPos = pivotNozzle.position;
		Vector3 targetPos = Vector3.zero;
		Transform targetSlime = null;
		int targetHunger = 0;
		BaseFood nextFood = null;
		int nextFoodValue = 0;
		if(foodStorage.Count > 0)
		{
			nextFood = foodStorage[0];
			nextFoodValue = nextFood.foodData.hungerPerBite;
		}

		targetSlime = GetNextTarget();
		if(targetSlime != null)
		{
			targetPos = targetSlime.position;
			Vector3 dir = ((targetPos) - myPos).normalized;
			float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
			Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
		
			pivotNozzle.rotation =  Quaternion.Lerp(pivotNozzle.rotation, rotation, Time.deltaTime * nozzleRotationSpeed);
		}
        if(Time.timeSinceLevelLoad > lastScanTime )
        {
            lastScanTime = Time.timeSinceLevelLoad + (foodAutoCooldown * 0.8f);
            randomPoint = (Vector3)(Random.insideUnitCircle * fireLandRadius);
        }
        bool foodIsNearTarget = FoodCheck(targetPos);
		if(Time.timeSinceLevelLoad > lastAutoFeedTime 
			&& !foodIsNearTarget 
			&& GameManager._Instance.ourFoodStorage.Count > 0 
			&& GameManager._Instance.ourFood.Count == 0
			&& targetSlime)
		{
			lastAutoFeedTime = Time.timeSinceLevelLoad + foodAutoCooldown;
			lastTarget = targetSlime;
           
            targetPos = targetSlime.transform.position;
            BaseFood food = GameManager._Instance.ourFoodStorage[0];
				FireFood(food, targetPos);
			pivotNozzle.DOScale(animScale0, animTime0).SetEase(animEase0).SetDelay(animDelay0);
			pivotNozzle.DOScale(animScale1, animTime1).SetEase(animEase1).SetDelay(animDelay1);
			pivotNozzle.DOScale(animScale2, animTime2).SetEase(animEase2).SetDelay(animDelay2).OnStart(()=>
			{
			});

    //        if (food == null)
    //            return;
    //        if (GameManager._Instance.ourFoodStorage.Contains(food))
    //            GameManager._Instance.ourFoodStorage.Remove(food);
    //        if (!GameManager._Instance.ourFood.Contains(food))
    //            GameManager._Instance.ourFood.Add(food);
    //        food.transform.position = fireSpawnPoint.position;
    //        food.transform.DOScale(1, fireTime).SetSpeedBased(true);
    //        Vector3 landingPos = targetPos + randomPoint;
    //        food.transform.DOMove(landingPos, fireTime, false)
    //            .SetSpeedBased(true)
    //            .SetDelay(fireDelay)
    //            .SetEase(fireEase)
    //            .OnStart(() =>
    //            {
    //                food.ResetFood();
    //                if (fireParticle != null)
    //                    fireParticle.Play(true);
    //                if (hitParticle != null)
    //                    hitParticle.transform.localPosition = Vector3.zero;
				//	if(trailParticle != null)
				//		trailParticle.Play(true);
    //            })
				//.OnUpdate(()=>
				//{
				//	if(trailParticle != null)
				//	{
				//		trailParticle.transform.position = food.transform.position;
				//	}
				//})
    //            .OnComplete(() =>
    //            {
    //                if (hitParticle != null)
    //                {
    //                    hitParticle.transform.position = landingPos;
    //                    hitParticle.Play(true);
    //                }
				//	if(trailParticle != null)
				//		trailParticle.Stop(true);
				//});
            
			

        }
		if(testAnim)
		{
			testAnim = false;
			PlayNozzleAnimation();
		}
		if(testReload)
		{
			testReload = false;
			int randomFood = Random.Range(0, SlimeDatabase.Food.Length);
			GameObject newFood = Instantiate(SlimeDatabase.Food[randomFood]);
			newFood.transform.position = foodStorageSpawnPoint.position;
			Rigidbody2D foodBody = newFood.GetComponent<Rigidbody2D>();
			foodBody.constraints = RigidbodyConstraints2D.None;
			foodBody.gravityScale = 1;
			GameManager._Instance.ourFoodStorage.Add(newFood.GetComponent<BaseFood>());
            if (GameManager._Instance.ourFood.Contains(newFood.GetComponent<BaseFood>()))
                GameManager._Instance.ourFood.Remove(newFood.GetComponent<BaseFood>());
            newFood.transform.localScale = new Vector3(0.4f, 0.4f, 1);
        }
		if(testFire)
		{
			testFire = false;
			if(GameManager._Instance.ourFoodStorage.Count > 0 && targetSlime != null)
			{
                targetPos = targetSlime.transform.position;
                Vector3 randomPoint = (Vector3)(Random.insideUnitCircle * fireLandRadius);

                BaseFood food = GameManager._Instance.ourFoodStorage[0];
                if (GameManager._Instance.ourFoodStorage.Contains(food))
                    GameManager._Instance.ourFoodStorage.Remove(food);
                if (!GameManager._Instance.ourFood.Contains(food))
                    GameManager._Instance.ourFood.Add(food);
                food.transform.position = fireSpawnPoint.position;
				food.transform.DOScale(1, fireTime);
				
				food.transform.DOMove(targetPos + randomPoint, fireTime, false)
                    .SetEase(fireEase)
                    .OnStart(()=> 
                    {
					    food.ResetFood();
                        if (fireParticle != null)
                            fireParticle.Play(true);
                        if (hitParticle != null)
                            hitParticle.transform.localPosition = Vector3.zero;
                    })
                    .OnComplete(()=>
				    {
                        if (hitParticle != null)
                        {
                            hitParticle.transform.position = targetPos + randomPoint;
                            hitParticle.Play(true);
                        }

				    });
			}
		}
	}
	void FireFood(BaseFood food, Vector3 targetPos)
	{
		if(food == null)
			return;
		if(GameManager._Instance.ourFoodStorage.Contains(food))
			GameManager._Instance.ourFoodStorage.Remove(food);
		if(!GameManager._Instance.ourFood.Contains(food))
			GameManager._Instance.ourFood.Add(food);
		Vector3 landingPos = targetPos + randomPoint;
		Vector3[] path = new Vector3[2] { fireSpawnPoint.position, landingPos };
		//food.transform.DOMove(landingPos, fireTime, false)
		food.transform.DOPath(path, fireTime, PathType.Linear, PathMode.Full3D, 10, Color.red)
			.SetSpeedBased(true)
			.SetDelay(fireDelay)
			.SetEase(fireEase)
			.OnStart(() =>
			{
				food.transform.position = fireSpawnPoint.position;
				food.transform.DOScale(1, fireTime).SetSpeedBased(true);
				food.ResetFood();
				if(fireParticle != null)
					fireParticle.Play(true);
				if(hitParticle != null)
					hitParticle.transform.localPosition = Vector3.zero;
				if(trailParticle != null)
				{
					ParticleSystem.MainModule main = trailParticle.main;
					main.loop = true;
					trailParticle.Play(true);
				}
			})
			.OnUpdate(() =>
			{
				if(trailParticle != null)
				{
					trailParticle.transform.position = food.transform.position;
				}
			})
			.OnComplete(() =>
			{
				if(hitParticle != null)
				{
					hitParticle.transform.position = landingPos;
					hitParticle.Play(true);
				}
				if(trailParticle != null)
				{
					ParticleSystem.MainModule main = trailParticle.main;
					main.loop = false;
					trailParticle.Stop(true);
				}
			});
	}
	Transform GetNextTarget()
	{
		Transform nextTarget = null;
		if(GameManager._Instance.ourSlimes.Count > 0)
		{
			for(int i = 0; i < GameManager._Instance.ourSlimes.Count; i++)
			{
				BaseSlime slime = GameManager._Instance.ourSlimes[i].GetComponent<BaseSlime>();
				float hunger = slime.slimeStats.currentHunger / slime.slimeStats.maxHunger;
				if(hunger < 0.6f)
				{
					nextTarget = slime.transform;
					break;
				}
			}
		}

		return nextTarget;
	}
	bool FoodCheck(Vector3 pos)
	{
		bool found = false;
		Collider2D[] food = Physics2D.OverlapCircleAll(pos, foodCheckRadius, whatIsFood);
		if(food.Length > 0)
			found = true;
		return found;
	}
	void PlayNozzleAnimation()
	{
		pivotNozzle.DOScale(animScale0, animTime0).SetEase(animEase0).SetDelay(animDelay0);
		pivotNozzle.DOScale(animScale1, animTime1).SetEase(animEase1).SetDelay(animDelay1);
		pivotNozzle.DOScale(animScale2, animTime2).SetEase(animEase2).SetDelay(animDelay2);
	}
	private void OnDrawGizmosSelected()
	{
		if(lastTarget != null)
		{
			Gizmos.color = Color.green;
			if(FoodCheck(lastTarget.position))
				Gizmos.color = Color.red;
			Gizmos.DrawWireSphere(lastTarget.position, foodCheckRadius);

		}
	}
}
