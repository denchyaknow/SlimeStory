﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PoopCleaner : BaseItem {

    SpriteRenderer sprite;
    bool canMove = true;
    bool canClear = false;
    float lastX = 0;

    private void Start() {
        lastX = this.transform.position.x;
        sprite = this.gameObject.GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame 
    void Update() {
        if (GameManager._Instance.ourPoop.Count > 0) {
            MoveToPoop();
        } else {
            if (canMove == true) {
                RandomPoint();
            }
        }
    }

    public void RandomPoint() {
        canMove = false;
        Vector3 screenPosition = Camera.main.ScreenToWorldPoint(new Vector3(Random.Range(0, Screen.width), Random.Range(0, Screen.height), Camera.main.farClipPlane / 2));
        float currX = screenPosition.x;
        if (lastX > currX) {
            sprite.flipX = true;
        } else {
            sprite.flipX = false;
        }
        this.transform.DOMove(screenPosition, 3, false).OnComplete(() => { canMove = true; });
        lastX = currX;
    }

    public void MoveToPoop() {
        this.gameObject.transform.DOMove(new Vector3(GameManager._Instance.ourPoop[0].transform.position.x, GameManager._Instance.ourPoop[0].transform.position.y, 0), 2, false);
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.tag == "Poop") {
            GameManager._Instance.ourPoop.Remove(collision.transform);
            Destroy(collision.gameObject);
        }
    }
} 
