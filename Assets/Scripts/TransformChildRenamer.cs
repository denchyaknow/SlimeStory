﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformChildRenamer : MonoBehaviour {
    public Transform targetTransform;// Can be null, if null this.Transform.chilren will be renamed
    public string prefix;
    public string suffix;
    public bool numbered = true;
    [Header("Click to rename")]
    public bool RenameAllChildren = false;
    public bool DeletePrefix = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}
    private void OnDrawGizmos()
    {
        if(RenameAllChildren)
        {
            RenameAllChildren = false;
            Debug.Log("TransformChildRenamer: Renamed children");
            if (targetTransform == null)
                targetTransform = transform;
            for (int i = 0; i < targetTransform.childCount; i++)
            {
                string preName = targetTransform.GetChild(i).gameObject.name;
                string finalName = string.Empty;
                if(numbered)
                {
                    string sub = preName.ToString().Substring(0, 3);
                    Debug.Log(sub);
                    //if (sub.ToCharArray()[0] == "0".ToCharArray()[0])
                      //  preName = preName.Remove(0, 3 + prefix.Length);
                     if (!DeletePrefix)
                    {
                        finalName += i.ToString("D3");

                    }
                    else
                    {
                        int extraCount = preName.ToCharArray().Length > 3 ? 3 : 0;
                        for (int j = 0; j < (preName.ToCharArray().Length - (extraCount + prefix.ToCharArray().Length)); j++)
                        {
                            string compare = preName.Substring(j, (extraCount + prefix.ToCharArray().Length));
                            if (compare == (i.ToString("D3") + prefix))
                            {
                                preName = preName.Remove(j, (extraCount + prefix.ToCharArray().Length));
                                targetTransform.GetChild(i).gameObject.name = preName;

                                break;
                            } else if (preName.Substring(j, (prefix.ToCharArray().Length)) == prefix)
                            {
                                preName = preName.Remove(j, (prefix.ToCharArray().Length));
                                targetTransform.GetChild(i).gameObject.name = preName;
                                break;
                            }
                            
                        }
                    }
                    
                  
                }
                if(!DeletePrefix)
                {

                finalName += prefix;
                finalName += preName;
                finalName += suffix;
                targetTransform.GetChild(i).gameObject.name = finalName;
                }
            }

        }
    }
}
