﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class BaseItem : BaseObject
{
	
	public enum ItemType
	{
		unset,
		coin,
		poop
	}

	public Vector3 itemScale = new Vector3(1,1,0);
	public int itemValue = 1;
	public ItemType itemType;

	//settings for the path from item position to endpos on click
	public float itemPathSpeed = 15f;
	public Ease itemPathEase;

	//settings for the idle animation of item when staying still
	public float itemAnimSpeed = 15;
	public Ease itemAnimEase;


	private bool itemAnimPlay = false;
	public bool itemSelected = false;
	private float timeSpawned;
	public override void Awake()
	{
		base.Awake();
		myRenderer = transform.GetComponentInChildren<SpriteRenderer>();
		itemScale = myRenderer.transform.localScale;
		timeSpawned = Time.timeSinceLevelLoad;
		
	}
	public override void Start()
	{
		base.Start();
	}
	public override void Update()
	{
		base.Update();


		//if(!itemAnimPlay && !itemSelected)
		//{
		//	itemAnimPlay = true;
		//	switch(itemType)
		//	{
		//		case ItemType.coin:
		//			if(Time.timeSinceLevelLoad - timeSpawned > 20)
		//				isSelected = true;
				
		//			break;
		//	}
			
		//}
		//if (isSelected && !itemSelected)
		//{
		//	//When OnTouchDown
		//	itemSelected = true;
		//	transform.DOPause();
		//	switch (itemType)
		//	{
		//		case ItemType.coin:

		//			myRenderer.transform.DOScale(itemScale, itemAnimSpeed).SetEase(itemAnimEase).SetSpeedBased(true);
		//			GameManager._Instance.playerData.coinCount += itemValue;
		//			Vector3 endPos = Camera.main.ScreenToWorldPoint(new Vector3(0, Camera.main.pixelHeight, 0));
		//			Vector3[] coinPath = new Vector3[] { transform.position, (Random.insideUnitCircle * 2), endPos };
		//			transform.DOPath(coinPath, itemPathSpeed, PathType.CatmullRom, PathMode.Full3D, 10, Color.red).SetEase(itemPathEase).SetSpeedBased(true).OnComplete(() =>
		//			{
		//				DestroyItem();
		//			});
		//			break;
		//		case ItemType.poop:
		//			GameManager._Instance.playerData.coinCount += itemValue;

		//			myRenderer.transform.DOScale(1.4f, itemAnimSpeed).SetEase(itemAnimEase).OnStart(() =>
		//			{
		//				myRenderer.DOFade(0, itemAnimSpeed).SetEase(itemAnimEase);
		//			}).OnComplete(()=>
		//			{
  //                      GameManager._Instance.ourPoop.Remove(transform);
		//				DestroyItem();
		//			});

		//			break;
		//	}
		//}
	}
	public override void TouchFunction(int _id, Vector3 _currentPos)
	{
		base.TouchFunction(_id, _currentPos);
	}
	public void DestroyItem()
	{
		Destroy(gameObject);
	}
}
