﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class BasePoop :BaseItem
{
	public Transform poopSprites;

	public PoopData poopData = new PoopData();
	public static ParticleSystem _InstancePoopParticles;
	public ParticleSystem poopParticle
	{
		get
		{
			if (_InstancePoopParticles == null)
			{
				GameObject poop = (GameObject)Instantiate(Resources.Load("Effects/PoopParticles"));
				poop.transform.SetParent(GameManager._Instance.garbageCan);
				_InstancePoopParticles = poop.GetComponent<ParticleSystem>();
			}
			return _InstancePoopParticles;
		}
	}

	public override void Awake()
	{
		base.Awake();
	}
	public override void Start()
	{
		base.Start();

	}
	public override void Update()
	{
		base.Update();
		if (isSelected && !itemSelected)
		{
			itemSelected = true;

			if (coinParticle != null)
			{
				Vector3 pos = transform.position;
				coinParticle.transform.position = pos;
				coinParticle.Emit(Random.Range(8, 16));
			}
			GameManager._Instance.GameAddCoin(itemValue);
			GameManager._Instance.uiManager.LaunchCounter(transform.position, itemValue, Color.black, "$");
			foreach (ParticleSystem sys in transform.GetComponentsInChildren<ParticleSystem>())
			{
				sys.Stop();
			}
			foreach (SpriteRenderer sprite in transform.GetComponentsInChildren<SpriteRenderer>())
			{
				sprite.DOFade(0, itemAnimSpeed);
			}
			myRenderer.transform.DOScale(1.4f, itemAnimSpeed).SetEase(itemAnimEase).OnComplete(() =>
					{
						GameManager._Instance.ourPoop.Remove(transform);
						DestroyItem();
					});
		}
	}
	public void Setup(int classType)
	{
		if (poopSprites != null)
		{
			poopSprites.GetChild(classType).gameObject.SetActive(true);
		}
	}
	public void Poop()//Just a method to emit poop particles
	{
		if (poopParticle != null)
		{
			Vector3 pos = transform.position;
			poopParticle.transform.position = pos;
			poopParticle.Emit(Random.Range(6, 10));
		}
	}
}
