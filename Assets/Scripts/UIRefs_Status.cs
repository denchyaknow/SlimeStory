﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIRefs_Status : MonoBehaviour
{

	public RectTransform rootMenuPivot;
	public Image curImage;
	public Text curName;
	public Text curLvl;
	public Text curHealth;
	public Image curHealthCircle;
	public Text curHappiness;
	public Image curHappinessCircle;
	public Text curHunger;
	public Image curHungerCircle;
	public Text curStrength;
	public Text curAgility;
	public Text curMagic;
	public Text curSpirit;
	public Text curDefense;
	public UIRadarStats playerStats;
	public Button openButton;
	public Button closeButton;
	public BaseHelpTipText tipStr;
	public BaseHelpTipText tipMag;
	public BaseHelpTipText tipDef;
	public BaseHelpTipText tipAgi;
	public BaseHelpTipText tipSpi;

}
