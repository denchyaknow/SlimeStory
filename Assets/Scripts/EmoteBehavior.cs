﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmoteBehavior : StateMachineBehaviour {

	private BaseSlime slime;
	//public BaseSlime.Mood mood;
	// OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		if(slime == null)
			slime = animator.GetComponent<BaseSlime>();
		float hapPerc = slime.slimeStats.GetHappinessPercent();
		if (hapPerc < 0.31f)
			slime.TakeDamage((int)(slime.slimeStats.maxHealth * 0.005f));
		else if (hapPerc > 0.69f)
			slime.HealDamage((int)(slime.slimeStats.maxHealth * 0.005f));
		//slime.EmoteBehavior(mood);
	}

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	//override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
