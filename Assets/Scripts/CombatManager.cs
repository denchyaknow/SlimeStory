﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Denchyaknow;
public class CombatManager : MonoBehaviour
{
	public delegate void Delegate();
	public Delegate OnCurrentMapChange;
	//PlayerSlimes
	public BaseSlime playerSlime;
	//public List<BaseSlime> friendlySlimes = new List<BaseSlime>();

	//Slimes we need to kill
	//public BaseSlime enemyTarget;
	public List<UniMob_Base> enemies = new List<UniMob_Base>();

	public bool autoAttacks = false;
	public bool autoMoves = false;
	//public BaseMap currentMap;
	//public List<Transform> currentMapTiles = new List<Transform>();

	public CameraManager camera
	{
		get
		{
			if (cm == null)
				cm = Camera.main.GetComponent<CameraManager>();
			return cm;
		}
	}
	private CameraManager cm;
	public int tiledMapCount = 0;
	public bool poolMapTiles = false;
	private BaseMap tiledMapLeft;
	private BaseMap tiledMapCurrent;
	private BaseMap tiledMapRight;
	private bool tiledMapSpawning = false;


	public int[] mobsToSpawn = new int[2];
	[Range(0.0f, 1.0f)]
	public float mobSpawnChance = 0.8f;
	public float mobSpawnRate = 0.5f;
	public int mobSpawnDistance = 5;
	public int mobGroupSize = 1;
	private float lastMobSpawnTime;
	private int lastMobSpawnDistance;

	public int difficulty = 0;
	public int difficultyRate = 20;
	private int difficultyCache = 0;
	private int travelDistance = 0;
	private int travelDistanceCache = 0;
	private Vector3 travelStartPos = new Vector3();
	private Transform[] mobSpawnPoints;
	private int[] mobCounts;
	public ParticleSystem deathParticles;
	public int mapLevelCurrent = 0;
	public int mapLevel
	{
		get
		{
			int lvl = 0;
			if (GameManager._Instance != null)
				lvl = GameManager._Instance.playerData.combatMapIndex;
			return lvl;
		}
		set
		{
			GameManager._Instance.playerData.combatMapIndex = value;
		}
	}

	private List<UniMob_Base> mobsToDestroy = new List<UniMob_Base>();
	private float mobDestroyDelay = 20f;
	private float mobLastDestroyTime = 0;
	[Header("Combat Movement")]
	public float defaultSpeed = 3f;
	public Vector2 playerDirection
	{
		get
		{
			Vector2 dir = new Vector2();
			dir = GameManager._Instance.inputManager.tPosSwipeLeftDir;
			return dir;
		}
	}
	private void OnEnable()
	{
		if (GameManager._Instance.OnOneShotDisable != null)
			GameManager._Instance.OnOneShotDisable();
		GameManager._Instance.combatManager = this;
		CombatProperties._Instance = transform.GetComponent<CombatProperties>();
		GameManager._Instance.gamePaused = true;
		GameManager._Instance.soundManager.PlayBackgroundMusic();
		GameManager._Instance.uiManager.UpdateCombatMap();
		GameManager._Instance.inputManager.SetControlScheme();
		GameManager._Instance.inputManager.touchJoyStickLeft.gameObject.SetActive(true);
		SpawnFirstTiledMap();
		mapLevelCurrent = mapLevel;
		
		playerSlime = GameManager._Instance.GameLoadSelectedSlime();

		playerSlime.combatMode = true;
		playerSlime.transform.position = GetSpawnStartPoint(true);
		GameManager._Instance.cameraManager.StartFollowing();
		GameManager._Instance.gamePaused = false;
		//GameManager._Instance.OnOneShotDisable += GameManager._Instance.GameSave;
		GameManager._Instance.OnOneShotDisable += GameManager._Instance.GameUnload;
		GameManager._Instance.OnOneShotDisable += GameManager._Instance.cameraManager.StopFollowing;
		GameManager._Instance.OnOneShotDisable += GameManager._Instance.inputManager.PrepForNextScene;
	}
	private void OnDisable()
	{

	}
	IEnumerator Start()
	{
		if (LoadingScreen._Instance != null)
		{
			while (!LoadingScreen._Instance.isDone)
			{
				yield return new WaitForSeconds(0.1f);

			}
		}
		GameManager._Instance.uiManager.UpdateCombatMap();

		StartMobSpawner();
	}
	private void LateUpdate()
	{
		if (enemies.Count > 1 && enemies[0] != null)
		{
			enemies.Sort((e1, e2) => Vector3.Distance(e1.transform.position, playerSlime.transform.position).CompareTo(Vector3.Distance(e2.transform.position, playerSlime.transform.position)));
		
		}
		if (mobsToDestroy.Count > 0)
		{
			if (Time.timeSinceLevelLoad < mobLastDestroyTime)
				return;
			mobLastDestroyTime = Time.timeSinceLevelLoad + mobDestroyDelay;
			UniMob_Base mob = mobsToDestroy[0];
			mobsToDestroy.Remove(mob);
			mob.gameObject.SetActive(false);
			
		}
	}
	void Update()
	{
		if (!LoadingScreen._Instance.isDone)
		{
			Debug.Log("WaitingForLoadingScreen");
			return;
		}
		if (playerSlime == null)
			return;
		bool enemiesOnScreen = (enemies.Count > 0 ? true : false);
		if (enemiesOnScreen)
		{
			camera.StopFollowing();
		}
		else
		{
			camera.StartFollowing();
		}
		UpdateTiledMaps();
		UpdateMobSpawner();
		if (!playerSlime.isDead)
		{
			if (autoMoves)
			{
				if (enemiesOnScreen)
				{
					float dis = Vector3.Distance(playerSlime.transform.position, enemies[0].transform.position);
					if (dis * 1.3f > playerSlime.slimeStats.attackDistance)
					{
						Vector3 dir = (enemies[0].transform.position - playerSlime.transform.position).normalized;
						playerSlime.MoveBody(dir);
					}
				}
				else
				{
					playerSlime.MoveBody(Vector2.right + new Vector2(0,Random.Range(-1f,1f)));
				}
				//if (playerSlime.speed > 0.2f)
				//{
				//	playerSlime.ChangeMoveDirection(Time.deltaTime);
				//	playerSlime.PlayMoveAnimation(playerSlime.speed);
				//}
				//else
				//{
				//	playerSlime.PlayIdleAnimation();
				//}
			}
			if (autoAttacks && enemiesOnScreen)
			{
				float dis = Vector3.Distance(playerSlime.transform.position, enemies[0].transform.position);
				if (dis < playerSlime.slimeStats.attackDistance)
				{
					playerSlime.Attack(enemies[0].transform.position, enemies[0].transform);
				}
			}
		}
	}
	#region Hit Damage Logic
	public enum DamageFormula
	{
		Enemy_Basic,
		Player_Basic,
		Physical_Tanky,
		Physical_Rapid,
		Physical_Burst,
		Physical_Long,
		Physical_Multi,
		Magic_Short,
		Magic_Long,
		Magic_Buff
	}
	//def = opposite owner of owner of atk,lvl,str,spd ect
	public int GetDamageTaken(int _atk, int _def, int _str, int _mag, int _spd, int _spr, int _lvl, DamageFormula _form)
	{
		float baseDamage = 0;
		float bonusDamage = 0;
		int totalDamage = 0;
		switch (_form)
		{
			case DamageFormula.Enemy_Basic:
				baseDamage = (_atk - _def);
				bonusDamage = (_str + Random.Range(0f, ((float)(_lvl + _str) / 4f) + 1.0f));
				totalDamage = Mathf.CeilToInt(baseDamage * bonusDamage);
				break;
			case DamageFormula.Player_Basic:
				baseDamage = (_atk - _def);
				bonusDamage = (_str + Random.Range(0f, ((float)(_lvl + _str) / 8f) + 1.0f));
				totalDamage = Mathf.CeilToInt(baseDamage * bonusDamage);
				break;
			case DamageFormula.Physical_Tanky:
				baseDamage = ((_atk + _lvl) - _def);
				bonusDamage = (_str + Random.Range(0f, ((float)(_lvl + _str) / 8f) + 1.0f));
				totalDamage = Mathf.CeilToInt(baseDamage * bonusDamage);
				break;
			case DamageFormula.Physical_Rapid:
				baseDamage = (_atk + _lvl);
				bonusDamage = ((_str + _spr) / 2f) + (Random.Range(0f, ((float)(_lvl + _str) / 8f) + 1.0f));
				totalDamage = Mathf.CeilToInt(baseDamage * bonusDamage);
				break;
			case DamageFormula.Physical_Burst:
				baseDamage = ((_atk + _lvl) - _def);
				bonusDamage = (Random.Range(0f, (1.0f + _str - 1.0f)) + 1.0f) + (Random.Range(0, (_lvl + _str) / 8f));
				totalDamage = Mathf.CeilToInt(baseDamage * bonusDamage);
				break;
			case DamageFormula.Physical_Long:
				baseDamage = _atk - _def;
				bonusDamage = ((float)((_str + _spd) / 2f)) + (Random.Range(0f, ((float)(_lvl + _str) / 8f) + 1.0f));
				totalDamage = Mathf.CeilToInt(baseDamage * bonusDamage);
				break;
			case DamageFormula.Physical_Multi://divided by targets.length
				baseDamage = (_atk - _def);
				bonusDamage = ((Random.Range(0f, (1.0f + _str - 3.0f))) + 3.0f) + (Random.Range(0f, (_lvl + _str) / 8f));
				totalDamage = Mathf.CeilToInt(baseDamage * (bonusDamage * 1.5f));
				break;
			case DamageFormula.Magic_Short://based off summon magic ffix
				baseDamage = (_atk - _def);
				bonusDamage = (_mag + (Random.Range(0, (_lvl + _mag) / 8f) + 1.0f));
				totalDamage = Mathf.CeilToInt(baseDamage * bonusDamage);
				break;
			case DamageFormula.Magic_Long://divided by half if aoe
				baseDamage = (_atk - _def);
				bonusDamage = (_mag + (Random.Range(0, (_lvl + _mag) / 8f) + 1.0f));
				totalDamage = Mathf.CeilToInt(baseDamage * bonusDamage);
				break;
			case DamageFormula.Magic_Buff:
				baseDamage = _atk;
				bonusDamage = (_mag + (Random.Range(0, (_lvl + _mag) / 8f) + 1.0f));
				totalDamage = Mathf.CeilToInt(baseDamage * bonusDamage);// bonus is * 1.5 if happy?
				break;
		}
		bool crit = Random.Range(0, (_spr / 4)) > Random.Range(0, 100);
		return totalDamage;
	}
	
	#endregion
	public void AddExperience(UniMob_Base mobData)
	{
		playerSlime.AddExperience(mobData.mobExp);
	}

	public Vector3 GetSpawnStartPoint(bool _friendly)
	{
		Vector2 screenCenter = (new Vector2(Camera.main.pixelWidth / 2, Camera.main.pixelHeight / 2));
		Vector2 spawnPoint;
		Vector3 tempPoint;
		if (_friendly)
		{
			spawnPoint = new Vector2(
				 screenCenter.x -
				 (Camera.main.pixelWidth + UnityEngine.Random.Range(-Camera.main.pixelWidth / 4, Camera.main.pixelWidth / 4)),
				 screenCenter.y +
				 (UnityEngine.Random.Range(-Camera.main.pixelHeight / 4, Camera.main.pixelHeight / 4)));
		}
		else
		{
			spawnPoint = new Vector2(
				 screenCenter.x +
				 (Camera.main.pixelWidth + UnityEngine.Random.Range(-Camera.main.pixelWidth / 4, Camera.main.pixelWidth / 4)),
				 screenCenter.y +
				 (UnityEngine.Random.Range(-Camera.main.pixelHeight / 4, Camera.main.pixelHeight / 4)));
		}
		tempPoint = Camera.main.ScreenToWorldPoint(spawnPoint);
		tempPoint.z = 0;
		return tempPoint;
	}
	
	//Infinite map logic
	void UpdateTiledMaps()
	{
		Vector2 cameraPos = camera.transform.position;
		Vector2 tiledMapPos = tiledMapCurrent.transform.position;
		bool rightOfMap = cameraPos.x > tiledMapPos.x;
		bool leftOfMap = cameraPos.x < tiledMapPos.x;
		if (rightOfMap && !tiledMapSpawning && tiledMapRight == null)
		{
			tiledMapSpawning = true;
			SpawnNextTiledMap(1);
		}
		else if (leftOfMap && !tiledMapSpawning && tiledMapLeft == null)
		{
			tiledMapSpawning = true;
			SpawnNextTiledMap(-1);
		}
		Vector2 tiledMapChangePos = tiledMapPos;
		bool changeMap = false;
		if (rightOfMap)//get the point at which to transition maps
		{
			tiledMapChangePos.x += tiledMapCurrent.playerStartDistance;
			changeMap = (cameraPos.x > tiledMapChangePos.x);
		}
		else if (leftOfMap)
		{
			tiledMapChangePos.x -= tiledMapCurrent.playerStartDistance;
			changeMap = (cameraPos.x < tiledMapChangePos.x);
		}
		//transition maps when point is reached
		if (rightOfMap && changeMap && tiledMapRight != null)
		{
			tiledMapLeft = tiledMapCurrent;
			tiledMapCurrent = tiledMapRight;
			tiledMapRight = null;
		}
		else if (leftOfMap && changeMap && tiledMapLeft != null)
		{
			tiledMapRight = tiledMapCurrent;
			tiledMapCurrent = tiledMapLeft;
			tiledMapLeft = null;
		}
	}
	public void SpawnFirstTiledMap()
	{
		tiledMapCount = 0;
		int zoneIndex = mapLevel;//what zone to spawn?
		GameObject newMap = Instantiate(SlimeDatabase.Maps[zoneIndex]);
		tiledMapCurrent = newMap.GetComponent<BaseMap>();
		Vector3 newPos = camera.transform.position;
		newPos.z = 0;
		newMap.transform.position = newPos;
	}
	public void SpawnNextTiledMap(int dir)
	{
		BaseMap nextMap = null;
		int zoneIndex = mapLevel;//what zone to spawn?
		if (dir == 1)
		{
			if (tiledMapLeft != null && !poolMapTiles)//Can we recycle the last map that was before the current map?
			{
				nextMap = tiledMapLeft;
				tiledMapLeft = null;
			}
			else
			{
				if (poolMapTiles && tiledMapLeft != null)
					Destroy(tiledMapLeft.gameObject);
				GameObject newMap = Instantiate(SlimeDatabase.Maps[zoneIndex]);
				nextMap = newMap.GetComponent<BaseMap>();
			}
			tiledMapRight = nextMap;
		}
		else if (dir == -1)
		{
			if (tiledMapRight != null && !poolMapTiles)//Can we recycle the last map that was before the current map?
			{
				nextMap = tiledMapRight;
				tiledMapRight = null;
			}
			else
			{
				if (poolMapTiles && tiledMapRight != null)
					Destroy(tiledMapRight.gameObject);
				GameObject newMap = Instantiate(SlimeDatabase.Maps[zoneIndex]);
				nextMap = newMap.GetComponent<BaseMap>();
			}
			tiledMapLeft = nextMap;
		}
		nextMap.ConnectToMap(tiledMapCurrent, dir);
		tiledMapCount++;
		tiledMapSpawning = false;
	}
	private void StartMobSpawner()
	{
		travelStartPos = camera.transform.position;
		mobSpawnPoints = camera.GetEnemySpawnPoints();
		int totalMobs = GameManager._Instance.database.Enemies.Length;
		mobCounts = new int[totalMobs];
		for (int i = 0; i < mobCounts.Length; i++)
		{
			mobCounts[i] = i;
		}
		difficulty = GameManager._Instance.database.ZoneLvlRequirements[mapLevel];
	}
	private void UpdateMobSpawner()
	{
		travelDistance = Mathf.CeilToInt(camera.transform.position.x - travelStartPos.x);
		if (enemies.Count > 50)
			return;
		if (travelDistance > travelDistanceCache)//if travel distance changes
		{
			travelDistanceCache = travelDistance;
			difficultyCache++;
		}
		int currentDis = travelDistance - lastMobSpawnDistance;
		if (currentDis >= mobSpawnDistance)
		{
			lastMobSpawnDistance = travelDistance;
			StartCoroutine(SpawnAMobGroup());
		}
		if (lastMobSpawnTime < Time.timeSinceLevelLoad)
		{
			lastMobSpawnTime = Time.timeSinceLevelLoad + mobSpawnRate;
			StartCoroutine(SpawnAMobGroup());
		}
	}
	Vector3 GetRandomSpawnPoint()
	{
		Vector3 mobSpawnPoint = Vector3.zero;
		if (mobSpawnPoints != null && mobSpawnPoints.Length > 0)
		{
			Transform point = mobSpawnPoints[Random.Range(0, mobSpawnPoints.Length - 1)];
			mobSpawnPoint = point.position;
			mobSpawnPoint.z = 0;
		}
		return mobSpawnPoint;
	}

	private int GetRandomMobID()
	{
		int max = GetHardestMobID();
		int mobID = Random.Range(0, max +1);
		int tryCount = 0;
		int maxTry = 50;
		if (mobsToSpawn != null && mobsToSpawn.Length > 0)
		{
			while (mobCounts[mobID] > 0)
			{
				tryCount++;
				mobID = Random.Range(0, max);
				if (tryCount > maxTry)
				{
					Debug.Log("Memory Leak Warning! GetRandomMobID failed to get a mob ID");
					break;
				}
			}
			for (int i = 0; i < mobCounts.Length; i++)
				mobCounts[i] -= mapLevelCurrent +1;
			
			mobCounts[mobID] = mobID;
		}
		return mobID;
	}
	private int GetHardestMobID()
	{
		int mobCount = GameManager._Instance.database.Enemies.Length;
		int mod = 8;
		if (mod >= mobCount)
			mod = mobCount;
		int id = difficulty;
		id %= mobCount;
		id %= mod;
		return id;
	}

	public void SpawnAMob()
	{
		int mobID = GetRandomMobID();
		Vector3 mobSpawnPoint = GetRandomSpawnPoint();
		
		float chance = Random.Range(0, 100) / 100f;
		if (chance > mobSpawnChance)
		{
			if (GameManager._Instance.database != null)
			{
				if (GameManager._Instance.database.Enemies.Length > 0)
				{
					GameObject newMob = Instantiate(GameManager._Instance.database.Enemies[mobID], null);
					UniMob_Base mob = newMob.GetComponent<UniMob_Base>();
					newMob.transform.SetParent(null);
					newMob.transform.position = mobSpawnPoint;
					//int lvl = GameManager._Instance.database.ZoneLvlRequirements[mapLevelCurrent];
					//lvl = difficulty % lvl;
					mob.mobLevel += Mathf.FloorToInt(difficulty * 0.1f);
					if (mob.mobLevel == 0)
						mob.mobLevel = 1;
					mob.SetStats();
					GameManager._Instance.combatManager.enemies.Add(mob);

				}
			}
		}

	}
	IEnumerator SpawnAMobGroup()
	{
		for (int i = 0; i < mobGroupSize; i++)
		{
			yield return new WaitForSeconds(0.1f);
			SpawnAMob();
		}
	}
	public void SpawnABossMob()
	{
		int mobID = GetRandomMobID();
		Vector3 mobSpawnPoint = GetRandomSpawnPoint();

	}
	public void KillAMob(UniMob_Base mob)
	{
		mob.DisablePhysics();
		
		difficulty++;
		bool detonate = false;
		bool playing = true;
		Vector2 dir = (mob.transform.position - playerSlime.transform.position).normalized;
		float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
		Vector3 rot = (Quaternion.AngleAxis(angle, Vector3.forward)).eulerAngles;
		mob.mob_SpriteRenderer.transform.DOLocalRotate(new Vector3(0, 0, 1500), 2f, RotateMode.FastBeyond360).OnUpdate(() =>
		{
			mob.mob_Rigidbody.velocity = dir * 16f;
			Vector3 top = new Vector3(Camera.main.pixelWidth, Camera.main.pixelHeight, 0);
			Vector3 bot = Vector3.zero;
			Vector3 camTR = Camera.main.ScreenToWorldPoint(top);
			Vector3 camBL = Camera.main.ScreenToWorldPoint(bot);
			if (mob.transform.position.y > camTR.y || mob.transform.position.y < camBL.y || mob.transform.position.x < camBL.x || mob.transform.position.x > camTR.x)
				detonate = true;

			if (detonate && playing)
			{
				playing = false;
				mob.DisableRenderers();
				mob.DisableTweeners();
				GameManager._Instance.soundManager.PlayMobDeathExplode();
				deathParticles.transform.SetParent(GameManager._Instance.garbageCan);
				deathParticles.transform.position = mob.transform.position;
				deathParticles.transform.eulerAngles = rot;
				deathParticles.Emit(15);
			}
		}).OnComplete(()=>
		{
			mob.mob_SpriteRenderer.transform.localEulerAngles = Vector3.zero;
			if (mobsToDestroy.Count == 0)
				mobLastDestroyTime = Time.timeSinceLevelLoad + mobDestroyDelay;
			mobsToDestroy.Add(mob);
		});
		GameManager._Instance.uiManager.UpdateCombatMap();
	}
	
	//Infinite map logic

}

