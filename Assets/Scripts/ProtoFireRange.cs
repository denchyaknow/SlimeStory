﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class ProtoFireRange : MonoBehaviour
{
    public float returnSpeed = 0.8f;
	public float fireRate = 0.5f;
	public float mobSpacing = 1.5f;
    public float mobsAutoDelay = 0.5f;
    public float slimesAutoDelay = 0.5f;
    public bool mobsAutoAttack = false;
    public bool slimesAutoAttack = true;
	public bool columnByColumn = false;
    public Transform slimePivot;
    public Transform mobPivot;
    private List<GameObject> allSlimes = new List<GameObject>();
    private List<GameObject> allMobs = new List<GameObject>();
    private List<Vector3> startPos = new List<Vector3>();
    private List<Vector3> mobStartPos = new List<Vector3>();
    private bool mobsAutoAttacking = false;
    private bool slimesAutoAttacking = false;
	private BaseMob currentMob;
	private BaseSlime currentSlime;
	// Use this for initialization
	void Awake()
    {
        for (int i = 0; i < SlimeDatabase.FriendlySlimes.Length; i++)
        {
            GameObject newSlime = Instantiate(SlimeDatabase.FriendlySlimes[i], slimePivot,true);
            allSlimes.Add(newSlime);
            newSlime.transform.localPosition = new Vector3(0, i * mobSpacing, 0);
            startPos.Add(newSlime.transform.position);
            BaseSlime slime = newSlime.transform.GetComponent<BaseSlime>();
            slime.Awake();
            slime.combatMode = true;
            int mobIndex = i;
            if (mobIndex >= SlimeDatabase.EnemyMobs.Length)
            {
                mobIndex = 0;
            }
            GameObject newMob = Instantiate(SlimeDatabase.EnemyMobs[mobIndex], mobPivot,true);
            allMobs.Add(newMob);
            newMob.transform.localPosition = new Vector3(0, i * mobSpacing, 0);
            mobStartPos.Add(newMob.transform.position);

            BaseMob mob = newMob.transform.GetComponent<BaseMob>();
            mob.Awake();
        }
    }
    public void LateUpdate()
    {
        for(int i = 0; i < allSlimes.Count; i++)
        {
            Vector3 pos = startPos[i];
                Rigidbody2D mobBody = allSlimes[i].transform.GetComponent<Rigidbody2D>();
                int id = mobBody.gameObject.GetInstanceID();
            if (!allSlimes[i].GetComponent<BaseSlime>().isKnockedBack && allSlimes[i].transform.position != pos && allSlimes[i].GetComponent<BaseSlime>().isMoving() == false && allSlimes[i].GetComponent<BaseSlime>().isAttacking == false)
            {
                if (!DOTween.IsTweening(id))
                    mobBody.DOMove(pos, returnSpeed, false).SetSpeedBased(true).SetId(id);
            } else if((allSlimes[i].GetComponent<BaseSlime>().isAttacking == true || allSlimes[i].GetComponent<BaseSlime>().isKnockedBack) && DOTween.IsTweening(id))
            {
                DOTween.Kill(id);
            }
        }
        for (int i = 0; i < allSlimes.Count; i++)
        {
            Vector3 pos = mobStartPos[i];
            Rigidbody2D mobBody = allMobs[i].transform.GetComponent<Rigidbody2D>();
            int id = mobBody.gameObject.GetInstanceID();
            if (!allMobs[i].GetComponent<BaseMob>().isKnockedBack && allMobs[i].transform.position != pos && allMobs[i].GetComponent<BaseMob>().isMoving() == false && allMobs[i].GetComponent<BaseMob>().isAttacking == false)
            {
                if (!DOTween.IsTweening(id))
                    mobBody.DOMove(pos, returnSpeed, false).SetSpeedBased(true).SetId(id);
            }
            else if ((allMobs[i].GetComponent<BaseMob>().isAttacking == true || allMobs[i].GetComponent<BaseMob>().isKnockedBack) && DOTween.IsTweening(id))
            {
                DOTween.Kill(id);
            }
        }
        if (!insideCoro)
        {
            insideCoro = true;
			if(columnByColumn)
				StartCoroutine(DanceAll());
			else
				StartCoroutine(Dance());
        }
    }
    private int slimeIndex = 0;
    private int mobIndex = 0;
    private bool insideCoro = false;
    IEnumerator Dance()
    {
		for(int i = 0; i < allSlimes.Count; i++)
		{
			int index = i;
			BaseSlime slime = allSlimes[index].transform.GetComponent<BaseSlime>();
			BaseMob mob = allMobs[index].transform.GetComponent<BaseMob>();
			while(slime.slimeStats.currentHealth < (slime.slimeStats.maxHealth * 0.6f))
			{
				slime.slimeStats.currentHealth += 10;
				GameManager._Instance.uiManager.LaunchCounter(slime.transform.position, 10, Color.green);
				yield return new WaitForSeconds(0.1f);
			}
			while(mob.mobStats.healthCurrent < (mob.mobStats.healthMax * 0.6f))
			{
				mob.mobStats.healthCurrent += 10;
				GameManager._Instance.uiManager.LaunchCounter(mob.transform.position, 10, Color.green);
				yield return new WaitForSeconds(0.1f);
			}
		}
		if (mobIndex > allMobs.Count - 1)
            mobIndex = 0;
        if (slimeIndex > allSlimes.Count - 1)
            slimeIndex = 0;
        currentMob = allMobs[mobIndex].GetComponent<BaseMob>();
        currentSlime = allSlimes[slimeIndex].GetComponent<BaseSlime>();
        if(slimesAutoAttack)
        {

        yield return new WaitForSeconds(slimesAutoDelay);
        currentSlime.Attack(currentMob.transform.position);
        }
        if(mobsAutoAttack)
        {

        yield return new WaitForSeconds(mobsAutoDelay);
        }
        currentMob.Attack(currentSlime.transform.position);
        mobIndex++;
		slimeIndex++;
		yield return new WaitForSeconds(fireRate);
		insideCoro = false;
    }
	IEnumerator DanceAll()
	{
        int index = 0;

        for (int i = 0; i < allSlimes.Count; i++)
		{
			index = i;
			BaseSlime slime = allSlimes[index].transform.GetComponent<BaseSlime>();
			BaseMob mob = allMobs[index].transform.GetComponent<BaseMob>();
			while(slime.slimeStats.currentHealth < (slime.slimeStats.maxHealth * 0.3f))
			{
				slime.slimeStats.currentHealth += 10;
				GameManager._Instance.uiManager.LaunchCounter(slime.transform.position, 10, Color.green);
				yield return new WaitForSeconds(0.1f);
			}
			while(mob.mobStats.healthCurrent < (mob.mobStats.healthMax * 0.3f))
			{
				mob.mobStats.healthCurrent += 10;
				GameManager._Instance.uiManager.LaunchCounter(mob.transform.position, 10, Color.green);
				yield return new WaitForSeconds(0.1f);
			}
		}
        if(slimesAutoAttack)
        {
		    yield return new WaitForSeconds(slimesAutoDelay);
            index = 0;
            foreach (GameObject slimeGo in allSlimes)
            {
                BaseSlime slime = slimeGo.transform.GetComponent<BaseSlime>();
                BaseMob mob = allMobs[index].transform.GetComponent<BaseMob>();
                slime.Attack(mob.transform.position);
                index++;
            }


        }
        if(mobsAutoAttack)
        {
            yield return new WaitForSeconds(mobsAutoDelay);

            index = 0;
            foreach (GameObject mobGo in allMobs)
            {
                BaseSlime slime = allSlimes[index].transform.GetComponent<BaseSlime>();
                BaseMob mob = mobGo.transform.GetComponent<BaseMob>();
                mob.Attack(slime.transform.position);
                index++;
            }

        }
  //      for (int i = 0; i < allSlimes.Count; i++)
		//{
		//	int index = i;
		//	BaseSlime slime = allSlimes[index].transform.GetComponent<BaseSlime>();
		//	BaseMob mob = allMobs[index].transform.GetComponent<BaseMob>();
		//	slime.Attack(mob.transform.position);
		//}
		//yield return new WaitForSeconds(mobsAutoDelay);
		//for(int i = 0; i < allSlimes.Count; i++)
		//{
		//	int index = i;
		//	BaseSlime slime = allSlimes[index].transform.GetComponent<BaseSlime>();
		//	BaseMob mob = allMobs[index].transform.GetComponent<BaseMob>();
		//	mob.Attack(slime.transform.position);
		//}
		 yield return new WaitForSeconds(fireRate);
		insideCoro = false;
	}
}
