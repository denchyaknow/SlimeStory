﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class UIMenu_Status : MonoBehaviour
{
	[Header("Open/Close Tween")]
	public float moveTime = 0.8f;
	public float moveAddDelay = 0;
	public Ease moveEase = Ease.InExpo;

	public UIMenuManager.Delegate OnCloseMenu;

	private UIMenuManager mg;
	private UIMenuManager manager
	{
		get
		{
			if(mg == null)
			{
				if(GameManager._Instance != null && GameManager._Instance.uiManager != null)
					mg = GameManager._Instance.uiManager;
				else
					mg = transform.GetComponentInParent<UIMenuManager>();
			}
			return mg;
		}
	}
	private BaseSlime slime
	{
		get
		{
			return manager.currentSlime;
		}
	}
	private UIRefs_Status refs;
	private bool isHidden = false;
	private bool isShowingStats = false;
	private Vector2 visiblePos = Vector2.zero;
	private Vector2 hiddenPos;
	private SlimeData stats;
	private void Awake()
	{
		refs = transform.GetComponent<UIRefs_Status>();
		isHidden = true;

	}
	private void OnEnable()
	{
		hiddenPos = new Vector2(-refs.rootMenuPivot.rect.width, 0);
		refs.rootMenuPivot.anchoredPosition = hiddenPos;
		manager.OnStatsChanged += OnStatChange;
		manager.OnUpdateHud += OnUpdatedHud;
	}
	private void OnDisable()
	{
		
	}
	private void Start ()
	{

	}
	
	private void Update ()
	{
		
	}
	public void OnUpdatedHud()
	{
		if(refs.curImage.sprite != manager.currentSlime.myRenderer.sprite)
		{
			manager.CopySprite(slime.myRenderer, null, null, refs.curImage);
		}
		Vector3 localS = slime.myRenderer.transform.localScale * 2;
		refs.curImage.transform.localScale = localS;
		
	}
	public void OnStatChange()
	{
		//Debug.Log("Slime Stat Changed");
		stats = slime.slimeStats;
		int[] statArray = new int[5] 
		{
			(stats.defense + stats.defenseAddative),
			(stats.strength + stats.strengthAddative),
			(stats.agility + stats.agilityAddative),
			(stats.spirit + stats.spiritAddative),
			(stats.magic + stats.magicAddative)
		};
		refs.playerStats.currentStats = new int[5];
		for (int i = 0; i < 5; i++)
			refs.playerStats.currentStats[i] = statArray[i];
		if(!isHidden)
			refs.playerStats.update = true;
		//refs.playerStats.OnResetComplete = null;
		//refs.playerStats.OnResetComplete += ()=> { refs.playerStats.ShowStats(stats); };
		//refs.playerStats.ResetStats();
		refs.curName.text = stats.name;
		refs.curLvl.text = "Lv." + stats.level.ToString();
		float healthPerc = stats.GetHealthPercent();
		float happyPerc = stats.GetHappinessPercent();
		float hungerPerc = stats.GetHungerPercent();
		int healthTotal = manager.FormatedPercent(healthPerc);
		int happyTotal = manager.FormatedPercent(happyPerc);
		int hungerTotal = manager.FormatedPercent(hungerPerc);
		refs.curHealth.text = healthTotal.ToString() + "%";
		refs.curHappiness.text = happyTotal.ToString() + "%";
		refs.curHunger.text = hungerTotal.ToString() + "%";
		refs.curHealthCircle.fillAmount = healthPerc;
		refs.curHappinessCircle.fillAmount = happyPerc;
		refs.curHungerCircle.fillAmount = hungerPerc;
		refs.curStrength.text = stats.GetStrength().ToString();
		refs.curMagic.text = stats.GetMagic().ToString();
		refs.curAgility.text = stats.GetAgility().ToString();
		refs.curSpirit.text = stats.GetSpirit().ToString();
		refs.curDefense.text = stats.GetDefense().ToString();
		refs.tipStr.helpText = "Strength: " + stats.GetStrength() + "\n Increases knockback and cleaves \n Increases physical damage";
		refs.tipMag.helpText = "Intelligence: " + stats.GetMagic() + "\n Increases explosion radius \n Increases magical damage";
		refs.tipAgi.helpText = "Agility: " + stats.GetAgility() + "\n Increases dodge \n Increases movement";
		refs.tipSpi.helpText = "Spirit: " + stats.GetSpirit() + "\n Decreases attack cooldown \n Increases recovery from hits";
		refs.tipDef.helpText = "Defense: " + stats.GetDefense() + "\n Decreases damage taken \n Increases max health";

	}

	public void OpenMenu()
	{
		if(isHidden && !DOTween.IsTweening(refs.rootMenuPivot.gameObject.GetInstanceID()))
		{
			isHidden = false;
			refs.openButton.gameObject.SetActive(false);
			refs.closeButton.gameObject.SetActive(true);
			DOTween.Kill(refs.rootMenuPivot);
			refs.rootMenuPivot.DOAnchorPosX(visiblePos.x, moveTime, true)
				.SetId(refs.rootMenuPivot.gameObject.GetInstanceID())
				.SetEase(moveEase)
				.SetDelay(moveAddDelay)
				.OnStart(() =>
				{
					OnStatChange();
				});
		}
	}
	public void CloseMenu()
	{
		if(!isHidden && !DOTween.IsTweening(refs.rootMenuPivot.gameObject.GetInstanceID()))
		{
			isHidden = true;
			if(OnCloseMenu != null)
			{
				OnCloseMenu();
				OnCloseMenu = null;
			}
			refs.openButton.gameObject.SetActive(true);
			refs.closeButton.gameObject.SetActive(false);
			DOTween.Kill(refs.rootMenuPivot);
			refs.rootMenuPivot.DOAnchorPosX(hiddenPos.x, moveTime, true)
				.SetId(refs.rootMenuPivot.gameObject.GetInstanceID())
				.SetEase(moveEase)
				.SetDelay(moveAddDelay)
				.OnStart(() =>
				{
					refs.playerStats.update = false;
					refs.playerStats.ClearStats();
				});
			isShowingStats = false;
		}
	}
}
