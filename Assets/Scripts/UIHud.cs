﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class UIHud : MonoBehaviour {

	[Header("Health Tween")]
	public float healthAddedRate = 0.4f;
	public Ease healthEase = Ease.InOutExpo;
	public float coinLerpRate = 0.8f;
	public float updateRate = 0.2f;
	private float lastUpdateTime = 0.1f;
	private UIMenuManager mg;
	private UIMenuManager manager
	{
		get
		{
			if(mg == null)
			{
				if(GameManager._Instance != null && GameManager._Instance.uiManager != null)
					mg = GameManager._Instance.uiManager;
				else
					mg = transform.GetComponentInParent<UIMenuManager>();
			}
			return mg;
		}
	}
	private UIRefs_Hud refs;
	private BaseSlime slime
	{
		get
		{
			return manager.currentSlime;
		}
	}
	
	private BaseSlime monitoredSlime;
	private int lastCoinCount = 0;
	private string lastCoinText = string.Empty;
	private void OnEnable()
	{
		refs = transform.GetComponent<UIRefs_Hud>();
		manager.OnUpdateHud += OnUpdateHud;
		if(GameManager._Instance.combatManager != null)
		manager.OnUpdateHud += OnLevelChange;

		manager.OnSlimeChanged += OnSlimeChange;
		manager.OnStatsChanged += OnStatChange;
		manager.OnCombatChanged += UpdateMapTitle;
		manager.OnCombatChanged += UpdateMapWave;
	}
	void Start ()
	{
		
	}
	void LateUpdate ()
	{
		refs.curImage.transform.localScale = slime.myRenderer.transform.localScale;

	}
	public void OnUpdateHud()//Is updated every frame in UIMenuManager.cs
	{
		if (lastCoinCount != GameManager._Instance.playerData.coinCount)
		{
			int lerpTime = GameManager._Instance.playerData.coinCount - lastCoinCount;
			if (lerpTime < 0)
				lerpTime *= -1;
			float newVal = GameManager._Instance.playerData.coinCount;
			lastCoinCount = Mathf.FloorToInt(newVal);
		}
		if (lastCoinText != lastCoinCount.ToString())
		{
			lastCoinText = lastCoinCount.ToString();
			refs.curCoin.text = lastCoinText;
		}
		
		if (refs.curImage.sprite != manager.currentSlime.myRenderer.sprite)
		{
			manager.CopySprite(slime.myRenderer, null, null, refs.curImage);
		}
	}
	public void OnSlimeChange()
	{
		
		refs.curLvl.text = "Lv." + manager.currentSlime.slimeStats.level;
		refs.curName.text = manager.currentSlime.slimeStats.name;
		if(GameManager._Instance.combatManager != null)
		{
			refs.combatHudRoot.gameObject.SetActive(true);
		} else
		{
			refs.combatHudRoot.gameObject.SetActive(false);
		}
	}
	public void OnStatChange()
	{
		refs.curLvl.text = "Lv." + manager.currentSlime.slimeStats.level;
		if(GameManager._Instance.combatManager != null)
		{
			refs.healthSlider.minValue = 0;
			refs.healthSlider.maxValue = manager.currentSlime.slimeStats.maxHealth;
			//refs.healthSlider.value = manager.currentSlime.slimeStats.currentHealth;
			refs.healthText.text = (manager.currentSlime.slimeStats.currentHealth.ToString()) + " | " + (manager.currentSlime.slimeStats.maxHealth);
			float hp = refs.healthSlider.value;
			if (Mathf.RoundToInt(hp) != manager.currentSlime.slimeStats.currentHealth)
			{
				DOTween.Kill(refs.healthSlider);
				DOTween.To(() => hp, x => hp = x, manager.currentSlime.slimeStats.currentHealth, healthAddedRate).SetId(refs.healthSlider).SetEase(healthEase).OnUpdate(()=> 
				{
					refs.healthSlider.value = hp;
				}).OnComplete(()=> 
				{
					refs.healthSlider.value = manager.currentSlime.slimeStats.currentHealth;
				});
			}
			refs.expSlider.minValue = 0;
			refs.expSlider.maxValue = manager.currentSlime.slimeStats.maxExp;
			//refs.expSlider.value = 
			float exp = refs.expSlider.value;
			if (Mathf.RoundToInt(exp) != manager.currentSlime.slimeStats.currentExp)
			{
				DOTween.Kill(refs.expSlider);
				DOTween.To(() => exp, x => exp = x, manager.currentSlime.slimeStats.currentExp, healthAddedRate).SetId(refs.healthSlider).SetEase(healthEase).OnUpdate(() =>
				{
					refs.expSlider.value = exp;
				}).OnComplete(() =>
				{
					refs.expSlider.value = manager.currentSlime.slimeStats.currentExp;
				});
			}
		}
	}
	public void OnLevelChange()
	{
		int mapID = GameManager._Instance.combatManager.mapLevel;
		string mapTitle = SlimeDatabase.MapNames[mapID];
		//if (mapID >= SlimeDatabase.MapNames.Length)
		//	mapID = 0;
		//refs.mapLevel.text = "Lv:" + 
		//	GameManager._Instance.combatManager.subLvlCur.ToString() + 
		//	"/" + 
		//	GameManager._Instance.combatManager.subLvlMax.ToString();
		//refs.mapTitle.text = ("Zone " + 
		//	(GameManager._Instance.combatManager.mapLevel).ToString() + 
		//	": " + 
		//	SlimeDatabase.MapNames[mapID]);

	}
	public void UpdateMapWave()
	{
		if (GameManager._Instance.combatManager == null)
			return;
		int diff = GameManager._Instance.combatManager.difficulty;
		float steps = 100f;//how many mobs to kill till max
		float iRate = 2 / steps;//mob kill = 1 step
		float sRate = 200 / steps;
		float intensity = iRate * diff;
		float speed = sRate * diff;
		refs.mapDifficulty._Value1 = speed;
		refs.mapDifficulty._Value2 = intensity;
	}
	public void UpdateMapTitle()
	{
		if (GameManager._Instance.combatManager == null)
			return;
		int mapID = GameManager._Instance.combatManager.mapLevel;
		if(mapID >= SlimeDatabase.MapNames.Length)
			mapID = 0;
		refs.mapTitle.text = ( SlimeDatabase.MapNames[mapID]);
	}
}
