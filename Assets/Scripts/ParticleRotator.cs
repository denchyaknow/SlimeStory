﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleRotator : MonoBehaviour {
    private ParticleSystem psys;
    private ParticleSystem.MainModule pmain;
    public ParticleSystem.MinMaxCurve startRot;
    private bool applied;
	// Use this for initialization
	void Start ()
    {
        psys = transform.GetComponent<ParticleSystem>();
        if (psys)
            pmain = psys.main;
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(psys != null&& !applied)
        {
            applied = true;
            pmain.startRotationZ= startRot;
        }
	}
}
