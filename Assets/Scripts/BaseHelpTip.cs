﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
public class BaseHelpTip : MonoBehaviour
{
	public static BaseHelpTip _Instance;
	public BaseHelpTipSlime moduleSlime;
	public float holdThreshold = 5f;
	private float holdTime = 0;
	private Vector2 windowOffset = new Vector2();
	public Vector2 windowOffsetDefault = new Vector2();
	public Vector2 windowOffsetSlime = new Vector2();
	public Vector2 textSize = new Vector2(0.6f,0.3f);
	public Vector2 textSizeName;
	public int maxCharsPerLine = 10;
	private int addedLineCount = 0;
	public float scaleTime = 0.2f;
	public float scaleDelay = 0.2f;
	public Ease scaleEase = Ease.InOutExpo;

	public Transform helpNameRect;
	public Text helpName;
	public Transform helpTextRect;
	public Text helpText;
	public Transform helpTipTarget;
	public GraphicRaycaster gRayCaster
	{
		get
		{
			if (gr == null)
			{
				gr = GameManager._Instance.uiManager.GetRayCaster;
			}
			return gr;
		}
	}
	private GraphicRaycaster gr;
	[Header("Ranking Colors")]//0=S, 1=A, 2=B, 3=C, 4=D
	public Color[] rankColors = new Color[5];

	void Awake()
	{
		if(BaseHelpTip._Instance == null)
		{
			BaseHelpTip._Instance = this;
		}
		RectTransform myRect = transform.GetComponent<RectTransform>();
		myRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 0);
		myRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 0);
		moduleSlime.transform.localScale = new Vector3(1, 0, 1);
		helpTextRect.transform.localScale = new Vector3(1, 0, 1);
	}

	void Update()
	{
		
	}

	public void HideHelpTIp()
	{
		RectTransform myRect = transform.GetComponent<RectTransform>();
		//moduleSlime.Hide(scaleTime, scaleDelay, scaleEase);
		DOTween.Kill(transform);
		moduleSlime.transform.DOScaleY(0, scaleTime).SetId(transform).SetEase(scaleEase);
		helpTextRect.transform.DOScaleY(0, scaleTime).SetId(transform).SetEase(scaleEase).OnComplete(()=>
		{
			myRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 0);
			myRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 0);
			moduleSlime.transform.localScale = new Vector3(1, 0, 1);
			helpTipTarget = null;
		});
	}
	public void ShowHelpTip(BaseHelpTipText target)
	{
		//RectTransform myRect = transform.GetComponent<RectTransform>();
		//myRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 0);
		//myRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 0);
		if (helpTipTarget == target)
			return;
		helpTipTarget = target.transform;
		helpText.text = target.helpText;
		helpName.text = string.Empty;
		moduleSlime.name.text = string.Empty;
		moduleSlime.description.text = string.Empty;
		windowOffset = windowOffsetDefault;
		addedLineCount = 0;

		if(target.slimeData != null)
		{

			helpText.text = string.Empty;
			windowOffset = windowOffsetSlime;
			moduleSlime.name.text = target.slimeData.name;
			moduleSlime.attack.text = (target.slimeData.attackType == 0 ? "Physical" : "Magical");
			moduleSlime.description.text = target.slimeData.description;

			moduleSlime.strength.text = GetAttribute(target.slimeData.growth[0]);
			moduleSlime.strength.color = rankColors[target.slimeData.growth[0]];

			moduleSlime.magic.text = GetAttribute(target.slimeData.growth[1]);
			moduleSlime.magic.color = rankColors[target.slimeData.growth[1]];

			moduleSlime.agility.text = GetAttribute(target.slimeData.growth[2]);
			moduleSlime.agility.color = rankColors[target.slimeData.growth[2]];

			moduleSlime.spirit.text = GetAttribute(target.slimeData.growth[3]);
			moduleSlime.spirit.color = rankColors[target.slimeData.growth[3]];

			moduleSlime.defense.text = GetAttribute(target.slimeData.growth[4]);
			moduleSlime.defense.color = rankColors[target.slimeData.growth[4]];

			//moduleSlime.Show(scaleTime, scaleDelay, scaleEase);
		} else if(target.decoData != null)
		{
			helpName.text = target.decoData.name;
			helpText.text = target.decoData.description;
		} else if(target.foodData != null)
		{
			helpName.text = target.foodData.name;
			helpText.text = target.foodData.description;
		} else if(target.upgradeData != null)
		{
			helpName.text = target.upgradeData.name;
			helpText.text = target.upgradeData.description;
		}


		DOTween.Kill(transform);
		Resize();
		Reposition();
	}
	private string GetAttribute(int lvl)
	{
		string txt = string.Empty;
		switch(lvl)
		{
			case 0:
				txt = "S";
				break;
			case 1:
				txt = "A";
				break;
			case 2:
				txt = "B";
				break;
			case 3:
				txt = "C";
				break;
			case 4:
				txt = "D";
				break;
		}
		return txt;
	}
	public Vector2 GetSizeViaString(Vector2 charSize,int maxChars, string txt)
	{
		Vector2 size = Vector2.zero;
		char[] letters = txt.ToCharArray();
		int charCnt = 0;
		int lineCnt = 1;
		bool limitBreak = false;
		for(int i = 0; i < letters.Length; i++)
		{
			if(charCnt < maxChars)
			{
				if(!limitBreak)
					size.x += charSize.x;
				charCnt++;
			} else
			{
				limitBreak = true;
				charCnt = 0;
				lineCnt++;
			}
		}
		size.y = charSize.y * lineCnt;
		return size;
	}
	public void Resize()
	{
		helpNameRect.localScale = new Vector3(1, 0, 1);
		helpTextRect.localScale = new Vector3(1, 0, 1);
		moduleSlime.transform.localScale = new Vector3(1, 0, 1);
		
		if(moduleSlime.description.text != string.Empty && moduleSlime.name.text != string.Empty)
		{
			RectTransform myRect = transform.GetComponent<RectTransform>();
			Vector2 moduleSize = moduleSlime.transform.GetComponent<RectTransform>().rect.size;
			myRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, moduleSize.x);
			myRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, moduleSize.y);
			//moduleSlime.transform.localScale = new Vector3(1, 1, 1);
			moduleSlime.transform.DOScaleY(1, scaleTime).SetId(transform).SetDelay(scaleDelay).SetEase(scaleEase);

			//moduleSlime.Show(scaleTime, scaleDelay, scaleEase);
		}
		if (helpName.text != string.Empty)
		{
			helpNameRect.localScale = new Vector3(1, 1, 1);
			//Vector2 size = GetSizeViaString(textSizeName, maxCharsPerLine, helpName.text);
			//RectTransform nameRect = helpNameRect.GetComponent<RectTransform>();
			//nameRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, size.x);
			//nameRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, size.y);
		} 
		if (helpText.text != string.Empty)
		{
			//char[] letters = helpText.text.ToCharArray();
			//Vector2 newSize = new Vector2();
			//int charCount = 0;
			//int lineCount = 1;
			//bool widthBreak = false;
			//for (int i = 0; i < letters.Length; i++)
			//{
				
			//	if (charCount <= maxCharsPerLine)
			//	{
			//		if(!widthBreak)
			//			newSize.x += textSize.x;

			//		charCount++;
			//	}
			//	else
			//	{
			//		widthBreak = true;
			//		lineCount++;
			//		charCount = 0;
			//	}
			//}
			
			//newSize.y = textSize.y * (lineCount + addedLineCount);
			Vector2 size = GetSizeViaString(textSize, maxCharsPerLine, helpText.text);
			RectTransform myRect = transform.GetComponent<RectTransform>();
			myRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, size.x);
			myRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, size.y);
			helpTextRect.transform.DOScaleY(1, scaleTime).SetId(transform).SetDelay(scaleDelay).SetEase(scaleEase);
		}
		
		//transform.localScale = new Vector3(1, 0, 1);
		//transform.DOScaleY(1, scaleTime).SetId(transform).SetDelay(scaleDelay).SetEase(scaleEase);
	}
	private Vector2 GetCenter(Transform target)
	{
		Vector2 center = Vector2.zero;
		if (target.GetComponent<RectTransform>())
		{
			RectTransform rectTran = target.GetComponent<RectTransform>();
			//center = Camera.main.WorldToScreenPoint(rectTran.position);
			center = Camera.main.WorldToScreenPoint(rectTran.TransformPoint(rectTran.rect.center)); // rectTran.TransformPoint(rectTran.rect.center);

		}
		else if (target.GetComponent<SpriteRenderer>())
		{
			SpriteRenderer rectSprite = target.GetComponent<SpriteRenderer>();
			center = rectSprite.bounds.center;
		}
		return Camera.main.ScreenToWorldPoint(center);
	}

	public void Reposition()
	{
		if (helpTipTarget == null)
			return;
		float width = Camera.main.pixelWidth;
		float height = Camera.main.pixelHeight;
		Vector2 screenCetner = new Vector2(width / 2, height / 2);
		Vector2 screenPos = GetCenter(helpTipTarget);
		Vector3 wtX = Camera.main.ScreenToWorldPoint(new Vector3(width * 0.25f, 0, 0));
		Vector3 wtY = Camera.main.ScreenToWorldPoint(new Vector3(width * 0.50f, 0, 0));
		Vector3 wtZ = Camera.main.ScreenToWorldPoint(new Vector3(width * 0.75f, 0, 0));
		Vector3 htX = Camera.main.ScreenToWorldPoint(new Vector3(0,height * 0.25f, 0));
		Vector3 htY = Camera.main.ScreenToWorldPoint(new Vector3(0,height * 0.50f, 0));
		Vector3 htZ = Camera.main.ScreenToWorldPoint(new Vector3(0,height * 0.75f, 0));
		Vector3 camScaleX = new Vector3(wtX.x, wtY.x, wtZ.x);
		//Vector3 camScaleX = new Vector3((width * 0.25f), (width * 0.5f), (width * 0.75f));
		//camScaleX = Camera.main.ScreenToWorldPoint(camScaleX);
		Vector3 camScaleY = new Vector3(htX.y, htY.y, htZ.y);
		//Vector3 camScaleY = new Vector3((height * 0.25f), (height * 0.5f), (height * 0.75f));
		//camScaleY = Camera.main.ScreenToWorldPoint(camScaleY);

		Vector2 newPivot = new Vector2();
		Vector2 newOffset = new Vector2();
		if (screenPos.x > camScaleX.y)
		{
			newPivot.x = 1f;
			newOffset.x = -1f;
		}
		else
		{
			newPivot.x = 0f;
			newOffset.x = 1f;
		}

		if (screenPos.y > camScaleY.y)
		{
			if (screenPos.y > camScaleY.z)
			{
				newPivot.y = 1f;
				newOffset.y = -1f;
			}
			else
			{
				newPivot.y = 0.5f;
				newOffset.y = 0f;
			}
		}
		else
		{
			if (screenPos.y < camScaleY.x)
			{
				newPivot.y = 0f;
				newOffset.y = 1f;
			}
			else
			{
				newPivot.y = 0.5f;
				newOffset.y = 0f;
			}
		}

		RectTransform myRect = transform.GetComponent<RectTransform>();
		myRect.pivot = newPivot;
		Vector2 offsetPos = screenPos;
		offsetPos.x += (windowOffset.x * newOffset.x);
		offsetPos.y += (windowOffset.y * newOffset.y);
		transform.position = offsetPos;
	}
	public void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		if (helpTipTarget != null)
		{
			Gizmos.DrawLine(GetCenter(helpTipTarget), (transform.position));
		}
	}
}
