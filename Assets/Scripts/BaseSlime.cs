﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Rendering;
using Denchyaknow;
[System.Serializable]
public class BaseSlime : BaseObject
{

	public enum SlimeStates { Idle, Pet, Move, Eat, Hungry, Poop, Starving, Dead };
	public SlimeStates slimeStates = SlimeStates.Idle;
	private int nextState = -1;
	[Header("LayerMasks")]
	public LayerMask whatIsWall;
	public LayerMask whatIsPoop;

	[HideInInspector] public Telegraph attackTelegraph;
	[HideInInspector] public CountModule attackCounter;
	[HideInInspector] public StaminaModule stamina;
	[HideInInspector] public Transform hidden;
	//Toy Settings
	public Transform targetToy = null;
	public float toyVelocity = 10;
	private bool isPlayingWithToy = false;


	//Eating Settings
	public float eatingTime = 1.2f;
	public float eatingRate = 1f;
	private int closestFood = -1;
	private float foodDistance = 0;
	private Animator slimeAnim;
	[HideInInspector] public Sprite[] spriteSheet;
	[HideInInspector] public BaseObject eatingFood = null;
	private int preLevel;
	private int preFrame = 0;
	[HideInInspector] public float currentFrame = 0;

	public virtual bool isDead
	{
		get
		{
			bool dead = false;
			if (slimeStats.currentHealth <= 0)
			{
				dead = true;
			}
			return dead;
		}
		set
		{
			if (value)
			{
				slimeStats.currentHealth = 0;
			}
			else
			{
				slimeStats.currentHealth = slimeStats.maxHealth;
			}
		}
	}
	public SlimeEffects myEffects = null;
	//Effects
	public Transform pParticles;// the parent of all particles
	private ParticleSystem[] cParticles;
	private bool fxScalingTweening;
	private float[] fxTimes;
	private int fxIndex;
	//public bool fxAttPlaying = false;
	public Vector3 fxAttScale = new Vector3(2, 1, 0);
	public float fxAttDuration = 0.2f;
	public Ease fxAttEase = Ease.Linear;
	//private bool fxHitPlaying = false;
	public Vector3 fxHitScale = new Vector3(2, 2, 0);
	public float fxHitDuration = 0.2f;
	public int fxHitVibrate = 2;
	public float fxHitElastic = 2f;
	public Ease fxHitEase = Ease.Linear;
	public float fxDeathDelay = 0.2f;

	[Header("Petting Settings")]
	public float petVelocityBase = 3f;
	public float petVelocityAdd = 1f;
	public float petCoolDown = 0.4f;
	private float petLastTime = 0;
	private float petVelocityCur = 0;
	[HideInInspector]public bool isBeingPet = false;
	[Header("Movement Settings")]
	public bool moveEnabled = false;
	public float moveRange = 3;
	public float moveSpeed = 5;
	public float moveDelay = 3;
	public Ease moveEase;
	private float moveDegree = 0;
	private float moveDegRate = 10;
	private float moveDegRateMax = 25;
	private float moveByRadian = 0;
	private int moveByTurnDir = 1;
	private int moveByFowardDir = 1;
	private float moveLerp = 5;
	private Vector2 RadianToVector2(float radian)
	{
		return new Vector2(Mathf.Cos(radian), Mathf.Sin(radian));
	}
	private Vector2 DegreeToVector2(float degree)
	{
		return RadianToVector2(degree * Mathf.Deg2Rad);
	}
	private Vector2 moveBodyDelayedDir;
	private float moveBodyDelayedDis;
	private bool moveBodyQued = false;
	[HideInInspector] public Collider2D myCollider;
	[HideInInspector] public Vector2 moveDirection;

	[Header("Attack Settings")]
	public int attackJumpCount = 2;
	public float attackJumpHeight = 0.5f;
	public float attackCutoff = 0.5f;
	public Ease attackInEase;
	public Ease attackOutEase;
	private float combatMoveLast;
	private bool attackQued = false;
	private BaseMob attackTargetQued;
	private List<GameObject> attacks = new List<GameObject>();
	[HideInInspector] public Transform currentTargetMob = null;
	private PointEffector2D attackEffector;
	[HideInInspector] public List<int> lastHitMobs = new List<int>();
	[HideInInspector] public bool combatMode = false;
	[HideInInspector] public float attackCharge = 1f;
	[HideInInspector] public float attackedLast = 0;
	[HideInInspector] public Vector3 attackPos;
	[HideInInspector] public bool attackCharging = false;
	[HideInInspector] public int attackDirection;
	[HideInInspector] public Vector3 attackRotation;

	public bool isAttacking = false;
	[Tooltip("When a mob is hit it is stunned until x time, if hit again before x time bool will be set to false, when stunned slime will not move to players input")]
	public bool isStunned = false;
	//[HideInInspector] public Vector3 previousPosition;

	public SlimeData slimeStats;
	public float slimeScale
	{
		get
		{
			float scale = slimeStats.currentScale;
			if (GameManager._Instance != null && GameManager._Instance.combatManager != null)
			{
				scale = slimeStats.currentScale * 0.7f;
			}
			return scale;
		}
		set
		{
			slimeStats.currentScale = value;
		}
	}
	private bool upgradesApplied = false;
	private float lastHappinessDecay = 0;
	private float lastHungerDecay = 0;
	//Delegates
	public delegate void OnStatChangeDel();
	public OnStatChangeDel OnStatChange;
	public delegate void Delegate();
	public Delegate OnAttack;
	public Delegate OnWalk;
	public Delegate OnPet;
	public Delegate OnHit;
	public Delegate OnDeath;
	public Delegate OnSpawn;

	//Delegates
	private void OnEnable()
	{
		GameManager._Instance.ourSlimes.Add(gameObject);
		OnAttack += ProcessAttack;
		OnPet += ProcessPet;
		//OnStatChange += GameManager._Instance.GameSave;
		OnStatChange += LayerCheck;
		dragMagnituge = SlimeProperties.slimeDragMagnitude;
	}
	private void OnDisable()
	{
		OnAttack = null;
		OnWalk = null;
		OnPet = null;
		OnHit = null;
		OnDeath = null;
		OnSpawn = null;
		OnStatChange = null;
			
		//GameManager._Instance.ourSlimes.Remove(gameObject);

	}
	public override void Awake()
	{

		base.Awake();
		slimeAnim = this.gameObject.GetComponent<Animator>();

		//(Material)Resources.Load("Matts/Sprites(Default)");

		if (transform.GetComponent<Rigidbody2D>())
			myBody = transform.GetComponent<Rigidbody2D>();

		if (transform.GetComponentInChildren<Telegraph>())
			attackTelegraph = transform.GetComponentInChildren<Telegraph>();

		if (transform.GetComponentInChildren<CountModule>())
		{
			attackCounter = transform.GetComponentInChildren<CountModule>();
			attackCounter.gameObject.SetActive(false);
		}

		if (transform.GetComponentInChildren<StaminaModule>())
			stamina = transform.GetComponentInChildren<StaminaModule>();
		if (transform.GetComponentInChildren<PointEffector2D>())
			attackEffector = transform.GetComponentInChildren<PointEffector2D>();
		myCollider = transform.GetComponent<Collider2D>();

		spriteSheet = Sprite_Database.GetSlimeSprites(slimeStats.spriteSheetIndex);

		attackedLast = 0;

		if (mySortingGroup == null)
		{
			if (transform.root.GetComponent<SortingGroup>() == null)
			{
				transform.root.gameObject.AddComponent<SortingGroup>();
			}
		}
	}
	public override void Start()
	{

		base.Start();
		Transform[] allChildrenTransforms = transform.GetComponentsInChildren<Transform>();
		if (allChildrenTransforms.Length > 0)
		{
			for (int i = 0; i < allChildrenTransforms.Length; ++i)
			{
				if (allChildrenTransforms[i].CompareTag("Hidden"))
				{
					hidden = allChildrenTransforms[i].transform;
					break;
				}
			}
		}
		int classType = transform.GetComponent<BaseMob>() != null ? transform.GetComponent<BaseMob>().mobStats.classType : slimeStats.classType;
		if (classType < SlimeDatabase.Attacks.Length && !GameManager._Instance.DoNotPool)
		{

			for (int i = 0; i < 10; ++i)
			{
				GameObject newAttack = Instantiate(SlimeDatabase.Attacks[classType], hidden);
				attacks.Add(newAttack);
				newAttack.transform.GetComponent<BaseAttack>().parent = hidden;
				newAttack.transform.localPosition = Vector3.zero;
				newAttack.SetActive(false);
			}
		}
		else
		{
			//Debug.Log("Did not pool attacks for " + gameObject.name + " class:" + slimeStats.classType + "|" + SlimeDatabase.Attacks.Length);
		}
		LayerCheck();
		lastHappinessDecay = Time.time;
		lastHungerDecay = Time.time;
	}
	public override void LateUpdate()
	{
		base.LateUpdate();
		//Debug.Log(Time.time);
	}
	public override void Update()
	{
		base.Update();
		//slimeStats.damage = slimeStats.GetDamage();
		if (isSelected && GameManager._Instance.selSlimeIndex != slimeStats.index)
		{
			GameManager._Instance.SetSelectedSlime(this);
		}
		if (Input.GetKey("a"))
		{
			slimeStats.currentHappiness--;
		}
		if (Input.GetKey("s"))
		{
			slimeStats.currentHappiness++;
		}

		if (preLevel != slimeStats.level)
		{
			preLevel = slimeStats.level;
			SetStats();
		}

		//ChangeMoveDirection(Time.deltaTime);
		if (myRenderer.sprite != spriteSheet[preFrame])
			myRenderer.sprite = spriteSheet[preFrame];
		if (speed > 0.2f)
			PlayMoveAnimation(speed);
		else
			PlayIdleAnimation();
		preFrame = (int)currentFrame;
	}
	public override void TouchFunction(int _id, Vector3 _currentPos)
	{
		//base.TouchFunction(_id, _currentPos);
		switch (_id)
		{
			case 0:
				break;
			case 1://move
				if (isMovable && !isBeingPet)
				{
					if (myBody != null)
					{
						Vector2 dir = (_currentPos - transform.position).normalized;
						float mag = Vector2.Distance(transform.position, _currentPos);
						//myBody.velocity = dir * mag * dragMagnituge;
						myBody.velocity = Vector2.Lerp(myBody.velocity, (dir * dragMagnituge), Time.deltaTime * mag);
					}
					else
					{
						transform.position = _currentPos;

					}
				}
				break;
			case 2:
				break;
			case 3:
				//Debug.Log("Touch-DoubleTap id:" + _id.ToString());
				if (isDead)
					return;
				OnPet();
				break;//Pres enter here to add new case, then make int relative
			case 4:
				//Debug.Log("Touch-TapCount id:" + _id.ToString());

				break;
			case 5:
				if (GameManager._Instance.ourSlimes.Contains(this.gameObject))
					GameManager._Instance.ourSlimes.Remove(this.gameObject);
				Destroy(gameObject);
				break;

		}
	}
	/*-------------------------------
             Movement Functions
    --------------------------------*/
	#region Movement Logic
	private int aIdleIndex = 0;
	private int aMoveIndex = 0;

	public void PlayIdleAnimation()
	{
		if (SlimeProperties.aIdleScales == null || SlimeProperties.aIdleScales.Length == 0 || isDead)
			return;
		if (DOTween.IsTweening("Scale" + myRenderer.gameObject.GetInstanceID()))
			DOTween.Kill("Scale" + myRenderer.gameObject.GetInstanceID());
		if (aIdleIndex >= SlimeProperties.aIdleScales.Length)
			aIdleIndex = 0;
		float aSpeed = SlimeProperties.aIdleSpeed;
		myRenderer.transform.localScale = Vector3.Lerp(myRenderer.transform.localScale, SlimeProperties.aIdleScales[aIdleIndex], Time.deltaTime * aSpeed);
		if (myRenderer.transform.localScale.x * 0.95f < SlimeProperties.aIdleScales[aIdleIndex].x && myRenderer.transform.localScale.y * 0.95f < SlimeProperties.aIdleScales[aIdleIndex].y)
			aIdleIndex++;
	}
	public void StopIdleAnimation()
	{
		DOTween.Kill("Scale" + myRenderer.gameObject.GetInstanceID());
		float timer = 0;
		myRenderer.transform.localScale = Vector3.Lerp(myRenderer.transform.localScale, new Vector3(1, 1, 1), Time.deltaTime * SlimeProperties.aMoveSpeedMax);
	}
	public void PlayMoveAnimation(float speed)
	{
		if (SlimeProperties.aMoveScales == null || SlimeProperties.aMoveScales.Length == 0 || isDead)
			return;
		if (DOTween.IsTweening("Scale" + myRenderer.gameObject.GetInstanceID()))
			DOTween.Kill("Scale" + myRenderer.gameObject.GetInstanceID());
		if (aMoveIndex >= SlimeProperties.aMoveScales.Length)
			aMoveIndex = 0;
		float aSpeed = SlimeProperties.aMoveSpeed * speed;
		if (aSpeed < SlimeProperties.aMoveSpeedMin)
			aSpeed = SlimeProperties.aMoveSpeedMin;
		if (aSpeed > SlimeProperties.aMoveSpeedMax)
			aSpeed = SlimeProperties.aMoveSpeedMax;
		myRenderer.transform.localScale = Vector3.Lerp(myRenderer.transform.localScale, SlimeProperties.aMoveScales[aMoveIndex], Time.deltaTime * aSpeed);
		if (myRenderer.transform.localScale.x * 0.95f < SlimeProperties.aMoveScales[aMoveIndex].x && myRenderer.transform.localScale.y * 0.95f < SlimeProperties.aMoveScales[aMoveIndex].y)
			aMoveIndex++;
	}
	public void StopMoveAnimation()
	{
		if (DOTween.IsTweening("Scale" + myRenderer.gameObject.GetInstanceID()))
			DOTween.Kill("Scale" + myRenderer.gameObject.GetInstanceID());
		float timer = 0;
		myRenderer.transform.localScale = Vector3.Lerp(myRenderer.transform.localScale, new Vector3(1, 1, 1), Time.deltaTime * SlimeProperties.aMoveSpeedMax);
	}
	public void ChangeMoveDirection(float timePassed)
	{
		if (dir == 0)
			return;
		else if (dir > 0)
			myRenderer.flipX = false;
		else if (dir < 0)
			myRenderer.flipX = true;
	}
	private bool CombatCanMove()
	{
		bool canMove = false;
		if ((Time.timeSinceLevelLoad - combatMoveLast) > slimeStats.moveCD && !isAttacking && !isDead && !attackCharging)
			canMove = true;
		//if (_Debug)
		//Debug.Log("CombatCanMove() = " + canMove + " Because: offCD=" + ((Time.timeSinceLevelLoad - combatMoveLast) > slimeStats.moveCD) + " timeSinceLevelLoad=" + Time.timeSinceLevelLoad + " isAttacking=" + isAttacking + " attackCharging=" + attackCharging);
		return canMove;
	}
	public virtual void MoveRandomly(float intensity = 1f)//for random movement
	{
		float rate = Random.Range(moveDegRate - slimeStats.GetAgility(), moveDegRate + slimeStats.GetAgility()) * moveByTurnDir;
		float lastRad = Mathf.Deg2Rad * moveDegree;
		bool leftWall = WallCheck(RadianToVector2(lastRad + 0.3f).normalized * moveByFowardDir);
		bool rightWall = WallCheck(RadianToVector2(lastRad - 0.3f).normalized * moveByFowardDir);
		if (leftWall)
			moveByTurnDir = -1;
		
		if (rightWall)
			moveByTurnDir = 1;

		if (rightWall || leftWall)
			rate *= moveDegRateMax;
		if (leftWall && rightWall)
		{
			moveByFowardDir *= -1;
			moveByTurnDir *= -1;
		}
		moveDegree += Time.deltaTime * rate * intensity;
		moveByRadian = Mathf.Deg2Rad * moveDegree;
		//Debug.DrawLine(transform.position, transform.position + (Vector3)RadianToVector2(moveByRadian) * moveSpeed * intensity * moveByFowardDir, Color.black);
		myBody.velocity = Vector2.Lerp(myBody.velocity,(RadianToVector2(moveByRadian) * moveSpeed * intensity * moveByFowardDir), Time.deltaTime * moveLerp);
		ChangeMoveDirection(Time.deltaTime);
	}
	public virtual void MoveBody(Vector2 dir, float intensity = 1f, bool lerp = true)//for specific movement like combat or w/e
	{
		if (isStunned || isAttacking|| isDead)
			return;
		ChangeMoveDirection(Time.deltaTime);
		if (lerp)
			myBody.velocity = Vector2.Lerp(myBody.velocity, dir * intensity * moveSpeed, Time.deltaTime * moveLerp);
		else
			myBody.velocity = dir * intensity;
	}
	
	
	public virtual void MoveToPosition(Vector2 _pos, float _delay)
	{
		if (GameManager._Instance.Debugging)
		{
			//Debug.Log("SLIMEMOVING_" + _pos);
		}
		GameManager._Instance.soundManager.PlaySlimeJump();
		slimeAnim.SetTrigger("Moving");
		combatMoveLast = Time.timeSinceLevelLoad;
		transform.DOPath(new Vector3[2] { transform.position, _pos }, moveSpeed, PathType.CatmullRom, PathMode.Full3D, 10, Color.red)
			.SetSpeedBased(true)
			.SetEase(moveEase);


	}
	public virtual void MoveToSpawn(Vector2 _pos, float _delay)
	{
		if (GameManager._Instance.Debugging)
		{
			//Debug.Log("SLIMEMOVING_" + _pos);
		}

		slimeAnim.SetTrigger("Moving");
		isMovingToSpawn = true;
		myCollider.enabled = false;
		myBody.DOMove(_pos, moveSpeed).SetSpeedBased(true).SetEase(moveEase).OnComplete(() =>
		{
			isMovingToSpawn = false;
			myCollider.enabled = true;
			previousPosition = _pos;
			attackedLast = Time.timeSinceLevelLoad;
			combatMoveLast = Time.timeSinceLevelLoad;
		});
	}
	public virtual void MoveToFood(int _food)//for eating
	{
		ChangeMoveDirection(Time.deltaTime);
		Vector2 direction = GameManager._Instance.ourFood[_food].transform.position - transform.position;
		RaycastHit2D _wallCheck = Physics2D.Linecast(transform.position, (Vector2)GameManager._Instance.ourFood[_food].transform.position, whatIsWall);
		if (_wallCheck)
			return;
		float distance = Vector2.Distance(this.gameObject.transform.position, GameManager._Instance.ourFood[_food].transform.position);
		if (distance > 0.5f && !GameManager._Instance.ourFood[_food].isMoving() && GameManager._Instance.ourFood[_food].isEdible)
		{
			Vector2 newPos = ((GameManager._Instance.ourFood[_food].transform.position - transform.position).normalized) * (moveSpeed);
			//newPos = distance < newPos.magnitude ? (Vector2.Lerp(transform.position, (Vector2)transform.position + newPos, 0.5f)) :
				//((Vector2)transform.position + newPos);
			Debug.DrawLine(transform.position, newPos, Color.magenta);
			myBody.velocity = Vector2.Lerp(myBody.velocity, newPos, Time.deltaTime * moveLerp);

			//transform.DOPath(new Vector3[2] { transform.position, newPos }, slimeAnim.GetNextAnimatorStateInfo(0).length).SetSpeedBased(false).SetEase(moveEase);
		}
		if (distance < 0.5f && !GameManager._Instance.ourFood[_food].isMoving() && GameManager._Instance.ourFood[_food].isEdible)
		{
			eatingFood = GameManager._Instance.ourFood[_food];
			eatingFood.GetComponent<BaseFood>().FocusSprites();
			slimeAnim.SetTrigger("Eating");
		}
	}
	
	public void OnCollisionEnter2D(Collision2D collision)
	{
		if (isPlayingWithToy || collision.gameObject.tag != "Toy" && collision.gameObject.tag != "Slime" && collision.gameObject.tag != "Food" || isDead)
			return;
		float mag = collision.relativeVelocity.magnitude;
		
		//Debug.Log(gameObject.name + " was hit by " + collision.gameObject.name + " at " + mag + " magnitude");
		if (mag > SlimeProperties.minCollisionVelocity && mag < SlimeProperties.maxCollisionVelocity)
		{
			isPlayingWithToy = true;
			targetToy = collision.transform;
			slimeAnim.SetTrigger("Toy_Good");
		}
		else if (mag > SlimeProperties.maxCollisionVelocity)
		{
			slimeAnim.SetTrigger("Toy_Bad");
		}
	}
	#endregion
	/*-------------------------------
            State Management
    --------------------------------*/
	#region State Logic
	private int[] stateChances = new int[5];
	public void IdleBehaviorEnter()
	{

		if (isDead)
		{
			slimeAnim.SetTrigger("Dieing");
			return;
		}
		slimeStates = SlimeStates.Idle;
		float happyPercent = slimeStats.GetHappinessPercent();
		float hungerPercent = slimeStats.GetHungerPercent();
		slimeAnim.SetFloat("Happiness", happyPercent);
		slimeAnim.SetFloat("Hunger", hungerPercent);
		if (combatMode && !isAttacking)
		{
			previousPosition = transform.position;
		}
		else if (!combatMode)
		{

			// 0 = Move
			// 1 = Poop
			// 2 = Emote
			// 3 = Sleep
			// 4 = Petting
			if (Time.time > lastHappinessDecay)
			{
				lastHappinessDecay = Time.time + SlimeProperties.slimeHappinesDecayRate;
				if (slimeStats.currentHunger < 40)
					RemoveHappiness(1);
			}
			if (stateChances.Length != 5)
				stateChances = new int[5];
			for (int i = 0; i < stateChances.Length; i++)
				stateChances[i] += 10;
			int stateChance = Random.Range(0, 100);
			int randomIdleState = Random.Range(0, 5);
			if (slimeStats.currentHunger < slimeStats.maxHunger && GameManager._Instance.ourFood.Count > 0)
			{
				nextState = 0;
			}
			if (nextState == -1)
			{
				for (int i = 0; i < stateChances.Length; i++)
				{
					if (stateChance < stateChances[i])
					{
						nextState = i;
						stateChances[i] -= (10 * stateChances.Length);
						break;
					}
				}
			}
			if (nextState >= 0)
			{
				randomIdleState = nextState;
				nextState = -1;
			}

			if (GameManager._Instance.Debugging)
			{
				//Debug.Log(randomIdleState);
			}

			if (slimeStats.currentHunger < slimeStats.maxHunger * 0.5f && GameManager._Instance.ourFood.Count < 0)
			{

			}
			switch (randomIdleState)
			{
				case 0:

					slimeAnim.SetTrigger("Moving");
					break;
				case 1:
					if (slimeStats.currentHunger > 0 && Time.time > lastHungerDecay)
					{
						if (PoopCheck())//is standing on poop so find somewhere else to poop
						{
							nextState = 1;
							slimeAnim.SetTrigger("Moving");
						}
						else
						{
							lastHungerDecay = Time.time + SlimeProperties.slimeHungerDecayRate;
							slimeAnim.SetTrigger("Pooping");
						}
					}
					else// let player know its hunger af
					{
						slimeAnim.SetTrigger("Moving");
					}
					break;
				case 2:
					slimeAnim.SetTrigger("Emote");
					break;
				case 3:
					slimeAnim.SetTrigger("Sleeping");
					break;
				case 4:
					slimeAnim.SetTrigger("Emote");
					break;
			}
			petVelocityCur -= petVelocityAdd;
			if (petVelocityCur < petVelocityBase)
				petVelocityCur = petVelocityBase;
		}
	}
	public void IdleBehaviorUpdate()
	{
		if (!combatMode)
		{
			//PlayIdleAnimation();
		}
	}
	public void IdleBehaviorExit()
	{
		if (!combatMode)
		{
			//StopIdleAnimation();
		}
	}
	public void MoveBehaviorEnter(Animator myAnime)
	{
		myAnime.speed = Random.Range(0.4f, 1.8f);
	}
	public void MoveBehaviorUpdate()
	{
		if (isDead)
			return;
		slimeStates = SlimeStates.Move;
		if (combatMode)
			previousPosition = transform.position;
		else
		{
			int closestFood = GetClosestFood();
			if (slimeStats.currentHunger < slimeStats.maxHunger && GameManager._Instance.ourFood.Count > 0 && closestFood != -1)
				MoveToFood(closestFood);
			else	
				MoveRandomly();
		}
		//if (speed > 0.2f)
		//{
		//	ChangeMoveDirection(Time.deltaTime);
		//	PlayMoveAnimation(speed);
		//}
		
	}
	public void MoveBehaviorExit(Animator myAnime)
	{
		myAnime.speed = 1;
		//StopMoveAnimation();
	}
	void ProcessPet()
	{
		if (isBeingPet && Time.timeSinceLevelLoad - petLastTime  < 0.5f)
			return;
		isBeingPet = true;
		AddHappiness(Random.Range(1, 4));
		petLastTime = Time.timeSinceLevelLoad + petCoolDown;
		slimeAnim.SetTrigger("Petting");
	}
	public void PetBehaviorEnter()
	{
		if (isDead)
			return;
		petVelocityCur += petVelocityAdd * (Random.Range(0.9f, 3.5f));
	
		slimeStates = SlimeStates.Pet;
	}
	public void PetBehaviorUpdate()
	{
		MoveRandomly(petVelocityCur);
		if (speed > 0.2f)
		{
			ChangeMoveDirection(Time.deltaTime);
			PlayMoveAnimation(speed);
		}
	}
	public void PetBehaviorExit()
	{
		petVelocityCur = petVelocityBase;
		isBeingPet = false;

	}
	public void ToyBehaviorEnter()
	{
		if (targetToy != null)
		{
			Vector2 dir = (targetToy.position - transform.position).normalized;
			MoveBody(dir, toyVelocity);
		}
	}
	public void ToyBehaviorUpdate()
	{
		if (targetToy != null)
		{
			Vector2 dir = (targetToy.position - transform.position).normalized;
			MoveBody(dir, toyVelocity /3);
		}
		float speed = EstimatedSpeed(Time.deltaTime);
		if (speed > 0.2f)
		{
			ChangeMoveDirection(Time.deltaTime);
			PlayMoveAnimation(speed);
		}
	}
	public void ToyBehaviorExit()
	{
		if (targetToy != null)
			targetToy = null;
		isPlayingWithToy = false;
	}
	public void EmoteBehavior()
	{
		if (isDead)
			return;
		
	}
	public void EatBehavior()
	{
		slimeStates = SlimeStates.Eat;
		if (slimeStats.currentHunger < slimeStats.maxHunger && eatingFood != null)
		{
			eatingFood.GetComponent<BaseFood>().Bite(this);
			GameManager._Instance.soundManager.PlaySlimeEat();
		}
	}
	public int GetClosestFood()
	{
		closestFood = -1;
		for (int i = 0; i < GameManager._Instance.ourFood.Count; i++)
		{
			float distance = Vector2.Distance(this.gameObject.transform.position, GameManager._Instance.ourFood[i].transform.position);
			if (i == 0 || distance < foodDistance && GameManager._Instance.ourFood[i].isEdible)//this is the first food object so its obvi gunna be the closer one in the list since we didnt check anyothers
			{
				foodDistance = distance;
				closestFood = i;
			}
		}
		return closestFood;
	}
	public void PoopBehavior()
	{
		RemoveHunger(1);
		AddHappiness(1);
		int poopID = slimeStats.classType;
		GameObject newPoop = (GameObject)Instantiate(SlimeDatabase.Poop, transform.position, Quaternion.identity);
		BasePoop newBasePoop = newPoop.gameObject.GetComponent<BasePoop>();
		newBasePoop.Setup(poopID);
		newBasePoop.Poop();
		PoopData newPoopData = new PoopData(GameManager._Instance.ourPoop.Count, poopID, transform.position.x, transform.position.y, transform.position.z);
		newPoop.transform.SetParent(GameManager._Instance.garbageCan);
		newBasePoop.poopData = newPoopData;
		GameManager._Instance.ourPoop.Add(newPoop.transform);
	}
	public void DamagedBehavior()
	{
		if (isDead)
		{
			slimeAnim.SetTrigger("Dieing");
		}
	}
	public void DiedBehavior()
	{
		StartCoroutine("KillTheMob");
	}
	private Collider2D standingOnPoop;
	public bool WallCheck(Vector2 relativeDir)
	{
		RaycastHit2D check = Physics2D.Raycast(transform.position,(Vector3)relativeDir, 2f, whatIsWall);
		Debug.DrawRay(transform.position, (Vector3)relativeDir * 2, (check.collider == null ? Color.red : Color.green));
		return check.collider != null;
	}
	public bool PoopCheck()
	{
		Collider2D[] check = Physics2D.OverlapCircleAll(transform.position, myRenderer.sprite.bounds.extents.magnitude, whatIsPoop);
		bool onPoop = check.Length > 0;
		//Debug.Log("Slime poop checker found " + (onPoop ? check.Length.ToString() : "no") + " poop(s), so it" + (onPoop ? " wont " : " will ") + "poop");
		return check.Length > 0 ? true : false;
	}
	public void LayerCheck()
	{
		int layer = 0;
		if (isDead)
			layer = LayerMask.NameToLayer(SlimeProperties.slimeLayerDead);
		else
			layer = LayerMask.NameToLayer(SlimeProperties.slimeLayerAlive);
		gameObject.layer = layer;
	}
	#endregion State Logic
	#region Stats Logic
	public void SetNoClip(float time)
	{
		isStunned = true;
		myRendererHitfx.enabled = true;
		myCollider.enabled = false;
		myEffects.PlayNoClip(time);
		float timer = 0;
		string tweenID = gameObject.GetInstanceID() + "NoClip";
		if (DOTween.TweensById(tweenID, true) != null)
		{//Stop allowing movement form player
			if ((float)DOTween.TweensById(tweenID, true)[0].ElapsedPercentage(false) > 0.5f)
			{
				DOTween.Kill(tweenID);
				isStunned = false;
			}
		}
		else
		{
			DOTween.To(() => timer, x => timer = x, time, time / 4).SetId(tweenID).OnComplete(() =>
			{
				isStunned = false;
			});
		}
		float clipTimer = 0;
		DOTween.Kill(gameObject.GetInstanceID() + "Invincible");
		DOTween.To(() => clipTimer, x => clipTimer = x, time, time / 3).SetDelay(time/4).SetId(gameObject.GetInstanceID() + "Invincible").OnComplete(() =>
		{//Stop NoClipping
			isKnockedBack = false;
			myCollider.enabled = true;
			myRendererHitfx.enabled = false;
		});
	}
	public void TakeDamage(int _amount)
	{
		if (isAttacking)
			return;
		if (!isKnockedBack)
		{
			isKnockedBack = true;
			SetNoClip(0.4f);
			//combatMoveLast = Time.timeSinceLevelLoad + slimeStats.moveCD;
			//StartCoroutine(ResolveKnockback());
			//PlayEffects(1);
			slimeAnim.SetTrigger("Damaged");
			float reduc = slimeStats.GetDamageReduction();
			int reducedBy = Mathf.CeilToInt(_amount * reduc);
			int total = _amount - reducedBy;
			slimeStats.currentHealth -= total;
			if (slimeStats.currentHealth < 0)
				slimeStats.currentHealth = 0;
			if (OnStatChange != null)
				OnStatChange();
			GameManager._Instance.soundManager.PlaySlimeOnHit();
			GameManager._Instance.uiManager.LaunchCounter(transform.position, total, Color.red);
			GameManager._Instance.uiManager.ShakeCamera();

		}


	}
	public void HealDamage(int _amount)
	{
		if (_amount <= 0)
			return;
		slimeStats.currentHealth += _amount;
		if (slimeStats.currentHealth > slimeStats.maxHealth)
			slimeStats.currentHealth = slimeStats.maxHealth;
		if (OnStatChange != null)
			OnStatChange();
		GameManager._Instance.soundManager.PlaySlimeOnHeal();
		GameManager._Instance.uiManager.LaunchCounter(transform.position, _amount, Color.green);
	}
	public void AddHunger(int _amount)
	{
		int added = _amount;
		if (_amount + slimeStats.currentHunger > slimeStats.maxHunger)
			added = slimeStats.maxHunger - slimeStats.currentHunger;
		slimeStats.currentHunger += added;
		if (OnStatChange != null)
			OnStatChange();
		GameManager._Instance.soundManager.PlaySlimeOnHeal();
		GameManager._Instance.uiManager.LaunchCounter(transform.position, added, Color.yellow);
	}
	public void RemoveHunger(int _amount)
	{
		slimeStats.currentHunger -= _amount;
		if (slimeStats.currentHunger < 0)
			slimeStats.currentHunger = 0;
		if (OnStatChange != null)
			OnStatChange();
	}
	public void AddHappiness(int hapValue)
	{
		//int amountAdded = slimeStats.currentHappiness;
		slimeStats.currentHappiness += hapValue;
		if (slimeStats.currentHappiness > slimeStats.maxHappiness)
			slimeStats.currentHappiness = slimeStats.maxHappiness;
		//amountAdded = slimeStats.currentHappiness - amountAdded;
		//GameManager._Instance.uiManager.LaunchCounter(transform.position, amountAdded, new Color(255, 170, 0), "md");
		if (OnStatChange != null)
			OnStatChange();
	}
	public void RemoveHappiness(int _amount)
	{
		slimeStats.currentHappiness -= _amount;
		if (slimeStats.currentHappiness < 0)
			slimeStats.currentHappiness = 0;
		if (OnStatChange != null)
			OnStatChange();
	}
	public void AddExperience(int expValue)
	{
		GameManager._Instance.uiManager.LaunchCounter(transform.position, expValue, Color.cyan, "xp");
		slimeStats.currentExp += Mathf.CeilToInt(expValue * 0.22f);
		if (slimeStats.currentExp > slimeStats.maxExp)
		{
			LevelUp();
		}
		if (OnStatChange != null)
			OnStatChange();
	}
	public void AddUpgrade(UpgradeData buff)
	{
		slimeStats.upgrades.Add(buff);
		GameManager._Instance.soundManager.PlaySlimeOnHeal();
		string buffText = string.Empty;
		switch (buff.upgradeEffect)
		{
			case UpgradeData.EffectType.ADD_STR:
				//slimeStats.strengthAddative += Mathf.CeilToInt(buff.upgradeValue);
				buffText = "STR +" + buff.upgradeValue;
				break;
			case UpgradeData.EffectType.ADD_MAG:
				//slimeStats.magicAddative += Mathf.CeilToInt(buff.upgradeValue);
				buffText = "MAG +" + buff.upgradeValue;

				break;
			case UpgradeData.EffectType.ADD_AGI:
				//slimeStats.agilityAddative += Mathf.CeilToInt(buff.upgradeValue);
				buffText = "AGI +" + buff.upgradeValue;

				break;
			case UpgradeData.EffectType.ADD_SPR:
				//slimeStats.spiritAddative += Mathf.CeilToInt(buff.upgradeValue);
				buffText = "SPR +" + buff.upgradeValue;

				break;
			case UpgradeData.EffectType.ADD_DEF:
				//slimeStats.defenseAddative += Mathf.CeilToInt(buff.upgradeValue);
				buffText = "DEF +" + buff.upgradeValue;

				break;
			case UpgradeData.EffectType.ADD_MAXHP:
				//slimeStats.currentHealth += Mathf.CeilToInt(buff.upgradeValue);
				//slimeStats.maxHealthAddative += Mathf.CeilToInt(buff.upgradeValue);
				buffText = "HP +" + buff.upgradeValue;

				break;
		}
		GameManager._Instance.uiManager.LaunchCounter(transform.position, 0, Color.magenta, buffText);
		SetStats();

	}
	public virtual void SetStats()
	{
		gameObject.name = slimeStats.name;
		ApplyUpgrades();
		int oldHealth = slimeStats.maxHealth;
		int newHealth = slimeStats.GetHealth();
		if (oldHealth != newHealth)
		{
			slimeStats.maxHealth = newHealth;
			slimeStats.currentHealth += (newHealth - oldHealth);
		}
		//slimeStats.maxHealth = slimeStats.GetHealth();
		//slimeStats.currentHealth = slimeStats.currentHealth;
		slimeStats.damage = slimeStats.GetDamage();
		slimeStats.maxExp = slimeStats.GetExperience();
		slimeStats.currentCoin = slimeStats.GetCoin();
		slimeScale = slimeStats.GetScale();
		transform.localScale = new Vector3(slimeScale, slimeScale, slimeScale);
		if (OnStatChange != null)
			OnStatChange();
	}
	public virtual void ApplyUpgrades()
	{
		slimeStats.strengthAddative = 0;
		slimeStats.magicAddative = 0;
		slimeStats.agilityAddative = 0;
		slimeStats.spiritAddative = 0;
		slimeStats.defenseAddative = 0;
		slimeStats.maxHealthAddative = 0;
		if (slimeStats.upgrades.Count > 0)
		{
			for (int i = 0; i < slimeStats.upgrades.Count; i++)
			{
				UpgradeData buff = slimeStats.upgrades[i];
				switch (buff.upgradeEffect)
				{
					case UpgradeData.EffectType.ADD_STR:
						slimeStats.strengthAddative += Mathf.CeilToInt(buff.upgradeValue);

						break;
					case UpgradeData.EffectType.ADD_MAG:
						slimeStats.magicAddative += Mathf.CeilToInt(buff.upgradeValue);

						break;
					case UpgradeData.EffectType.ADD_AGI:
						slimeStats.agilityAddative += Mathf.CeilToInt(buff.upgradeValue);

						break;
					case UpgradeData.EffectType.ADD_SPR:
						slimeStats.spiritAddative += Mathf.CeilToInt(buff.upgradeValue);

						break;
					case UpgradeData.EffectType.ADD_DEF:
						slimeStats.defenseAddative += Mathf.CeilToInt(buff.upgradeValue);

						break;
					case UpgradeData.EffectType.ADD_MAXHP:
						slimeStats.maxHealthAddative += Mathf.CeilToInt(buff.upgradeValue);
						//slimeStats.currentHealth += Mathf.CeilToInt(buff.upgradeValue);

						break;
				}
			}
		}
		upgradesApplied = true;
	}
	public virtual void LevelUp()
	{
		slimeStats.level += 1;
		ApplyUpgrades();
		int[] preGrowth = slimeStats.growth;
		AddGrowth();

		int preHealth = slimeStats.maxHealth;
		slimeStats.maxHealth = slimeStats.GetHealth();
		int addHealth = slimeStats.maxHealth - preHealth;
		slimeStats.currentHealth += addHealth;
		string healthText = "Def +" + (slimeStats.growth[4] - preGrowth[4]) +
			"\n" + "HP +" + addHealth.ToString();
		int preDamage = slimeStats.damage;
		slimeStats.damage = slimeStats.GetDamage();
		int addDamage = slimeStats.damage - preDamage;
		string damageText = "Str +" + (slimeStats.growth[0] - preGrowth[0]) + "  Mag +" + (slimeStats.growth[1] - preGrowth[1]) +
			"\n" + "Dmg +" + addDamage;
		slimeStats.maxExp = slimeStats.GetExperience();
		slimeStats.currentExp = 0;
		slimeStats.currentCoin = slimeStats.GetCoin();
		slimeScale = slimeStats.GetScale();
		transform.DOScale(new Vector3(slimeScale, slimeScale, slimeScale), 1);
		string[] lvlUpText = new string[2] { healthText, damageText };
		StartCoroutine(ShowLevelUpText(lvlUpText));
		if (GameManager._Instance.Debugging)
		{
			//Debug.Log("LvlUpTo=(Lvl:" + slimeStats.level + "_HP:" + slimeStats.maxHealth + "_DMG:" + slimeStats.damage + "_XP:" + slimeStats.maxExp + "_SCL" + slimeScale);
		}
		if (OnStatChange != null)
			OnStatChange();
		
		//slimeStats.currentHappiness += 10;
		//this.gameObject.transform.DOScale(this.gameObject.transform.localScale * 1.1f, 1);
	}
	IEnumerator ShowLevelUpText(string[] lvlUp)
	{
		if (lvlUp.Length > 0)
		{
			for (int i = 0; i < lvlUp.Length; i++)
			{
				yield return new WaitForSeconds(0.4f);
				GameManager._Instance.uiManager.LaunchCounter(transform.position, 0, Color.magenta, lvlUp[i]);

			}
		}

	}
	public virtual void AddGrowth()
	{
		if (slimeStats.growth == null || slimeStats.growth.Length != 5)
		{
			slimeStats.growth = new int[5] { 4, 4, 4, 4, 4 };
		}
		for (int i = 0; i < slimeStats.growth.Length; ++i)
		{
			int add = 0;
			switch (slimeStats.growth[i])
			{
				case 0://S Rate
					add = 3;
					break;
				case 1://A Rate
					add = Random.Range(0, 100) > 50 ? 3 : 2;
					break;
				case 2://B RAte
					add = 2;
					break;
				case 3://C rate
					add = Random.Range(0, 100) > 50 ? 2 : 1;
					break;
				case 4://D rate
					add = 1;
					break;
			}
			switch (i)
			{
				case 0://strength
					slimeStats.strength += add;
					break;
				case 1://magic
					slimeStats.magic += add;
					break;
				case 2://agility
					slimeStats.agility += add;
					break;
				case 3://spirit
					slimeStats.spirit += add;
					break;
				case 4://defence
					slimeStats.defense += add;
					break;
			}
		}
	}
	
	#endregion
	#region Combat Logic
	public virtual bool CanAttack()
	{
		bool attack = false;
		float cd = Time.timeSinceLevelLoad - attackedLast;
		float attackCD = slimeStats.attackCD - slimeStats.GetAttackCDReduction();
		if (cd > attackCD && !isAttacking && !isMovingToSpawn && !isDead)
			attack = true;
		
		return attack;
	}
	
	public virtual void Attack(Vector3 _pos, Transform mob = null)
	{
		bool doAttack = CanAttack();
		if (!doAttack)
			return;
		attackPos = _pos;
		currentTargetMob = mob;
		if(OnAttack != null)
			OnAttack();
	}
	void ProcessAttack()
	{
		isAttacking = true;
		attackedLast = Time.timeSinceLevelLoad;
		if (stamina != null && stamina.gameObject.activeInHierarchy)
			stamina.RemoveCounter(slimeStats.attackCost);
		slimeAnim.SetInteger("AttackType", slimeStats.attackType);
		slimeAnim.SetTrigger("Attacking");
		//StartCoroutine("AttackTheTarget");				
		GameManager._Instance.soundManager.PlaySlimeAttack(slimeStats.classType);
		Vector2 dir = (attackPos - transform.position).normalized;
		float ang = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
		attackRotation = (Quaternion.AngleAxis(ang, Vector3.forward)).eulerAngles;
		attackDirection = dir.x > 0 ? 1 : -1;
		myRenderer.flipX = dir.x > 0 ? true : false;
		BaseAttack baseAtt = ReloadedAttack();
		baseAtt.Fire(attackPos);
		
		float dur = slimeStats.attackDuration;
		float dis = slimeStats.attackDistance;
		Vector2 movePos = attackPos;

		float timer = 0;
		string id = gameObject.GetInstanceID() + "Attack";
		DOTween.Kill(id);
		if (slimeStats.attackType == 1)
		{
			dur *= 0.6f;//Clutch for Ranged people		
			dir = -dir;
			dis *= 0.4f;
			DOTween.To(() => timer, x => timer = x, 1, dur).SetDelay(slimeStats.attackDelay).SetId(id).OnStart(() =>
			{
				myBody.velocity = dir * slimeStats.moveDistance;
			}).OnComplete(()=>
			{
				isAttacking = false;
			});
		}
		else
		{

			myBody.DOMove(movePos, dur * 0.9f, false).SetDelay(slimeStats.attackDelay).SetId(id).OnUpdate(()=>
			{
				//ChangeMoveDirection(Time.deltaTime);
			}).OnComplete(()=>
			{
				isAttacking = false;
			});
		}
	}
	//IEnumerator AttackTheTarget()
	//{
	//	Vector2 _direction;
	//	Vector3[] _path;
	//	Vector3 targetPos;
	//	float charge = attackCharge;
	//	previousPosition = transform.position;

	//	if (currentTargetMob != null)
	//	{
	//		_direction = (currentTargetMob.previousPosition - previousPosition).normalized;
	//		targetPos = currentTargetMob.previousPosition;
	//	}
	//	else
	//	{
	//		_direction = (attackPos - previousPosition).normalized;
	//		targetPos = attackPos;
	//	}
	//	_path = new Vector3[3] { transform.position, targetPos, transform.position };
	//	float _angle = Mathf.Atan2(_direction.y, _direction.x) * Mathf.Rad2Deg;
	//	attackRotation = (Quaternion.AngleAxis(_angle, Vector3.forward)).eulerAngles;//rotate transform towards target
	//	if (_direction.x > 0)
	//		attackDirection = 1;
	//	else
	//		attackDirection = -1;
	//	switch (slimeStats.attackType)
	//	{
	//		case 0:
	//			int hitCount = slimeStats.attackHitCount;

	//			yield return new WaitForSeconds(slimeStats.attackDelay);

	//			GameManager._Instance.soundManager.PlaySlimeAttack(0);
	//			if (attackCounter != null)
	//				attackCounter.DisconnectModule();
	//			myRenderer.transform.DOLocalJump(Vector3.zero, attackJumpHeight, attackJumpCount, slimeStats.attackDuration).SetEase(attackInEase);
	//			BaseAttack baseAtt0 = ReloadedAttack();
	//			baseAtt0.Fire(targetPos);
	//			myCollider.enabled = false;
	//			myBody.DOMove(targetPos, slimeStats.attackDuration).SetEase(attackInEase).OnComplete(() =>
	//			{
	//				if (myRendererAttfx != null)
	//					myRendererAttfx.enabled = false;
	//				if (attackEffector != null)
	//					attackEffector.enabled = false;

	//				if (attackCounter != null)
	//					attackCounter.ConnectModule(transform);
	//				if (stamina && stamina.gameObject.activeInHierarchy)
	//					stamina.ConnectModule(transform);
	//				isAttacking = false;
	//				myCollider.enabled = true;
	//			});
	//			break;
	//		case 1:
	//			BaseAttack baseAtt = ReloadedAttack();
	//			yield return new WaitForSeconds(slimeStats.attackDelay);
	//			GameManager._Instance.soundManager.PlaySlimeAttack(slimeStats.classType);
	//			baseAtt.Fire(targetPos);
	//			if (myRendererAttfx != null)
	//				myRendererAttfx.enabled = false;
	//			yield return new WaitForSeconds(slimeStats.attackDuration);
	//			isAttacking = false;
	//			if (stamina && stamina.gameObject.activeInHierarchy)
	//				stamina.ConnectModule(transform);

	//			break;
	//	}
	//}
	IEnumerator AttackDelayed()
	{
		float maxQueTime = 0.5f;
		float queTime = 0;
		while (!CanAttack())
		{
			queTime += 0.1f;
			if (queTime > maxQueTime)
			{
				attackQued = false;
				break;
			}
			yield return new WaitForEndOfFrame();
			if (!attackQued)
				break;
		}
		if (attackQued)
		{
			attackQued = false;
			//if(moveBodyQued)
			//moveBodyQued = false;
			OnAttack();
		}
		attackQued = false;
	}
	IEnumerator ResetHit(int mobID)
	{
		yield return new WaitForSeconds(slimeStats.attackHitRate);
		lastHitMobs.Remove(mobID);
	}
	public BaseAttack ReloadedAttack()
	{
		BaseAttack baseAtt = null;
		BaseMob baseMob = transform.GetComponent<BaseMob>();
		if (!GameManager._Instance.DoNotPool && attacks.Count > 0)
		{

			for (int i = 0; i < attacks.Count; ++i)
			{
				if (attacks[i] == null)
				{
					attacks.Remove(attacks[i]);
				}
				else if (!attacks[i].activeInHierarchy)
				{
					attacks[i].SetActive(true);
					baseAtt = attacks[i].GetComponent<BaseAttack>();
					break;
				}
			}
		}
		else if (GameManager._Instance.DoNotPool && attacks.Count > 0)
		{
			for (int i = 0; i < attacks.Count; i++)
			{
				if (attacks[i] != null)
					Destroy(attacks[i].gameObject);
			}
			attacks.Clear();
		}
		if (baseAtt == null)
		{
			int classType = slimeStats.classType;
			baseAtt = Instantiate(SlimeDatabase.Attacks[classType], hidden).GetComponent<BaseAttack>();
			if (!GameManager._Instance.DoNotPool)
				attacks.Add(baseAtt.gameObject);
		}

		baseAtt.transform.SetParent(hidden);
		baseAtt.parent = hidden;
		baseAtt.transform.localPosition = Vector3.zero;
		baseAtt.gameObject.SetActive(true);

		if (baseMob != null)
			baseAtt.Setup(baseMob.mobStats);
		else
			baseAtt.Setup(this);

		baseAtt.enabled = true;
		return baseAtt;
	}
	IEnumerator ResolveKnockback()
	{
		yield return new WaitForSeconds(0.4f);
		isKnockedBack = false;
	}

	public virtual IEnumerator KillTheMob()
	{
		//PlayEffects(2);

		//Set this to dead layer on input controller
		//myCollider.enabled = false;
		foreach (ParticleSystem sys in transform.GetComponentsInChildren<ParticleSystem>())
		{
			//articleSystem.MainModule main = sys.main;
			//sys.loop = false;
			sys.Stop(true);
		}
		if (GameManager._Instance.combatManager != null)
		{
			//GameManager._Instance.combatManager.playerSlime = null;
		}
		//foreach(SpriteRenderer spriteRend in transform.GetComponentsInChildren<SpriteRenderer>()) {
		//	spriteRend.enabled = false;
		//}
		yield return new WaitForSeconds(5);
		//Destroy(gameObject);//feels bad mayn :(

	}

	#endregion
	public bool CheckIfSpawning()
	{
		bool spawning = true;
		Vector3 left = new Vector3(0, 0, 0);
		Vector3 right = new Vector3(Camera.main.pixelWidth, Camera.main.pixelHeight, 0);
		if (previousPosition.x > Camera.main.ScreenToWorldPoint(left).x &&
			 previousPosition.x < Camera.main.ScreenToWorldPoint(right).x)
		{
			spawning = false;
		}
		return spawning;
	}

	
	public override void OnDrawGizmosSelected()
	{
		//forplaypen
		base.OnDrawGizmosSelected();
		Gizmos.color = Color.cyan;
		// Gizmos.DrawWireSphere(transform.position, slimeRadius);//where can i poop?
		Gizmos.DrawWireSphere(transform.position, moveRange);//where can i move?
		Gizmos.DrawLine(transform.position, transform.position + (Vector3)moveDirection);
		//Gizmos.DrawWireSphere(moveDirection, slimeRadius);//where can i move?
		//for combat
		Gizmos.color = Color.red;

		Gizmos.DrawWireSphere(transform.position, slimeStats.attackRadius);

	}

}
