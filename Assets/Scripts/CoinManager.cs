﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class CoinManager : MonoBehaviour {
	public int maxCoins = 20;
	public Color lowColor;
	public Color hightColor;
	public Vector2 lowToHighSize = new Vector2(1, 2);

	private ParticleSystem sys;
    private ParticleSystem.Particle[] coins;
    private List<int> coinValues = new List<int>();
	private List<ParticleSystem.Particle> oldCoins = new List<ParticleSystem.Particle>();
    private int coinsOnScreen = 0;
    public Transform coinToucher;
    public float coinSpeed = 20;
	public float coinLerp = 8f;
	public float coinSpawnRadius = 1;
	
	private Vector3 screenCorner;
	private int highestValue;
	private List<ParticleSystem> extraSys = new List<ParticleSystem>();
	public bool poolParticles;
    //private void LateUpdate()
    //{
    //    InitializeIfNeeded();

    //    // GetParticles is allocation free because we reuse the m_Particles buffer between updates
    //    int numParticlesAlive = m_System.GetParticles(m_Particles);

    //    // Change only the particles that are alive
    //    for (int i = 0; i < numParticlesAlive; i++)
    //    {
    //        m_Particles[i].velocity += Vector3.up * m_Drift;
    //    }

    //    // Apply the particle changes to the particle system
    //    m_System.SetParticles(m_Particles, numParticlesAlive);
    //}

    //void InitializeIfNeeded()
    //{
    //    if (m_System == null)
    //        m_System = GetComponent<ParticleSystem>();

    //    if (m_Particles == null || m_Particles.Length < m_System.maxParticles)
    //        m_Particles = new ParticleSystem.Particle[m_System.maxParticles];
    //}
    // Use this for initialization
    void Start ()
    {
        sys = GetComponentInChildren<ParticleSystem>();
        coins = new ParticleSystem.Particle[sys.maxParticles];
		screenCorner = Camera.main.ScreenToWorldPoint(new Vector2(0, Camera.main.pixelHeight));
	}


    // Update is called once per frame
    void LateUpdate () {
		//foreach (ParticleSystem sys in extraSys)
		//{
		//	coinsOnScreen = sys.GetParticles(coins);
		//	if (coinsOnScreen > 0)
		//	{
		//		for (int i = 0; i < coinsOnScreen; i++)
		//		{
		//			Vector3 posTo = Camera.main.ScreenToWorldPoint(new Vector2(0, Camera.main.pixelHeight));
		//			Vector3 dir = posTo - coins[i].position;
		//			coins[i].velocity = Vector3.Lerp(coins[i].velocity, dir * coinSpeed, Time.deltaTime * coinLerp);
		//		}
		//		sys.SetParticles(coins, coinsOnScreen);
		//	}
		//}
	}
    public void SpawnCoin(Vector3 pos, int value)
    {
		GameManager._Instance.GameAddCoin(value);

		if (value > highestValue)
			highestValue = value;
		if (value == 0)
			value = 1;
		if (highestValue == 0)
			highestValue = 1;
		float perc = value / highestValue;
		//ParticleSystem curSys = null;
		//if (!poolParticles)
		//{
		//	GameObject newSys = Instantiate(sys.gameObject, transform);
		//	curSys = newSys.GetComponent<ParticleSystem>();
		//}
		//else
		//{
		//	if (extraSys.Count > 0)
		//	{
		//		for (int i = 0; i < extraSys.Count; i++)
		//		{
		//			if (extraSys[i].isStopped)
		//			{
		//				curSys = extraSys[i];
		//				break;
		//			}
		//		}
		//	}
		//	if (curSys == null)
		//	{
		//		GameObject newSys = Instantiate(sys.gameObject, transform);
		//		curSys = newSys.GetComponent<ParticleSystem>();
		//		extraSys.Add(curSys);
		//	}
		//}
		sys.transform.position = pos;

		ParticleSystem.MainModule main = sys.main;
		main.startColor = Color.Lerp(lowColor, hightColor, perc);
		
		
		main.startSize = Mathf.Lerp(lowToHighSize.x,  lowToHighSize.y, perc);
		int emitAmount = Mathf.CeilToInt(value * perc);
		if (emitAmount == 0)
			emitAmount = 1;
      sys.Emit(emitAmount);
		//coinsOnScreen = curSys.GetParticles(coins);
		//if (coinsOnScreen > 0)
		//{
		//	for (int i = 0; i < coinsOnScreen; i++)
		//	{

		//			coins[i].position = pos + (Vector3)(Random.insideUnitCircle * coinSpawnRadius);

		//	}
		//	curSys.SetParticles(coins, coinsOnScreen);
		//}
		//coinsOnScreen = sys.GetParticles(coins);
		// int coinsOnScreen = sys.GetParticles(coins);
		// Debug.Log("Spawnd coin:"+ coinsOnScreen);
		//ins[sys.GetParticles(coins) - 1];
		//coins[coinsOnScreen - 1].randomSeed = (uint)value;
		//coins[coinsOnScreen - 1].position = pos;
		//sys.SetParticles(coins, coinsOnScreen);


		//if (!poolParticles)
		//	StartCoroutine(DelayDestroy(curSys));
    }
	IEnumerator DelayDestroy(ParticleSystem sys)
	{
		yield return new WaitForSeconds(5);
		
		if (extraSys.Contains(sys))
			extraSys.Remove(sys);
		Destroy(sys.gameObject);
		
	}
    void OnParticleTrigger()
    {
        ParticleSystem ps = GetComponent<ParticleSystem>();

        // particles
        List<ParticleSystem.Particle> enter = new List<ParticleSystem.Particle>();
        List<ParticleSystem.Particle> exit = new List<ParticleSystem.Particle>();

        // get
        int numEnter = ps.GetTriggerParticles(ParticleSystemTriggerEventType.Enter, enter);
        //int numExit = ps.GetTriggerParticles(ParticleSystemTriggerEventType.Exit, exit);

        // iterate
        for (int i = 0; i < numEnter; i++)
        {
            ParticleSystem.Particle p = enter[i];
            Vector3 endPos = Camera.main.ScreenToWorldPoint(new Vector3(0, Camera.main.pixelHeight, 0));
            Vector3 dir = (endPos - p.position).normalized;
            p.remainingLifetime = 0.5f;
            p.velocity = dir * coinSpeed ;
            Vector3 pPos = p.position;
            Debug.Log("Coin Added:" + p.randomSeed);
            int id = i;
            Vector3[] coinPath = new Vector3[] { p.position, (UnityEngine.Random.insideUnitCircle * 2), endPos };
            DOTween.To(() => p.position, x => p.position = x, endPos, 0.5f)
                .OnUpdate(()=> 
                {
                    //int coinsOnScreen = sys.GetParticles(coins);
                    enter[id] = p;
                    //sys.SetParticles(coins, coinsOnScreen);

                })
            .OnComplete(() =>
            {
                GameManager._Instance.GameAddCoin((int)p.randomSeed);
            });
            enter[i] = p;
            //enter[i] = p;
        }
        //for (int i = 0; i < numExit; i++)
        //{
        //    ParticleSystem.Particle p = exit[i];
        //    p.startColor = new Color32(0, 255, 0, 255);
        //    exit[i] = p;
        //}

        // set
        ps.SetTriggerParticles(ParticleSystemTriggerEventType.Enter, enter);
        //ps.SetTriggerParticles(ParticleSystemTriggerEventType.Exit, exit);
    }
}
