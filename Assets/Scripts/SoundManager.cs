﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class SoundManager : MonoBehaviour
{

    [Header("BackgroundSounds")]
    private int lastBackground;
    private bool backgroundQued;
    public float crossFadePos = 5f;
    public float crossfadeTime = 1f;
    public Ease crossFadeEase = Ease.InOutCubic;
    public int backgroundMaxRepeat = 1;
    [Range(0.05f, 1f)]
    public float backgroundVolume = 0.25f;
    public AudioClip[] backgroundPlaypenMusic;
    public AudioClip[] backgroundCombatMusic;

    [Header("SlimeSounds")]
    [Range(0.01f, 0.5f)]
    public float slimeChargeVol = 0.15f;
    public AudioClip slimeCharge;
    [Range(0.01f, 0.5f)]
    public float slimeEatVol = 0.15f;
    public AudioClip slimeEat;
    [Range(0.01f, 0.5f)]
    public float slimeJumpVol = 0.15f;
    public AudioClip slimeJump;
	[Range(0.01f, 0.5f)]
	public float slimeOnHitVol = 0.2f;
	public AudioClip slimeOnHit;
	[Range(0.01f, 0.5f)]
	public float slimeOnHealVol = 0.2f;
	public AudioClip slimeOnHeal;
	[Range(0.01f, 0.5f)]
    public float slimeAttackVol = 0.3f;
    public AudioClip[] slimeAttack;


    [Header("MobSounds")]
    public float mobDefaultPitch = 1f;
    [Range(0.01f, 0.5f)]
    public float mobTalkVol = 0.15f;
    public AudioClip mobTalk;
    [Range(0.01f, 0.5f)]
    public float mobWalkVol = 0.15f;
    public float mobWalkPitch = 1.6f;
    public float mobWalkOffset = 0.4f;
    public AudioClip mobWalk;
    [Range(0.01f, 0.5f)]
    public float mobOnHitVol = 0.15f;
    public AudioClip[] mobOnHit;
    [Range(0.01f, 0.5f)]
    public float mobAttackVol = 0.15f;
    public AudioClip[] mobAttack;
	[Range(0.01f, 0.5f)]
	public float mobDeathScreamVol = 0.15f;
	[Range(0.01f, 2f)]
	public float[] mobDeathScreamPit = {0.5f,0.8f };
	public AudioClip[] mobDeathScream;
	[Range(0.01f, 0.5f)]
	public float mobDeathExplodeVol = 0.15f;
	public AudioClip mobDeathExplode;

	[Header("UISounds")]
    [Range(0.01f, 0.5f)]
    public float uiClickVol = 0.15f;
    public AudioClip uiClick;
    [Range(0.01f, 0.5f)]
    public float uiAcceptVol = 0.15f;
    public AudioClip uiAccept;
    [Range(0.01f, 0.5f)]
    public float uiDeclineVol = 0.15f;
    public AudioClip uiDecline;
    [Range(0.01f, 0.5f)]
    public float uiErrorVol = 0.15f;
    public AudioClip uiError;
    [Range(0.01f, 0.5f)]
    public float uiMoneyVol = 0.15f;
    public AudioClip uiMoney;

    #region Audio Sources
    public AudioSource[] myBackAudio
    {
        get
        {
            if (myBackAS == null)
                myBackAS = transform.GetChild(0).transform.GetComponents<AudioSource>();
            return myBackAS;
        }
        set
        {
            myBackAS = value;
        }
    }
    private AudioSource[] myBackAS;
    public AudioSource[] mySlimeAudio
    {
        get
        {
            if (mySlimeAS == null)
                mySlimeAS = transform.GetChild(1).transform.GetComponents<AudioSource>();
            return mySlimeAS;
        }
        set
        {
            mySlimeAS = value;
        }
    }
    private AudioSource[] mySlimeAS;
    public AudioSource[] myMobAudio
    {
        get
        {
            if (myMobAS == null)
                myMobAS = transform.GetChild(2).transform.GetComponents<AudioSource>();
            return myMobAS;
        }
        set
        {
            myMobAS = value;
        }
    }
    private AudioSource[] myMobAS;
    public AudioSource[] myUIAudio
    {
        get
        {
            if (myUIAS == null)
                myUIAS = transform.GetChild(3).transform.GetComponents<AudioSource>();
            return myUIAS;
        }
        set
        {
            myUIAS = value;
        }
    }
	private AudioSource nextMobAudio
	{
		get
		{
			AudioSource mobAudio = null;
			if (myMobAudio.Length > 0)
			{
				bool found = false;
				for (int i = 0; i < myMobAudio.Length; i++)
				{
					if (!myMobAudio[i].isPlaying)
						mobAudio = myMobAudio[i];
				}
				if (mobAudio == null)
				{
					mobAudio = transform.GetChild(3).gameObject.AddComponent<AudioSource>();
					myUIAS = transform.GetChild(3).transform.GetComponents<AudioSource>();
				}
			}
			return mobAudio;
		}
	}
    private AudioSource[] myUIAS;
    #endregion
    // Use this for initialization
    void Start ()
    {
        myBackAudio = transform.GetChild(0).transform.GetComponents<AudioSource>();
        mySlimeAudio = transform.GetChild(1).transform.GetComponents<AudioSource>();
        myMobAudio = transform.GetChild(2).transform.GetComponents<AudioSource>();
        myUIAudio = transform.GetChild(3).transform.GetComponents<AudioSource>();

    }

    // Update is called once per frame
    void Update () {
		
	}
    public void PlayBackgroundMusic()
    {
        int last = 0;
        int next = 0;
        int repeat = Random.Range(1, backgroundMaxRepeat);
        for(int i = 0; i < myBackAudio.Length; i++)
        {
            if(myBackAudio[i].isPlaying)
            {
                last = i;
            } else
            {
                next = i;
            }
        }
                FadeOut(myBackAudio[last]);
        if(GameManager._Instance.combatManager == null)
        {
            int mrRandom = Random.Range(0, backgroundPlaypenMusic.Length);
            while(mrRandom == lastBackground)
            {
                mrRandom = Random.Range(0, backgroundPlaypenMusic.Length);
            }
            lastBackground = mrRandom;
            myBackAudio[next].clip = backgroundPlaypenMusic[mrRandom];
            //FadeIn(myBackAudio[next]);
        }else
        {
            int mrRandom = Random.Range(0, backgroundCombatMusic.Length);
            while (mrRandom == lastBackground)
            {
                mrRandom = Random.Range(0, backgroundCombatMusic.Length);
            }
            lastBackground = mrRandom;
            myBackAudio[next].clip = backgroundCombatMusic[mrRandom];
        }
            FadeIn(myBackAudio[next]);
        if (!backgroundQued)
            StartCoroutine(PlayNextBackground((myBackAudio[next].clip.length - crossFadePos) * repeat));

    }
    void FadeOut(AudioSource source)
    {
        source.volume = backgroundVolume;
        source.DOFade(0, crossfadeTime).SetEase(crossFadeEase).OnComplete(()=>
        {
            source.Stop();
        });
    }
    void FadeIn(AudioSource source)
    {
        source.volume = 0;
        source.DOFade(backgroundVolume, crossfadeTime).SetEase(crossFadeEase).OnStart(() =>
        {
            source.Play();
        });
    }
    IEnumerator PlayNextBackground(float time)
    {
        backgroundQued = true;
        yield return new WaitForSeconds(time);
        backgroundQued = false;
        PlayBackgroundMusic();
    }
    #region Slime Sound Methods
    private float lastCharge;
    public void PlaySlimeCharge()
    {
        for (int i = 0; i < mySlimeAudio.Length; i++)
        {
            if (!mySlimeAudio[i].isPlaying || mySlimeAudio[i].clip == slimeCharge)
            {
                if (mySlimeAudio[i].isPlaying)
                {
                    mySlimeAudio[i].volume += Time.deltaTime;
                    mySlimeAudio[i].time = lastCharge += Time.deltaTime;

                } else
                {
                    lastCharge = 0;
                    mySlimeAudio[i].volume = slimeChargeVol;
                    mySlimeAudio[i].time = 0;
                    mySlimeAudio[i].clip = slimeCharge;
                    mySlimeAudio[i].Play();
                }
                break;
            }
        }
    }
    public void PlaySlimeEat()
    {
        for (int i = 0; i < mySlimeAudio.Length; i++)
        {
            if (!mySlimeAudio[i].isPlaying || mySlimeAudio[i].clip == slimeEat)
            {
                mySlimeAudio[i].volume = slimeEatVol;
                mySlimeAudio[i].time = 0;
                mySlimeAudio[i].clip = slimeEat;
                mySlimeAudio[i].Play();
                break;
            }
        }
    }
    public void PlaySlimeJump()
    {
        for (int i = 0; i < mySlimeAudio.Length; i++)
        {
            if(!mySlimeAudio[i].isPlaying || mySlimeAudio[i].clip == slimeJump)
            {
                mySlimeAudio[i].volume = slimeJumpVol;
                mySlimeAudio[i].time = 0;
                mySlimeAudio[i].clip = slimeJump;
                mySlimeAudio[i].Play();
                break;
            }
        }
    }
    public void PlaySlimeOnHit()
    {
        for (int i = 0; i < mySlimeAudio.Length; i++)
        {
            if (!mySlimeAudio[i].isPlaying || mySlimeAudio[i].clip == slimeOnHit)
            {
                mySlimeAudio[i].volume = slimeOnHitVol;
                mySlimeAudio[i].time = 0;
                mySlimeAudio[i].clip = slimeOnHit;
                mySlimeAudio[i].Play();
                break;
            }
        }
    }
	public void PlaySlimeOnHeal()
	{
		for(int i = 0; i < mySlimeAudio.Length; i++)
		{
			if(!mySlimeAudio[i].isPlaying || mySlimeAudio[i].clip == slimeOnHeal)
			{
				mySlimeAudio[i].volume = slimeOnHealVol;
				mySlimeAudio[i].time = 0;
				mySlimeAudio[i].clip = slimeOnHeal;
				mySlimeAudio[i].Play();
				break;
			}
		}
	}
	public void PlaySlimeAttack(int attack = 0)
    {
        if (attack >= slimeAttack.Length)
            attack = 0;
        for (int i = 0; i < mySlimeAudio.Length; i++)
        {
            if (!mySlimeAudio[i].isPlaying || mySlimeAudio[i].clip == slimeAttack[attack])
            {
                mySlimeAudio[i].volume = slimeAttackVol;
                mySlimeAudio[i].time = 0;
                mySlimeAudio[i].clip = slimeAttack[attack];
                mySlimeAudio[i].Play();
                break;
            }
        }
    }
    #endregion
    #region Mob Sound Methods

    
    public void PlayMobTalk()
    {
        for(int i = 0; i < myMobAudio.Length;i++)
        {
            if(!myMobAudio[i].isPlaying || myMobAudio[i].clip == mobTalk)
            {
                myMobAudio[i].pitch = mobDefaultPitch;

                myMobAudio[i].volume = mobTalkVol;
                myMobAudio[i].clip = mobTalk;
                myMobAudio[i].time = 0;
                myMobAudio[i].Play();
                break;
            }
        }
    }
    public void PlayMobWalk()
    {
        for (int i = 0; i < myMobAudio.Length; i++)
        {
            if (!myMobAudio[i].isPlaying || myMobAudio[i].clip == mobWalk)
            {
                myMobAudio[i].pitch = mobWalkPitch;
                myMobAudio[i].volume = mobWalkVol;
                if(myMobAudio[i].isPlaying)
                myMobAudio[i].time = mobWalkOffset;
                    else
                {
                myMobAudio[i].clip = mobWalk;
                    myMobAudio[i].time = mobWalkOffset;
                    myMobAudio[i].Play();

                }
                break;
            }
        }
    }
    public void PlayMobOnHit()
    {
		AudioSource nextAudio = nextMobAudio;
		if (nextAudio != null)
		{
			nextAudio.pitch = mobDefaultPitch;
			nextAudio.volume = mobOnHitVol;
			nextAudio.clip = mobOnHit[Random.Range(0,mobOnHit.Length)];
			nextAudio.time = 0;
			nextAudio.Play();
		}
		//for (int i = 0; i < myMobAudio.Length; i++)
  //      {
  //          if (!myMobAudio[i].isPlaying || myMobAudio[i].clip == mobOnHit)
  //          {
  //              myMobAudio[i].pitch = mobDefaultPitch;

  //              myMobAudio[i].volume = mobOnHitVol;
  //              myMobAudio[i].clip = mobOnHit;
  //              myMobAudio[i].time = 0;
  //              myMobAudio[i].Play();
  //              break;
  //          }
  //      }
    }
    public void PlayMobAttack(int attack = 0)
    {
		AudioSource nextAudio = nextMobAudio;
		if (nextAudio != null)
		{
			nextAudio.pitch = mobDefaultPitch;
			nextAudio.volume = mobAttackVol;
			nextAudio.clip = mobAttack[Random.Range(0,mobAttack.Length)];
			nextAudio.time = 0;
			nextAudio.Play();
		}
		//if (attack > mobAttack.Length - 1)
  //          attack = 0;
  //      for (int i = 0; i < myMobAudio.Length; i++)
  //      {
  //          if (!myMobAudio[i].isPlaying || myMobAudio[i].clip == mobAttack[attack])
  //          {
  //              myMobAudio[i].pitch = mobDefaultPitch;

  //              myMobAudio[i].volume = mobAttackVol;
  //              myMobAudio[i].clip = mobAttack[attack];
  //              myMobAudio[i].time = 0;
  //              myMobAudio[i].Play();
  //              break;
  //          }
  //      }
    }
	public void PlayMobDeathScream(int gender = 0)
	{
		AudioSource nextAudio = nextMobAudio;
		if (nextAudio != null)
		{
			nextAudio.pitch = mobDeathScreamPit[gender];
			nextAudio.volume = mobDeathScreamVol;
			nextAudio.clip = mobDeathScream[Random.Range(0,mobDeathScream.Length)];
			nextAudio.time = 0;
			nextAudio.Play();
		}
		//if(nextMobAudio != null)

		//for (int i = 0; i < myMobAudio.Length; i++)
		//{
		//	if (!myMobAudio[i].isPlaying || myMobAudio[i].clip == mobDeathScream)
		//	{
		//		myMobAudio[i].pitch = mobDeathScreamPit[gender];
		//		myMobAudio[i].volume = mobDeathScreamVol;
		//		myMobAudio[i].clip = mobDeathScream;
		//		myMobAudio[i].time = 0;
		//		myMobAudio[i].Play();
		//		break;
		//	}
		//}
	}
	public void PlayMobDeathExplode()
	{
		AudioSource nextAudio = nextMobAudio;
		if (nextAudio != null)
		{
			nextAudio.pitch = mobDefaultPitch;
			nextAudio.volume = mobDeathExplodeVol;
			nextAudio.clip = mobDeathExplode;
			nextAudio.time = 0;
			nextAudio.Play();
		}
		//for (int i = 0; i < myMobAudio.Length; i++)
		//{
		//	if (!myMobAudio[i].isPlaying || myMobAudio[i].clip == mobDeathExplode)
		//	{
		//		myMobAudio[i].pitch = mobDefaultPitch;
		//		myMobAudio[i].volume = mobDeathExplodeVol;
		//		myMobAudio[i].clip = mobDeathExplode;
		//		myMobAudio[i].time = 0;
		//		myMobAudio[i].Play();	
		//		break;
		//	}
		//}
	}
	#endregion
	#region UI Sound Methods
	public void PlayUIClick()
    {
        for (int i = 0; i < myUIAudio.Length; i++)
        {
            if (!myUIAudio[i].isPlaying || myUIAudio[i].clip == uiClick)
            {
                myUIAudio[i].volume = uiClickVol;
                myUIAudio[i].clip = uiClick;
                myUIAudio[i].time = 0;
                myUIAudio[i].Play();
                break;
            }
        }
    }
    public void PlayUIAccept()
    {
        for (int i = 0; i < myUIAudio.Length; i++)
        {
            if (!myUIAudio[i].isPlaying || myUIAudio[i].clip == uiAccept)
            {
                myUIAudio[i].volume = uiAcceptVol;
                myUIAudio[i].clip = uiAccept;
                myUIAudio[i].time = 0;
                myUIAudio[i].Play();
                break;
            }
        }
    }
    public void PlayUIDecline()
    {
        for (int i = 0; i < myUIAudio.Length; i++)
        {
            if (!myUIAudio[i].isPlaying || myUIAudio[i].clip == uiDecline)
            {
                myUIAudio[i].volume = uiDeclineVol;
                myUIAudio[i].clip = uiDecline;
                myUIAudio[i].time = 0;
                myUIAudio[i].Play();
                break;
            }
        }
    }
    public void PlayUIError()
    {
        for (int i = 0; i < myUIAudio.Length; i++)
        {
            if (!myUIAudio[i].isPlaying || myUIAudio[i].clip == uiError)
            {
                myUIAudio[i].volume = uiErrorVol;
                myUIAudio[i].clip = uiError;
                myUIAudio[i].time = 0;
                myUIAudio[i].Play();
                break;
            }
        }
    }
    public void PlayUIMoney()
    {
        for (int i = 0; i < myUIAudio.Length; i++)
        {
            if (!myUIAudio[i].isPlaying || myUIAudio[i].clip == uiMoney)
            {
                myUIAudio[i].volume = uiMoneyVol;
                myUIAudio[i].clip = uiMoney;
                myUIAudio[i].time = 0;
                myUIAudio[i].Play();
                break;
            }
        }
    }
    #endregion

}
