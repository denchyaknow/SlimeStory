﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIRefs_Hud : MonoBehaviour
{

	public Image curImage;
	public Text curName;
	public Text curLvl;
	public Text curCoin;

	public RectTransform combatHudRoot;
	public Slider healthSlider;
	public Text healthText;
	public Slider expSlider;
	public Text mapTitle;
	public _2dxFX_Fire mapDifficulty;
}
