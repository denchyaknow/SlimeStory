﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class BaseHelpTipSlime : MonoBehaviour {
	public Text name;
	public Text attack;
	public Text description;
	public Text strength;
	public Text magic;
	public Text agility;
	public Text spirit;
	public Text defense;
	private void OnEnable()
	{

	}
	public void Show(float time, float delay, Ease ease)
	{
		transform.DOScaleY(1, time).SetDelay(delay).SetId(transform).SetEase(ease);
	}
	public void Hide(float time, float delay, Ease ease)
	{
		transform.DOScaleY(0, time).SetDelay(delay).SetId(transform).SetEase(ease);
	}
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
