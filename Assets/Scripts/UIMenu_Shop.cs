﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class UIMenu_Shop : MonoBehaviour
{
	public Vector2 itemSize_Scrollable = new Vector2(155,155);
	public Vector2 itemSize_NonScrollable = new Vector2(175,175);
	[Header("Open/Close Tween")]
	public float moveTime = 0.8f;
	public Ease moveEase = Ease.InExpo;
	private bool showingScrollbar = false;
	private UIMenuManager mg;
	private UIMenuManager manager
	{
		get
		{
			if(mg == null)
			{
				if(GameManager._Instance != null && GameManager._Instance.uiManager != null)
					mg = GameManager._Instance.uiManager;
				else
					mg = transform.GetComponentInParent<UIMenuManager>();
			}
			return mg;
		}
	}
	private UIRefs_Shop refs;
	public UIMenuManager.Delegate OnChangeTab;
	private Vector2 visiblePos = Vector2.zero;
	private Vector2 hiddenPos;
	private bool isLoading = false;
	private bool isHidden = false;
	private int currentTab = 0;
	private void OnEnable()
	{
		refs = transform.GetComponent<UIRefs_Shop>();
		OnChangeTab += () =>
		{
			HideScrollbar();
			ShowScrollbar();
		};
	}
	private void Awake()
	{
		isLoading = true;
		isHidden = true;
		StartCoroutine(SetupButtons());
	}

	void Start ()
	{
		hiddenPos = new Vector2(refs.rootMenuPivot.rect.width, 0);
		refs.rootMenuPivot.anchoredPosition = hiddenPos;
	}
	
	void Update ()
	{
		//Debug.Log(refs.tabMenus[currentTab].GetChild(0).transform.childCount);
		//if (refs.tabMenus[currentTab].GetChild(0).transform.childCount > 12  && !refs.scrollBar.gameObject.activeInHierarchy)
		//{

		//	refs.scrollBar.gameObject.SetActive(false);
		//	//ShowScrollbar();
		//}
		//else if (refs.tabMenus[currentTab].GetChild(0).transform.childCount < 13 && refs.scrollBar.gameObject.activeInHierarchy)
		//{
		//	//HideScrollbar();
		//	refs.scrollBar.gameObject.SetActive(true);
		//}
	}
	public void OpenMenu()
	{
		if(isHidden && !DOTween.IsTweening(refs.rootMenuPivot))
		{
			isHidden = false;
			refs.openButton.gameObject.SetActive(false);
			refs.closeButton.gameObject.SetActive(true);
			DOTween.Kill(refs.rootMenuPivot);
			refs.rootButtonPivot.DOAnchorPosY(0, moveTime, false).SetEase(Ease.InBounce).SetDelay(moveTime);
			refs.rootMenuPivot.DOAnchorPosX(visiblePos.x, moveTime, true).SetId(refs.rootMenuPivot).SetEase(moveEase).OnStart(() =>
			{
				StartCoroutine(OpenTab(currentTab));
			});
			ChangeCurrentTab(currentTab);
		}
	}
	public void CloseMenu()
	{
		if(!isHidden && !DOTween.IsTweening(refs.rootMenuPivot))
		{
			isHidden = true;
			refs.closeButton.gameObject.SetActive(false);
			refs.openButton.gameObject.SetActive(true);
			DOTween.Kill(refs.rootMenuPivot);
			refs.rootMenuPivot.DOAnchorPosX(hiddenPos.x, moveTime, true).SetId(refs.rootMenuPivot).SetEase(moveEase).OnStart(()=>
			{
				
			}).OnComplete(() =>
			{
				refs.rootButtonPivot.anchoredPosition = new Vector2(0, refs.rootButtonPivot.rect.height * 3);
				//HideScrollbar(true);
			});
		}
	}
	void UpdateScroller(float scrollPos)
	{
		ScrollRect scroller = refs.tabMenus[currentTab].GetComponent<ScrollRect>();
		scroller.verticalNormalizedPosition = scrollPos;
	}

	private void ShowScrollbar(int tabID = -1)
	{
		Debug.Log("Eanabled");

		int tab = currentTab;
		if(tabID != -1)
			tab = tabID;
		//Debug.Log("Showing Scrollbar for " + tab.ToString());
		if (refs.tabMenus[currentTab].GetChild(0).transform.childCount > 12)
		{
			//DOTween.Kill(refs.scrollBar);
			showingScrollbar = true;
			int butCount = refs.tabMenus[tab].GetChild(0).transform.childCount;
			ScrollRect scroller = refs.tabMenus[tab].GetComponent<ScrollRect>();
			GridLayoutGroup grid = scroller.GetComponentInChildren<GridLayoutGroup>();
			grid.cellSize = itemSize_Scrollable;
			if (refs.scrollBar != null)
			{
				refs.scrollBar.gameObject.SetActive(true);
				//refs.scrollBar.transform.DOScaleX(1, 0.6f).SetEase(Ease.OutExpo).SetId(refs.scrollBar);
				refs.scrollBar.onValueChanged.RemoveAllListeners();
				refs.scrollBar.onValueChanged.AddListener(UpdateScroller);


			}
		}
	}
	
	private void HideScrollbar()
	{
		//Debug.Log("Disabled");
		if (refs.tabMenus[currentTab].GetChild(0).transform.childCount < 13)
		{
			showingScrollbar = false;
			//DOTween.Kill(refs.scrollBar);
			ScrollRect scroller = refs.tabMenus[currentTab].GetComponent<ScrollRect>();
			GridLayoutGroup grid = scroller.GetComponentInChildren<GridLayoutGroup>();
			grid.cellSize = itemSize_NonScrollable;
			refs.scrollBar.value = 1;
			refs.scrollBar.gameObject.SetActive(false);
			//refs.scrollBar.transform.DOScaleX(0, 0.6f).SetEase(Ease.OutExpo).SetId(refs.scrollBar);

		}
	}
	private bool openingTab = false;
	private bool loadingButtons = false;
	public void ChangeCurrentTab(int tabID)
	{
		
		if(refs.tabMenus.Length > 0)
		{
			CloseAllTabs(tabID);
			if(currentTab != tabID)
			{
				StartCoroutine(OpenTab(tabID));
				currentTab = tabID;
			}
		}
		if (OnChangeTab != null)
			OnChangeTab();
	}
	private void CloseAllTabs(int butTab = -1)
	{
		if(openingTab && currentTab != butTab)
		{
			StopCoroutine(OpenTab(currentTab));
		}
		for(int i = 0; i < refs.tabMenus.Length; ++i)
		{
			if(currentTab != butTab && i != butTab)
			CloseTab(i);
		}
	}
	private void CloseTab(int tabID)
	{
		UIStoreButtonManager menu = refs.tabMenus[tabID].transform.GetComponentInChildren<UIStoreButtonManager>();
		
		DOTween.Kill(menu.gameObject.GetInstanceID());
		for(int i = 0; i < menu.listButtons.Count; i++)
		{
			menu.listButtons[i].buttMain.image.color = new Color(255, 255, 255, 0);
			menu.listButtons[i].buttMain.gameObject.SetActive(false);
		}
		//Debug.Log("Closed tab " + tabID.ToString());
	}
	private IEnumerator OpenTab(int tabID)
	{
		while (loadingButtons)
		{
			yield return new WaitForSeconds(0.05f);
		}
		EnableTabButtons(tabID);
		UIStoreButtonManager menu = refs.tabMenus[tabID].transform.GetComponentInChildren<UIStoreButtonManager>();
		for(int i = 0; i < menu.listButtons.Count; i++)
		{
			menu.listButtons[i].buttMain.image.DOFade(1,0.3f).SetId(menu.gameObject.GetInstanceID());
			yield return new WaitForEndOfFrame();
		}
		//Debug.Log("Opened tab " + tabID.ToString());
		openingTab = false;
	}
	private void EnableTabButtons(int tabID)
	{
		UIStoreButtonManager menu = refs.tabMenus[tabID].transform.GetComponentInChildren<UIStoreButtonManager>();
		for(int i = 0; i < menu.listButtons.Count; i++)
		{
			menu.listButtons[i].buttMain.image.color = new Color(255, 255, 255, 0);
			menu.listButtons[i].buttMain.gameObject.SetActive(true);
		}
	}
	private IEnumerator SetupButtons()
	{
		loadingButtons = true;
		foreach(UIStoreButtonManager buttMan in transform.GetComponentsInChildren<UIStoreButtonManager>())
		{//When ever we switch tabs hide all opened up buttons
			UIStoreButtonManager man = buttMan;
			OnChangeTab += man.ResetAllButtons;
		}
		yield return new WaitForEndOfFrame();
		#region Upgrades
		if(SlimeDatabase.Upgrades.Length > 0)
		{
			for(int i = 0; i < SlimeDatabase.Upgrades.Length; i++)
			{
				GameObject butt = (GameObject)Instantiate(Resources.Load("GameObjects/UI/ShopItemButton"), refs.tabMenus[2].transform.GetChild(0).transform);
				butt.transform.localPosition = Vector3.zero;
				Button upgradeButtMain = butt.transform.GetComponent<UIStoreButton>().buttMain;
				Button upgradeButtCoin = butt.transform.GetComponent<UIStoreButton>().buttCoin;
				int index = i;

				BaseHelpTipText helpText = upgradeButtMain.transform.GetComponent<BaseHelpTipText>();
				helpText.upgradeData = SlimeDatabase.Upgrades[index].GetComponent<BaseUpgrade>().upgradeData;

				upgradeButtMain.onClick.AddListener(() =>
				{
					upgradeButtMain.GetComponent<UIStoreButton>().ButtonClick();
				});
				upgradeButtCoin.onClick.AddListener(() =>
				{
					int cost = SlimeDatabase.Upgrades[index].GetComponent<BaseUpgrade>().upgradeData.goldCost;
					if(GameManager._Instance.GameRemoveCoin(cost))
					{
						GameObject newBuff = Instantiate(SlimeDatabase.Upgrades[index], GameManager._Instance.garbageCan);
						newBuff.transform.position = (upgradeButtMain.transform.position);
						BaseUpgrade buff = newBuff.GetComponent<BaseUpgrade>();
						BaseSlime slime = GameManager._Instance.GetSelectedSlime();
						buff.UpgradeSlime(slime);
						GameManager._Instance.soundManager.PlayUIMoney();
					} else
					{
						GameManager._Instance.soundManager.PlayUIError();
						upgradeButtMain.GetComponent<UIStoreButton>().ButtonClick();
					}
				});
				Image imageTo = upgradeButtMain.transform.GetChild(0).transform.GetComponent<Image>();
				SpriteRenderer renderFrom = SlimeDatabase.Upgrades[i].GetComponentInChildren<SpriteRenderer>();
				manager.CopySprite(renderFrom, null, null, imageTo);
				upgradeButtMain.transform.GetChild(1).transform.GetComponent<Text>().text = manager.FormatedNumber(SlimeDatabase.Upgrades[i].GetComponent<BaseUpgrade>().upgradeData.goldCost);
			}
		}
		#endregion
		yield return new WaitForEndOfFrame();
		#region Food
		if(SlimeDatabase.Food.Length > 0)
		{
			for(int i = 0; i < SlimeDatabase.Food.Length; i++)
			{
				GameObject butt = (GameObject)Instantiate(Resources.Load("GameObjects/UI/ShopItemButton"), refs.tabMenus[1].transform.GetChild(0).transform);
				butt.transform.localPosition = Vector3.zero;
				Button foodButtMain = butt.transform.GetComponent<UIStoreButton>().buttMain;
				Button foodButtCoin = butt.transform.GetComponent<UIStoreButton>().buttCoin;
				int index = i;

				BaseHelpTipText helpText = foodButtMain.transform.GetComponent<BaseHelpTipText>();
				helpText.foodData = SlimeDatabase.Food[index].GetComponent<BaseFood>().foodData;

				foodButtMain.onClick.AddListener(() =>
				{
					foodButtMain.GetComponent<UIStoreButton>().ButtonClick();
				});
				foodButtCoin.onClick.AddListener(() =>
				{
					int cost = SlimeDatabase.Food[index].GetComponent<BaseFood>().foodData.goldCost;
					if(GameManager._Instance.GameRemoveCoin(cost))
					{
						GameManager._Instance.soundManager.PlayUIMoney();
						Vector3 pos = foodButtMain.transform.position;
						GameManager._Instance.GameAddFood(index, pos);
					} else
					{
						GameManager._Instance.soundManager.PlayUIError();
						foodButtMain.GetComponent<UIStoreButton>().ButtonClick();
					}
				});
				Image imageTo = foodButtMain.transform.GetChild(0).transform.GetComponent<Image>();
				SpriteRenderer renderFrom = SlimeDatabase.Food[i].GetComponentInChildren<SpriteRenderer>();
				manager.CopySprite(renderFrom, null, null, imageTo);
				foodButtMain.transform.GetChild(1).transform.GetComponent<Text>().text = manager.FormatedNumber(SlimeDatabase.Food[i].GetComponent<BaseFood>().foodData.goldCost);
			}
		}
		#endregion
		yield return new WaitForEndOfFrame();
		#region Decorations and Maps
		if(SlimeDatabase.Decorations.Length > 0)
		{
			for(int i = 0; i < SlimeDatabase.Decorations.Length; i++)
			{
				GameObject butt = (GameObject)Instantiate(Resources.Load("GameObjects/UI/ShopItemButton"), refs.tabMenus[0].transform.GetChild(0).transform);
				butt.transform.localPosition = Vector3.zero;
				Button decoButtMain = butt.transform.GetComponent<UIStoreButton>().buttMain;
				Button decoButtCoin = butt.transform.GetComponent<UIStoreButton>().buttCoin;
				int index = i;
				int decoID = SlimeDatabase.Decorations[index].GetComponent<BaseDeco>().decoData.decoID;
				OnChangeTab += butt.transform.GetComponent<UIStoreButton>().HideBuyButtons;
				bool purchasedBefore = GameManager._Instance.playerData.playPenDecoPurchased.Contains(decoID);
				if(purchasedBefore)
				{
					decoButtMain.transform.GetChild(1).transform.GetComponent<Text>().text = "Got";
				} else
				{
					decoButtMain.transform.GetChild(1).transform.GetComponent<Text>().text = SlimeDatabase.Decorations[index].GetComponent<BaseDeco>().decoData.goldCost.ToString();
				}
				BaseHelpTipText helpText = decoButtMain.transform.GetComponent<BaseHelpTipText>();
				helpText.decoData = SlimeDatabase.Decorations[index].GetComponent<BaseDeco>().decoData;

				decoButtMain.onClick.AddListener(() =>
				{
					bool purchased = GameManager._Instance.playerData.playPenDecoPurchased.Contains(decoID);
					bool isSpawned = false;
					if(GameManager._Instance.ourDeco.Count > 0)
					{
						for(int j = 0; j < GameManager._Instance.ourDeco.Count; j++)
						{
							if(GameManager._Instance.ourDeco[j].decoData.decoID == decoID)
							{
								isSpawned = true;
								break;
							}
						}
					}
					if(purchased && !isSpawned)
					{
						GameManager._Instance.soundManager.PlayUIAccept();
						GameManager._Instance.GameAddDeco(index);
					} else if(purchased && isSpawned)
					{
						GameManager._Instance.soundManager.PlayUIDecline();
					} else if(!purchased)
					{
						decoButtMain.GetComponent<UIStoreButton>().ButtonClick();
					}
				});
				decoButtCoin.onClick.AddListener(() =>
				{
					int cost = SlimeDatabase.Decorations[index].GetComponent<BaseDeco>().decoData.goldCost;
					if(GameManager._Instance.GameRemoveCoin(cost) && decoButtMain.transform.GetChild(1).transform.GetComponent<Text>().enabled)
					{
						GameManager._Instance.GameAddDeco(index);
						decoButtMain.transform.GetChild(1).transform.GetComponent<Text>().text = "Got";

						GameManager._Instance.soundManager.PlayUIMoney();
					} else if(!decoButtMain.transform.GetChild(1).transform.GetComponent<Text>().enabled)
					{
						GameManager._Instance.soundManager.PlayUIAccept();
						string desc = "You already bought this decoration";
					} else
					{
						GameManager._Instance.soundManager.PlayUIError();
					}
					decoButtMain.GetComponent<UIStoreButton>().ButtonClick();
				});
				Image imageTo = decoButtMain.transform.GetChild(0).transform.GetComponent<Image>();
				SpriteRenderer renderFrom = SlimeDatabase.Decorations[i].GetComponentInChildren<SpriteRenderer>();
				Sprite overrideImage = SlimeDatabase.Decorations[i].GetComponent<BaseDeco>().decoIcon;
				manager.CopySprite(renderFrom, null, null, imageTo, overrideImage);
			}
		}
		yield return new WaitForEndOfFrame();
		if (SlimeDatabase.Maps.Length > 0)
		{
			for (int i = 0; i < SlimeDatabase.Maps.Length; i++)
			{
				GameObject butt = (GameObject)Instantiate(Resources.Load("GameObjects/UI/ShopItemButton"), refs.tabMenus[0].transform.GetChild(0).transform);
				butt.transform.localPosition = Vector3.zero;
				Button decoButtMain = butt.transform.GetComponent<UIStoreButton>().buttMain;
				Button decoButtCoin = butt.transform.GetComponent<UIStoreButton>().buttCoin;
				int index = i;
				OnChangeTab += butt.transform.GetComponent<UIStoreButton>().HideBuyButtons;
				bool purchasedBefore = GameManager._Instance.playerData.playPenMaps.Contains(index);
				if (purchasedBefore)
				{
					decoButtMain.transform.GetChild(1).transform.GetComponent<Text>().text = "Got";
				}
				else
				{
					decoButtMain.transform.GetChild(1).transform.GetComponent<Text>().text = SlimeDatabase.Maps[index].GetComponent<BaseMap>().decoData.goldCost.ToString();
				}
				BaseHelpTipText helpText = decoButtMain.transform.GetComponent<BaseHelpTipText>();
				helpText.decoData = SlimeDatabase.Maps[index].GetComponent<BaseMap>().decoData;
				decoButtMain.onClick.AddListener(() =>
				{
					bool purchased = GameManager._Instance.playerData.playPenMaps.Contains(index);
					bool isSpawned = false;
					if (GameManager._Instance.playerData.playPenMapIndex == index)
					{
						isSpawned = true;
					}
					if (purchased && !isSpawned)
					{
						GameManager._Instance.soundManager.PlayUIAccept();
						GameManager._Instance.GameAddMap(index);
					}
					else if (purchased && isSpawned)
					{
						GameManager._Instance.soundManager.PlayUIDecline();
					}
					else if (!purchased)
					{
						decoButtMain.GetComponent<UIStoreButton>().ButtonClick();
					}
				});
				decoButtCoin.onClick.AddListener(() =>
				{
					int cost = SlimeDatabase.Maps[index].GetComponent<BaseMap>().decoData.goldCost;
					if (GameManager._Instance.GameRemoveCoin(cost))
					{
						GameManager._Instance.GameAddMap(index);
						decoButtMain.transform.GetChild(1).transform.GetComponent<Text>().text = "Got";

						GameManager._Instance.soundManager.PlayUIMoney();
					}
					else
					{
						GameManager._Instance.soundManager.PlayUIError();
					}
					decoButtMain.GetComponent<UIStoreButton>().ButtonClick();
				});
				Image imageTo = decoButtMain.transform.GetChild(0).transform.GetComponent<Image>();
				SpriteRenderer renderFrom = SlimeDatabase.Maps[i].GetComponent<BaseMap>().mapBackground;
				manager.CopySprite(renderFrom, null, null, imageTo);
			}
		}
		#endregion
		yield return new WaitForEndOfFrame();
		#region Slimes
		if(SlimeDatabase.FriendlySlimes.Length > 0)
		{
			for(int i = 0; i < SlimeDatabase.FriendlySlimes.Length; ++i)
			{
				GameObject butt = (GameObject)Instantiate(Resources.Load("GameObjects/UI/ShopItemButton"), refs.tabMenus[3].transform.GetChild(0).transform);
				butt.transform.localPosition = Vector3.zero;
				butt.name = SlimeDatabase.FriendlySlimes[i].name;
				Button slimeButtMain = butt.transform.GetComponent<UIStoreButton>().buttMain;
				Button slimeButtCoin = butt.transform.GetComponent<UIStoreButton>().buttCoin;
				int index = i;
				OnChangeTab += butt.transform.GetComponent<UIStoreButton>().HideBuyButtons;

				int classIndex = SlimeDatabase.FriendlySlimes[i].transform.GetComponent<BaseSlime>().slimeStats.classType;
				Vector3 pos = butt.GetComponent<RectTransform>().anchoredPosition;

				BaseHelpTipText helpText = slimeButtMain.transform.GetComponent<BaseHelpTipText>();
				helpText.showSlimeData = true;
				helpText.slimeData = SlimeDatabase.FriendlySlimes[i].transform.GetComponent<BaseSlime>().slimeStats;

				slimeButtMain.onClick.AddListener(() =>
				{
					slimeButtMain.GetComponent<UIStoreButton>().ButtonClick();
				});
				slimeButtCoin.onClick.AddListener(() =>
				{
					int cost = SlimeDatabase.FriendlySlimes[index].GetComponent<BaseSlime>().slimeStats.goldCost;
					if(GameManager._Instance.GameRemoveCoin(cost))
					{
						GameManager._Instance.GameAddSlime(classIndex);
						GameManager._Instance.soundManager.PlayUIMoney();
					} else
					{
						GameManager._Instance.soundManager.PlayUIError();
						slimeButtMain.GetComponent<UIStoreButton>().ButtonClick();
					}
				});
				Image imageTo = slimeButtMain.transform.GetChild(0).transform.GetComponent<Image>();
				SpriteRenderer renderFrom = SlimeDatabase.FriendlySlimes[i].GetComponentInChildren<SpriteRenderer>();
				manager.CopySprite(renderFrom, null, null, imageTo);
				slimeButtMain.transform.GetChild(1).transform.GetComponent<Text>().text = manager.FormatedNumber(SlimeDatabase.FriendlySlimes[i].GetComponent<BaseSlime>().slimeStats.goldCost);
			}
		}
		#endregion
		yield return new WaitForEndOfFrame();
		isLoading = false;
		loadingButtons = false;
		CloseAllTabs();

	}
}
