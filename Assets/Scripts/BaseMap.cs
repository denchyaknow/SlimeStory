﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using Denchyaknow;
public class BaseMap : BaseDeco {
	//public string mapName = "UnNamedMap"; 
	//public int mapCoinValue = 10;
	public MapData mapData
	{
		get
		{
			if (_mapData != null)
				return _mapData;
			else
				return new MapData();
		}
		set
		{
			_mapData = value;
		}
	}


		
	[SerializeField] private MapData _mapData;
	public enum MapSize
	{
		Small,
		Medium,
		Large
	}
	public MapSize mapSize;
	public SpriteRenderer mapBackground;
	public Transform mapEdge;
	public Transform[] mapBackgrounds;
	public Transform[] mapEdges;
	public Transform[] mapWalls;
	public Transform mapObjects; 
	public BaseMap lastConnectedMap;
	public BaseMap nextConnectedMap;
	public float connectDistance;
	public float playerStartDistance;
#region Player and Camera Positions
	public Vector3 playerStartPos0
	{
		get
		{
			Vector3 pos = Vector3.zero;
			pos = mapEdge.GetChild(0).transform.position;
			Vector3 camCenter = Camera.main.ScreenToWorldPoint(new Vector2(Camera.main.pixelWidth / 2, 0));
			Vector3 camLeft = Camera.main.ScreenToWorldPoint(Vector2.zero);
			float camHalfWidth = (camCenter.x - camLeft.x)/2;
			pos.x -= camHalfWidth;
			pos.y = Camera.main.transform.position.y;

			return pos;
		}
	}
	public Vector3 playerStartPos1
	{
		get
		{
			Vector3 pos = Vector3.zero;
			pos = mapEdge.GetChild(0).transform.position;
			Vector3 camCenter = Camera.main.ScreenToWorldPoint(new Vector2(Camera.main.pixelWidth / 2, 0));
			Vector3 camLeft = Camera.main.ScreenToWorldPoint(Vector2.zero);
			float camHalfWidth = (camCenter.x - camLeft.x)/2;
			pos.x += camHalfWidth;
			pos.y = Camera.main.transform.position.y;

			return pos;
		}
	}
	public Vector3 playerEndPos0
	{
		get
		{
			Vector3 pos = Vector3.zero;
			pos = mapEdge.GetChild(1).transform.position;
			Vector3 camCenter = Camera.main.ScreenToWorldPoint(new Vector2(Camera.main.pixelWidth / 2, 0));
			Vector3 camLeft = Camera.main.ScreenToWorldPoint(Vector2.zero);
			float camHalfWidth = (camCenter.x - camLeft.x)/2;
			pos.x -= camHalfWidth;
			pos.y = Camera.main.transform.position.y;

			return pos;
		}
	}
	public Vector3 playerEndPos1
	{
		get
		{
			Vector3 pos = Vector3.zero;
			pos = mapEdge.GetChild(1).transform.position;
			Vector3 camCenter = Camera.main.ScreenToWorldPoint(new Vector2(Camera.main.pixelWidth / 2, 0));
			Vector3 camLeft = Camera.main.ScreenToWorldPoint(Vector2.zero);
			float camHalfWidth = (camCenter.x - camLeft.x)/2;
			pos.x += camHalfWidth;
			pos.y = GameManager._Instance.combatManager.playerSlime.transform.position.y;

			return pos;
		}
	}
	public Vector3 cameraStartPos
	{
		get
		{
			Vector3 pos = Vector3.zero;
			pos = mapEdge.GetChild(0).transform.position;
			Vector3 camCenter = Camera.main.ScreenToWorldPoint(new Vector2(Camera.main.pixelWidth / 2, 0));
			Vector3 camLeft = Camera.main.ScreenToWorldPoint(Vector2.zero);
			float camHalfWidth = (camCenter.x - camLeft.x);
			pos.x += camHalfWidth;
			pos.y = Camera.main.transform.position.y;

			return pos;
		}
	}
	public Vector3 cameraEndPos
	{
		get
		{
			Vector3 pos = Vector3.zero;
			pos = mapEdge.GetChild(1).transform.position;
			Vector3 camCenter = Camera.main.ScreenToWorldPoint(new Vector2(Camera.main.pixelWidth / 2, 0));
			Vector3 camRight = Camera.main.ScreenToWorldPoint(new Vector2(Camera.main.pixelWidth, 0));
			float camHalfWidth = (camRight.x - camCenter.x);
			pos.x -= camHalfWidth;
			pos.y = Camera.main.transform.position.y;
			return pos;
		}
	}
#endregion
	[Range(1, 10)]
	public int xQuadrants = 4;
	[Range(1, 10)]
	public int yQuadrants = 4;
	[Range(0,10)]
	public float spawnRadius = 4f;
	public List<Vector3> quadrantCenters = new List<Vector3>();
	[HideInInspector]
	public float[] spawnableChances = new float[0];
	[HideInInspector]
	public bool[] spawnableRandomlyFlip = new bool[0];
	public bool[] spawnableOnTopOnly = new bool[0];
	public bool[] spawnableOnBottomOnly = new bool[0];
	public Transform spawnableObjectsParent;
	public Transform spawnedObjectsParent;
	public bool showGizmos = false;
	public bool connectToMapTest = false;
	public BaseMap textMapToConnectTo;
	public void OnEnable()
	{
		SetMapSize(mapSize);
	}
	public override void Awake()
	{
		
	}
	public override void Start () {
		if(mapBackground != null)
		{
			MapResizeBackground();
			if(GameManager._Instance.combatManager != null)
			{
				//GetQuadrantCenters();
				//SpawnObjects();
			} else if(GameManager._Instance.playPenManager != null)
			{
				ClearObjects();
			}
		} else
		{
			//Debug.Log("MAP: NULL_VAR_MAPBACKGROUND_MAP_RESIZE_FAILED");
		}
	}
	
	// Update is called once per frame
	public override void Update () {
		
	}
	public void SetMapSize(MapSize size)
	{
		mapSize = size;
		Transform mapWall = null;
		foreach(Transform tr in mapBackgrounds)
		{
			tr.gameObject.SetActive(false);
		}
		foreach(Transform tr in mapEdges)
		{
			tr.gameObject.SetActive(false);
		}
		foreach (Transform tr in mapWalls)
		{
			tr.gameObject.SetActive(false);
		}
		switch (mapSize)
		{
			case MapSize.Small:
				mapBackground = mapBackgrounds[0].GetComponent<SpriteRenderer>();
				mapEdge = mapEdges[0];
				mapWall = mapWalls[0];
				//connectDistance = 20;
				//playerStartDistance = 8;
				break;
			case MapSize.Medium:
				mapBackground = mapBackgrounds[1].GetComponent<SpriteRenderer>();
				mapEdge = mapEdges[1];
				mapWall = mapWalls[1];
				//connectDistance = 48;
				//playerStartDistance = 36;
				break;
			case MapSize.Large:
				mapBackground = mapBackgrounds[2].GetComponent<SpriteRenderer>();
				mapEdge = mapEdges[2];
				mapWall = mapWalls[2];
				//connectDistance = 76;
				//playerStartDistance = 64;
				break;
		}
		mapBackground.gameObject.SetActive(true);
		//mapEdge.gameObject.SetActive(true);
		//mapWall.gameObject.SetActive(true);
	}

	public void ConnectToMap(BaseMap lastMap, int dir)
	{
		if(dir > 0)
			lastMap.nextConnectedMap = this;
		else if(dir < 0)
			lastConnectedMap = lastMap;

		Vector3 lastPos = lastMap.transform.position;
		float lastDis = lastMap.connectDistance;
		Vector3 newPos = lastPos;
		newPos.x += (lastDis + (connectDistance)) * dir;
		transform.position = newPos;
	}
	public void MapRandomize()
	{
		int maxObjects = mapObjects.childCount;
		int mrRandom = Random.Range(0, maxObjects);
		for(int i = 0; i < maxObjects; i++)
		{
			if(i == mrRandom)
				mapObjects.GetChild(i).gameObject.SetActive(true);
		}
	}
	public void MapReloadBackground()
	{
		Texture2D[] allTextures = Resources.LoadAll<Texture2D>("Sprites/Backgrounds/");
		foreach(Transform renderer in mapBackgrounds)
		{
			renderer.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Sprites/Backgrounds/" + allTextures[decoData.decoID].name);
		}
	}
	public void MapResizeBackground()
	{
		MapReloadBackground();
		SetMapSize(mapSize);
		float width = mapBackground.sprite.bounds.extents.x;
		float height = mapBackground.sprite.bounds.extents.y;
		float worldScreenHeight = Camera.main.orthographicSize * 2.0f;
		float worldScreenWidth = ((worldScreenHeight / Camera.main.pixelHeight) * Camera.main.pixelWidth);
		Vector3 scale = new Vector3(worldScreenWidth / width, worldScreenHeight / height, 1);
		mapBackground.transform.localScale = scale;
	}
	public void GetQuadrantCenters()
	{
		quadrantCenters.Clear();
		//Vector3 centerOFBack = transform.TransformPoint(mapBackground.sprite.bounds.center);
		//Gizmos.DrawWireSphere(centerOFBack, spawnRadius);
		//Gizmos.DrawWireSphere(centerOFBack + (Vector3.up * ((mapBackground.sprite.bounds.extents.y / 2) * mapBackground.transform.localScale.y)), spawnRadius);

		Vector3 centerOFBack = transform.TransformPoint(mapBackground.sprite.bounds.center);
		Vector2 extents = mapBackground.bounds.extents * 2;
		Vector2 size = mapBackground.bounds.size ;
		extents.x *= mapBackground.transform.localScale.x;
		extents.y *= mapBackground.transform.localScale.y;
		float xLength = size.x / mapBackground.transform.localScale.x;
		float yLength = size.y / mapBackground.transform.localScale.y;
		size.x *= mapBackground.transform.localScale.x;
		size.y *= mapBackground.transform.localScale.y;
		Vector3 mostLeftOfBack = new Vector3(centerOFBack.x - extents.x, centerOFBack.y - extents.y, centerOFBack.z);
		Vector3 mostBottomOfBack = new Vector3(mostLeftOfBack.x, centerOFBack.y - size.y, centerOFBack.z);
		Vector3 xQuadrantSpacing = ((size.x * 2) / (xQuadrants + 1)) * Vector3.right;
		Vector3 yQuadrantSpacing = ((size.y * 2) / (yQuadrants + 1)) * Vector3.up;
		for(int j = 0; j < yQuadrants; j++)
		{
			
				mostLeftOfBack.y += (yQuadrantSpacing.y);
			for(int i = 0; i < xQuadrants; i++)
			{
				quadrantCenters.Add(mostLeftOfBack + (xQuadrantSpacing + (xQuadrantSpacing * i)));
			}
		}
	}
	public void ClearObjects()
	{
		if(spawnedObjectsParent != null)
		{
			if(spawnedObjectsParent.childCount > 0)
			{

				for(int i = 0; i < spawnedObjectsParent.childCount; i++)
				{
					spawnedObjectsParent.GetChild(i).gameObject.SetActive(false);
				}
			}
		}
	}
	public void SpawnMobs()
	{
		if (mapData.mobs.Count > 0)
		{
			for (int i = 0; i < mapData.mobs.Count; i++)
			{
				int mobID = mapData.mobs[i].mobID;
				int mobChance = mapData.mobs[i].spawnChance;
				int mobCountMax = mapData.mobs[i].spawnCount;
				int mobCountCur = 0;
				int tryCount = 0;
				int tryMax = mobCountMax * 100;
				bool giveUp = false;
				while (!giveUp)
				{
					for (int o = 0; o < quadrantCenters.Count; o++)
					{
						int chance = Random.Range(0, 100);
						tryCount++;
						if (chance < mobChance)
						{
							Vector3 pos = quadrantCenters[o] + (Vector3)(Random.insideUnitCircle * spawnRadius);
							pos.z = 0;
							SpawnMob(pos, mobID);
							mobCountCur++;
						}
						if (tryCount > tryMax)
							giveUp = true;
						if (mobCountCur > mobCountMax)
							giveUp = true;
						if (giveUp)
						{
							Debug.Log("Gaveup trying to spawn mobs:" + gameObject.name);
							break;
						}
					}
						if (giveUp)
							break;
				}
			}
		}
		else
		{
			Debug.Log("No Mobs in mapData");
		}
	}
	void SpawnMob(Vector3 pos, int id)
	{
		if (GameManager._Instance.database != null)
		{
			if (GameManager._Instance.database.Enemies.Length > 0)
			{
				GameObject newMob = Instantiate(GameManager._Instance.database.Enemies[id], null);
				newMob.transform.SetParent(null);
				newMob.transform.position = pos;
				GameManager._Instance.combatManager.enemies.Add(newMob.transform.GetComponent<UniMob_Base>());
			}
		}
	}
	public void SpawnObjects()
	{
		if(spawnedObjectsParent == null || spawnableObjectsParent == null)
			return;
		
		if(spawnedObjectsParent.childCount> 0)
		{
			while(spawnedObjectsParent.childCount > 0)
			{
				DestroyImmediate(spawnedObjectsParent.GetChild(0).gameObject);
			}
		
		}
		if(quadrantCenters.Count > 0 && spawnableChances.Length > 0)
		{
			int topRowLimiter = xQuadrants;
			int bottomRowLimiter = (xQuadrants * yQuadrants) - xQuadrants;
			for(int j = 0; j < spawnableObjectsParent.childCount; j++)
			{
				int child = j;

				for(int i = 0; i < quadrantCenters.Count; i++)
				{
					int mrRandom = Random.Range(0, 100);
					if(mrRandom < (spawnableChances[child] * 100) && spawnableObjectsParent.childCount > 0)
					{
						bool doNotSpawn = false;
						if(spawnableOnTopOnly[child] || spawnableOnBottomOnly[child])
						{
							if(i >= topRowLimiter && i < bottomRowLimiter)
							{
								doNotSpawn = true;
							}
							if(i >= bottomRowLimiter && !spawnableOnBottomOnly[child])
							{
								doNotSpawn = true;
							}
							if(i < topRowLimiter && !spawnableOnTopOnly[child])
							{
								doNotSpawn = true;
							}
						}
						////Debug.Log("ID="+j.ToString() + " i="+i.ToString() + " spawn=" + doNotSpawn + " mrRan=" + mrRandom.ToString());
						//if(i >= topRowLimiter && i < bottomRowLimiter && (spawnableOnTopOnly[child] || spawnableOnBottomOnly[child]))
						//{
						//	doNotSpawn = true;
						//} else if (i < topRowLimiter && !spawnableOnTopOnly[child] && spawnableOnBottomOnly[child])
						//{
						//	doNotSpawn = true;
						//} else if(i > bottomRowLimiter && !spawnableOnBottomOnly[child] && spawnableOnBottomOnly[child])
						//{
						//	doNotSpawn = true;
						//}
						if(!doNotSpawn)
						{
							GameObject spawnedObj = Instantiate(spawnableObjectsParent.GetChild(child).gameObject, spawnedObjectsParent);
							spawnedObj.name = "Obj " + child.ToString() + i.ToString();
							spawnedObj.transform.SetParent(spawnedObjectsParent);
							spawnedObj.transform.position = quadrantCenters[i] + (Vector3)(Random.insideUnitCircle * spawnRadius);
							bool flipX = spawnableRandomlyFlip[child] ? (Random.Range(0, 100) > 50 ? true : false) : false;
							spawnedObj.transform.localScale = new Vector3((flipX?-1:1), 1, 1);
							if (spawnedObj.transform.GetComponent<SortingGroup>() == null)
							{
								SortingGroup sorter = spawnedObj.AddComponent<SortingGroup>();
								sorter.sortingLayerID = mapBackground.sortingLayerID;
							}
							spawnedObj.transform.GetComponent<SortingGroup>().sortingOrder = (int)(spawnedObj.transform.position.y * -1000);
							spawnedObj.gameObject.SetActive(true);
						}
					}
				}
			}
		}
	}
	private void OnDrawGizmosSelected()
	{
		if(!showGizmos)
			return;
		Gizmos.color = Color.black;
		if(quadrantCenters.Count > 0)
		{
			for(int i = 0; i < quadrantCenters.Count; i++)
			{
				Gizmos.DrawWireSphere(quadrantCenters[i],spawnRadius);

			}
			
		}
		Gizmos.color = Color.magenta;
		Vector3 posLeft = transform.position - (Vector3.right * connectDistance);
		Vector3 posRight = transform.position + (Vector3.right * connectDistance);
		Gizmos.DrawLine(posLeft, posRight);
		Gizmos.DrawLine(posLeft + (Vector3.up * 2f), posLeft - (Vector3.up * 2f));
		Gizmos.DrawLine(posRight + (Vector3.up * 2f), posRight - (Vector3.up * 2f));
		posLeft = transform.position - (Vector3.right * playerStartDistance);
		posRight = transform.position + (Vector3.right * playerStartDistance);
		Gizmos.DrawLine(posLeft, posRight);
		Gizmos.DrawLine(posLeft + (Vector3.up * 2f), posLeft - (Vector3.up * 1f));
		Gizmos.DrawLine(posRight + (Vector3.up * 2f), posRight - (Vector3.up * 1f));
		if(connectToMapTest)
		{
			connectToMapTest = false;
			if(textMapToConnectTo != null)
				ConnectToMap(textMapToConnectTo, 1);
		}
	}
}
