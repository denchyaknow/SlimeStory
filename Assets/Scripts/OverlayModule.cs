﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OverlayModule : MonoBehaviour {

	private SpriteRenderer myRenderer;
	private SpriteRenderer myParentRenderer;
	// Use this for initialization
	void Start () {
		myRenderer = transform.GetComponent<SpriteRenderer>();
		myParentRenderer = transform.parent.transform.GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(myRenderer.sprite != myParentRenderer.sprite)
			myRenderer.sprite = myParentRenderer.sprite;
        if (myRenderer.flipX != myParentRenderer.flipX)
            myRenderer.flipX = myParentRenderer.flipX;
	}
}
