﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
[System.Serializable]
public class Database : MonoBehaviour {
	//public static Database _Instance;
	public GameObject[] Friendlies;
	public GameObject[] Enemies;
	public GameObject[] Attacks;
	public GameObject[] Maps;
	public GameObject[] Food;
	public GameObject Poop;
    public List<SpriteArray> EnemyWalkSheets = new List<SpriteArray>();
    public List<SpriteArray> EnemyAttackSheets = new List<SpriteArray>();
    public Sprite[] EnemyFaceSprites;
	public int[] ZoneLvlRequirements;
	// Use this for initialization
	void Awake ()
	{
		
		ZoneLvlRequirements = new int[Maps.Length];
		for (int i = 0; i < ZoneLvlRequirements.Length; i++)
		{
			ZoneLvlRequirements[i] = i * 25;
		}

		#region Logic Reff
		//Friendlies = Resources.LoadAll<GameObject>("GameObjects/Friendlies").OrderBy(slime => slime.name).ToArray<GameObject>();
		//Debug.Log("Loaded: " + Friendlies.Length + " Friendlies");

		//Enemies = Resources.LoadAll<GameObject>("GameObjects/Enemies").OrderBy(enemy => enemy.name).ToArray<GameObject>();
		//Debug.Log("Loaded: " + Enemies.Length + " Enemies");

		//Attacks = Resources.LoadAll<GameObject>("GameObjects/Attacks").OrderBy(attack => attack.name).ToArray<GameObject>();
		//Debug.Log("Loaded: " + Attacks.Length + " Attacks");

		//Maps = Resources.LoadAll<GameObject>("GameObjects/Maps").OrderBy(map => map.name).ToArray<GameObject>();//Maps must have a child object named- 'Ground' and it has to be a sprite renderer of the main background image for the map  
		//Debug.Log("Loaded: " + Maps.Length + " Maps");

		//Food = Resources.LoadAll<GameObject>("GameObjects/Food").OrderBy(food => food.name).ToArray<GameObject>();
		//Debug.Log("Loaded: " + Food.Length + " Food");

		//Poop = Resources.Load<GameObject>("GameObjects/Poop");
		//Debug.Log("Loaded: " + Poop.transform.childCount + " Poop");



		//Texture2D[] allTextures = Resources.LoadAll<Texture2D>("Sprites/Enemies/Walk/").OrderBy(go => go.name).ToArray();
  //      if(allTextures.Length > 0)
  //      {
  //          for(int i = 0; i < allTextures.Length; i++)
  //          {
  //              int index = i;
  //              string name = allTextures[index].name;
            
  //              SpriteArray sheet = new SpriteArray();
  //              Sprite[] tempSprites = Resources.LoadAll<Sprite>("Sprites/Enemies/Walk/" + name);
  //              sheet.sprites = tempSprites;
  //              EnemyWalkSheets.Add(sheet);
  //              }
  //      }
  //      allTextures = Resources.LoadAll<Texture2D>("Sprites/Enemies/Battle/").OrderBy(go => go.name).ToArray();
  //      if (allTextures.Length > 0)
  //      {
  //          for (int i = 0; i < allTextures.Length; i++)
  //          {
  //              int index = i;
  //              string name = allTextures[index].name;

  //              SpriteArray sheet = new SpriteArray();
  //              Sprite[] tempSprites = Resources.LoadAll<Sprite>("Sprites/Enemies/Battle/" + name);
  //              sheet.sprites = tempSprites;
  //              EnemyAttackSheets.Add(sheet);
  //          }
  //      }
  //      EnemyFaceSprites = Resources.LoadAll<Sprite>("Sprites/Enemies/Face/").OrderBy(go => go.name).ToArray();

  //      Debug.Log("Sprites Loaded: " + EnemyAttackSheets.Count + " Enemy battle sheets");
  //      Debug.Log("Sprites Loaded: " + EnemyWalkSheets.Count + " Enemy walk sheets");
  //      Debug.Log("Sprites Loaded: " + EnemyFaceSprites.Length + " Enemy face sprites");

#endregion

    }

    // Update is called once per frame
    void Update ()
	{
		
	}
}
[System.Serializable]
public class SpriteArray
{
    public Sprite[] sprites;
    public SpriteArray()
    {
        sprites = null;
    }
}
