﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class TextFloater : MonoBehaviour {
	public float fadeRate = 0.8f;
	public float time = 1;
	public float radius = 1;
	public Ease ease = Ease.InExpo;
	private Vector3[] path;
	private Vector3 up = Vector3.up;
	private Vector3 right = Vector3.right;
	private Vector3 down = Vector3.down;
	private Vector3 left = Vector3.left;
	private Vector3 localPos;
	private bool rewind = false;
	// Use this for initialization
	void Start ()
	{
		localPos = transform.localPosition;
		
		Play();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	private void Play()
	{
		path = new Vector3[3] {(Vector3.right + Vector3.down) * Random.Range(radius, 2 * radius),down * Random.Range(2 * radius, 3 * radius),(Vector3.left + Vector3.down) * Random.Range(radius, 2 * radius) };
		transform.DOPath(path, time * 3, PathType.CatmullRom, PathMode.Full3D, 10, Color.white).SetRelative(true).SetId(gameObject.GetInstanceID()).SetEase(ease)
			.OnComplete(() =>
			{
				//Play();
				transform.DOLocalMove(localPos, time, false).SetEase(ease)
				.OnComplete(() =>
				{
					Play();

				});
			});
	}
	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.white;
		Gizmos.DrawWireSphere(transform.position, radius);
	}
}
