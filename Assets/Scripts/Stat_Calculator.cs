﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
public class Stat_Calculator : MonoBehaviour {
	public Stat_Calculator playerCalc;
	public Stat_Calculator mobCalc;
	public float expMulti = 0.22f;
	public List<int> linkedHP = new List<int>();
	public List<int> linkedEX = new List<int>();
	public List<int> linkedDM = new List<int>();
	public List<int> linkedCO = new List<int>();
	public List<float> linkedSC = new List<float>();

	public GameObject prefab;
    public bool CalculateStats = false;
    [Range(1,50)]
    public int maxStats = 26;
    [Range(1,255)]
    public int levelsMax = 200;
    private int levelSet = 0;
    private List<GameObject> prefabs = new List<GameObject>();
    [Range(1,40)]
    public int levelStep = 10;
    private int levelStepSet = 0;
    
    [Header("BASE/RATE/FLAT")]
    
    public Vector3 hp = new Vector3(10f, 0.1f, 10f);
    private Vector3 setHP;
    public Vector3 ex = new Vector3(100f, 5f, 5f);
    private Vector3 setEX;
    public Vector3 dm = new Vector3(1f, 5f, 5f);
    private Vector3 setDM;
    public Vector3 co = new Vector3(1f, 5f, 5f);
    private Vector3 setCO;
    public Vector3 sc = new Vector3(1f, 1.2f, 1.5f);
    private Vector3 setSC;

    public bool calculate = false;
	public void Update () 
    {
        StartCalculation();
	}
    public void OnDrawGizmosSelected()
    {
        if (Application.isPlaying)
            return;
		if(playerCalc == null)
			StartCalculation();
    }
    private int currHP = 0;
    private int currEX = 0;
    private int currDM = 0;
    private int currCO = 0;
    private float currSC = 0;
    void StartCalculation()
    {
        if (!CalculateStats || prefab == null)
            return;
        if (levelStep < 1)
            levelStep = 1;
        if (levelsMax < 1)
            levelsMax = 1;
        bool bypass = MobCalculateBypass() && mobCalc != null;
        if (levelSet != levelsMax || levelStepSet != levelStep
            || setHP != hp || setEX != ex || setDM != dm || setCO != co || setSC != sc || !calculate || bypass)
        {
            if (currentlyCalculating)
                return;
            calculate = true;
            currentlyCalculating = true;
            levelSet = levelsMax;
            levelStepSet = levelStep;
            setHP = hp;
            setEX = ex;
            setDM = dm;
            setCO = co;
            setSC = sc;
            SpawnValues();
        }
    }
    private bool currentlyCalculating = false;
    IEnumerator StartNextCalculation()
    {
        currentlyCalculating = false;
        yield return new WaitForSeconds(10f);
        if (!currentlyCalculating)
            calculate = false;
    }
    
    public bool MobCalculateBypass()
    {
        bool bypass = false;
        if (mobCalc.levelSet != levelsMax || mobCalc.levelStepSet != levelStep
           || mobCalc.setHP != mobCalc.hp 
           || mobCalc.setEX != mobCalc.ex 
           || mobCalc.setDM != mobCalc.dm 
           || mobCalc.setCO != mobCalc.co 
           || mobCalc.setSC != mobCalc.sc)
        {
            bypass = true;
        }
        return bypass;
    }
    public void StartMobCalculation()
    {
        linkedHP.Clear();
        linkedEX.Clear();
        linkedDM.Clear();
        linkedCO.Clear();
        linkedSC.Clear();
        mobCalc.levelsMax = levelsMax;
        mobCalc.levelSet = mobCalc.levelsMax;
        mobCalc.levelStep = levelStep;
        mobCalc.levelStepSet = mobCalc.levelStep;
        mobCalc.maxStats = maxStats;
        mobCalc.setHP = mobCalc.hp;
        mobCalc.setEX = mobCalc.ex;
        mobCalc.setDM = mobCalc.dm;
        mobCalc.setCO = mobCalc.co;
        mobCalc.setSC = mobCalc.sc;
        mobCalc.SpawnValues();
    }
    void ClearPrefabs()
    {
        prefabs.Clear();
        for (int i = 0; i < transform.childCount; ++i)
        {
            if (transform.GetChild(i).gameObject != prefab.gameObject)
                prefabs.Add(transform.GetChild(i).gameObject);
        }
        foreach(GameObject fab in prefabs)
        {
            DestroyImmediate(fab);
        }
        prefabs.Clear();
    }
    IEnumerator SpawnValuesCoroutine()
    {
        if (mobCalc != null)
        {
            StartMobCalculation();
            yield return new WaitForSeconds(0.5f);

        }
        ClearPrefabs();
        //yield return new WaitForSeconds(0.4f);
        int count = levelsMax / levelStep;
        int current = 0;
        currHP = (int)hp.x; if (currHP == 0) currHP = 1;
        currEX = (int)ex.x; if (currEX == 0) currEX = 1;
        currDM = (int)dm.x; if (currDM == 0) currDM = 1;
        currCO = (int)co.x; if (currCO == 0) currCO = 1;
        currSC = 1;
        Text[] prefabTxt = prefab.GetComponentsInChildren<Text>();
        prefabTxt[0].text = "HP(" + hp + ")";
        prefabTxt[1].text = "EX(" + ex + ")";
        prefabTxt[2].text = "DM(" + dm + ")";
        prefabTxt[3].text = "CO(" + co + ")";
        prefabTxt[4].text = "SC(" + sc + ")";
        for (int i = 0; i < levelsMax; i++)
        {
            if (!currentlyCalculating)
                StopCoroutine(SpawnValuesCoroutine());
            current++;
            int addHP = (int)(MobHP(hp.x, hp.y, hp.z, i));
            currHP += addHP;
            if (playerCalc != null)
                playerCalc.linkedHP.Add(currHP);
            if (mobCalc != null)
                mobCalc.linkedHP.Add(currHP);
            int addDM = (int)MobDM(dm.x, dm.y, dm.z, i);
            currDM += addDM;
            if (playerCalc != null)
                playerCalc.linkedDM.Add(currDM);
            if (mobCalc != null)
                mobCalc.linkedDM.Add(currDM);
            int addEX = ScaledEXP(ex.x, ex.y, ex.z, i);
            currEX += addEX;
            if (playerCalc != null)
                playerCalc.linkedEX.Add(currEX);
            if (mobCalc != null)
                mobCalc.linkedEX.Add(currEX);
            float addSC = ((0.01f * ScaledTransform(i, sc.x, sc.y, sc.z)));
            currSC += addSC;
            int addCO = ScaledCoin(currHP, co.x, co.y, i, co.z);
            currCO += addCO;
            if (playerCalc != null)
                playerCalc.linkedCO.Add(currCO);
            if (mobCalc != null)
                mobCalc.linkedCO.Add(currCO);
            if (current == levelStep)
            {


                if (playerCalc != null)
                {
                    GameObject newPLinkedFab = Instantiate(prefab, prefab.transform.parent);
                    newPLinkedFab.name = "_" + i.ToString() + "_" + current.ToString();
                    current = 0;
                    Text[] allPText = newPLinkedFab.GetComponentsInChildren<Text>();
                    // allText[0].text = ("Lv:" + current + " HP=" + (currHP += ScaledHP(current, (int)hp.x, hp.y, hp.z)) + "(" + ScaledHP(current, (int)hp.x, hp.y, hp.z) + ")");
                    allPText[0].text = ("Stg:" + (i + 1) + " HP=" + (currHP) + "(+" + (addHP) + ")");
                    allPText[1].text = ("EX=" + (currEX) + "(+" + addEX + "[" + (currEX) / (expMulti * addEX) + "MbsToLvl])");
                    allPText[2].text = ("DM=" + (currDM) + "(+" + (addDM) + "|[" + (currHP / currDM) + "])");
                    allPText[3].text = ("CO=" + (currCO));
                    allPText[4].text = ("SC=" + currSC + "(+" + addSC + ")");

                    prefabs.Add(newPLinkedFab);
                }
                else if (mobCalc != null)
                {
                    GameObject newMLinkedFab = Instantiate(prefab, prefab.transform.parent);
                    newMLinkedFab.name = "_" + i.ToString() + "_" + current.ToString();
                    current = 0;
                    Text[] allMText = newMLinkedFab.GetComponentsInChildren<Text>();
                    // allText[0].text = ("Lv:" + current + " HP=" + (currHP += ScaledHP(current, (int)hp.x, hp.y, hp.z)) + "(" + ScaledHP(current, (int)hp.x, hp.y, hp.z) + ")");

                    string mobHP = " |HP/MbDM-(" + (currHP / linkedDM[i]).ToString() + "HpLife) MbDM-(" + linkedDM[i].ToString() + ")|";
                    /*linkedHP.Count > i ? " |HP/MbDM-(" + (currHP / linkedDM[i]).ToString() + "HpLife) MbDM-(" + linkedDM[i].ToString() + ")|" :
                    " |HP/MbDM-(" + (currHP / linkedDM[linkedDM.Count - 1]).ToString() + "HpLife) MbDM-(" + linkedDM[linkedDM.Count - 1].ToString() + ")|";*/
                    allMText[0].text = ("Lv:" + (i + 1) + " HP=" + (currHP) + "(+" + (addHP) + ")||(" + mobHP + ")");

                    string mobEX = " |EX/MbEX-(" + (currEX / (expMulti * linkedEX[i])).ToString() + "KpLvl) MbEX-(" + linkedEX[i].ToString() + ")|";
                    //linkedEX.Count > i ? " |EX/MbEX-(" + (currEX / (expMulti * linkedEX[i])).ToString() + "KpLvl) MbEX-(" + linkedEX[i].ToString() + ")|" :
                    //" |#MobsTillLvl-(" + (currEX / (expMulti * linkedEX[linkedEX.Count - 1])).ToString() + "KpLvl) MbEX-(" + linkedEX[linkedEX.Count - 1].ToString() + ")|";
                    allMText[1].text = ("EX=" + (currEX) + "(+" + addEX + ")|||(" + mobEX + ")");

                    string mobDM = " |MbHP/DM-(" + (linkedHP[i] / currDM).ToString() + "HpMob) MbHP-(" + (currHP / linkedDM[i]).ToString() + ")|";
                    //linkedDM.Count > 1 ? " |MbHP/DM-(" + (linkedHP[i] / currDM).ToString() + "HpMob) MbHP-(" + (currHP / linkedDM[i]).ToString() + ")|" :
                    //" |MbHP/DM-(" + (currHP / linkedDM[linkedDM.Count - 1]).ToString() + "HpMob) MbHP-(" + (currHP / linkedDM[linkedDM.Count - 1]).ToString() + ")|";
                    allMText[2].text = ("DM=" + (currDM) + "(+" + (addDM) + ")||(" + mobDM + ")");

                    string mobCO = linkedCO.Count > i ? " |MbCO/LV-(" + (linkedCO[i]) + "=> " + ((linkedCO[i] == 0 ?
                        1 : linkedCO[i]) / (i == 0 ? 1 : i)).ToString() + ")|" : " |MbCO/LV-(" + (linkedCO[linkedCO.Count - 1]) + "=> " + ((linkedCO[linkedCO.Count - 1] == 0 ? 1 :
                        (linkedCO[linkedCO.Count - 1])) / (i == 0 ? 1 : i)).ToString() + ")|";
                    allMText[3].text = ("CO=" + (currCO) + ")||(" + (mobCO) + ")");

                    allMText[4].text = ("SC=" + (currSC) + "(+" + addSC + ")");

                    prefabs.Add(newMLinkedFab);
                }
                else
                {
                    GameObject newFab = Instantiate(prefab, prefab.transform.parent);
                    newFab.name = "_" + i.ToString() + "_" + current.ToString();
                    Text[] allText = newFab.GetComponentsInChildren<Text>();
                    // allText[0].text = ("Lv:" + current + " HP=" + (currHP += ScaledHP(current, (int)hp.x, hp.y, hp.z)) + "(" + ScaledHP(current, (int)hp.x, hp.y, hp.z) + ")");
                    allText[0].text = ("Lv:" + (i + 1) + " HP=" + (currHP) + "(+" + (addHP) + ")");
                    allText[1].text = ("EX=" + (currEX) + "(+" + addEX + "[" + (currEX) / (expMulti * addEX) + "MbsToLvl])");
                    allText[2].text = ("DM=" + (currDM) + "(+" + (addDM) + "|[" + (currHP / currDM) + "])");
                    allText[3].text = ("CO=" + (currCO));
                    allText[4].text = ("SC=" + currSC + "(+" + addSC + ")");
                    prefabs.Add(newFab);
                }
                //current = 0;
            yield return new WaitForSeconds(0.01f);
            }

            if (prefabs.Count > maxStats)
                break;
        }

        prefabs.OrderBy(ob => ob.name).ToList();
        StartCoroutine(StartNextCalculation());
    }
    public void SpawnValues()
    {
        StopAllCoroutines();
        StartCoroutine(SpawnValuesCoroutine());

  //      if (mobCalc != null)
		//{
  //          StartMobCalculation();
		//}
  //      ClearPrefabs();
  //      int count = levelsMax / levelStep;
  //      int current = 0;
  //      currHP = (int)hp.x;
  //      currEX = (int)ex.x;
  //      currDM = (int)dm.x;
  //      currCO = (int)co.x;
  //      currSC = 1;
  //      Text[] prefabTxt = prefab.GetComponentsInChildren<Text>();
  //      prefabTxt[0].text = "HP(" + hp + ")";
  //      prefabTxt[1].text = "EX(" + ex + ")";
  //      prefabTxt[2].text = "DM(" + dm + ")";
  //      prefabTxt[3].text = "CO(" + co + ")";
  //      prefabTxt[4].text = "SC(" + sc + ")";
		//for(int i = 0; i < levelsMax; i++)
		//{
		//	current++;
		//	int addHP = (int)(MobHP(hp.x, hp.y, hp.z, i));
		//	currHP += addHP;
		//	if(playerCalc != null)
		//		playerCalc.linkedHP.Add(currHP);
		//	if(mobCalc != null)
		//		mobCalc.linkedHP.Add(currHP);
		//	int addDM = (int)MobDM(dm.x, dm.y, dm.z, i);
		//	currDM += addDM;
		//	if(playerCalc != null)
		//		playerCalc.linkedDM.Add(currDM);
		//	if(mobCalc != null)
		//		mobCalc.linkedDM.Add(currDM);
		//	int addEX = ScaledEXP(ex.x, ex.y, ex.z, i);
		//	currEX += addEX;
		//	if(playerCalc != null)
		//		playerCalc.linkedEX.Add(currEX);
		//	if(mobCalc != null)
		//		mobCalc.linkedEX.Add(currEX);
		//	float addSC = ((0.01f * ScaledTransform(i, sc.x, sc.y, sc.z)));
		//	currSC += addSC;
		//	int addCO = ScaledCoin(currHP, co.x, co.y, i, co.z);
		//	currCO += addCO;
		//	if(playerCalc != null)
		//		playerCalc.linkedCO.Add(currCO);
		//	if(mobCalc != null)
		//		mobCalc.linkedCO.Add(currCO);
		//	if(current == levelStep)
		//	{
				
				
		//		if(playerCalc != null)
		//		{
		//			GameObject newPLinkedFab = Instantiate(prefab, prefab.transform.parent);
		//			newPLinkedFab.name = "_" + i.ToString() + "_" + current.ToString();
		//			current = 0;
		//			Text[] allPText = newPLinkedFab.GetComponentsInChildren<Text>();
		//			// allText[0].text = ("Lv:" + current + " HP=" + (currHP += ScaledHP(current, (int)hp.x, hp.y, hp.z)) + "(" + ScaledHP(current, (int)hp.x, hp.y, hp.z) + ")");
		//			allPText[0].text = ("Stg:" + (i + 1) + " HP=" + (currHP) + "(+" + (addHP) + ")");
		//			allPText[1].text = ("EX=" + (currEX) + "(+" + addEX + "[" + (currEX) / (expMulti * addEX) + "MbsToLvl])");
		//			allPText[2].text = ("DM=" + (currDM) + "(+" + (addDM) + "|[" + (currHP / currDM) + "])");
		//			allPText[3].text = ("CO=" + (currCO));
		//			allPText[4].text = ("SC=" + currSC + "(+" + addSC + ")");

		//			prefabs.Add(newPLinkedFab);
		//		} else if(mobCalc != null)
		//		{
		//			GameObject newMLinkedFab = Instantiate(prefab, prefab.transform.parent);
		//			newMLinkedFab.name = "_" + i.ToString() + "_" + current.ToString();
		//			current = 0;
		//			Text[] allMText = newMLinkedFab.GetComponentsInChildren<Text>();
  //                  // allText[0].text = ("Lv:" + current + " HP=" + (currHP += ScaledHP(current, (int)hp.x, hp.y, hp.z)) + "(" + ScaledHP(current, (int)hp.x, hp.y, hp.z) + ")");

  //                  string mobHP = " |HP/MbDM-(" + (currHP / linkedDM[i]).ToString() + "HpLife) MbDM-(" + linkedDM[i].ToString() + ")|";
  //                      /*linkedHP.Count > i ? " |HP/MbDM-(" + (currHP / linkedDM[i]).ToString() + "HpLife) MbDM-(" + linkedDM[i].ToString() + ")|" :
  //                      " |HP/MbDM-(" + (currHP / linkedDM[linkedDM.Count - 1]).ToString() + "HpLife) MbDM-(" + linkedDM[linkedDM.Count - 1].ToString() + ")|";*/
  //                  allMText[0].text = ("Lv:" + (i + 1) + " HP=" + (currHP) + "(+" + (addHP) + ")||(" + mobHP + ")");

  //                  string mobEX = " |EX/MbEX-(" + (currEX / (expMulti * linkedEX[i])).ToString() + "KpLvl) MbEX-(" + linkedEX[i].ToString() + ")|";
  //                      //linkedEX.Count > i ? " |EX/MbEX-(" + (currEX / (expMulti * linkedEX[i])).ToString() + "KpLvl) MbEX-(" + linkedEX[i].ToString() + ")|" :
  //                      //" |#MobsTillLvl-(" + (currEX / (expMulti * linkedEX[linkedEX.Count - 1])).ToString() + "KpLvl) MbEX-(" + linkedEX[linkedEX.Count - 1].ToString() + ")|";
  //                  allMText[1].text = ("EX=" + (currEX) + "(+" + addEX + ")|||(" + mobEX + ")");

  //                  string mobDM = " |MbHP/DM-(" + (linkedHP[i] / currDM).ToString() + "HpMob) MbHP-(" + (currHP / linkedDM[i]).ToString() + ")|";
  //                      //linkedDM.Count > 1 ? " |MbHP/DM-(" + (linkedHP[i] / currDM).ToString() + "HpMob) MbHP-(" + (currHP / linkedDM[i]).ToString() + ")|" :
  //                      //" |MbHP/DM-(" + (currHP / linkedDM[linkedDM.Count - 1]).ToString() + "HpMob) MbHP-(" + (currHP / linkedDM[linkedDM.Count - 1]).ToString() + ")|";
  //                  allMText[2].text = ("DM=" + (currDM) + "(+" + (addDM) + ")||(" + mobDM + ")");

  //                  string mobCO = linkedCO.Count > i ? " |MbCO/LV-(" + (linkedCO[i]) + "=> " + ((linkedCO[i] == 0 ?
  //                      1 : linkedCO[i]) / (i == 0 ? 1 : i)).ToString() + ")|" : " |MbCO/LV-(" + (linkedCO[linkedCO.Count - 1]) + "=> " + ((linkedCO[linkedCO.Count - 1] == 0 ? 1 :
  //                      (linkedCO[linkedCO.Count - 1])) / (i == 0 ? 1 : i)).ToString() + ")|";
  //                  allMText[3].text = ("CO=" + (currCO) + ")||(" + (mobCO) + ")");

  //                  allMText[4].text = ("SC=" + (currSC) + "(+" + addSC + ")");

  //                  prefabs.Add(newMLinkedFab);
		//		} else
		//		{
		//			GameObject newFab = Instantiate(prefab, prefab.transform.parent);
		//			newFab.name = "_" + i.ToString() + "_" + current.ToString();
		//			Text[] allText = newFab.GetComponentsInChildren<Text>();
		//			// allText[0].text = ("Lv:" + current + " HP=" + (currHP += ScaledHP(current, (int)hp.x, hp.y, hp.z)) + "(" + ScaledHP(current, (int)hp.x, hp.y, hp.z) + ")");
		//			allText[0].text = ("Lv:" + (i + 1) + " HP=" + (currHP) + "(+" + (addHP) + ")");
		//			allText[1].text = ("EX=" + (currEX) + "(+" + addEX + "[" + (currEX) / (expMulti * addEX) + "MbsToLvl])");
		//			allText[2].text = ("DM=" + (currDM) + "(+" + (addDM) + "|[" + (currHP / currDM) + "])");
		//			allText[3].text = ("CO=" + (currCO));
		//			allText[4].text = ("SC=" + currSC + "(+" + addSC + ")");
		//			prefabs.Add(newFab);
		//		}
		//		//current = 0;
		//	}

		//	if(prefabs.Count > maxStats)
		//		break;
		//}

  //      prefabs.OrderBy(ob => ob.name).ToList();
  //      StartCoroutine(StartNextCalculation());
    }

    public float MobHP(float baseHP, float rate, float flat, int stage)
    {
        float hp = (baseHP * (Mathf.Pow(rate, (Mathf.Min(stage, 115))) * (Mathf.Pow(flat, (Mathf.Max((stage - 115), 0))))));
        return hp;
    }
    public int ScaledHP(int _level, int _base, float _rate, float _flat)
    {
        //b * (1 + (level - 1) * r) + (f * (level - 1))
        
        float hp = _base * (1 + (_level - 1) * _rate) + (_flat * (_level - 1));
        return (int)hp;
    }
    public float MobDM(float baseDM, float rate, float flat, int stage) {
        float dm = (baseDM * (Mathf.Pow(rate, (Mathf.Min(stage, 115))) * (Mathf.Pow(flat, (Mathf.Max((stage - 115), 0))))));
        return dm;
    }
    public int ScaledDamage(int _level, int _base, float _rate, float _flat)
    {
        float damage = _base * (1 + (_level - 1) * _rate) + (_flat * (_level - 1));
        return (int)damage;
    }
    public int ScaledEXP(float baseEX, float rate, float flat, int stage)
    {
        //b * (1 + (level - 1) * r) + (f * (level - 1))
        //float exp = _base * (1 + (_level - 1) * _rate) + (_flat * (_level - 1
        float exp = (baseEX * (Mathf.Pow(rate, (Mathf.Min(stage, 115))) * (Mathf.Pow(flat, (Mathf.Max((stage - 115), 0))))));
        return (int)exp;

    }
    
    public int ScaledCoin(int baseCO, float rate, float flat, int stage, float bonus)
    {
        //b * (1 + (level - 1) * r) + (f * (level - 1))
        float coin = baseCO * rate + flat * Mathf.Min(stage, 150) * bonus;
        return (int)coin;

    }
    public float ScaledTransform(int _level, float _base, float _rate, float _flat)
    {
        //b * (1 + (level - 1) * r) + (f * (level - 1))

        float xyz = _base * (1 + (_level - 1) * _rate) + (_flat * (_level - 1));

        return xyz;
    }
}
