﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
public class LoadingScreen : MonoBehaviour {
	public static LoadingScreen _Instance = null;
	public delegate void OnActiveSceneChangeDelegate();
	public OnActiveSceneChangeDelegate OnActiveSceneChange;
	public bool isDone = false;
	private bool initialized = false;
	// Use this for initialization


	public Slider progressBar;
	public Text progressText;
	public Transform loadingParent;
	public Scene activeScene;
	public bool disableASyncLoading = true;
	private void OnEnable()
	{
		//OnActiveSceneChange += GameManager._Instance.GameLoadMap;
	}
	private void OnDisable()
	{
		//OnActiveSceneChange = null;
	}
	void Start ()
	{
		if(_Instance == null)
		{
			_Instance = this;
			DontDestroyOnLoad(gameObject);

		} else
		{
			gameObject.SetActive(false);
		}
	}
	
	void ResetLogic()// anything that the player does not notice, like a bool to tell other object the load is done
	{
		initialized = false;
		isDone = true;
	}
	void ResetVisuals()//Anything the player would see during the loadscreen
	{
		progressBar.value = 0f;
		progressText.text = (0f).ToString() + "%";
		loadingParent.gameObject.SetActive(false);
	}
	// Update is called once per frame
	public void GoToScene( string sceneName)
	{
		if(!initialized)
		{
			isDone = false;
			initialized = true;
			loadingParent.gameObject.SetActive(true);
			StartCoroutine(LoadScene(sceneName));
		}
	}
	void Update () {
		
	}
	AsyncOperation aSync;
	AsyncOperation deSync;
	private string nextScene;
	private string lastScene;
	IEnumerator LoadScene(string sceneName)
	{
		if (disableASyncLoading)
		{
			GameObject[] cache = SceneManager.GetActiveScene().GetRootGameObjects();
			for (int i = 0; i < cache.Length; i++)
			{
				cache[i].SetActive(false);
			}
			aSync = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);
			aSync.allowSceneActivation = true;

		}
		else
		{
			lastScene = SceneManager.GetActiveScene().name;
			nextScene = sceneName;
			GameObject[] cache = SceneManager.GetActiveScene().GetRootGameObjects();
			for (int i = 0; i < cache.Length; i++)
			{
				cache[i].SetActive(false);
			}
			aSync = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);
			//aSync.allowSceneActivation = false;
			EventSystem eSys = GameObject.FindObjectOfType<EventSystem>();
			if (eSys != null)
				eSys.gameObject.SetActive(false);
			while (aSync.progress < 0.9f)
			{
				float scaledPerc = 0.5f * aSync.progress / 0.9f;
				progressText.text = ((int)(100f * scaledPerc)).ToString() + "%";
				progressBar.value = scaledPerc;
			}
			aSync.allowSceneActivation = true;
			float perc = 0.5f;
			float time = 0.05f;
			while (!aSync.isDone)
			{
				yield return new WaitForSeconds(0.1f);
				perc = Mathf.Lerp(perc, 1f, time);
				progressText.text = ((int)(100f * perc)).ToString() + "%";
				progressBar.value = perc;
			}
			progressText.text = (100).ToString() + "%";
			progressBar.value = 1f;
			SceneManager.SetActiveScene(SceneManager.GetSceneByName(sceneName));
		//SceneManager.UnloadSceneAsync(SceneManager.GetSceneByName(lastScene));

		//yield return new WaitForSeconds(0.5f);
		//Scene lastScene = SceneManager.GetActiveScene();
		//Scene nextScene = SceneManager.GetSceneByName(sceneName);

		////Camera oldMainCamera = Camera.main;
		////Camera.main.tag = "Untagged";

		//AsyncOperation aSyncOp = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);
		//float perc = 0.5f;
		//float time = 0.05f;
		//while(!aSyncOp.isDone)
		//{
		//	perc = Mathf.Lerp(perc, 1f, time);
		//	progressText.text = ((int)(100f * perc)).ToString() + "%";
		//	progressBar.value = perc;
		//	if(aSyncOp.progress == 0.9f)
		//	{
		//		//oldMainCamera.gameObject.SetActive(false);
		//		GameObject[] cache = lastScene.GetRootGameObjects();
		//		for(int i = 0; i < cache.Length; i++)
		//		{
		//			cache[i].SetActive(false);
		//		}
		//		yield return null;
		//	}
			
		//		//SceneManager.SetActiveScene(nextScene);
		//		//SceneManager.UnloadSceneAsync(lastScene);
		//}


		
		//progressText.text = (100).ToString() + "%";
		//progressBar.value = 1f;
		}
		yield return new WaitForSeconds(0.8f);// a half a sec window for scripts to update and for the player to realize the scene loaded
		ResetLogic();

		if (OnActiveSceneChange != null)
			OnActiveSceneChange();
		yield return new WaitForSeconds(0.8f);// a half a sec window for scripts to update and for the player to realize the scene loaded
		ResetVisuals();
	}
}
