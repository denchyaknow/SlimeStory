﻿using System.Collections;
using System.Collections.Generic;
using Prototype.NetworkLobby;
using System;
using System.IO;
using System.Xml;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using Denchyaknow;
[System.Serializable]
public class GameManager : MonoBehaviour
{
	public bool gamePaused = false;
	#region Vars and Properties
	public static GameManager _Instance = null;
	public delegate void Delegate();
	public Delegate OnOneShotDisable;
	public Delegate OnGamePause;
	public Delegate OnGameResume;
	private Transform grb;
	public Transform garbageCan
	{
		get
		{
			if (grb == null)
			{
				GameObject newGarbage = new GameObject("GarbageCan");
				grb = newGarbage.transform;
			}
			return grb;
		}
	}
	private Database dB;
	public Database database
	{
		get
		{
			if (dB == null)
				dB = transform.GetComponentInChildren<Database>();
			return dB;
		}
	}
	private CameraManager cM;
	public CameraManager cameraManager
	{
		get
		{
			if (cM == null)
				cM = transform.GetComponentInChildren<CameraManager>();
			return cM;
		}
		set
		{
			cM = value;
		}
	}
	private PlayerControls iM;
	public PlayerControls inputManager
	{
		get
		{
			if(iM == null && transform.GetComponentInChildren<PlayerControls>())
			{
				iM = transform.GetComponentInChildren<PlayerControls>();
			}
			return iM;
		}
		set
		{
			iM = value;
		}
	}
	private CombatManager cbM;
	public CombatManager combatManager
	{
		get
		{
			return cbM;
		}
		set
		{
			cbM = value;
		}
	}
	private PlayPenManager ppM;
	public PlayPenManager playPenManager
	{
		get
		{
			return ppM;
		}
		set
		{
			ppM = value;
		}
	}
	private UIMenuManager uiM;
	public UIMenuManager uiManager
	{
		get
		{
			if(uiM == null)
				uiM = GameObject.FindObjectOfType<UIMenuManager>();
			return uiM;
		}
		set
		{
			uiM = value;
		}
	}
	private CoinManager cnM;
	public CoinManager coinManager
	{
		get
		{
			if(cnM != null)
				cnM = transform.GetComponentInChildren<CoinManager>();
			return cnM;
		}
		set
		{
			cnM = value;
		}
	}
	private SoundManager sdM;
	public SoundManager soundManager
	{
		get
		{
			if(sdM == null)
				sdM = transform.GetComponentInChildren<SoundManager>();
			return sdM;
		}
		set
		{
			sdM = value;
		}
	}
	private MessageManager msM;
	public MessageManager messageManager
	{
		get
		{
			if(msM == null)
			{
				msM = transform.GetComponentInChildren<MessageManager>();
			}
			return msM;
		}
		set
		{
			msM = value;
		}
	}
	public PlayerData playerData;
	public AutoFeeder autoFeeder;
	public TrashCanProperties trashCan;
	public List<GameObject> foodList = new List<GameObject>();
	public List<GameObject> ourSlimes = new List<GameObject>();
	public List<BaseFood> ourFood = new List<BaseFood>();
	public List<BaseFood> ourFoodStorage = new List<BaseFood>();
	public List<BaseDeco> ourDeco = new List<BaseDeco>();
	public List<Transform> ourPoop = new List<Transform>();

	//Coin Count


	public List<GameObject> slimeList = new List<GameObject>();

	public List<GameObject> ourSlimeParty = new List<GameObject>();

	public int selSlimeIndex
	{
		get
		{
			int index = -1;
			if(selSlime != null)
				index = selSlime.slimeStats.index;
			return index;
		}
		set
		{
			playerData.slimeLeader = value;
		}
	}
	private BaseSlime selSlime;
	public void SetSelectedSlime(BaseSlime slime)
	{
		playerData.slimeLeader = slime.slimeStats.index;
	}
	public void SetSelectedSlime(int slimeIndex)
	{
		playerData.slimeLeader = slimeIndex;
	}
	public BaseSlime GetSelectedSlime()
	{
		int selectedSlime = -1;
		if(ourSlimes.Count > 0)
		{
			for(int i = 0; i < ourSlimes.Count; ++i)
			{
				if(ourSlimes[i] != null && ourSlimes[i].GetComponent<BaseSlime>().slimeStats.index == playerData.slimeLeader)
				{
					selectedSlime = i;
					break;
				}
			}
			if(selectedSlime == -1 && ourSlimes[0] != null)
			{
				//Debug.Log("GetSelectSlime:No slime had index(" + playerData.slimeLeader + ") returning first slime");
				return ourSlimes[0].GetComponent<BaseSlime>();
			}
		} else if(playerData.slimeData.Count == 0)
		{
			GameObject newSlime = (GameObject)Instantiate(GetSlime(0), GetSpawnEndPoint(true), Quaternion.identity);
			BaseSlime _slime = newSlime.gameObject.GetComponent<BaseSlime>();
			_slime.slimeStats.name = "Mr Basic";
			_slime.SetStats();
			_slime.slimeStats.index = playerData.slimeData.Count;
			playerData.slimeData.Add(_slime.slimeStats);
			//ourSlimes.Add(newSlime);
			//GameSave();
			Debug.Log("GetSelectSlime:No slimes returning new slime");
			return _slime;
		} else
		{
			GameLoad();
			selectedSlime = 0;
		}
		if(selectedSlime == -1)
			return null;
		else
			return ourSlimes[selectedSlime].GetComponent<BaseSlime>();

	}
	public void UnSelectSlime()
	{
		selSlime = null;
		selSlimeIndex = 0;
	}
	#endregion
	private void OnEnable()
	{
		
	}
	private void OnDisable()
	{
		
	}
	private void OnApplicationQuit()
	{
		GameSave();
	}
	private void Awake()
	{
		if(_Instance == null)
		{
			_Instance = this;
			DontDestroyOnLoad(this.gameObject);
		} else if(_Instance != this)
		{
			Destroy(this.gameObject);
			return;
		}

		if(inputManager == null)
		{
			inputManager = transform.GetComponentInChildren<PlayerControls>();
		}
		if(soundManager == null)
		{
			soundManager = transform.GetComponentInChildren<SoundManager>();
		}
		if(coinManager == null)
		{
			coinManager = transform.GetComponentInChildren<CoinManager>();
		}
		if(debugCanvas == null)
		{
			debugCanvas = transform.GetComponentInChildren<Canvas>();

		}
		mapTredmilPoints = new Transform[3] { mapTredmil.GetChild(0).transform, mapTredmil.GetChild(1).transform, mapTredmil.GetChild(2).transform };
	}

	void Start()
	{
		if(_Instance == null)
		{
			_Instance = this;
			DontDestroyOnLoad(this.gameObject);
		} else if(_Instance != this)
		{
			Destroy(this.gameObject);
			return;
		}

		//LoadXmlSlime();
		//GameLoad();
		//GameSave();
		//playerData.coinCount = 99999;
		dPanel.gameObject.SetActive(false);
		dStatPanel.gameObject.SetActive(false);
		dLog.gameObject.SetActive(false);
		EventSystem eventSys = transform.GetComponentInChildren<EventSystem>();
		//if (eventSys != null)
		//{
		//eventSys.gameObject.SetActive(false);
		//eventSys.gameObject.SetActive(true);
		//}
		//StartCoroutine(SpawnLog());
		//Application.logMessageReceived += HandleLog;
		//LoadMap();
		//LoadMapList();

		//GameManager._Instance.inputManager.PrepForNextScene();
		//yield return new WaitForSeconds(fadeTime);
		//SceneManager.LoadScene("Loading");
		//AsyncOperation async = SceneManager.LoadSceneAsync("PlayPen");
		//yield return async;
		//Debug.Log("Loading complete");

		GameTutorial();
	}
	#region Debugging
	IEnumerator SpawnLog()
	{
		for(int i = 0; i < 200; i++)
		{
			yield return new WaitForSeconds(0.1f);
			GameObject newText = Instantiate(dInfoText.transform.parent.gameObject, dInfoText.transform.parent.parent);
			dTexts.Add(newText.transform.GetComponentInChildren<Text>());

		}
	}
	[Header("Debug Stuff")]
	public _DebugType _Debug;
	public enum _DebugType
	{
		GameSave = 0,
		GameLoad = 1,
		GameClear = 2,
		GameAddSlime = 3,
		GameDebugInfo = 4,
		GameStopPooling = 5,
		GameShowStats = 6,
		GameShowLog = 7,
		GameKillWave = 8,
		GameAddCoin = 9
	}
	private Canvas debugCanvas;
	public bool DebugToggle;
	public bool Debugging;
	public bool DoNotPool;
	public Transform dPanel;
	public Transform dStatPanel;
	public Transform dLog;
	public Text dInfoText;
	private List<Text> dTexts = new List<Text>();
	void HandleLog(string logString, string stackTrace, LogType type)
	{
		if(type == LogType.Error || type == LogType.Log)
		{
			Text newText = dTexts[0].GetComponentInChildren<Text>();
			dTexts[0].transform.parent.transform.SetSiblingIndex(dTexts[0].transform.parent.childCount - 1);
			newText.text = logString;
			Color newColor = type == LogType.Error ? Color.magenta : Color.cyan;
			newText.color = newColor;
			if(type == LogType.Error)
			{
				dLog.gameObject.SetActive(true);
				dPanel.gameObject.SetActive(true);
			}
		}

	}
	#endregion
	void Update()
	{

		if(DebugToggle)
		{
			DebugToggle = false;
			debugCanvas.worldCamera = Camera.main;
			switch(_Debug)
			{

				case _DebugType.GameLoad:

					GameLoad();
					break;
				case _DebugType.GameSave:

					GameSave();
					break;
				case _DebugType.GameClear:
					GameClear();
					break;
				case _DebugType.GameAddSlime:
					GameAddSlime();
					break;
				case _DebugType.GameDebugInfo:
					Debugging = !Debugging;
					dInfoText.text = ("Debugging=" + Debugging);
					break;
				case _DebugType.GameStopPooling:
					DoNotPool = !DoNotPool;
					dInfoText.text = ("Pooling=" + DoNotPool);

					break;
				case _DebugType.GameShowStats:
					dStatPanel.gameObject.SetActive(!dStatPanel.gameObject.activeInHierarchy);
					break;
				case _DebugType.GameShowLog:
					dLog.gameObject.SetActive(!dLog.gameObject.activeInHierarchy);
					break;
				case _DebugType.GameKillWave:
					if(combatManager != null)
					{
						if(combatManager.enemies.Count > 0)
						{
							for(int i = 0; i < combatManager.enemies.Count; i++)
							{
								if(combatManager.enemies[i] != null)
									combatManager.enemies[i].TakeDamage(999999);
							}
						}
					}
					break;
				case _DebugType.GameAddCoin:
					coinManager.SpawnCoin(map.transform.position, 50);
					break;

			}
		}

	}
	#region Map Logic
	public BaseMap map;
	public List<BaseMap> mapLeft = new List<BaseMap>();
	public List<BaseMap> mapRight = new List<BaseMap>();
	public float mapTransTime = 2f;
	public Ease mapTransEase = Ease.Linear;
	public Transform mapTredmil;
	private Transform[] mapTredmilPoints;
	public bool movingToMap = false;
	public void GameLoadMap()
	{
		if(playPenManager != null)
		{
			SpawnPlaypenMap();
		}
		if(combatManager != null)
		{
			mapLeft.Clear();
			mapRight.Clear();
			SpawnCombatMap();
		}
	}
	private void SpawnPlaypenMap()
	{
		int mapIndex = 0;
		mapIndex = playerData.playPenMapIndex;
		if(playerData.playPenMapIndex >= SlimeDatabase.Maps.Length)
		{
			//Debug.Log("Tried to load map:" + playerData.playPenMapIndex.ToString() + "/" + SlimeDatabase.Maps.Length.ToString());
			mapIndex = SlimeDatabase.Maps.Length - 1;
		}
		GameObject loadedMap = Instantiate(SlimeDatabase.Maps[mapIndex]);
		GameObject lastMap = null;
		loadedMap.transform.SetParent(null);
		loadedMap.name = "Map: Playpen";
		foreach(SpriteRenderer renderer in loadedMap.transform.GetComponentsInChildren<SpriteRenderer>())
		{
			if(renderer.gameObject.activeInHierarchy)
			{
				renderer.sortingOrder = -20000;
			}
		}
		if(map != null)
		{
			lastMap = map.gameObject;
			foreach(SpriteRenderer render in map.GetComponentsInChildren<SpriteRenderer>())
			{
				render.sortingOrder = -10000;
				render.DOFade(0, 0.8f);
			}
			float time = 1;
			DOTween.To(() => time, x => time = x, 0, 1.1f).OnComplete(() =>
			{
				Destroy(lastMap);
			});
		}
		map = loadedMap.GetComponent<BaseMap>();
		map.SetMapSize(BaseMap.MapSize.Small);
		map.MapResizeBackground();
		map.ClearObjects();
		map.transform.position = mapTredmilPoints[1].position;
	}
	private void SpawnCombatMap()
	{
		int mapIndex = 0;
		mapIndex = playerData.combatMapIndex;
		if(mapIndex >= SlimeDatabase.Maps.Length)
		{
			mapIndex = SlimeDatabase.Maps.Length - 1;
			//Debug.Log("Cant spawn null combat map:" + playerData.combatMapIndex + " using last map");
		}
		GameObject loadedMap = Instantiate(SlimeDatabase.Maps[mapIndex]);
		loadedMap.transform.SetParent(null);
		loadedMap.transform.GetComponent<BaseMap>().MapRandomize();
		loadedMap.name = ("Map: " + SlimeDatabase.MapNames[mapIndex < SlimeDatabase.MapNames.Length ? mapIndex : 0]);
		if(map == null)
		{
			map = loadedMap.GetComponent<BaseMap>();
			map.transform.position = mapTredmilPoints[1].position;
			SpawnCombatMap();
		} else
		{
			mapRight.Add(loadedMap.GetComponent<BaseMap>());
			BaseMap newMap = mapRight[mapRight.Count - 1];
			newMap.transform.position = mapTredmilPoints[2].position;
			newMap.gameObject.SetActive(false);
		}

	}
	public void GameGoToRightMap()
	{

		if(mapRight.Count == 0)
		{
			if(combatManager != null)
				SpawnCombatMap();
			if(playPenManager != null)
				return;
		}
		map.transform.DOMoveX(mapTredmilPoints[0].position.x, mapTransTime, false).SetEase(mapTransEase)
			 .OnStart(() =>
			 {
				 movingToMap = true;
			 });
		mapLeft.Add(map);
		BaseMap nextMap = mapRight[mapRight.Count - 1];
		nextMap.gameObject.SetActive(true);
		nextMap.transform.DOMoveX(mapTredmilPoints[1].position.x, mapTransTime, false).SetEase(mapTransEase)
			 .OnComplete(() =>
			 {
				 uiManager.ShakeCamera();
				 movingToMap = false;
			 });
		mapRight.Remove(nextMap);
		map = nextMap;
	}
	public void GameGoToLeftMap()
	{
		if(mapLeft.Count == 0)
			return;
		map.transform.DOMoveX(mapTredmilPoints[0].position.x, mapTransTime, false).SetEase(mapTransEase)
			 .OnStart(() =>
			 {
				 movingToMap = true;
			 });
		mapRight.Add(map);
		BaseMap nextMap = mapLeft[mapRight.Count - 1];
		nextMap.gameObject.SetActive(true);
		nextMap.transform.DOMoveX(mapTredmilPoints[1].position.x, mapTransTime, false).SetEase(mapTransEase)
			 .OnComplete(() =>
			 {
				 uiManager.ShakeCamera();
				 movingToMap = false;
			 });
		mapLeft.Remove(nextMap);
		map = nextMap;
	}
	public void GameAddMap(int _ID)
	{

		playerData.playPenMapIndex = _ID;
		playerData.playPenMaps.Add(_ID);

		GameLoadMap();
	}
	public bool ConfirmedMapPurchase(int _ID)
	{
		bool bought = false;
		if(playerData.playPenMaps.Count > 0)
		{
			for(int i = 0; i < playerData.playPenMaps.Count; ++i)
			{
				if(playerData.playPenMaps[i] == _ID)
				{
					bought = true;
					break;
				}
			}
		}
		return bought;
	}
	public void LoadMapList()
	{
		if(GameManager._Instance.playerData.combatMapHighest > 0)
		{
			GameObject _defaultButton = GameObject.FindGameObjectWithTag("MapListItem");

			int mapNameID = 0;
			for(int i = 0; i < GameManager._Instance.playerData.combatMapHighest; ++i)
			{
				if(i == 0)
				{
					if(mapNameID >= SlimeDatabase.MapNames.Length)
						mapNameID = 0;
					string _name = "(" + i.ToString() + "+) " + SlimeDatabase.MapNames[mapNameID];
					_defaultButton.name = _name;
					_defaultButton.transform.GetComponentInChildren<Text>().text = _name;
					mapNameID++;
					_defaultButton.GetComponent<Button>().onClick.AddListener(() =>
					{
						GameManager._Instance.playerData.combatMapIndex = 0;
						GameManager._Instance.uiManager.GoToScene(1);
					});
				} else
				{
					GameObject butt = (GameObject)Instantiate(_defaultButton);
					butt.transform.SetParent(_defaultButton.transform.parent);
					if(mapNameID >= SlimeDatabase.MapNames.Length)
						mapNameID = 0;
					string _name = "(" + i.ToString() + "+) " + SlimeDatabase.MapNames[mapNameID];
					butt.name = _name;
					butt.transform.GetComponentInChildren<Text>().text = _name;
					mapNameID++;
					int index = i;
					butt.GetComponent<Button>().onClick.AddListener(() =>
					{
						GameManager._Instance.playerData.combatMapIndex = index;
						GameManager._Instance.uiManager.GoToScene(1);
					});
					butt.transform.localScale = new Vector3(1, 1, 1);
				}
			}
		}
	}
	#endregion
	public void ToggleDebugMenu()
	{
		dPanel.gameObject.SetActive(!dPanel.gameObject.activeInHierarchy);
	}
	public void DebugOption(int option)
	{
		_Debug = (_DebugType)option;
		DebugToggle = true;
		switch((_DebugType)option)
		{
			case _DebugType.GameSave:

				break;
			case _DebugType.GameLoad:

				break;
			case _DebugType.GameClear:

				break;
			case _DebugType.GameAddSlime:

				break;
			case _DebugType.GameDebugInfo:

				break;
		}
	}
	private string fileExt = "/SlimeSwag.swagger";
	private string filePath()
	{
		return Application.persistentDataPath;
	}
	private string fileVersion()
	{
		return "_" + Application.version;
	}
	public string GetDataPath()
	{
		string path = filePath() + fileExt + fileVersion();
		return path;
	}
	private bool isLoading = false;
	private bool isSaving = false;
	private bool isClearing = false;
	public void GameSave()
	{
		if (isLoading||isClearing)
			return;
		isSaving = true;
		if (combatManager != null)
		{
			CombatSave();
			return;
		}
		if(ourFood.Count > 0)
		{
			playerData.slimeFood.Clear();
			for(int i = 0; i < ourFood.Count; ++i)
			{
				playerData.slimeFood.Add(ourFood[i].foodData);

			}
		} else if(playerData.slimeFood.Count > 0)
		{
			playerData.slimeFood.Clear();
			
		}
		if(ourFoodStorage.Count > 0)
		{
			playerData.slimeFoodStorage.Clear();
			for(int i = 0; i < ourFoodStorage.Count; i++)
			{
				playerData.slimeFoodStorage.Add(ourFoodStorage[i].foodData);
			}
		} else if(playerData.slimeFoodStorage.Count > 0)
		{
			playerData.slimeFoodStorage.Clear();

		}
		if(ourDeco.Count > 0)
		{
			playerData.playPenDeco.Clear();
			for(int i = 0; i < ourDeco.Count; i++)
			{
				playerData.playPenDeco.Add(ourDeco[i].decoData);
			}
		} else if(playerData.playPenDeco.Count > 0)
		{
			playerData.playPenDeco.Clear();

		}
		if(ourPoop.Count > 0)
		{
			playerData.slimePoop.Clear();
			for(int i = 0; i < ourPoop.Count; ++i)
			{
				playerData.slimePoop.Add(ourPoop[i].GetComponent<BasePoop>().poopData);
			}
		} else if(playerData.slimePoop.Count > 0)
		{
			playerData.slimePoop.Clear();

		}
		if(ourSlimes.Count > 0)
		{
			Debug.Log("Saving " + ourSlimes.Count + " slimes");
			playerData.slimeData.Clear();
			for(int i = 0; i < ourSlimes.Count; i++)
			{
				BaseSlime bSlime = ourSlimes[i].GetComponent<BaseSlime>();
				bSlime.slimeStats.penPosX = ourSlimes[i].transform.position.x;
				bSlime.slimeStats.penPosY = ourSlimes[i].transform.position.y;
				playerData.slimeData.Add(bSlime.slimeStats);
				Debug.Log((bSlime != null ? "Cached " : "Couldnt cache ") + " slime:" + i.ToString());
			}
		}
		string path = GetDataPath();
		playerData.version = Application.version;
		BinaryFormatter formatter = new BinaryFormatter();
		if (File.Exists(path))
		{
			File.Delete(path);
			Debug.Log("Overwriting SaveData...");
		}
		FileStream file = File.Create(path);
		formatter.Serialize(file, playerData);
		file.Close();


		Debug.Log((File.Exists(path)?"Saved":"Couldnt save") + " playerData to " + path +"\n(" 
			+ playerData.slimeData.Count.ToString() + " Slimes)\n("
			+ playerData.slimeFood.Count.ToString() + " Food)\n(" 
			+ playerData.slimeFoodStorage.Count.ToString() + " FoodStor)\n("
			+ playerData.playPenDeco.Count.ToString() + " Deco)\n("
			+ playerData.slimePoop.Count.ToString() + " Poop)\n("
			+ playerData.slimeLeader.ToString() + " Leader)\n("
			+ playerData.version + " Version)");
		isSaving = false;
	}
	public void GameLoad()
	{
		if (isSaving || isClearing)
			return;
		isLoading = true;
		string path = GetDataPath();
		bool _slimesLoaded = false;
		if(File.Exists(path))
		{
			
			_slimesLoaded = true;
			
		}

		if(!_slimesLoaded)
		{
			playerData = new PlayerData();
			playerData.version = Application.version;
			GameAddSlime();
			if(autoFeeder != null)
				autoFeeder.gameObject.SetActive(false);
			//Debug.Log("No playerData in " + Application.persistentDataPath + "/SlimeSwag.swagger so spawned default slime");

		} else
		{
			BinaryFormatter formatter = new BinaryFormatter();
			FileStream file = File.Open(path, FileMode.Open);
			playerData = (PlayerData)formatter.Deserialize(file);
			file.Close();
			Debug.Log((File.Exists(path)?"Loading":"Failed to load") + " playerData from " + GetDataPath());
			if (playerData.version != Application.version)
			{
				Debug.Log("Version Mismatch: " + playerData.version + "!=" + Application.version);
				playerData = new PlayerData();
			}
			
			if (playerData.slimePoop.Count > 0)
			{
				for(int i = 0; i < playerData.slimePoop.Count; ++i)
				{
					GameObject newPoop = (GameObject)Instantiate(SlimeDatabase.Poop, transform.position, Quaternion.identity);
					BasePoop newBasePoop = newPoop.GetComponent<BasePoop>();
					PoopData newPoopData = playerData.slimePoop[i];
					newPoopData.pooID = i;
					newBasePoop.poopData = newPoopData;
					newPoop.transform.position = new Vector3(newPoopData.x, newPoopData.y, newPoopData.z);
					;
					newPoop.transform.SetParent(garbageCan);
					ourPoop.Add(newPoop.transform);
				}
			}
			if(playerData.slimeFood.Count > 0)
			{
				for(int i = 0; i < playerData.slimeFood.Count; ++i)
				{
					GameObject newFood = (GameObject)Instantiate(SlimeDatabase.Food[playerData.slimeFood[i].index], transform.position, Quaternion.identity);
					Vector3 foodPos = new Vector3(playerData.slimeFood[i].x, playerData.slimeFood[i].y, 0);
					BaseFood food = newFood.transform.GetComponent<BaseFood>();
					newFood.transform.position = foodPos;
					newFood.transform.localScale = new Vector3(1, 1, 1);
					newFood.transform.SetParent(garbageCan);
					newFood.transform.eulerAngles = Vector3.zero;
					newFood.transform.GetComponent<Collider2D>().enabled = true;
					newFood.transform.GetComponent<Rigidbody2D>().gravityScale = 0f;
					newFood.transform.GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
					food.foodData = playerData.slimeFood[i];
					food.foodData.instanceID = i;
					if(ourFoodStorage.Contains(food))
						ourFoodStorage.Remove(food);
					if(!ourFood.Contains(food))
						ourFood.Add(food);
					//food.currentBite = newFood.GetComponent<BaseFood>().foodData.biteCount;
					//food.SetupFood();
					//ourFood.Add(newFood.GetComponent<BaseFood>());
				}
			}
			if(playerData.slimeFoodStorage.Count > 0)
			{
				for(int i = 0; i < playerData.slimeFoodStorage.Count; ++i)
				{
					GameObject newFood = (GameObject)Instantiate(SlimeDatabase.Food[playerData.slimeFoodStorage[i].index], transform.position, Quaternion.identity);
					Vector3 foodPos = new Vector3(playerData.slimeFoodStorage[i].x, playerData.slimeFoodStorage[i].y, 0);
					BaseFood food = newFood.transform.GetComponent<BaseFood>();
					newFood.transform.position = foodPos;

					newFood.transform.SetParent(garbageCan);
					food.foodData = playerData.slimeFoodStorage[i];
					food.foodData.instanceID = i;
					if(ourFood.Contains(food))
						ourFood.Remove(food);
					if(!ourFoodStorage.Contains(food))
						ourFoodStorage.Add(food);
					//food.currentBite = newFood.GetComponent<BaseFood>().foodData.biteCount;
					//food.SetupFood();
					//ourFood.Add(newFood.GetComponent<BaseFood>());
				}
			}
			if(playerData.playPenDeco.Count > 0)
			{
				for(int i = 0; i < playerData.playPenDeco.Count; i++)
				{
					int index = 0;
					for(int j = 0; j < SlimeDatabase.Decorations.Length; j++)
					{
						if(SlimeDatabase.Decorations[j].GetComponent<BaseDeco>().decoData.decoID == playerData.playPenDeco[i].decoID)
							index = j;
					}
					GameObject newDeco = null;
					bool isAutoFeeder = SlimeDatabase.Decorations[index].GetComponent<AutoFeeder>();
					if (isAutoFeeder)
					{
						newDeco = autoFeeder.gameObject;
					}
					else
					{
						newDeco = Instantiate(SlimeDatabase.Decorations[index]);
					}
					BaseDeco deco = newDeco.GetComponent<BaseDeco>();
					newDeco.name = deco.decoData.name;
					deco.decoData = playerData.playPenDeco[i];
					ourDeco.Add(deco);
					if (!isAutoFeeder)
					{
						Vector3 decoPos = new Vector3(playerData.playPenDeco[i].x, playerData.playPenDeco[i].y, 0);
						Vector3 decoRot = new Vector3(0, 0, 0);
						newDeco.transform.position = decoPos;
						newDeco.transform.eulerAngles = decoRot;
						newDeco.transform.SetParent(garbageCan);
					}
				}
			}
			if(playerData.slimeData.Count > 0)
			{
				Debug.Log("Loading "+ playerData.slimeData.Count + " slimes");

				for (int i = 0; i < playerData.slimeData.Count; ++i)
				{
					GameLoadSlime(playerData.slimeData[i]);
					Debug.Log("Loaded slime " + playerData.slimeData[i].name);
				}
			} else
			{
				Debug.Log("Loading no slimes");

				//Debug.Log("Player has no effing slimes");
			}
		}
		isLoading = false;
		inputManager.SetControlScheme();
	}
	private void CombatSave()
	{
		if (combatManager.playerSlime != null)
		{
			BaseSlime currentSlime = combatManager.playerSlime;
			int selectedSlimeIndex = currentSlime.slimeStats.index;
			for (int i = 0; i < playerData.slimeData.Count; i++)
			{
				if (playerData.slimeData[i].index == selectedSlimeIndex)
				{
					playerData.slimeData[i] = currentSlime.slimeStats;
					break;
				}
			}
		}
		string path = GetDataPath();

		playerData.version = Application.version;
		BinaryFormatter formatter = new BinaryFormatter();
		if (File.Exists(path))
		{
			File.Delete(path);
			Debug.Log("Overwriting SaveData...");
		}
		FileStream file = File.Create(GetDataPath());
		formatter.Serialize(file, playerData);
		file.Close();
		
		Debug.Log((File.Exists(path) ? "Saved" : "Couldnt save") + " combat playerData to " + path + "\n("
			+ playerData.slimeData.Count.ToString() + " Slimes)\n("
			+ playerData.slimeFood.Count.ToString() + " Food)\n("
			+ playerData.slimeFoodStorage.Count.ToString() + " FoodStor)\n("
			+ playerData.playPenDeco.Count.ToString() + " Deco)\n("
			+ playerData.slimePoop.Count.ToString() + " Poop)\n("
			+ playerData.slimeLeader.ToString() + " Leader)\n("
			+ playerData.version + " Version)");
		isSaving = false;
	}
	public void GamePause()
	{
		gamePaused = !gamePaused;
	}
	public void GamePause(bool pause)
	{
		gamePaused = pause;
	}
	public void GameClear()
	{
		
		if(File.Exists(GetDataPath()))
		{
			File.Delete(GetDataPath());
		}
		//if (ourDeco.Count > 0)
		//{
		//	for (int i = 0; i < ourDeco.Count; i++)
		//	{
		//		if (ourDeco[i] == null)
		//			return;
		//		BaseDeco deco = ourDeco[i];
		//		ourDeco.Remove(deco);
		//		bool isAutoFeeder = ourDeco[i].GetComponent<AutoFeeder>();
		//		if (!isAutoFeeder)
		//		{
		//			Destroy(deco.gameObject);
		//		}
		//	}
		//}
		if (GameObject.FindObjectOfType<BaseDeco>())
		{
			BaseDeco[] allDeco = GameObject.FindObjectsOfType<BaseDeco>();
			if (allDeco.Length > 0)
			{
				for (int i = 0; i < allDeco.Length; i++)
				{
					if (!allDeco[i].gameObject.GetComponent<AutoFeeder>())
					{
						Destroy(allDeco[i]);
					}
				}
			}
		}
		if (GameObject.FindGameObjectWithTag("Poop"))
		{
			GameObject[] allPoops = GameObject.FindGameObjectsWithTag("Poop");
			if(allPoops.Length > 0)
			{
				for(int i = 0; i < allPoops.Length; ++i)
				{
					Destroy(allPoops[i]);
				}
			}
		}
		if(GameObject.FindGameObjectWithTag("Food"))
		{
			GameObject[] allFood = GameObject.FindGameObjectsWithTag("Food");
			if(allFood.Length > 0)
			{
				for(int i = 0; i < allFood.Length; ++i)
				{
					Destroy(allFood[i]);
				}
			}
		}
		if(GameObject.FindGameObjectWithTag("Slime"))
		{
			GameObject[] allSlimes = GameObject.FindGameObjectsWithTag("Slime");
			if(allSlimes.Length > 0)
			{
				for(int i = 0; i < allSlimes.Length; ++i)
				{
					Destroy(allSlimes[i]);
				}
			}
		}
		playerData = new PlayerData();
		ourPoop.Clear();
		ourSlimes.Clear();
		ourFood.Clear();
		ourFoodStorage.Clear();
		ourDeco.Clear();
		//GameAddSlime();
		GameLoad();
		playerData.coinCount = 50;
		GameTutorial(0);
		GameSave();
		SceneManager.LoadScene(1);
		
	}
	public void GameUnload()
	{
		if (isLoading || isSaving)
			return;
		isClearing = true;
		if (ourSlimes.Count > 0)
		{
			for (int i = 0; i < ourSlimes.Count; i++)
			{
				if (ourSlimes[i] != null)
					Destroy(ourSlimes[i]);
			}
		}
		if (ourPoop.Count > 0)
		{
			for (int i = 0; i < ourPoop.Count; i++)
			{
				if (ourPoop[i] != null)
					Destroy(ourPoop[i].gameObject);
			}
		}
		if (ourFood.Count > 0)
		{
			for (int i = 0; i < ourFood.Count; i++)
			{
				if (ourFood[i] != null)
					Destroy(ourFood[i].gameObject);
			}
		}
		if (ourFoodStorage.Count > 0)
		{
			for (int i = 0; i < ourFoodStorage.Count; i++)
			{
				if (ourFoodStorage[i] != null)
					Destroy(ourFoodStorage[i].gameObject);
			}
		}

		if (ourDeco.Count > 0)
		{
			for (int i = 0; i < ourDeco.Count; i++)
			{
				if (ourDeco[i] != null)
					Destroy(ourDeco[i].gameObject);
			}
		}
		ourSlimes.Clear();
		ourPoop.Clear();
		ourFood.Clear();
		ourFoodStorage.Clear();
		ourDeco.Clear();
		Debug.Log("Game Unloaded");
		isClearing = false;
	}
	public void GameTutorial(int finished = -1)
	{
		if (!PlayerPrefs.HasKey("TUTORIAL"))
		{
			PlayerPrefs.SetInt("TUTORIAL", 0);
			Debug.Log("Added key to PlayerPrefs: TUTORIAL");
		}
		if (finished != -1)
		{
			PlayerPrefs.SetInt("TUTORIAL", finished);
		}
		GameObject tut = null;
		if (PlayerPrefs.GetInt("TUTORIAL") == 0)
		{
			if (Resources.Load("GameObjects/UI/Tutorial") != null)
				tut = (GameObject)Instantiate(Resources.Load("GameObjects/UI/Tutorial"));
			else
				Debug.Log("Failed to load Tutorial, No obj Found in Resources/GameObject/UI/Tutorial");
		}
	}

	private string GetRandomName(BaseSlime slime)
	{
		int randomName = UnityEngine.Random.Range(0, SlimeDatabase.SlimeNames.Length - 1);
		string newName = SlimeDatabase.SlimeNames[randomName];
		bool foundMatch = false;
		if (ourSlimes.Count > 0)
		{
			for (int i = 0; i < ourSlimes.Count; i++)
			{
				if (ourSlimes[i] != slime.gameObject)
				{
					BaseSlime compareSlime = ourSlimes[i].GetComponent<BaseSlime>();
					if (compareSlime.slimeStats.name == newName)
					{
						foundMatch = true;
						break;
					}
				}
			}
		}
		if (foundMatch)
		{
			return GetRandomName(slime);
		}
		else
		{
			return newName;
		}
	}
	public void ResetSlimeIndex()
	{
		if (combatManager != null)
			return;
		
		
		for (int i = 0; i < ourSlimes.Count; i++)
		{
			BaseSlime slime = ourSlimes[i].transform.GetComponent<BaseSlime>();
			slime.slimeStats.index = i;
		}
	
	}
	public SlimeData GetSlimeData(int index)
	{
		SlimeData newSlimeData = new SlimeData();
		if(playerData.slimeData.Count > 0)
		{
			for(int i = 0; i < playerData.slimeData.Count; ++i)
			{
				if(playerData.slimeData[i].index == index)
				{
					newSlimeData = playerData.slimeData[i];
					break;
				}
			}
		}
		return newSlimeData;
	}
	public GameObject GetSlime(int classType)
	{
		GameObject newSlime = SlimeDatabase.FriendlySlimes[0];
		if(SlimeDatabase.FriendlySlimes.Length > 0)
		{
			for(int i = 0; i < SlimeDatabase.FriendlySlimes.Length; ++i)
			{
				if(SlimeDatabase.FriendlySlimes[i].GetComponent<BaseSlime>().slimeStats.classType == classType)
				{
					newSlime = SlimeDatabase.FriendlySlimes[i];
					break;
				}
			}
		}
		return newSlime;
	}
	public BaseSlime GameLoadSelectedSlime()
	{
		SlimeData newSlimeData = GetSlimeData(playerData.slimeLeader);
		GameObject newSlime = Instantiate(GetSlime(newSlimeData.classType));

		BaseSlime _slime = newSlime.gameObject.GetComponent<BaseSlime>();
		_slime.slimeStats = newSlimeData;
		_slime.SetStats();
		return _slime;
	}
	public void GameLoadSlime(SlimeData slimeStats)
	{
		GameObject newSlime = Instantiate(GetSlime(slimeStats.classType));
		BaseSlime _slime = newSlime.gameObject.GetComponent<BaseSlime>();
		_slime.slimeStats = slimeStats;
		_slime.SetStats();
		newSlime.transform.position = new Vector3(_slime.slimeStats.penPosX, _slime.slimeStats.penPosY, 0);
	}
	public void GameAddSlime(int classType)
	{
		GameObject newSlime = (GameObject)Instantiate(GetSlime(classType), GetSpawnEndPoint(true), Quaternion.identity);
		BaseSlime _slime = newSlime.gameObject.GetComponent<BaseSlime>();
		_slime.slimeStats.name = GetRandomName(_slime);
		_slime.SetStats();
		playerData.slimeData.Add(_slime.slimeStats);
		GameSave();
	
	}
	public void GameAddSlime()
	{
		GameObject newSlime = Instantiate(SlimeDatabase.FriendlySlimes[0]);
		BaseSlime _slime = newSlime.gameObject.GetComponent<BaseSlime>();
		_slime.slimeStats.index = playerData.slimeData.Count;
		_slime.slimeStats.name = "Nooblet";
		_slime.moveEnabled = true;
		newSlime.name = _slime.slimeStats.name;
		playerData.slimeData.Add(_slime.slimeStats);
		GameSave();
	}

	public void GameAddCoin(BaseItem coin)
	{
		playerData.coinCount += coin.itemValue;
		Vector3 endPos = Camera.main.ScreenToWorldPoint(new Vector3(0, Camera.main.pixelHeight, 0));
		Vector3[] coinPath = new Vector3[] { coin.transform.position, (UnityEngine.Random.insideUnitCircle * 2), endPos };
		coin.transform.DOPath(coinPath, 1.2f, PathType.CatmullRom, PathMode.Full3D, 10, Color.red)
		.OnUpdate(() =>
		{
			float dis = Vector3.Distance(coinPath[3], coin.transform.position);
			if(dis < (dis / 2))
			{
				SpriteRenderer render = coin.transform.GetComponentInChildren<SpriteRenderer>();
				render.DOFade(0, 0.4f);
			}
		}).OnComplete(() =>
		{
			coin.DestroyItem();
		});
		//GameSave();
	}
	public void GameAddCoin(int coin)
	{
		playerData.coinCount += coin;
		Vector3 endPos = Camera.main.ScreenToWorldPoint(new Vector3(0, Camera.main.pixelHeight, 0));
		//Vector3[] coinPath = new Vector3[] { coin.transform.position, (UnityEngine.Random.insideUnitCircle * 2), endPos };
		//coin.transform.DOPath(coinPath, 1.2f, PathType.CatmullRom, PathMode.Full3D, 10, Color.red)
		//.OnUpdate(() =>
		//{
		//    float dis = Vector3.Distance(coinPath[3], coin.transform.position);
		//    if (dis < (dis / 2))
		//    {
		//        SpriteRenderer render = coin.transform.GetComponentInChildren<SpriteRenderer>();
		//        render.DOFade(0, 0.4f);
		//    }
		//}).OnComplete(() =>
		//{
		//    coin.DestroyItem();
		//});
		//GameSave();
	}
	public bool GameRemoveCoin(int amount)
	{
		bool removed = false;
		if(playerData.coinCount > amount)
		{
			removed = true;
			playerData.coinCount -= amount;

		}
		return removed;
	}


	public void GameAddFood(int foodItem, Vector3 spawnPoint)
	{
		float rangeX = UnityEngine.Random.Range(-Camera.main.pixelWidth / 2, Camera.main.pixelWidth / 2);
		float rangeY = UnityEngine.Random.Range(-Camera.main.pixelHeight / 2, Camera.main.pixelHeight / 2);
		Vector2 center = new Vector2(Camera.main.pixelWidth / 2, Camera.main.pixelHeight / 2);
		Vector3 pos = Camera.main.ScreenToWorldPoint(new Vector2(center.x + rangeX, center.y + rangeY));
		GameObject food = Instantiate(SlimeDatabase.Food[foodItem], spawnPoint, Quaternion.identity);
		BaseFood newFood = food.GetComponent<BaseFood>();
		food.name = ("Food_" + foodItem.ToString() + "_" + newFood.name + "_" + ourFood.Count);
		newFood.foodData.instanceID = ourFood.Count;
		newFood.transform.SetParent(garbageCan);

		// if (Debugging)
		//Debug.Log("SetFoodID:" + ourFood.Count);
		//ourFood.Add(newFood);
		if(playerData.playPenDecoPurchased.Contains(100))
		{
			newFood.MoveToAutoFeeder();
		} else
		{
			newFood.MoveToPosition(pos);
		}
	}
	public void GameAddDeco(int decoItem)
	{
		GameObject newDeco = null;
		bool isAutoFeeder = SlimeDatabase.Decorations[decoItem].GetComponent<AutoFeeder>();
		if (isAutoFeeder)
		{
			newDeco = autoFeeder.gameObject;
		}
		else
		{
			newDeco = Instantiate(SlimeDatabase.Decorations[decoItem]);
		}
		BaseDeco deco = newDeco.transform.GetComponent<BaseDeco>();
		DecoData data = deco.decoData;
		newDeco.name = data.name;
		ourDeco.Add(deco);
		playerData.playPenDecoPurchased.Add(data.decoID);
		playerData.playPenDeco.Add(data);
		if (!isAutoFeeder)
		{
			newDeco.transform.SetParent(garbageCan);
			playPenManager.SpawnObject(deco);
		}
	}
	public bool ConfirmDecoPurchase(int decoItem)
	{
		bool bought = false;
		int itemID = SlimeDatabase.Decorations[decoItem].GetComponent<BaseDeco>().decoData.decoID;
		if(SlimeDatabase.Decorations[decoItem].GetComponent<AutoFeeder>() && playerData.playPenDecoPurchased.Contains(100))
		{
			bought = true;
		}
		if(playerData.playPenDeco.Count > 0)
		{
			for(int i = 0; i < playerData.playPenDeco.Count; i++)
			{
				if(playerData.playPenDeco[i].decoID == itemID)
				{
					bought = true;
					break;
				}
			}
		}

		return bought;
	}
	
	public void GameCleanUp()
	{//execute before loading and after saving
		ourFood.Clear();
		ourFoodStorage.Clear();

	}



	/*----------------------------------------
				  XML STUFF
  ---------------------------------------*/

	public int ScaledHP(float baseHP, float rate, float flat, int stage)
	{
		//b * (1 + (level - 1) * r) + (f * (level - 1))
		float hp = (baseHP * (Mathf.Pow(rate, (Mathf.Min(stage, 115))) * (Mathf.Pow(flat, (Mathf.Max((stage - 115), 0))))));
		return (int)hp;
	}
	public int ScaledDamage(float baseDM, float rate, float flat, int stage)
	{
		float damage = (baseDM * (Mathf.Pow(rate, (Mathf.Min(stage, 115))) * (Mathf.Pow(flat, (Mathf.Max((stage - 115), 0))))));
		return (int)damage;
	}
	public int ScaledEXP(float baseEX, float rate, float flat, int stage)
	{
		//b * (1 + (level - 1) * r) + (f * (level - 1))
		//float exp = _base * (1 + (_level - 1) * _rate) + (_flat * (_level - 1
		float exp = (baseEX * (Mathf.Pow(rate, (Mathf.Min(stage, 115))) * (Mathf.Pow(flat, (Mathf.Max((stage - 115), 0))))));
		return (int)exp;

	}
	public int ScaledCoin(int baseCO, float rate, float flat, int stage, float bonus)
	{
		//b * (1 + (level - 1) * r) + (f * (level - 1))
		float coin = baseCO * rate + flat * Mathf.Min(stage, 150) * bonus;
		return (int)coin;

	}
	public float ScaledTransform(int _level, float _base, float _rate, float _flat)
	{
		//b * (1 + (level - 1) * r) + (f * (level - 1))

		float xyz = _base * (1 + (_level - 1) * _rate) + (_flat * (_level - 1));

		return xyz;
	}
	public Vector3 GetSpawnEndPoint(bool _friendly)
	{
		Vector2 screenCenter = (new Vector2(Camera.main.pixelWidth / 2, Camera.main.pixelHeight / 2));
		Vector2 spawnPoint;
		Vector3 tempPoint;
		if(_friendly)
		{
			spawnPoint = new Vector2(
				screenCenter.x -
				(Camera.main.pixelWidth / 4 + UnityEngine.Random.Range(-Camera.main.pixelWidth / 8, Camera.main.pixelWidth / 8)),
				screenCenter.y +
				(UnityEngine.Random.Range(-Camera.main.pixelHeight / 4, Camera.main.pixelHeight / 4)));
		} else
		{
			spawnPoint = new Vector2(
				screenCenter.x +
				(Camera.main.pixelWidth / 4 + UnityEngine.Random.Range(-Camera.main.pixelWidth / 8, Camera.main.pixelWidth / 8)),
				screenCenter.y +
				(UnityEngine.Random.Range(-Camera.main.pixelHeight / 4, Camera.main.pixelHeight / 4)));
		}
		tempPoint = Camera.main.ScreenToWorldPoint(spawnPoint);
		tempPoint.z = 0;

		return tempPoint;
	}



}