﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class BaseDeco : BaseObject
{
	public float spawnfxScale = 1f;
	//public bool fixedAngualarVelocity;
	public Sprite decoIcon;
    public DecoData decoData;
    private bool isPlaced = true;
	public bool isStatic = false;
    private float timeHeld = 0;
	//private int dir;
	//private Vector3 previousPosition;
	// Use this for initialization
	public override void Start ()
    {
        base.Start();
	}
	
	// Update is called once per frame
	public override void Update ()
    {
        base.Update();
      
		if(isPlaced)
        {
            if (decoData.x != transform.position.x)
                decoData.x = transform.position.x;
            if (decoData.y != transform.position.y)
                decoData.y = transform.position.y;
            if (decoData.angle != transform.eulerAngles.z)
                decoData.angle = transform.eulerAngles.z;
        }
	}
    
	public virtual void EnableObject()
    {
		foreach(SpriteRenderer rend in myRenderers)
		{
			rend.color = Color.white;
			_2dxFX_Hologram2 holofx = rend.transform.GetComponent<_2dxFX_Hologram2>();
			if(holofx != null)
				holofx.enabled = false;
		}
		foreach(Collider2D col in myGeneralColliders)
		{
			col.enabled = true;
		}
    }
    public virtual void DisableObject()
    {
		foreach(SpriteRenderer rend in myRenderers)
		{
			rend.color = GameManager._Instance.playPenManager.objectDisabledColor;
			_2dxFX_Hologram2 holofx = rend.transform.GetComponent<_2dxFX_Hologram2>();
			if(holofx != null)
				holofx.enabled = true;
		}
		foreach(Collider2D col in myGeneralColliders)
		{
			col.enabled = false;
		}
		
    }
	
    public override void TouchFunction(int _id, Vector3 _currentPos)
    {
        switch (_id)
        {
            case 0://pickup this object
                timeHeld = 0;
                if (isMovable)
                {
                    transform.position = _currentPos;

                }
                string name = decoData.name;
                string happiness = "Adds " + decoData.decoValue.ToString() + " happiness";
                string flavorText = name + "\n" + happiness;
                break;
            case 1://move
				if (isStatic)
				{
					if (isMovable && isPlaced)
					{
						//timeHeld += Time.deltaTime;
						Color col = myRenderer.color;
						col = Color.Lerp(col, Color.red, Time.deltaTime * 3);
						foreach (SpriteRenderer sprite in myRenderers)
							sprite.color = col;
						timeHeld = Mathf.Lerp(timeHeld, 1.1f, Time.deltaTime * 3);
						if (timeHeld > 1f)
						{
							isPlaced = false;
							DisableObject();
						}
					}
					if (isMovable && !isPlaced)
					{
						if (myBody != null)
						{
							Vector2 dir = (_currentPos - transform.position).normalized;
							float mag = Vector2.Distance(transform.position, _currentPos);
							//myBody.velocity = dir * mag * dragMagnituge;
							myBody.velocity = Vector2.Lerp(myBody.velocity, (dir * dragMagnituge), Time.deltaTime * mag);

						}
						else
						{
							transform.position = _currentPos;

						}
					}
				}
				else
				{
					Vector2 dir = (_currentPos - transform.position).normalized;
					float mag = Vector2.Distance(transform.position, _currentPos);
					//myBody.velocity = dir * mag * dragMagnituge;
					myBody.velocity = Vector2.Lerp(myBody.velocity, (dir * dragMagnituge), Time.deltaTime * mag);

				}
				break;
            case 2:
				foreach (SpriteRenderer sprite in myRenderers)
					sprite.color = Color.white;
				if (isMovable && !isPlaced)
                {
                    isPlaced = true;
                    EnableObject();
                }
                if (isMovable)
                {
                    transform.position = _currentPos;

                }
                // foodLetGo = true;
                break;
            case 3:
                ////Debug.Log("Touch-DoubleTap id:" + _id.ToString());

                break;//Pres enter here to add new case, then make int relative
            case 4:
                //Debug.Log("Touch-TapCount id:" + _id.ToString());

                break;
			case 5:
				if(GameManager._Instance.ourDeco.Contains(this))
					GameManager._Instance.ourDeco.Remove(this);
				Destroy(gameObject);
				//GameManager._Instance.GameSave();
				
				break;
        }//when adding additional cases, match the logic of the below if()VVV
        if (_id > 4)
        {
            //Debug.Log("Touch-TapCount id:" + _id.ToString());

        }
    }
}
