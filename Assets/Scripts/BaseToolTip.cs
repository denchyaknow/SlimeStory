﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Denchyaknow;
public class BaseToolTip : MonoBehaviour {
	public string textBody = string.Empty;
	public string textOption1 = string.Empty;
	public string textOption2 = string.Empty;
	private bool isInteractable = false;
	[HideInInspector] public float tipSize = 0.3f;
	[HideInInspector] public float tipSizeX = 0.3f;
	[HideInInspector] public float tipSizeY = 0.3f;
	//[HideInInspector] public float tipBorder = 0.5f;
	[HideInInspector] public BaseToolTip.ToopTipType toolTipType;
	[Header("Text")]
	public float textTime = 1.2f;
	public ScrambleMode textScramble;
	public Ease textEase;
	[Header("Move")]
	public float tipMoveTime = 0.8f;
	public Ease tipMoveEase;
	[Header("Scale")]
	public float tipScaleTime = 0.8f;
	public Ease tipScaleEase;
	[Header("Fade")]
	public float tipFadeTime = 0.8f;
	[Header("Offsetting")]
	public float tipOffsetRadius = 0.8f;
	public Transform tipParent = null;

	public static BaseToolTip _Instance;
	public delegate void Delegate();
	public Delegate onClickAnywhere;
	public Delegate onClickOption1;
	public Delegate onClickOption2;
	public UniMob_Base uniMob = null;
	private float lastTimeActivated = 0;
	private Vector3 localPos;
	private float localMoveDis = 1.12f;
	private void OnEnable()
	{
		localPos = transform.localPosition;
		ShowToolTip();
	}
	private void OnDisable()
	{
		CanvasGroup opGroup = tipOptionGroup;
		RectTransform myRect = transform.GetComponent<RectTransform>();
		Text textMain1 = tipTextBody(false);
		Text textMain2 = tipTextBody(true);
		EventTrigger mainOption = tipMainOption;
		EventTrigger fillOption = tipFillOption;
		Button option1 = tipOption1;
		Button option2 = tipOption2;
		Text option1Text = option1.GetComponentInChildren<Text>();
		Text option2Text = option2.GetComponentInChildren<Text>();
		mainOption.triggers.Clear();
		fillOption.triggers.Clear();
		option1.onClick.RemoveAllListeners();
		option2.onClick.RemoveAllListeners();
		onClickAnywhere = null;
		onClickOption1 = null;
		onClickOption2 = null;
		DOTween.Kill(opGroup);
		DOTween.Kill(myRect);
		DOTween.Kill(textMain1);
		DOTween.Kill(textMain2);
		DOTween.Kill(option1Text);
		DOTween.Kill(option2Text);
		tipFillOption.gameObject.SetActive(false);
		opGroup.alpha = 0;
		opGroup.interactable = false;
		opGroup.blocksRaycasts = false;
		uniMob = null;
	}
	public void AttachToObject<T>(T target, float offsetRadius) where T : Transform
	{
		
	}
	

	public void Update()
	{
	
	}
	private void OnDrawGizmosSelected()
	{
		
	}

	public void ShowToolTip()
	{
		CanvasGroup opGroup = tipOptionGroup;
		opGroup.alpha = 0;
		opGroup.interactable = false;
		opGroup.blocksRaycasts = false;
		RectTransform myRect = transform.GetComponent<RectTransform>();
		RectTransform myBack = tipMainOption.GetComponent<RectTransform>();
		bool usePortrait = uniMob != null;
		Text textMain = tipTextBody(usePortrait);
		EventTrigger mainOption = tipMainOption;
		EventTrigger fillOption = tipFillOption;
		textMain.text = string.Empty;
		Vector2 endSize = GetSize();
		Vector2 backgroundSize = GetBackgroundSize();
		myRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, endSize.x);
		myRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 0);
		myBack.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, endSize.x);
		myBack.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 0);
		tipPortraitPivot.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, myRect.sizeDelta.x * 0.3f);
		tipTextPortrait.rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, myRect.sizeDelta.x * 0.66f);
		Color txtCol = textMain.color;
		txtCol.a = 255f;
		textMain.color = txtCol;

		if (usePortrait)
			tipPortraitImage.sprite = uniMob.faceSprite;
		
		EventTrigger.Entry quickTrigger = new EventTrigger.Entry();
		quickTrigger.eventID = EventTriggerType.PointerDown;
		quickTrigger.callback.AddListener((eventData) => QuickFinishToolTip());
		mainOption.triggers.Clear();
		fillOption.triggers.Clear();
		mainOption.triggers.Add(quickTrigger);
		fillOption.triggers.Add(quickTrigger);


		myRect.DOSizeDelta(endSize, tipScaleTime, true).SetId(transform).SetEase(tipScaleEase);
		textMain.DOText(textBody, textTime, false, textScramble, textBody).SetId(textMain).SetEase(textEase).SetDelay(tipMoveTime/2)
			.OnComplete(()=>
			{

				if (textOption1 != string.Empty && textOption2 != string.Empty)
				{

					ApplyOptionTiggers();
					ShowOptions();
				}
				else
				{
					ApplyMainOption();
				}
					isInteractable = true;
			});

	}
	public void HideToolTip()
	{
		RectTransform myRect = transform.GetComponent<RectTransform>();
		Vector2 size = myRect.sizeDelta;
		Vector2 endSize = new Vector2(size.x,0);
		bool usePortrait = uniMob != null;

		Text textMain = tipTextBody(usePortrait);
		DOTween.Kill(myRect);
		DOTween.Kill(textMain);
		if(textOption1 != string.Empty && textOption2 != string.Empty)
		{
			HideOptions();
		}
		textMain.DOFade(0, tipFadeTime).SetId(textMain)
			.OnComplete(() =>
			{
				textMain.text = string.Empty;
			});
		myRect.DOSizeDelta(endSize, tipScaleTime, true).SetId(transform).SetEase(tipScaleEase).OnComplete(() =>
		{
			gameObject.SetActive(false);

		});
	}
	public void ApplyMainOption()
	{
		onClickAnywhere += () =>
		{
			if (isInteractable)
			{
				isInteractable = false;
				HideToolTip();
			}
		};
		EventTrigger mainOption = tipMainOption;
		EventTrigger fillOption = tipFillOption;
		EventTrigger.Entry trigger = new EventTrigger.Entry();
		trigger.eventID = EventTriggerType.PointerDown;
		trigger.callback.AddListener((eventData) => onClickAnywhere());
		mainOption.triggers.Clear();
		tipFillOption.triggers.Clear();
		mainOption.triggers.Add(trigger);
		tipFillOption.triggers.Add(trigger);
	}
	public void QuickFinishToolTip()
	{
		bool usePortrait = uniMob != null;
		Text textMain = tipTextBody(usePortrait);
		DOTween.Kill(textMain);
		CanvasGroup opGroup = tipOptionGroup;

		//EventTrigger mainOption = tipMainOption;
		//EventTrigger fillOption = tipFillOption;
		//EventTrigger.Entry trigger = new EventTrigger.Entry();
		//trigger.eventID = EventTriggerType.PointerDown;
		//trigger.callback.AddListener((eventData) => onClickAnywhere());
		Button option1 = tipOption1;
		Button option2 = tipOption2;
		Text option1Text = option1.GetComponentInChildren<Text>();
		Text option2Text = option2.GetComponentInChildren<Text>();
		DOTween.Kill(option1Text);
		DOTween.Kill(option2Text);
		DOTween.Kill(tipOptionGroup);

		textMain.text = textBody;
		if(textOption1 != string.Empty && textOption2 != string.Empty)
		{
			option1Text.text = textOption1;
			option2Text.text = textOption2;
			opGroup.alpha = 1;
			Vector3 newLocalPos = localPos;
			newLocalPos.y += localMoveDis;
			transform.localPosition = newLocalPos;
			ApplyOptionTiggers();
			
			//ShowOptions();
		} else
		{
			ApplyMainOption();
			//mainOption.triggers.Clear();
			
			//mainOption.triggers.Add(trigger);
		}
		isInteractable = true;

	}
	public void ShowOptions()
	{
		transform.DOLocalMoveY(localMoveDis, tipMoveTime, false).SetDelay(textTime / 2).SetId(transform).SetEase(tipMoveEase);
		float time = 0;
		CanvasGroup opGroup = tipOptionGroup;
		ApplyOptionTiggers();

		Button option1 = tipOption1;
		Text option1Text = option1.GetComponentInChildren<Text>();
		option1Text.text = string.Empty;

		Button option2 = tipOption2;
		Text option2Text = option2.GetComponentInChildren<Text>();
		option2Text.text = string.Empty;

		
		DOTween.To(() => time, x => time = x, 1, tipFadeTime).SetDelay(textTime/2).SetId(opGroup).OnUpdate(()=>
		{
			opGroup.alpha = time;
		});
		option1Text.DOText(textOption1, textTime/2, true, textScramble, textOption1).SetId(option1Text).SetDelay(textTime / 2).SetEase(textEase);
		option2Text.DOText(textOption2, textTime/2, true, textScramble, textOption2).SetId(option2Text).SetDelay(textTime / 2).SetEase(textEase).OnComplete(()=>
		{
			isInteractable = true;
		});
	}
	public void HideOptions()
	{
		CanvasGroup opGroup = tipOptionGroup;
		Button option1 = tipOption1;
		Button option2 = tipOption2;
		
		Text option1Text = option1.GetComponentInChildren<Text>();
		Text option2Text = option2.GetComponentInChildren<Text>();
		option1.onClick.RemoveAllListeners();
		option2.onClick.RemoveAllListeners();
		
		DOTween.Kill(opGroup);
		DOTween.Kill(option1Text);
		DOTween.Kill(option2Text);
		transform.DOLocalMoveY(0, tipMoveTime, false).SetId(transform).SetEase(tipMoveEase);
		option1Text.text = string.Empty;
		option2Text.text = string.Empty;
		float time = 1;

		DOTween.To(() => time, x => time = x, 0, tipFadeTime).OnUpdate(() =>
		{
			opGroup.alpha = time;
		});
	}
	public void ApplyOptionTiggers()
	{
		CanvasGroup opGroup = tipOptionGroup;
		opGroup.interactable = true;
		opGroup.blocksRaycasts = true;
		onClickOption1 += () =>
		{
			if (isInteractable)
			{
				isInteractable = false;
				HideToolTip();
			}
		};
		Button option1 = tipOption1;
		tipOption1.onClick.RemoveAllListeners();
		tipOption1.onClick.AddListener(() => onClickOption1());
		onClickOption2 += () =>
		{
			if (isInteractable)
			{
				isInteractable = false;
				HideToolTip();
			}
		};
		Button option2 = tipOption2;
		tipOption2.onClick.RemoveAllListeners();
		tipOption2.onClick.AddListener(() => onClickOption2());
		//EventTrigger.Entry trigger1 = new EventTrigger.Entry();
		//trigger1.eventID = EventTriggerType.PointerDown;
		//trigger1.callback.AddListener((eventData) => onClickOption1());
		//option1.triggers.Clear();
		//option1.triggers.Add(trigger1);
		//EventTrigger option2 = tipOption2;
		//EventTrigger.Entry trigger2 = new EventTrigger.Entry();
		//trigger2.eventID = EventTriggerType.PointerDown;
		//trigger2.callback.AddListener((eventData) => onClickOption2());
		//option2.triggers.Clear();
		//option2.triggers.Add(trigger2);

	}
	public Image tipBack;
	
	public Text tipTextBody(bool portrait)
	{
		if(portrait)
		{
			tipTextNoPortrait.gameObject.SetActive(false);
			tipTextPortrait.gameObject.SetActive(true);
			tipPortraitPivot.gameObject.SetActive(true);
			return tipTextPortrait;
		} else
		{
			tipTextNoPortrait.gameObject.SetActive(true);
			tipTextPortrait.gameObject.SetActive(false);
			tipPortraitPivot.gameObject.SetActive(false);
			return tipTextNoPortrait;
		}
	}
	public Text tipTextPortrait;
	public Text tipTextNoPortrait;
	public RectTransform tipPortraitPivot;
	public Image tipPortraitImage;
	public CanvasGroup tipOptionGroup;
	//{
	//	get
	//	{
	//		return transform.GetChild(4).transform.GetComponent<CanvasGroup>();
	//	}
	//}
	public EventTrigger tipFillOption;
	public EventTrigger tipMainOption;
	//{
	//	get
	//	{
	//		return transform.GetChild(0).transform.GetComponent<EventTrigger>();
	//	}
	//}
	public Button tipOption1;
	//{
	//	get
	//	{
	//		return transform.GetChild(4).transform.GetChild(2).transform.GetComponent<EventTrigger>();
	//	}
	//}
	public Button tipOption2;
	//{
	//	get
	//	{
	//		return transform.GetChild(4).transform.GetChild(3).transform.GetComponent<EventTrigger>();
	//	}
	//}
	public enum ToopTipType
	{
		Narrative,
		Bubble,
		System
	}
	//public void RepositionTexts()
	//{
	//	Image back = tipBack;
	//	Text text = tipTextMain;
	//	float size = back.rectTransform.sizeDelta.y;
	//	Transform textMain = transform.GetChild(1).transform;
	//	Vector3 textMainPos = new Vector3(0, size - (tipTextMain.fontSize * tipBorder), 0);
	//	textMain.localPosition = textMainPos;
	//}
	//public string FormatString(string text)
	//{
	//	string formatedText = string.Empty;
	//	char[] oldChars = text.ToCharArray();
	//	float width = 0;
	//	switch(toolTipType)
	//	{
	//		case BaseToolTip.ToopTipType.Narrative:
	//			width = (((Camera.main.orthographicSize * 2.0f) / Camera.main.pixelHeight) * Camera.main.pixelWidth) - tipBorder;
	//			break;
	//		case BaseToolTip.ToopTipType.Bubble:
	//			width = ((((Camera.main.orthographicSize * 2.0f) / Camera.main.pixelHeight) * Camera.main.pixelWidth) * tipSizeX) - (tipBorder * 3);
	//			break;

	//	}
	//	Text mainText = tipTextMain;
	//	float charWidth =  mainText.fontSize;
	//	float curWidth = 0;
	//	bool limitBreak = false;
	//	for(int i = 0; i < oldChars.Length; i++)
	//	{
	//		formatedText += oldChars[i].ToString();
	//		if(curWidth > width && char.IsWhiteSpace(oldChars[i]))
	//		{
	//			limitBreak = true;
	//			formatedText += "\n";
	//			curWidth = 0;
	//		}
	//		if(i != 0)
	//		{
	//			string lastChars = oldChars[i - 1].ToString() + oldChars[i].ToString();
	//			if(lastChars.Contains("\n"))
	//			{
	//				limitBreak = false;
	//				curWidth = 0;
	//			}
	//		}
	//		curWidth += charWidth;
	//	}
	//	return formatedText;
	//}
	public Vector2 GetSize()
	{
		Vector2 size = Vector2.zero;
		switch(toolTipType)
		{
			case BaseToolTip.ToopTipType.Narrative:
				size = new Vector2(1780, 340 * tipSize);
				break;
			case BaseToolTip.ToopTipType.Bubble:
				size = new Vector2(500, 160 * tipSize);
				break;
			case BaseToolTip.ToopTipType.System:
				size = new Vector2(tipSizeX, tipSizeY);
				break;
		}
		return size;
	}
	public Vector2 GetBackgroundSize()
	{
		Vector2 size = Vector2.zero;
		switch (toolTipType)
		{
			case BaseToolTip.ToopTipType.Narrative:
				size = new Vector2(-100, -100);
				break;
			case BaseToolTip.ToopTipType.Bubble:
				size = new Vector2(0, 0);
				break;
			case BaseToolTip.ToopTipType.System:
				size = new Vector2(tipSizeX, tipSizeY);
				break;
		}
		return size;
	}
}
