﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class BaseSlimeProperties : MonoBehaviour{

	[Header("Eating Vars")]
	public float eatingTime = 1.2f;
	public float eatingRate = 1f;

}
