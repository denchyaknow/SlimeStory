﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
[System.Serializable]
public class SlimeProperties
{
	public static string slimeLayerAlive = "Player";
	public static string slimeLayerDead = "PlayerBoundry";
	public static float slimeDragMagnitude = 20f;
	public static float slimeHappinesDecayRate = 15f;
	public static float slimeHungerDecayRate = 5f;
	
	public static float aIdleSpeed = 5f;
	public static float aMoveSpeed = 6f;
	public static float aMoveSpeedMin = 4f;
	public static float aMoveSpeedMax = 8f;
	public static Vector2[] aIdleScales = { new Vector2(1f, 1f), new Vector2(1.2f, 0.85f) };
	public static Vector2[] aMoveScales = { new Vector2(0.8f, 1.15f) , new Vector2(1.2f, 0.85f) };
	public static float minCollisionVelocity = 6f;
	public static float maxCollisionVelocity = 25f;

}
