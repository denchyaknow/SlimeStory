﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class TextFader : MonoBehaviour {
	public float fadeRate = 0.8f;
	public bool fadingStarted = false;
	public bool fadingDone = false;
	public Image tapToStartImage;
	// Use this for initialization
	public void Fade()
	{
		if(tapToStartImage != null)
			tapToStartImage.DOFade(0, 1).OnComplete(()=>
			{
				fadingDone = true;
			});
		//StartCoroutine(FadeCoro());
		else
		{
			Debug.Log("Fader has no assigned Image");
			fadingDone = true;
		}
	}
	IEnumerator FadeCoro()
	{
		
			while(tapToStartImage.color.a > 1f)
			{
				yield return new WaitForSeconds(0.1f);
				Color newCol = tapToStartImage.color;
				newCol.a = Mathf.Lerp(newCol.a, 0, Time.deltaTime * fadeRate);
			tapToStartImage.color = newCol;
			}

		Debug.Log("FadingDone");
		fadingDone = true;
	}
	//void Awake () {
	//	if(transform.GetComponent<Text>())
	//	{
	//		tapToStartImage = transform.GetComponent<Text>();
	//	}
	//	fadingDone = false;
	//}
	
}
