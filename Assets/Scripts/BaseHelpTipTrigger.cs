﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BaseHelpTipTrigger : EventTrigger {
	private bool hidden = true;
	//public override void OnInitializePotentialDrag(PointerEventData eventData)
	//{
	//	base.OnInitializePotentialDrag(eventData);

	//	ShowHelpTip();
	//	Debug.Log("OnInitializePotentialDrag");

	//}
	//public override void OnSelect(BaseEventData eventData)
	//{
	//	base.OnSelect(eventData);
	//	ShowHelpTip();
	//	Debug.Log("OnSelect");
	//}
	//public override void OnEndDrag(PointerEventData eventData)
	//{
	//	base.OnEndDrag(eventData);
	//	HideHelpTip();
	//	Debug.Log("OnEndDrag");

	//}
	//public override void OnDrag(PointerEventData eventData)
	//{
	//	base.OnDrag(eventData);

	//	ShowHelpTip();
	//	Debug.Log("OnCancel");

	//}
	//public override void OnBeginDrag(PointerEventData eventData)
	//{
	//	base.OnBeginDrag(eventData);
	//	ShowHelpTip();
	//	Debug.Log("OnBeginDrag");

	//}
	public override void OnPointerDown(PointerEventData eventData)
	{
		base.OnPointerDown(eventData);
		ShowHelpTip();
		Debug.Log("OnPointerDown");
	}
	//public override void OnPointerEnter(PointerEventData eventData)
	//{
	//	base.OnPointerExit(eventData);
	//	//ShowHelpTip();		
	//	Debug.Log("OnPointerEnter");


	//}
	public override void OnPointerUp(PointerEventData eventData)
	{
		base.OnPointerUp(eventData);
		HideHelpTip();
		Debug.Log("OnPointerUp");

	}
	//public override void OnPointerExit(PointerEventData eventData)
	//{
	//	base.OnPointerExit(eventData);
	//	//HideHelpTip();
	//	Debug.Log("OnPointerExit");

	//}
	//public override void OnMove(AxisEventData eventData)
	//{
	//	base.OnMove(eventData);

	//	ShowHelpTip();
	//	Debug.Log("OnMove");

	//}
	//public override void OnDrop(PointerEventData eventData)
	//{
	//	base.OnDrop(eventData);

	//	HideHelpTip();
	//	Debug.Log("OnDrop");

	//}
	public void ShowHelpTip()
	{
		if(hidden)
		{
			hidden = false;
		}
			BaseHelpTipText target = transform.GetComponent<BaseHelpTipText>();
			BaseHelpTip._Instance.ShowHelpTip(target);
	}
	public void HideHelpTip()
	{
		if(!hidden)
		{
			hidden = true;
		}
			BaseHelpTip._Instance.HideHelpTIp();
	}
}
