﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
public class MessageDatabase
{
	public static MessageObj[] GetAllMessages()
	{
		MessageObj[] all = AllMessages;//new MessageObj[AllMessages.Length];
		//for (int i = 0; i < all.Length; i++)
		//{
		//	all[i].mainText = AllMessages[i].mainText;
		//	all[i].option1Text = AllMessages[i].option1Text;
		//	all[i].option2Text = AllMessages[i].option2Text;
		//	all[i].msgType = AllMessages[i].msgType;
		//}
		return all;
	}
	private static MessageObj[] AllMessages = 
	{
		(new MessageObj("ALLSLIMESDEAD",
			"Aw snap! All your slimes are dead, would you like to rewind time?(Restart)",
			"Yes, ill do better next time",
			"No I wanna stare at dead a bit longer",
			BaseToolTip.ToopTipType.Narrative)),
		(new MessageObj("GAMECLEARED",//The MessageID, this is how you refference it
			"All data cleared...",//The Mainbody of the message
			string.Empty,//The first option text
			string.Empty,//The second option text
			BaseToolTip.ToopTipType.Narrative)),//Narrative = bottom of screen style, Bubble = chat bubble over target
		(new MessageObj("RESETEVERYTHING",//The MessageID, this is how you refference it
			"Are you sure you want to reset everything?",//The Mainbody of the message
			"Yes I'm sure!",//The first option text
			"Nevermind...",//The second option text
			BaseToolTip.ToopTipType.Narrative)),//Narrative = bottom of screen style, Bubble = chat bubble over target
		(new MessageObj("YOURSLIMEDEAD",//The MessageID, this is how you refference it
			"You can't do this with a dead slime.",//The Mainbody of the message
			string.Empty,//The first option text
			string.Empty,//The second option text
			BaseToolTip.ToopTipType.System))//Narrative = bottom of screen style, Bubble = chat bubble over target
	};
	public static MessageObj GetMessage(string msgID)
	{
		MessageObj newMsg = new MessageObj();
		for(int i = 0; i < AllMessages.Length; i++)
		{
			if(AllMessages[i].msgID == msgID)
			{
				newMsg = AllMessages[i];
				break;
			}
		}
		//MessageObj newMsg = AllMessages.Where(item => item.msgID == msgID);
		return newMsg;
	}
}
