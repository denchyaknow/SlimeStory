﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class UIStoreButton : MonoBehaviour {

	[HideInInspector]public UIStoreButtonManager manager;
	public Button buttMain;
	public Button buttCoin;
	public Button buttGem;
	// Use this for initialization
	void Start ()
	{
		if(manager == null)
		{
			manager = transform.GetComponentInParent<UIStoreButtonManager>();
		}
		if(manager != null)
		{
			manager.listButtons.Add(this);
		}
		buttCoin.transform.localScale = new Vector3(0, 1, 1);
		buttGem.transform.localScale = new Vector3(0, 1, 1);
	}
	public void OnMouseUpAsButton()
	{
		if(manager != null)
		{
			manager.buttNext = this;
		}
	}
	public void ButtonClick()
	{
		if(manager != null)
		{
			manager.buttNext = this;
		}
	}
	public void ShowBuyButtons()
	{
		DOTween.Kill(transform);
		buttCoin.transform.DOScaleX(1, 0.6f).SetId(transform).SetEase(Ease.OutExpo);
		//buttGem.transform.DOScaleX(1, 0.6f).SetId(transform).SetEase(Ease.OutExpo);
	}
	public void HideBuyButtons()
	{
		DOTween.Kill(transform);
		buttCoin.transform.DOScaleX(0, 0.4f).SetId(transform).SetEase(Ease.OutExpo);
		//buttGem.transform.DOScaleX(0, 0.4f).SetId(transform).SetEase(Ease.OutExpo);
	}
	void Update () {
		
	}
}
