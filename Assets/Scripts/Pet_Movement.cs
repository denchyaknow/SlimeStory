﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

using DG.Tweening;
public class Pet_Movement : MonoBehaviour
{
    [Header("MustBeSet")]
    public Transform path_Parent;

    [Header("Move_To_What")]
    public Transform move_Target;//if null then do random movement
    [Header("Move_Params")]
    public float move_speed = 10f;
    public float move_IdleTime;
    public float move_Cooldown = 1;
    public int move_Resolution = 10;
    public Ease move_Ease;
    public DG.Tweening.PathMode move_Mode;
    public DG.Tweening.PathType move_Type;

    public float[] move_IdleMinMAX = new float[2] { 0, 5 };

    public Vector2 move_Dir;

    public Animator anim;

    public bool isMoving = false;
    public LayerMask whatIsWall;
    public bool _Debug = false;
    public bool _Debug_TweenThatFlipSon = false;

    // Use this for initialization
    void Start(){
        move_Target = null;
        isMoving = false;
        //anim = gameObject.GetComponent<Animator>();

    }

    private void OnEnable()
    {
        move_Target = null;
        isMoving = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isMoving)
        {
            move_IdleTime -= Time.deltaTime;
            anim.SetBool("isMove", false);

        }
        if (!isMoving && move_IdleTime < 0)
        {
            Vector3[] myPath = move_Path();
            Vector3 myRot = path_Parent.localEulerAngles;
            myRot.z = Random.Range(0, 360);
            path_Parent.localEulerAngles = myRot;
            if (myPath == null || myPath.Length < 1)
                return;
            anim.SetBool("isMove", true);

            isMoving = true;
            if (Physics2D.Linecast(myPath[0], myPath[myPath.Length - 1], whatIsWall))
            {
                move_IdleTime = 0.1f;
                isMoving = false;
                return;
            }
            move_Dir = (myPath[myPath.Length - 1] - myPath[0]).normalized;
            anim.SetFloat("Movedir", move_Dir.x);
            move_IdleTime = Random.Range(move_IdleMinMAX[0], move_IdleMinMAX[1]) + move_Cooldown;
            transform.DOPath(myPath, move_speed, move_Type, move_Mode, move_Resolution, Color.magenta).SetEase(move_Ease).SetSpeedBased(true).OnComplete(() =>
            {
                isMoving = false;
            });
        }
    }


    public Vector3[] move_Path()
    {
        Vector3[] myPath = null;
        if (path_Parent.childCount > 0)
        {
            myPath = new Vector3[path_Parent.childCount];
            for (int i = 0; i < path_Parent.childCount; i++)
            {
                myPath[i] = path_Parent.GetChild(i).transform.position;
            }
        }
        return myPath;
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        if (_Debug)
        {
            Vector3[] myPAth = move_Path();
            if (myPAth.Length > 0 && myPAth != null)
                Gizmos.DrawLine(myPAth[0], myPAth[myPAth.Length - 1]);
        }
    }

}
