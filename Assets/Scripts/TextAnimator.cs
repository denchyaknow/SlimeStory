﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextAnimator : MonoBehaviour {

    public string[] strings = new string[0];
    public float frameTime = 0.2f;
    private bool animating = false;
    public bool start = false;
    public bool stop = false;
    private int frame = 0;
    private TextMesh myTextMesh;
    private Text myText;
    // Use this for initialization
    void Start()
    {
        if (transform.GetComponent<TextMesh>() != null)
        {
            myTextMesh = transform.GetComponent<TextMesh>();

        }
        if (transform.GetComponent<Text>() != null)
        {
            myText = transform.GetComponent<Text>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (stop)
        {
            StopAllCoroutines();
            animating = false;
            start = false;
            if (myText != null && myText.text != strings[strings.Length - 1])
                myText.text = strings[strings.Length - 1];
            if (myTextMesh != null && myTextMesh.text != strings[strings.Length - 1])
                myTextMesh.text = strings[strings.Length - 1];
        }
        if (!animating && start)
        {
            animating = true;
            StartCoroutine(Animate());
        }
    }
    IEnumerator Animate()
    {
        frame++;
        if (frame > strings.Length -2)
            frame = 0;
        yield return new WaitForSeconds(frameTime);
        if (myText != null)
            myText.text = strings[frame];
        if (myTextMesh != null)
            myTextMesh.text = strings[frame];
        animating = false;
    }
    public void StartAnimation()
    {
        start = true;
        stop = false;
    }
    public void StopAnimation()
    {
        stop = true;
       
    }
}
