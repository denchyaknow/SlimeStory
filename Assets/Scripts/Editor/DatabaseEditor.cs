﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
[CustomEditor(typeof(Database))]
public class DatabaseEditor : Editor {


	public override void OnInspectorGUI()
	{
		Database database = target as Database;
		
		
		if(GUILayout.Button("Refresh Database"))
		{
			#region Logic Reff
			//GameObject[] Friendlies = Resources.LoadAll<GameObject>("GameObjects/Friendlies").OrderBy(slime => slime.name).ToArray<GameObject>();
			//GameObject[] Enemies = Resources.LoadAll<GameObject>("GameObjects/Enemies").OrderBy(enemy => enemy.name).ToArray<GameObject>();
			//GameObject[] Attacks = Resources.LoadAll<GameObject>("GameObjects/Attacks").OrderBy(attack => attack.name).ToArray<GameObject>();
			//GameObject[] Maps = Resources.LoadAll<GameObject>("GameObjects/Maps").OrderBy(map => map.name).ToArray<GameObject>();
			//GameObject[] Food = Resources.LoadAll<GameObject>("GameObjects/Food").OrderBy(food => food.name).ToArray<GameObject>();
			//GameObject Poop = Resources.Load<GameObject>("GameObjects/Poop");
#endregion
			database.Friendlies = Resources.LoadAll<GameObject>("GameObjects/Friendlies").OrderBy(slime => slime.name).ToArray<GameObject>();
			Debug.Log("Loaded: " + database.Friendlies.Length + " Friendlies");
			database.Enemies = Resources.LoadAll<GameObject>("GameObjects/Enemies").OrderBy(enemy => enemy.name).ToArray<GameObject>();
			Debug.Log("Loaded: " + database.Enemies.Length + " Enemies");
			database.Attacks = Resources.LoadAll<GameObject>("GameObjects/Attacks").OrderBy(attack => attack.name).ToArray<GameObject>();
			Debug.Log("Loaded: " + database.Attacks.Length + " Attacks");
			database.Maps = Resources.LoadAll<GameObject>("GameObjects/Maps").OrderBy(map => map.name).ToArray<GameObject>();
			Debug.Log("Loaded: " + database.Maps.Length + " Maps");
			database.Food = Resources.LoadAll<GameObject>("GameObjects/Food").OrderBy(food => food.name).ToArray<GameObject>();
			Debug.Log("Loaded: " + database.Food.Length + " Food");
			database.Poop = Resources.Load<GameObject>("GameObjects/Poop");
			Debug.Log("Loaded: " + database.Poop.transform.childCount + " Poop");
			Texture2D[] allTextures = Resources.LoadAll<Texture2D>("Sprites/Enemies/Walk/").OrderBy(go => go.name).ToArray();
			if(allTextures.Length > 0)
			{
				database.EnemyWalkSheets.Clear();
				for(int i = 0; i < allTextures.Length; i++)
				{
					int index = i;
					string name = allTextures[index].name;

					SpriteArray sheet = new SpriteArray();
					Sprite[] tempSprites = Resources.LoadAll<Sprite>("Sprites/Enemies/Walk/" + name);
					sheet.sprites = tempSprites;
					database.EnemyWalkSheets.Add(sheet);
				}
			}
			allTextures = Resources.LoadAll<Texture2D>("Sprites/Enemies/Attack/").OrderBy(go => go.name).ToArray();
			if(allTextures.Length > 0)
			{
				database.EnemyAttackSheets.Clear();

				for(int i = 0; i < allTextures.Length; i++)
				{
					int index = i;
					string name = allTextures[index].name;

					SpriteArray sheet = new SpriteArray();
					Sprite[] tempSprites = Resources.LoadAll<Sprite>("Sprites/Enemies/Attack/" + name);
					sheet.sprites = tempSprites;
					database.EnemyAttackSheets.Add(sheet);
				}
			}

			database.EnemyFaceSprites = Resources.LoadAll<Sprite>("Sprites/Enemies/Face/").OrderBy(go => go.name).ToArray();

			Debug.Log("Sprites Loaded: " + database.EnemyAttackSheets.Count + " Enemy attack sheets");
			Debug.Log("Sprites Loaded: " + database.EnemyWalkSheets.Count + " Enemy walk sheets");
			Debug.Log("Sprites Loaded: " + database.EnemyFaceSprites.Length + " Enemy face sprites");

		}
		base.OnInspectorGUI();

	}
}
