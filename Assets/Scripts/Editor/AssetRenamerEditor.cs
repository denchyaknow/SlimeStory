﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(AssetRenamer))]
public class AssetRenamerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("RenameTextures"))
        {
            AssetRenamer editor = target as AssetRenamer;
            Texture2D[] objects = Resources.LoadAll<Texture2D>(editor.GetPath);
            editor.ObjectsFound = objects.Length;
            if (objects.Length > 0)
            {
                for(int i = 0; i < objects.Length; i++)
                {
                    string newName = editor.SetPath + i.ToString("D3") + ".png";
                    Debug.Log(AssetDatabase.GetAssetPath(objects[i])+" > "+newName);
                    AssetDatabase.CopyAsset(AssetDatabase.GetAssetPath(objects[i]), newName);
                    AssetDatabase.Refresh();


                }
                AssetDatabase.SaveAssets();
                AssetDatabase.Refresh();
            }
        }
    }
}