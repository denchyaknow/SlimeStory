﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(MapGenerator))]
public class MapGeneratorEditor : Editor
{
	public const string rootPath = "MapGenerator/";

	public override void OnInspectorGUI()
	{

		base.OnInspectorGUI();
	}

}
public enum ZoneType
{
	Plains,
	Meadows,
	Stoned,
	Frozen, 
	Social,
	Dreamy,
	Radient

}
[System.Serializable]
public class ZoneProperties : System.Object
{
	ZoneType type = ZoneType.Plains;
}
[CustomPropertyDrawer(typeof(ZoneProperties))]
public class ZonePropertyDrawer : PropertyDrawer
{
	void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		EditorGUI.BeginProperty(position, label, property);
		position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
		int indentLvl = EditorGUI.indentLevel;
		EditorGUI.indentLevel = 0;

		// Calculate rects
		Rect amountRect = new Rect(position.x, position.y, 30, position.height);
		Rect unitRect = new Rect(position.x + 35, position.y, 50, position.height);
		Rect nameRect = new Rect(position.x + 90, position.y, position.width - 90, position.height);

		// Draw fields - passs GUIContent.none to each so they are drawn without labels
		EditorGUI.PropertyField(amountRect, property.FindPropertyRelative("amount"), GUIContent.none);
		EditorGUI.PropertyField(unitRect, property.FindPropertyRelative("unit"), GUIContent.none);
		EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("name"), GUIContent.none);
		// Set indent back to what it was

		EditorGUI.indentLevel = indentLvl;

		EditorGUI.EndProperty();

	}

}
