﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
[CustomEditor(typeof(BaseToolTip))]
public class BaseToolTipEditor : Editor {
	public float sizeN;
	public float sizeX;
	public float sizeY;
	public float sizeBorder = 5;
	public BaseToolTip.ToopTipType type;
	public bool allowEditorUpdate = false;
	private float m_LastEditorUpdateTime;
	private float lastFrameTime;
	

	
	public void EditorUpdate()
	{
		m_LastEditorUpdateTime = Time.realtimeSinceStartup;
		if (lastFrameTime < m_LastEditorUpdateTime)
		{
			lastFrameTime = m_LastEditorUpdateTime + 0.03f;
			Repaint();
		}
	}

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();
		BaseToolTip toolTip = (BaseToolTip)target;
		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.PrefixLabel("Type");
		type = (BaseToolTip.ToopTipType)EditorGUILayout.EnumPopup(toolTip.toolTipType);
		if(type != toolTip.toolTipType)
		{
			toolTip.toolTipType = type;
			Preview();
		}

		EditorGUILayout.EndHorizontal();
		switch(toolTip.toolTipType)
		{
			case BaseToolTip.ToopTipType.Narrative:
				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.PrefixLabel("Size");
				sizeN = EditorGUILayout.Slider(toolTip.tipSize, 0, 1);
				if(sizeN != toolTip.tipSize)
				{
					toolTip.tipSize = sizeN;
					Preview();
				}
				toolTip.tipSize = sizeN;

				EditorGUILayout.EndHorizontal();
				break;
			case BaseToolTip.ToopTipType.Bubble:

				EditorGUILayout.BeginHorizontal();
				EditorGUILayout.PrefixLabel("Size");
				EditorGUILayout.BeginVertical();
				sizeX = EditorGUILayout.Slider(toolTip.tipSizeX, 300, 1700);
				sizeY = EditorGUILayout.Slider(toolTip.tipSizeY, 80, 400);
				EditorGUILayout.EndVertical();
				if(sizeX != toolTip.tipSizeX || sizeY != toolTip.tipSizeY)
				{
					toolTip.tipSizeX = sizeX;
					toolTip.tipSizeY = sizeY;
					Preview();
				}
				EditorGUILayout.EndHorizontal();
				break;
		}
		if (allowEditorUpdate)
		{
			GUILayout.Label("TOOLTIP RUNNING...");
		}
		if (GUILayout.Button("Update Test =" + allowEditorUpdate.ToString()))
		{
			allowEditorUpdate = !allowEditorUpdate;
			if (allowEditorUpdate)
				EditorApplication.update += EditorUpdate;
			else if (!allowEditorUpdate && EditorApplication.update != null)
				EditorApplication.update -= EditorUpdate;
		}
		if(GUILayout.Button("Show ToolTip"))
		{
			toolTip.gameObject.SetActive(true);
		}
		if(GUILayout.Button("Hide ToolTip"))
		{
			toolTip.HideToolTip();
		}
		if(GUILayout.Button("Preview ToolTip"))
		{
			Preview();
		}
	}
	public bool showOptions = false;
	void Preview()
	{
		BaseToolTip toolTip = (BaseToolTip)target;

		string text = "dapibus libero non consectetuer quis non class tincidunt odio purus donec ante nisl varius arcu felis ut diam enim consectetuer";
		//Image background = toolTip.tipBack;
		Vector2 backgroundSize = toolTip.GetSize();
		toolTip.GetComponent<RectTransform>().sizeDelta = backgroundSize;
		bool usePortrait = toolTip.uniMob != null;
		Text textMain = toolTip.tipTextBody(usePortrait);
		textMain.text = text;
		showOptions = toolTip.textOption1 != string.Empty && toolTip.textOption2 != string.Empty;
		
		if(showOptions)
		{
			string op1Text = "non class tincidunt";
			string op2Text = "ante nisl varius";
			Text op1 = toolTip.tipOption1.transform.GetComponentInChildren<Text>();
			Text op2 = toolTip.tipOption2.transform.GetComponentInChildren<Text>();
			op1.text = op1Text;
			op2.text = op2Text;
			CanvasGroup cGroup = toolTip.tipOptionGroup;
			cGroup.alpha = 1;
		} else
		{
			CanvasGroup cGroup = toolTip.tipOptionGroup;
			cGroup.alpha = 0;
		}
	}
}
