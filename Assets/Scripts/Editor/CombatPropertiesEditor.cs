﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Denchyaknow;
[CustomEditor(typeof(CombatProperties))]
public class CombatPropertiesEditor : Editor
{
	private bool resetDatabase = false;
	private bool showDatabase = false;
	private Vector2 databaseScollPos = new Vector2();
	private bool[] showDatabaseSection = new bool[0];
	private int[] showTileSet = new int[0];
	private int[] showSelectedMob = new int[0];
	private int[] selectedMobSpawnChance = new int[0];
	private int[] selectedMobSpawnCount = new int[0];
	private string mobPath = "Assets/Resources/GameObjects/Enemies/";
	private float mobButtonWidth = 30f;
	private int mobGridIndex = -1;
	private UniMob_Base[] allMobs =  new UniMob_Base[0];
	private Texture2D[] allMobPreviews = new Texture2D[0];
	public override void OnInspectorGUI()
	{
		/*
private void DrawScroll () {
if (_categorizedItems [_categorySelected].Count == 0) {
EditorGUILayout.HelpBox ("This category is empty!",
MessageType.Info);
return;
}
int rowCapacity =
Mathf.FloorToInt (position.width / (ButtonWidth));
_scrollPosition =
GUILayout.BeginScrollView (_scrollPosition);
int selectionGridIndex = -1;
selectionGridIndex = GUILayout.SelectionGrid (
selectionGridIndex,
GetGUIContentsFromItems (),
rowCapacity,
GetGUIStyle ());
GetSelectedItem (selectionGridIndex);
GUILayout.EndScrollView ();
}

public static List<T> GetAssetsWithScript<T> (string path) where T :
MonoBehaviour {
 T tmp;
 string assetPath;
 GameObject asset;
 List<T> assetList = new List<T> ();
 string[] guids = AssetDatabase.FindAssets ("t:Prefab", new
string[] {path});
 for (int i = 0; i < guids.Length; i++) {
 assetPath = AssetDatabase.GUIDToAssetPath (guids [i]);
 asset = AssetDatabase.LoadAssetAtPath (assetPath,
typeof(GameObject)) as GameObject;
 tmp = asset.GetComponent<T> ();
 if (tmp != null) {
 assetList.Add (tmp);
 }
 }
 return assetList;
 }
		 * 
		 */
		InitializeDatabase();
		CombatProperties props = (CombatProperties)target;

		if (GUILayout.Button(showDatabase?"Hide/Save Database":"Show Database"))
		{
			showDatabase = !showDatabase;
		}
		if (showDatabase)
		{
			using (var scrollScope = new EditorGUILayout.ScrollViewScope(databaseScollPos))
			{
				databaseScollPos = scrollScope.scrollPosition;
				if (props.ZoneDatabase.Count > 0)
				{
					allMobs = Resources.LoadAll<UniMob_Base>("GameObjects/Enemies/");
					if(showTileSet.Length != props.ZoneDatabase.Count)
						showTileSet = new int[props.ZoneDatabase.Count];
					if (showSelectedMob.Length != props.ZoneDatabase.Count)
						showSelectedMob = new int[props.ZoneDatabase.Count];
					if (selectedMobSpawnChance.Length != props.ZoneDatabase.Count)
						selectedMobSpawnChance = new int[props.ZoneDatabase.Count];
					if (selectedMobSpawnCount.Length != props.ZoneDatabase.Count)
						selectedMobSpawnCount = new int[props.ZoneDatabase.Count];



					Texture2D[] allMobPreviews = GenerateMobPreviews(allMobs);
					if (showDatabaseSection.Length != props.ZoneDatabase.Count)
					{
						showDatabaseSection = new bool[props.ZoneDatabase.Count];
					}
					if (allMobs.Length == 0)
					{
						EditorGUILayout.HelpBox("No Mobs Found", MessageType.Warning);
					}
					
					for (int i = 0; i < props.ZoneDatabase.Count; i++)
					{
						int index = i;
						if (GUILayout.Button((showDatabaseSection[index] ? "Hide " : "Show ") + "Zone " + index))
						{
							showDatabaseSection[index] = !showDatabaseSection[index];
						}
						if (showDatabaseSection[index])
						{

							//int rowCapacity = Mathf.FloorToInt(EditorGUIUtility.currentViewWidth / (mobButtonWidth));
							//mobGridIndex = GUILayout.SelectionGrid(mobGridIndex, allMobPreviews, rowCapacity);
							if (GUILayout.Button("New Tileset") ||props.ZoneDatabase[index].mapTiles.Count == 0)
							{
								MapData newMapTile = new MapData();
								newMapTile.tileID = props.ZoneDatabase[index].mapTiles.Count;
								props.ZoneDatabase[index].mapTiles.Add(newMapTile);
							}
							int tileSet = showTileSet[index];
							MapData tileSetData = props.ZoneDatabase[index].mapTiles[tileSet];
							bool refreshTileSetData = false;
							string[] allZoneTiles = new string[props.ZoneDatabase[index].mapTiles.Count];
							for (int s = 0; s < props.ZoneDatabase[index].mapTiles.Count; s++)
							{
								allZoneTiles[s] = "Tileset:" + s.ToString();
							}
							showTileSet[index] = GUILayout.Toolbar(showTileSet[index], allZoneTiles);
							if (GUILayout.Button("Delete Tileset") && props.ZoneDatabase[index].mapTiles.Count > 0)
							{
								props.ZoneDatabase[index].mapTiles.Remove(tileSetData);
							}
							GUIContent[] mobContents = new GUIContent[allMobPreviews.Length];
							for (int d = 0; d < allMobPreviews.Length; d++)
							{
								mobContents[d] = new GUIContent(allMobs[d].gameObject.name, allMobs[d].gameObject.name);
							}
							int mobSelected = showSelectedMob[index];
							using (var hScope = new GUILayout.HorizontalScope())
							{
								using (var vScope = new GUILayout.VerticalScope())
								{
									showSelectedMob[index] = EditorGUILayout.Popup(showSelectedMob[index], mobContents);
									ProgressBar((float)selectedMobSpawnChance[index] / 100f, "Spawn Chance");
									selectedMobSpawnChance[index] = EditorGUILayout.IntSlider(selectedMobSpawnChance[index], 1, 100);
									ProgressBar((float)selectedMobSpawnCount[index] / 30f, "Spawn Count");
									selectedMobSpawnCount[index] = EditorGUILayout.IntSlider(selectedMobSpawnCount[index], 1, 30);
									if (GUILayout.Button("Add Mob"))
									{
										tileSetData.mobs.Add(new MapData_Mobs(showSelectedMob[index], selectedMobSpawnChance[index], selectedMobSpawnCount[index]));
									}
								}
								GUIContent mobContent = new GUIContent("", allMobPreviews[mobSelected], "");
								GUILayout.Box(mobContent, new GUIStyle(GUI.skin.button));
							}
							GUILayout.Label("Current Mobs");
							if (tileSetData != null && !refreshTileSetData)
							{
								using (var vScope = new GUILayout.VerticalScope())
								{
									int curMobCount = tileSetData.mobs.Count;
									if (curMobCount > 0)
									{
										for (int m = 0; m < curMobCount; m++)
										{
											int curMobId = tileSetData.mobs[m].mobID;
											UniMob_Base mob = GetMob(curMobId);
											Texture2D mobPreview = GetMobPreview(curMobId);
											MapData_Mobs mobData = tileSetData.mobs[m];
											if (mobData != null && !refreshTileSetData)
											{
												using (var hScopeChild = new GUILayout.HorizontalScope())
												{
													using (var vScopeChild = new GUILayout.VerticalScope())
													{
														GUILayout.Label("Name: " + mob.mob_Name);
														ProgressBar((float)mobData.spawnChance / 100f, "Spawn Chance");
														mobData.spawnChance = EditorGUILayout.IntSlider(mobData.spawnChance, 1, 100);
														ProgressBar((float)mobData.spawnCount / 30f, "Spawn Count");
														mobData.spawnCount = EditorGUILayout.IntSlider(mobData.spawnCount, 1, 30);
														if (GUILayout.Button("Delete Mob"))
														{
															tileSetData.mobs.Remove(mobData);
															refreshTileSetData = true;
														}
													}
													GUIContent mobContent = new GUIContent("",mobPreview, "");
													GUILayout.Box(mobContent, new GUIStyle(GUI.skin.button));

												}
											}
										}
									}
								}
							}
							
						}
					}
				}
				else
				{
					GUILayout.Label("Database is empty! Initialize it.");
				}

			}
		}
		serializedObject.ApplyModifiedProperties();

		base.OnInspectorGUI();
	}

	void ProgressBar(float value, string label)
	{
		Rect rect = GUILayoutUtility.GetRect(18, 18, "TextField", GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
		EditorGUI.ProgressBar(rect, value, string.Empty);
		float fValue = Mathf.RoundToInt(value * 100);
		fValue *= 0.01f;
		GUI.Label(rect, label);
	}
	UniMob_Base GetMob(int id)
	{
		if (allMobs.Length == 0)
			allMobs = Resources.LoadAll<UniMob_Base>("GameObjects/Enemies/");
		UniMob_Base mob = allMobs[id];
		return mob;
	}
	Texture2D GetMobPreview(int id)
	{
		if (allMobPreviews.Length == 0)
		{
			if (allMobs.Length == 0)
				allMobs = Resources.LoadAll<UniMob_Base>("GameObjects/Enemies/");
			allMobPreviews = GenerateMobPreviews(allMobs);
		}
		Texture2D mobPreview = allMobPreviews[id];
		return mobPreview;
	}
	void InitializeDatabase()
	{
		CombatProperties props = (CombatProperties)target;
		using (var hScope = new GUILayout.HorizontalScope())
		{
			if (!resetDatabase)
			{
				if (GUILayout.Button("Initialize Database"))
				{
					if (props.initialized)
						resetDatabase = true;
					else
						props.InitializeZoneDatabase();

				}
			}
			if (resetDatabase)
			{
				GUILayout.Label("Are you Sure?");
				if (GUILayout.Button("Yes"))
				{
					props.InitializeZoneDatabase();
					resetDatabase = false;
				}
				if (GUILayout.Button("No"))
				{
					resetDatabase = false;
				}
			}
		}
	}
	private Texture2D[] GenerateMobPreviews(UniMob_Base[] mobs)
	{
		List<Texture2D> previews = new List<Texture2D>();
		for (int i = 0; i < mobs.Length; i++)
		{
			Texture2D preview = AssetPreview.GetAssetPreview(mobs[i].gameObject);
		   if (preview != null)
			{
				previews.Add(preview);
			}
		}
		return previews.ToArray();
	}
	public List<T> GetAssetsWithScript<T>(string path) where T : MonoBehaviour
	{
		T tmp;
		string assetPath;
		GameObject asset;
		List<T> assetList = new List<T>();
		string[] guids = AssetDatabase.FindAssets("t:Prefab", new string[] { path });
		for (int i = 0; i < guids.Length; i++)
		{
			assetPath = AssetDatabase.GUIDToAssetPath(guids[i]);
			asset = AssetDatabase.LoadAssetAtPath(assetPath,typeof(GameObject)) as GameObject;
			tmp = asset.GetComponent<T>();
			if (tmp != null)
			{
				assetList.Add(tmp);
			}
		}
		return assetList;
	}
}



