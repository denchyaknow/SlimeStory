﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEditor;

[CustomEditor(typeof(SlimeEffects))]
public class SlimeEffectsEditor : Editor
{
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();
		SlimeEffects fx = (SlimeEffects)target;
		if(fx.onAttackParent == null)
		{
			Transform parent = Instantiate(new GameObject(), fx.transform).transform;
			parent.gameObject.name = "OnAttack";
			fx.onAttackParent = parent;
		} else if(fx.onAttackParent.gameObject.name != "OnAttack")
			fx.onAttackParent.gameObject.name = "OnAttack";
		if(fx.onWalkParent == null)
		{
			Transform parent = Instantiate(new GameObject("OnWalk"), fx.transform).transform;
			parent.gameObject.name = "OnWalk";
			fx.onWalkParent = parent;
		} else if(fx.onWalkParent.gameObject.name != "OnWalk")
			fx.onWalkParent.gameObject.name = "OnWalk";
		if(fx.onPetParent == null)
		{
			Transform parent = Instantiate(new GameObject("OnPet"), fx.transform).transform;
			parent.gameObject.name = "OnPet";
			fx.onPetParent = parent;
		} else if(fx.onPetParent.gameObject.name != "OnPet")
			fx.onPetParent.gameObject.name = "OnPet";
		if(fx.onHitParent == null)
		{
			Transform parent = Instantiate(new GameObject("OnHit"), fx.transform).transform;
			parent.gameObject.name = "OnHit";
			fx.onHitParent = parent;
		} else if(fx.onHitParent.gameObject.name != "OnHit")
			fx.onHitParent.gameObject.name = "OnHit";
		if(fx.onDeathParent == null)
		{
			Transform parent = Instantiate(new GameObject("OnDeath"), fx.transform).transform;
			parent.gameObject.name = "OnDeath";
			fx.onDeathParent = parent;
		} else if(fx.onDeathParent.gameObject.name != "OnDeath")
			fx.onDeathParent.gameObject.name = "OnDeath";
		if(fx.onSpawnParent == null)
		{
			Transform parent = Instantiate(new GameObject("OnSpawn"), fx.transform).transform;
			parent.gameObject.name = "OnSpawn";
			fx.onSpawnParent = parent;
		} else if(fx.onSpawnParent.gameObject.name != "OnSpawn")
			fx.onSpawnParent.gameObject.name = "OnSpawn";
		if (fx.onNoClipParent == null)
		{
			Transform parent = Instantiate(new GameObject("OnSpawn"), fx.transform).transform;
			parent.gameObject.name = "OnNoClip";
			fx.onNoClipParent = parent;
		}
		else if (fx.onNoClipParent.gameObject.name != "OnNoClip")
			fx.onNoClipParent.gameObject.name = "OnNoClip";

		if (GUILayout.Button("Apply to all Slimes"))
		{
			SlimeEffects[] allEffects = GameObject.FindObjectsOfType<SlimeEffects>();
			
			if(allEffects.Length > 0)
			{
				foreach(SlimeEffects effects in allEffects)
				{
					if(effects.transform != fx.transform)
					{
						for(int i = 0; i < effects.transform.childCount; i++)
						{
							Transform effectParent = effects.transform.GetChild(i).transform;
							Transform effectRoot = null;

							for(int j = 0; j < fx.transform.childCount; j++)
							{
								if(fx.transform.GetChild(j).gameObject.name == effectParent.gameObject.name)
								{
									effectRoot = fx.transform.GetChild(j).transform;
									break;
								}
							}

							UpdateChildParticles(effectRoot, effectParent);

						}
					}
				}
				GameObject[] gos = new GameObject[allEffects.Length];
				for(int i = 0; i < gos.Length; i++)
				{
					gos[i] = allEffects[i].gameObject;
				}
				Selection.objects = gos;

			}
		}
	}
	void UpdateChildParticles(Transform root, Transform parent)
	{

		//Debug.Log("rootparticles=" + root.childCount.ToString() + root.gameObject.name + " childparticles=" + parent.childCount.ToString() +  parent.gameObject.name + " parent=" + parent.GetComponentInParent<UniMob_Base>().gameObject.name);
		while(parent.childCount > 0)
		{
			DestroyImmediate(parent.GetChild(0).gameObject);
		}
		for(int i = 0; i < root.childCount; i++)
		{
			GameObject newChild = Instantiate(root.GetChild(i).gameObject, parent);
			newChild.transform.localPosition = root.GetChild(i).transform.localPosition;
			newChild.name = root.GetChild(i).gameObject.name;

		}

	}
}

