﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(BaseMap))]
[CanEditMultipleObjects]
public class BaseMapEditor : Editor
{
	private Vector2 scrollPos = Vector2.zero;
	private int xQuad = 0;
	private int yQuad = 0;
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();
		BaseMap baseMap = target as BaseMap;
		if (baseMap.spawnableChances.Length != baseMap.spawnableObjectsParent.childCount)
		{
			baseMap.spawnableChances = new float[baseMap.spawnableObjectsParent.childCount];
			for (int i = 0; i < baseMap.spawnableChances.Length; i++)
			{
				if (baseMap.spawnableChances[i] == 0)
					baseMap.spawnableChances[i] = 1f;
			}
		}
		if (baseMap.spawnableRandomlyFlip.Length != baseMap.spawnableObjectsParent.childCount)
			baseMap.spawnableRandomlyFlip = new bool[baseMap.spawnableObjectsParent.childCount];
		if (baseMap.spawnableOnTopOnly.Length != baseMap.spawnableObjectsParent.childCount)
			baseMap.spawnableOnTopOnly = new bool[baseMap.spawnableObjectsParent.childCount];
		if (baseMap.spawnableOnBottomOnly.Length != baseMap.spawnableObjectsParent.childCount)
			baseMap.spawnableOnBottomOnly = new bool[baseMap.spawnableObjectsParent.childCount];

		if (GUILayout.Button("ResizeMapBackground"))
		{
			if (Selection.gameObjects.Length > 0)
			{
				foreach (GameObject go in Selection.gameObjects)
				{
					if (go.transform.GetComponent<BaseMap>() != null)
					{
						go.transform.GetComponent<BaseMap>().MapResizeBackground();
					}
				}
			}
			else
			{
				baseMap.MapResizeBackground();

			}

		}
		if (xQuad != baseMap.xQuadrants || yQuad != baseMap.yQuadrants)
		{
			xQuad = baseMap.xQuadrants;
			yQuad = baseMap.yQuadrants;
			baseMap.GetQuadrantCenters();
		}

		if (GUILayout.Button("Regenerate Objects"))
		{
			if (Selection.gameObjects.Length > 0)
			{
				foreach (GameObject go in Selection.gameObjects)
				{
					if (go.transform.GetComponent<BaseMap>() != null)
					{
						go.transform.GetComponent<BaseMap>().GetQuadrantCenters();
						go.transform.GetComponent<BaseMap>().SpawnObjects();
					}
				}
			}
			else
			{
				baseMap.GetQuadrantCenters();
				baseMap.SpawnObjects();
			}
		}

		if (baseMap.spawnableChances.Length > 0)
		{
			scrollPos = GUILayout.BeginScrollView(scrollPos, GUILayout.MinHeight(200));
			for (int i = 0; i < baseMap.spawnableChances.Length; i++)
			{
				Texture2D tex = AssetPreview.GetAssetPreview(baseMap.spawnableObjectsParent.GetChild(i).transform.GetComponentInChildren<SpriteRenderer>().sprite.texture);
				//using(var box= new GUILayout.)
				//GUILayout.Box(tex, GUILayout.ExpandWidth(true));
				//private void DrawLevelDataGUI()
				//{
				//	EditorGUILayout.LabelField("Data", _titleStyle);
				//	EditorGUILayout.BeginVertical("box");
				//	EditorGUILayout.PropertyField(_serializedTotalTime);
				//	_myTarget.Settings = (LevelSettings)EditorGUILayout.
				//	ObjectField("Level Settings", _myTarget.Settings,
				//	typeof(LevelSettings), false);
				//	if (_myTarget.Settings != null)
				//	{
				//		Editor.CreateEditor(_myTarget.Settings).OnInspectorGUI();
				//	}
				//	else
				//	{
				//		EditorGUILayout.HelpBox("You must attach a LevelSettings
					  
				//		asset!", MessageType.Warning);
				// }
				//	EditorGUILayout.EndVertical();
				//	_myTarget.SetGravity();
				//}
				GUILayout.Box("", GUILayout.MaxHeight(5), GUILayout.ExpandWidth(true));
				using (var horOption = new GUILayout.HorizontalScope())
				{
					GUILayout.Box(tex);
					//GUILayout.FlexibleSpace();

					using (var subVerOption0 = new GUILayout.VerticalScope(GUILayout.ExpandWidth(true)))
					{
						float tempChance = baseMap.spawnableChances[i];
						tempChance *= 100f;
						int tempInt = (int)tempChance;
						GUILayout.Label("SpawnChance(" + (tempInt).ToString("D3") + "%)", GUILayout.ExpandWidth(true));
						baseMap.spawnableChances[i] = GUILayout.HorizontalSlider(baseMap.spawnableChances[i], 0f, 1f, GUILayout.ExpandWidth(true));

						GUILayout.Label("FlipXRandomly", GUILayout.ExpandWidth(true));
						using (var subHorOption0 = new GUILayout.HorizontalScope(GUILayout.ExpandWidth(true)))
						{
							GUILayout.FlexibleSpace();
							baseMap.spawnableRandomlyFlip[i] = GUILayout.Toggle(baseMap.spawnableRandomlyFlip[i], "(" + baseMap.spawnableRandomlyFlip[i].ToString() + ")", GUILayout.ExpandWidth(true));
							GUILayout.FlexibleSpace();
						}

						GUILayout.Label(("Spawn in " + (baseMap.spawnableOnTopOnly[i] ? "Top" : "") + ((baseMap.spawnableOnTopOnly[i] && baseMap.spawnableOnBottomOnly[i]) ? " and " : "") + ((!baseMap.spawnableOnTopOnly[i] && !baseMap.spawnableOnBottomOnly[i]) ? "any" : "") + (baseMap.spawnableOnBottomOnly[i] ? "Bottom" : "") + " Row"), GUILayout.ExpandWidth(true));
						using (var subHorOption0 = new GUILayout.HorizontalScope(GUILayout.ExpandWidth(true)))
						{
							GUILayout.FlexibleSpace();
							baseMap.spawnableOnBottomOnly[i] = GUILayout.Toggle(baseMap.spawnableOnBottomOnly[i], "Top");
							baseMap.spawnableOnTopOnly[i] = GUILayout.Toggle(baseMap.spawnableOnTopOnly[i], "Bottom");
							GUILayout.FlexibleSpace();
						}

					}

					//GUILayout.FlexibleSpace();
					//using(var subVerOption1 = new GUILayout.VerticalScope(GUILayout.ExpandWidth(true)))
					//{

					//}
					//GUILayout.FlexibleSpace();

				}
			}
			GUILayout.EndScrollView();
		}
	}
}


