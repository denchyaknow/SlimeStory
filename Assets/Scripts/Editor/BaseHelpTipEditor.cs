﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(BaseHelpTip))]
public class BaseHelpTipEditor : Editor
{
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();
		BaseHelpTip helpTip = (BaseHelpTip)target;
		if (GUILayout.Button("Resize"))
		{
			helpTip.Resize();
		}
		if (GUILayout.Button("Reposition"))
		{
			helpTip.Reposition();
		}
	}

}
