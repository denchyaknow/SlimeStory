﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(CameraManager))]
public class CameraManagerEditor : Editor {
	Transform node = null;
	public override void OnInspectorGUI()
	{
		CameraManager cam = (CameraManager)target;
		if(GUILayout.Button(cam.isFollowing?"Stop Following":"Start Following"))
		{
			if (!cam.isFollowing)
				cam.StartFollowing();
			else
				cam.StopFollowing();
		}
	
		
		base.OnInspectorGUI();
	}
}
