﻿using UnityEngine;
using UnityEditor;
using UnityEngine.TestTools;
using NUnit.Framework;
using System.Collections;
[CustomEditor(typeof(ParticleTriggers))]
public class ParticleTriggersEditor : Editor {
	public ParticleTriggers trigger;
	
	public void OnEnable()
	{
		if (trigger == null)
			trigger = (ParticleTriggers)target as ParticleTriggers;
	}
	public override void OnInspectorGUI()
	{
		using (var hScope = new GUILayout.HorizontalScope(GUILayout.ExpandWidth(true)))
		{
			if (trigger.myParticles == null)
				trigger.myParticles = trigger.transform.GetComponent<ParticleSystem>();
			ParticleSystem.CollisionModule col = trigger.myParticles.collision;
			ParticleSystem.TriggerModule trig = trigger.myParticles.trigger;
			if (GUILayout.Button("Reset Trigger"))
			{
				switch (trigger.triggerType)
				{
					case ParticleTriggers.TriggerType.None:
						if (trigger.myCollider != null)
						{
							DestroyImmediate(trigger.myCollider);
						}
						if (trigger.myArea != null)
						{
							DestroyImmediate(trigger.myArea);
						}
						col.enabled = false;
						trig.enabled = false;
						break;
					case ParticleTriggers.TriggerType.ColliderCollision:
						if (trigger.myCollider == null)
						{
							if(trigger.transform.GetComponent<Collider2D>())
								trigger.myCollider = trigger.transform.GetComponent<CircleCollider2D>();
							else
								trigger.myCollider = trigger.gameObject.AddComponent<CircleCollider2D>();
						}
						if (trigger.myArea == null)
						{
							if (trigger.transform.GetComponent<AreaEffector2D>())
								trigger.myArea = trigger.transform.GetComponent<AreaEffector2D>();
							else
								trigger.myArea = trigger.gameObject.AddComponent<AreaEffector2D>();
						}
						trigger.myCollider.isTrigger = false;
						trigger.myCollider.usedByEffector = true;
						trigger.myArea.useGlobalAngle = true;
						col.enabled = false;
						trig.enabled = false;
						break;
					case ParticleTriggers.TriggerType.ColliderTrigger:
						if (trigger.myCollider == null)
						{
							if (trigger.transform.GetComponent<Collider2D>())
								trigger.myCollider = trigger.transform.GetComponent<CircleCollider2D>();
							else
								trigger.myCollider = trigger.gameObject.AddComponent<CircleCollider2D>();
						}
						if (trigger.myArea == null)
						{
							if (trigger.transform.GetComponent<AreaEffector2D>())
								trigger.myArea = trigger.transform.GetComponent<AreaEffector2D>();
							else
								trigger.myArea = trigger.gameObject.AddComponent<AreaEffector2D>();
						}
						trigger.myCollider.isTrigger = true;
						trigger.myCollider.usedByEffector = true;
						trigger.myArea.useGlobalAngle = true;
						col.enabled = false;
						trig.enabled = false;
						break;
					case ParticleTriggers.TriggerType.ParticleCollision:
						if (trigger.myCollider != null)
						{
							DestroyImmediate(trigger.myCollider);
						}
						if (trigger.myArea != null)
						{
							DestroyImmediate(trigger.myArea);
						}
						col.enabled = true;
						trig.enabled = false;
						break;
					//case ParticleTriggers.TriggerType.ParticleTrigger:
					//	if (trigger.myCollider != null)
					//	{
					//		DestroyImmediate(trigger.myCollider);
					//	}
					//	if (trigger.myArea != null)
					//	{
					//		DestroyImmediate(trigger.myArea);
					//	}
					//	col.enabled = false;
					//	trig.enabled = true;
					//	break;
				}
			}
		}
		base.OnInspectorGUI();
	}
}
