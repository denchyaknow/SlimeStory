﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using DG.Tweening;
[CustomEditor(typeof(BaseAttack))]
[CanEditMultipleObjects]
public class BaseAttackEditor : Editor
{
    Ease tEase;
	Ease tJumpEase;
	BaseAttack.PathType tPathType;
	SerializedProperty flip;
	SerializedProperty rotate;
	SerializedProperty lobHeight;
	SerializedProperty speedBased;
	SerializedProperty lobJumpHeightMultiplier;
	SerializedProperty lobJumpCount;
	SerializedProperty lobEase;
	SerializedProperty lobJumpEase;
	SerializedProperty duration;
	SerializedProperty durationMultiplier;
	SerializedProperty disableDelay;
	SerializedProperty pathType;
	private void OnEnable()
	{
		flip = serializedObject.FindProperty("flipAttack");
		rotate = serializedObject.FindProperty("rotateAttack");
		lobHeight = serializedObject.FindProperty("lobHeight");
		speedBased = serializedObject.FindProperty("speedBased");
		lobJumpHeightMultiplier = serializedObject.FindProperty("lobJumpHeightMultiplier");
		lobJumpCount = serializedObject.FindProperty("lobJumpCount");
		lobEase = serializedObject.FindProperty("lobEase");
		lobJumpEase = serializedObject.FindProperty("lobJumpEase");
		duration = serializedObject.FindProperty("lobDuration");
		durationMultiplier = serializedObject.FindProperty("lobDurationMultiplier");
		disableDelay = serializedObject.FindProperty("disableDelay");
		pathType = serializedObject.FindProperty("pathType");
	}
	public override void OnInspectorGUI()
    {
        BaseAttack baseAttack = target as BaseAttack;
		serializedObject.Update();
		using(var hScope = new GUILayout.HorizontalScope())
		{
			GUILayout.Label("Flip/Rotate");
			GUILayout.FlexibleSpace();
			GUILayout.FlexibleSpace();
			flip.boolValue = GUILayout.Toggle(flip.boolValue, ("Flip:" + flip.boolValue));
			GUILayout.FlexibleSpace();
			rotate.boolValue = GUILayout.Toggle(rotate.boolValue, ("Rotate:" + rotate.boolValue));
			GUILayout.FlexibleSpace();

		}
		tPathType = (BaseAttack.PathType)EditorGUILayout.EnumPopup("Path Type", (BaseAttack.PathType)pathType.enumValueIndex);
		pathType.enumValueIndex = (int)tPathType;
		switch(tPathType)
		{
			case BaseAttack.PathType.LobRigidbody:
			case BaseAttack.PathType.LobTransform:
				speedBased.boolValue = EditorGUILayout.Toggle("Distance Based?", speedBased.boolValue);
				lobHeight.floatValue = EditorGUILayout.FloatField("Lob Height", lobHeight.floatValue);
				if(speedBased.boolValue)
					lobJumpHeightMultiplier.floatValue = EditorGUILayout.FloatField("Height Multiplier", lobJumpHeightMultiplier.floatValue);
				lobJumpCount.intValue = EditorGUILayout.IntField("Jump Count", lobJumpCount.intValue);
				duration.floatValue = EditorGUILayout.FloatField("Lob Duration", duration.floatValue);
				if(speedBased.boolValue)
					durationMultiplier.floatValue = EditorGUILayout.FloatField("Duration Multiplier", durationMultiplier.floatValue);
				disableDelay.floatValue = EditorGUILayout.FloatField("Disable Delay", disableDelay.floatValue);
				tEase = (Ease)EditorGUILayout.EnumPopup("Lob Ease", (Ease)lobEase.enumValueIndex);
				lobEase.enumValueIndex = (int)tEase;
				tJumpEase = (Ease)EditorGUILayout.EnumPopup("LobJump Ease", (Ease)lobJumpEase.enumValueIndex);
				lobJumpEase.enumValueIndex = (int)tJumpEase;
				break;
			case BaseAttack.PathType.MoveRigibody:
			case BaseAttack.PathType.MoveTransform:
				speedBased.boolValue = EditorGUILayout.Toggle("Speed Based", speedBased.boolValue);
				duration.floatValue = EditorGUILayout.FloatField(speedBased.boolValue == true ? "Move Speed" : "Move Duration", duration.floatValue);
				disableDelay.floatValue = EditorGUILayout.FloatField("Disable Delay", disableDelay.floatValue);
				tEase = (Ease)EditorGUILayout.EnumPopup("Move Ease", (Ease)lobEase.enumValueIndex);
				lobEase.enumValueIndex = (int)tEase;
				break;
			case BaseAttack.PathType.None:
				duration.floatValue = EditorGUILayout.FloatField("Attack Time", duration.floatValue);
				disableDelay.floatValue = EditorGUILayout.FloatField("Disable Delay", disableDelay.floatValue);

				break;

		}
		serializedObject.ApplyModifiedProperties();
        base.OnInspectorGUI();
    }
}
