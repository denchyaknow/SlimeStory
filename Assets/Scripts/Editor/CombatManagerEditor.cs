﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(CombatManager))]
public class CombatManagerEditor : Editor
{
	public override void OnInspectorGUI()
	{
		CombatManager mng = (CombatManager)target;
		if (GUILayout.Button("Regenerate Map"))
		{
			mng.transform.GetComponent<CombatProperties>().InitializeZoneProperties();
			Debug.Log("Initialized Zone Properties: " + CombatProperties._Instance.AllZones.Count);
			//mng.ClearMap();
			//mng.SpawnMap();
		}
		if (GUILayout.Button("Clear Map"))
		{
			//mng.ClearMap();
		}
		if (GUILayout.Button("Kill All Mobs"))
		{
			while (mng.enemies.Count > 0)
			{
				GameObject enemy = mng.enemies[0].gameObject;
				mng.enemies.Remove(mng.enemies[0]);
				Destroy(enemy);
			}
		}
		base.OnInspectorGUI();
	}
}
