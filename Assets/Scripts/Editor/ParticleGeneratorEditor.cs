﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
namespace Denchyaknow
{

	[CustomEditor(typeof(ParticleGenerator))]
	public class ParticleGeneratorEditor : Editor
	{
		public const string pathToTextures = "Effects/Textures/";
		public const string pathToMaterials = "Assets/Resources/Effects/Materials/";
		public const string pathToPrefabs = "Assets/Resources/Effects/Prefabs/";
		private Vector2 scrollPos = Vector2.zero;
		public bool overrideTextures = false;
		public override void OnInspectorGUI()
		{
			ParticleGenerator gen = (ParticleGenerator)target;
			using(var vertRect = new GUILayout.HorizontalScope())
			{
				GUILayout.Label("Loaded_Textures (" + gen.Textures.Length + ")");


				if(GUILayout.Button("Reload Textures"))
				{
					gen.Textures = Resources.LoadAll<Texture2D>(pathToTextures);
				}
			}
			if(gen.ParentGenerated == null)
			{
				Transform parent = Instantiate(new GameObject(), gen.transform).transform;
				parent.gameObject.name = "Generated";
				gen.ParentGenerated = parent;
			}
			if(gen.ParentSamples == null)
			{
				Transform parent = Instantiate(new GameObject(), gen.transform).transform;
				parent.gameObject.name = "Samples Go Here";
				gen.ParentSamples = parent;
			}





			if(GUILayout.Button("Generate Random Particles"))
			{
				gen.SampledSystems = gen.ParentSamples.GetComponentsInChildren<ParticleSystem>();
				if(gen.SampledSystems.Length > 0)
				{
					for(int i = 0; i < gen.NumOfPartcleSystems; i++)
					{
						int ranTex = Random.Range(0, gen.Textures.Length);
						int ranSys = Random.Range(0, gen.SampledSystems.Length);
						int ranSha = Random.Range(0, gen.Shaders.Length);
						ParticleSystem sys = Instantiate(gen.SampledSystems[ranSys].gameObject).transform.GetComponent<ParticleSystem>();
						if(sys.transform.childCount > 0)
						{
							for(int j = 0; j < sys.transform.childCount; j++)
							{
								ParticleSystem childSys = sys.transform.GetChild(j).transform.GetComponent<ParticleSystem>();
								int ranChiTex = Random.Range(0, gen.Textures.Length);
								int ranChildSha = Random.Range(0, gen.Shaders.Length);
								Shader childShader = gen.Shaders[ranChildSha];
								childSys.gameObject.name = UniMob_Properties.RandomAttackNames[Random.Range(0, UniMob_Properties.RandomAttackNames.Length)] + "_" + ranChildSha.ToString("D3");
								Material childMat = (Material)Instantiate(new Material(childShader));
								childMat.mainTexture = gen.Textures[ranChiTex];
								AssetDatabase.CreateAsset(childMat, pathToMaterials + ranChiTex.ToString("D3") + ".mat");
								ParticleSystemRenderer childRen = childSys.GetComponent<ParticleSystemRenderer>();
								childRen.sharedMaterial = childMat;
								//AssetDatabase.CreateAsset(childSys, pathToPrefabs + sys.gameObject.name + ".prefab");
							}
						}
						Shader shader = gen.Shaders[ranSha];
						sys.gameObject.name = UniMob_Properties.RandomAttackNames[Random.Range(0, UniMob_Properties.RandomAttackNames.Length)] + "_" + ranTex.ToString("D3");
						Material mat = (Material)Instantiate(new Material(shader));
						mat.mainTexture = (gen.Textures[ranTex]);
						AssetDatabase.CreateAsset(mat, pathToMaterials + ranTex.ToString("D3") + ".mat");
						ParticleSystemRenderer sysRen = sys.GetComponent<ParticleSystemRenderer>();
						sysRen.sharedMaterial = mat;
						Object prefab = PrefabUtility.CreateEmptyPrefab(pathToPrefabs + ranTex.ToString("D3") + ".prefab");
						PrefabUtility.ReplacePrefab(sys.gameObject, prefab, ReplacePrefabOptions.ConnectToPrefab);
						sys.transform.SetParent(gen.ParentGenerated);
						sys.transform.localScale = new Vector3(1, 1, 1);
						sys.transform.localPosition = Vector3.zero;

					}
				}
			}
			if(gen.Textures.Length > 0)
			{
				using(var num = new GUILayout.HorizontalScope())
				{
					GUILayout.Label("Number Of Particles: " + gen.NumOfPartcleSystems.ToString());
					gen.NumOfPartcleSystems = (int)GUILayout.HorizontalSlider( gen.NumOfPartcleSystems, 1, (gen.Textures.Length - 1));
				}
				if(GUILayout.Button("Generate Sampled Particles"))
				{
					gen.SampledSystems = gen.ParentSamples.GetComponentsInChildren<ParticleSystem>();
					if(gen.SampledSystems.Length > 0)
					{
						for(int i = 0; i < gen.NumOfPartcleSystems; i++)
						{

						}
					}
				}
			}

			base.OnInspectorGUI();

		}
	}
}
