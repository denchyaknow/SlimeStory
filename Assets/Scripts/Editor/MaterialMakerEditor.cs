﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(MaterialMaker))]
public class MaterialMakerEditor : Editor {

	public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        MaterialMaker maker = target as MaterialMaker;
		string buttText = "Create Materials";
		if(maker.AssetShader == null)
			buttText = "Error (Shader == null)";
		else if(maker.ResourcePathOfMaterials == string.Empty)
			buttText = "Error (Material path == null)";
		else if(Resources.LoadAll<Texture>("Effects/" + maker.ResourcePathOfTextures + "/").Length < 1)
			buttText = "Error (Texture path == null)";
		if(GUILayout.Button(buttText) && maker.ResourcePathOfMaterials != string.Empty && maker.AssetShader != null )
        {
			if(maker.AssetShader == null)
			{
				//Debug.Log("EEROR_NULL_SHADER");
				return;
			}
            string actualPath = "Assets/Resources/Effects/";
			string ext = ".mat";
			//string resourcePathOfTextures = "Effects/" + maker.ResourcePathOfTextures + "/";
			string resourcePathOfTextures =  maker.ResourcePathOfTextures;
			string resourcePathOfMaterials = actualPath + maker.ResourcePathOfMaterials + "/";
			Shader shader = maker.AssetShader;
			
			string shaderName = shader.name;
			char[] ch = shaderName.ToCharArray();
			shaderName = string.Empty;
			for(int p = 0; p < ch.Length; p++)
			{
				char chr = ch[p];
				if(chr.ToString() == "/")
					shaderName += "_";
				else
					shaderName += chr.ToString();
			}
			if(!AssetDatabase.IsValidFolder("Assets/Resources/Effects"))
			{
				AssetDatabase.GUIDToAssetPath(AssetDatabase.CreateFolder("Assets/Resources", "Effects"));
				//Debug.Log("NEW_DIR(" + "Assets/Resources/Effects" + ")");
			}
			if(!AssetDatabase.IsValidFolder("Assets/Resources/Effects/" + maker.ResourcePathOfMaterials))
			{
				AssetDatabase.GUIDToAssetPath(AssetDatabase.CreateFolder(actualPath, maker.ResourcePathOfMaterials));
				//Debug.Log("NEW_DIR(" + resourcePathOfMaterials + ")");
			}
			if(resourcePathOfTextures == string.Empty)
			{
				resourcePathOfTextures = "Textures";
				maker.ResourcePathOfTextures = resourcePathOfTextures;
			}
			if(!AssetDatabase.IsValidFolder("Assets/Resources/Effects/" + resourcePathOfTextures))
			{
				AssetDatabase.GUIDToAssetPath(AssetDatabase.CreateFolder(actualPath, resourcePathOfTextures));
				//Debug.Log("NEW_DIR(" + resourcePathOfTextures + ")");
			}
			
			Texture2D[] tex = Resources.LoadAll<Texture2D>("Effects/" + resourcePathOfTextures + "/");
			Material[] mats = Resources.LoadAll<Material>("Effects/" + maker.ResourcePathOfMaterials + "/");

			if(tex.Length > 0)
			{
				string matExportPath = actualPath + resourcePathOfMaterials + "/";
				for(int i = 0; i < tex.Length; i++)
				{
					if(tex[i].name != i.ToString("D3"))
						AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(tex[i]), i.ToString("D3"));
					string matName = maker.AssetNamePrefix + "_" + i.ToString("D3") + "_" + shaderName;
					Material newMat = new Material(shader);
					newMat.name = matName;
					newMat.mainTexture = tex[i];
					Material duplicate = null;
					if(mats.Length > 0)
					{
						for(int o = 0; o < mats.Length; o++)
						{
							if(mats[o].name == (matName))
							{
								duplicate = mats[o];
								break;
							}
						}
					}
					if(!duplicate)
					{
						AssetDatabase.CreateAsset(newMat, resourcePathOfMaterials + newMat.name + ext);
						//Debug.Log("NEW_MAT(" + AssetDatabase.GetAssetPath(newMat) + ")");
					} else
					{
						duplicate.mainTexture = tex[i];
						//Debug.Log("DUPLICATE_MAT(" + (matName) + ")");
					}
				}
			} else
			{
				//Debug.Log("ERROR_NO_TEXTURES(" + resourcePathOfTextures + ")");
			}
		}
  //      if(maker.pathOfTextures != string.Empty && maker.textures.Count < 1)
  //      {
		//	Texture2D[] tex = Resources.LoadAll<Texture2D>(maker.pathOfTextures);
			
		//	//object[] tex = AssetDatabase.LoadAllAssetsAtPath(maker.pathOfTextures);
		//	if(tex.Length > 0 && maker.textures.Count < 1)
  //          {
  //              for(int i = 0; i < tex.Length; i++)
  //              {
		//			//if(tex[i].GetType() == typeof(Texture2D))
		//				maker.textures.Add((Texture2D)tex[i]);
  //              }
  //          } else
  //          {
  //              //Debug.Log("Cannot find texture at path: " + maker.pathOfTextures);
  //          }
  //      }
  //      if(GUILayout.Button("Create Materials"))
  //      {
  //          string path = AssetDatabase.GetAssetPath(maker.getPathOfThis);
  //          char[] pathChars = path.ToCharArray();
  //          char[] nameChars = maker.getPathOfThis.name.ToCharArray();
  //          string substring = path.Substring(0, pathChars.Length - (nameChars.Length + 4));

  //          if (maker.textures.Count > 0)
  //          {
		//		string folderPath = AssetDatabase.CreateFolder(substring, "Mats");
		//		string newFolderPath = AssetDatabase.GUIDToAssetPath(folderPath);
		//		string pathForMat = substring + maker.folderForMaterials;
		//		for(int i = 0; i < maker.textures.Count; i++)
		//		{
		//			Material newMat = new Material(maker.shader);
		//			newMat.name = i.ToString("D3") + "_" + maker.textures[i].name + " <" + maker.shader.name + ">";
		//			newMat.mainTexture = maker.textures[i];
		//			AssetDatabase.CreateAsset(newMat, "Assets/Effects/newMat.mat");
		//			//Debug.Log("Created Mat: " + AssetDatabase.GetAssetPath(newMat));
		//		}

		//	}
		//}
    }
}
