﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CustomEditor(typeof(TextureMaker))]
public class TextureMakerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        TextureMaker texMaker = target as TextureMaker;
        if (texMaker.path == string.Empty && texMaker.packedTextures.Count > 0)
        {
            string path = AssetDatabase.GetAssetPath(texMaker.packedTextures[0]);
            char[] pathChars = path.ToCharArray();
            char[] nameChars = texMaker.packedTextures[0].name.ToCharArray();
            string substring = path.Substring(0, pathChars.Length - (nameChars.Length + 4));
            texMaker.path = substring;
        }
    
        if (GUILayout.Button("Export Sprites"))
        {

            if (texMaker.packedTextures.Count > 0)
            {
                for (int i = 0; i < texMaker.packedTextures.Count; i++)
                {
                    texMaker.nameOfTex = texMaker.packedTextures[i].name;
                    string path = AssetDatabase.GetAssetPath(texMaker.packedTextures[i]);
                    char[] pathChars = path.ToCharArray();
                    char[] nameChars = texMaker.packedTextures[i].name.ToCharArray();
                    string substring = path.Substring(0, pathChars.Length - (nameChars.Length + 4));
                    texMaker.path = substring;
                    Sprite[] sheet = Resources.LoadAll<Sprite>(texMaker.path + texMaker.packedTextures[i].name);
                    if (sheet.Length > 0)
                    {
                        for (int o = 0; o < sheet.Length; o++)
                        {
                            Sprite sprite = sheet[o];
                            Texture2D tex = sprite.texture;
                            Rect r = sprite.textureRect;
                            Texture2D subtex = tex.CropTexture((int)r.x, (int)r.y, (int)r.width, (int)r.height);
                            byte[] data = subtex.EncodeToPNG();
                            System.IO.File.WriteAllBytes("Assets/Effects/Resources/" + texMaker.path + "Extracted/" + sprite.name + "_" + o.ToString("D3") + ".png", data);
                            //File.WriteAllBytes(Application.persistentDataPath + "/" + sprite.name + ".png", data);
                            //Debug.Log("Extracted to: " + "Assets/Effects/Resources/" + texMaker.path + "Extracted/" + sprite.name + "_" + o.ToString("D3") + ".png");
                        }
                    } else
                    {
                        //Debug.Log("No Sprites found at " + texMaker.path + texMaker.packedTextures[i].name);
                    }
                }
            }
        }
    }

}
