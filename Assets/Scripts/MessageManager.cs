﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Denchyaknow;
public class MessageManager : MonoBehaviour
{
	public GameManager gameManager
	{
		get
		{
			if (gm == null)
				gm = GameManager._Instance;
			return gm;
		}
	}
	private GameManager gm;
	public float mesgDelay = 0.5f;
	public float usedMsgClearTime = 30f;
	private float lastClearTime;
	public List<MessageObj> messages = new List<MessageObj>();
	public List<string> usedMessages = new List<string>();
	private BaseToolTip ttp;
	private bool[] messagesLoaded = new bool[0];
	public BaseToolTip toolTip
	{
		get
		{
			if(ttp == null)
			{
				GameObject newTTP = (GameObject)Instantiate(Resources.Load("GameObjects/ToolTip"));
				ttp = newTTP.transform.GetComponent<BaseToolTip>();
			}
			return ttp;
		}
	}
	public bool playingMessage = false;
	public bool messageLoaded = false;
	public MessageObj[] database
	{
		get
		{
			if (db == null)
				db = MessageDatabase.GetAllMessages();
			return db;
		}
	}
	private MessageObj[] db;
	int GetMsgID(string txt)
	{
		int id = 0;
		for (int i = 0; i < database.Length; i++)
		{
			if (database[i].mainText == txt)
			{
				id = i;
				break;
			}
		}
		return id;
	}
	private float deadCheckTIme = 30f;
	private float lastDeadCheck = 0;
	private void Awake()
	{
		transform.GetComponentInParent<GameManager>().messageManager = this;
	}
	// Use this for initialization
	void Start ()
	{
		
		lastClearTime = Time.timeSinceLevelLoad + usedMsgClearTime;
		messagesLoaded = new bool[database.Length];
	}
	// Update is called once per frame
	void Update ()
	{
		if (messages.Count > 0 && !playingMessage)
		{
			playingMessage = true;
			PlayNextMessage();
			return;
		}
		
		
		
		//bool msgAlreadyLoaded = false;
		//if(messageLoaded)
		//{
		//	if(messages.Count > 0)
		//	{
		//		PlayNextMessage();
		//	} else
		//	{
		//		messageLoaded = false;
		//	}
		//	return;
		//}
		#region All Slimes are dead?
		//int id = GetMsgID("ALLSLIMESDEAD");
		//msgAlreadyLoaded = messagesLoaded[id];
		if (Time.timeSinceLevelLoad > lastDeadCheck)
		{
			lastDeadCheck = Time.timeSinceLevelLoad + deadCheckTIme;
			CheckDeadSlimes();
		}
		
		#endregion

		//if(usedMessages.Count > 0)
		//{
		//	if(Time.timeSinceLevelLoad > lastClearTime)
		//	{
		//		lastClearTime = Time.timeSinceLevelLoad + usedMsgClearTime;
		//		usedMessages.Remove(usedMessages[0]);
		//	}
		//}

	}
	bool usedMessage(MessageObj msg)
	{
		return usedMessages.Contains(msg.msgID);
	}
	void CheckDeadSlimes()
	{
		if (gameManager.ourSlimes.Count > 0 && gameManager.combatManager == null)
		{
			bool allDead = true;
			//messagesLoaded[id] = true;
			MessageObj newMsg = MessageDatabase.GetMessage("ALLSLIMESDEAD");
			bool msgAlreadyUsed = usedMessages.Contains(newMsg.msgID);
			for (int i = 0; i < gameManager.ourSlimes.Count; i++)
			{
				if (gameManager.ourSlimes[i] != null)
				{
					BaseSlime slime = GameManager._Instance.ourSlimes[i].GetComponent<BaseSlime>();
					if (slime.slimeStats.currentHealth > 0)
					{
						//Debug.Log("Found live slime msg not playing");
						allDead = false;
						break;
					}
				}
			}

			if (allDead && !usedMessage(newMsg))
			{
				InitializeMessage("ALLSLIMESDEAD");
			}
		}
	}
	#region Initialization Logic
	public void InitializeMessage(MessageObj message)
	{
		//messageLoaded = true;
		message.mainDelegate += () =>
		 {
			 FinishPlayingMessage(message);
		 };
		if (!usedMessage(message))
		{
			usedMessages.Add(message.msgID);
			messages.Add(message);
		}
	}
	public void InitializeMessage(string msgID, Transform target = null)
	{
		//messageLoaded = true;
		MessageObj newMessage = MessageDatabase.GetMessage(msgID);
		if(newMessage == null || newMessage.msgID != msgID)
			Debug.Log("msg is null or returned bad msgID: " + msgID);

		switch (newMessage.msgID)
		{
			case "ALLSLIMESDEAD":
					//usedMessages.Add("ALLSLIMESDEAD");
					//Debug.Log("ALLSLIMESDEAD msg is not null");
					newMessage.option1Delegate += GameManager._Instance.GameClear;
					string nextMsgID = "GAMECLEARED";
					newMessage.option1Delegate += ()=>
					{
						InitializeMessage(nextMsgID);
						//PlayNextMessage();
					};
				
					//messages.Add(newMessage);
				break;
			case "GAMECLEARED":
				//newMessage = MessageDatabase.GetMessage("GAMECLEARED");
				
					//int thisMsgID = GetMsgID("ALLSLIMESDEAD");
					//usedMessages.Add("GAMECLEARED");
					
					//messages.Add(newMessage);
				break;
			case "RESETEVERYTHING":
				//newMessage = MessageDatabase.GetMessage("RESETEVERYTHING");

				//usedMessages.Add("RESETEVERYTHING");
				newMessage.option1Delegate += () =>
				{
					GameManager._Instance.GameClear();
				};
					//messages.Add(newMessage);
				

				break;
		}
		if (newMessage.option1Text != string.Empty && newMessage.option2Text != string.Empty)
		{
			newMessage.option1Delegate += () =>
			{
				FinishPlayingMessage(newMessage);
			};
			newMessage.option2Delegate += () =>
			{
				FinishPlayingMessage(newMessage);
			};
		}
		else
		{
			newMessage.mainDelegate += () =>
			{
				FinishPlayingMessage(newMessage);
			};
		}
		if (!usedMessage(newMessage))
		{
			usedMessages.Add(newMessage.msgID);
			messages.Add(newMessage);
		}
	}
	#endregion
	
	//public bool MessageIsLoaded(string messageID)
	//{
	//	bool loaded = false;
	//	if(messages.Count > 0)
	//	{
	//		for(int i = 0; i < messages.Count; i++)
	//		{
	//			if(messages[i].msgID == messageID)
	//			{
	//				loaded = true;
	//				break;
	//			}
	//		}
	//	}
	//	return loaded;
	//}
	public void PlayNextMessage(float delay = 0)
	{
		if(delay == 0)
			delay = mesgDelay;
		StartCoroutine(PlayMessage(delay));
		
	}
	IEnumerator PlayMessage(float delay)
	{
		while(toolTip.gameObject.activeInHierarchy)
		{
			yield return new WaitForSeconds(0.1f);
		}
		yield return new WaitForSeconds(delay);
		//MessageObj currentMessage = new MessageObj(messages[0]);
		//messages.Remove(messages[0]);
		int time = 0;
		while(messages.Count == 0)
		{
			yield return new WaitForSeconds(0.1f);
			time++;
			if(time > 100)
			{
				Debug.Log("Cannot Play Message, there is no message to play");
				 StopCoroutine(PlayMessage(delay));
				break; 
			}
		}

		if(SetNextMessage(messages[0]))
		{
			toolTip.gameObject.SetActive(true);
			messages.Remove(messages[0]);

		}
		
	}
	public void FinishPlayingMessage(MessageObj msg)
	{
		if (usedMessage(msg))
			usedMessages.Remove(msg.msgID);
		playingMessage = false;
	}
	public bool SetNextMessage(MessageObj message)
	{
		bool messageSet = false;
		MessageObj curMsg = message;
		if(!toolTip.gameObject.activeInHierarchy && curMsg != null)
		{
			toolTip.textBody = curMsg.mainText;
			toolTip.textOption1 = curMsg.option1Text;
			toolTip.textOption2 = curMsg.option2Text;
			//toolTip.onClickAnywhere += FinishPlayingMessage;
			if(curMsg.mainDelegate != null)
			{
				toolTip.onClickAnywhere += curMsg.mainDelegate;
				curMsg.mainDelegate = null;
			}
			if(curMsg.option1Delegate != null)
			{
				toolTip.onClickOption1 += curMsg.option1Delegate;
				//toolTip.onClickOption1 += FinishPlayingMessage;
				curMsg.option1Delegate = null;

			}
			if(curMsg.option2Delegate != null)
			{
				toolTip.onClickOption2 += curMsg.option2Delegate;
				//toolTip.onClickOption2 += FinishPlayingMessage;
				curMsg.option2Delegate = null;

			}
			toolTip.toolTipType = curMsg.msgType;
			switch(toolTip.toolTipType)
			{
				case BaseToolTip.ToopTipType.Bubble:
					if(curMsg.msgTarget != null)
					{
						toolTip.transform.SetParent(curMsg.msgTarget);
						Vector2 pos = Vector2.zero;
						pos.y += 1.2f;
						toolTip.transform.localPosition = pos;
						toolTip.uniMob = curMsg.msgTarget.GetComponent<UniMob_Base>();
						toolTip.gameObject.SetActive(true);
					}
					break;
				case BaseToolTip.ToopTipType.Narrative:
					toolTip.transform.SetParent(transform);
					toolTip.transform.localPosition = Vector3.zero;
					break;
				case BaseToolTip.ToopTipType.System:
					toolTip.transform.SetParent(GameManager._Instance.uiManager.transform);
					toolTip.transform.localPosition = Vector3.zero;
					toolTip.tipFillOption.gameObject.SetActive(true);
					break;
			}
			
			messageSet = true;
		}
		return messageSet;
	}
}
[System.Serializable]
public class MessageObj
{
	public string msgID = string.Empty;
	public string mainText = string.Empty;
	public string option1Text = string.Empty;
	public string option2Text = string.Empty;
	public BaseToolTip.Delegate mainDelegate;
	public BaseToolTip.Delegate option1Delegate;
	public BaseToolTip.Delegate option2Delegate;
	public BaseToolTip.ToopTipType msgType;
	public Transform msgTarget = null;
	public MessageObj(MessageObj message)
	{
		msgID = message.msgID;
		mainText = message.mainText;
		option1Text = message.option1Text;
		option2Text = message.option2Text;
		if(message.mainDelegate != null)
		{
			mainDelegate += message.mainDelegate;
			message.mainDelegate = null;
		}
		if(message.option1Delegate != null)
		{
			option1Delegate += message.option1Delegate;
			message.option1Delegate = null;
		}
		if(message.option2Delegate != null)
		{
			option2Delegate += message.option2Delegate;
			message.option2Delegate = null;
		}
		msgType = message.msgType;
		msgTarget = message.msgTarget;
	}
	public MessageObj(string id,string main, string op1, string op2, BaseToolTip.ToopTipType type)
	{
		msgID = id;
		mainText = main;
		option1Text = op1;
		option2Text = op2;
		mainDelegate = null;
		option1Delegate = null;
		option2Delegate = null;
		msgType = type;
	}
	public MessageObj(string id, string main, string op1, string op2, BaseToolTip.ToopTipType type, Transform target = null)
	{
		msgID = id;
		mainText = main;
		option1Text = op1;
		option2Text = op2;
		mainDelegate = null;
		option1Delegate = null;
		option2Delegate = null;
		msgType = type;
		msgTarget = target;
	}
	public MessageObj()
	{
		msgID = Random.Range(0, 10000).ToString();
		mainText = string.Empty;
		option1Text = string.Empty;
		option2Text = string.Empty;
		mainDelegate = null;
		option1Delegate = null;
		option2Delegate = null;
		msgType = BaseToolTip.ToopTipType.Narrative;
	}
}
