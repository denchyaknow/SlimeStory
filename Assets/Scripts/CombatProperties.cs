﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class CombatProperties : MonoBehaviour
{
	public static CombatProperties _Instance;
	public bool initialized = false;
	public List<ZoneData> ZoneDatabase = new List<ZoneData>();
	public List<ZoneProperties> AllZones = new List<ZoneProperties>();
	public void Awake()
	{
		
	}
	private void Start()
	{
		if (ZoneDatabase.Count == 0)
		{
			InitializeZoneDatabase();
		}
		else
		{
			initialized = true;
		}
	}

	public void InitializeZoneDatabase()
	{
		BaseMap[] allZones = Resources.LoadAll<BaseMap>("GameObjects/Maps");
		if (allZones.Length > 0)
		{
			ZoneDatabase.Clear();
			for (int i = 0; i < allZones.Length; i++)
			{
				ZoneData newZone = new ZoneData();
				
				newZone.mapTiles = new List<MapData>();
				newZone.mapTiles.Add(new MapData());
				ZoneDatabase.Add(newZone);
			}

		}
		initialized = true;
	}
	public void InitializeZoneProperties()
	{
		#region Zone 0
		List<BaseMap.MapSize> zoneTiles_0 = new List<BaseMap.MapSize>();
		zoneTiles_0.Add(BaseMap.MapSize.Small);
		zoneTiles_0.Add(BaseMap.MapSize.Medium);
		zoneTiles_0.Add(BaseMap.MapSize.Medium);
		zoneTiles_0.Add(BaseMap.MapSize.Medium);
		List<MobProperties> zoneMobs_0 = new List<MobProperties>();
		zoneMobs_0.Add(new MobProperties(0, 100, 10));
		zoneMobs_0.Add(new MobProperties(0, 100, 10));
		AllZones.Add(new ZoneProperties(zoneTiles_0, zoneMobs_0));
		#endregion
		#region Zone 1
		List<BaseMap.MapSize> zoneTiles_1 = new List<BaseMap.MapSize>();
		zoneTiles_1.Add(BaseMap.MapSize.Small);
		zoneTiles_1.Add(BaseMap.MapSize.Medium);
		zoneTiles_1.Add(BaseMap.MapSize.Medium);
		zoneTiles_1.Add(BaseMap.MapSize.Medium);
		List<MobProperties> zoneMobs_1 = new List<MobProperties>();
		zoneMobs_1.Add(new MobProperties(0, 100, 10));
		zoneMobs_1.Add(new MobProperties(0, 100, 10));
		AllZones.Add(new ZoneProperties(zoneTiles_1, zoneMobs_1));
		#endregion
		#region Zone 2
		List<BaseMap.MapSize> zoneTiles_2 = new List<BaseMap.MapSize>();
		zoneTiles_2.Add(BaseMap.MapSize.Small);
		zoneTiles_2.Add(BaseMap.MapSize.Medium);
		zoneTiles_2.Add(BaseMap.MapSize.Medium);
		zoneTiles_2.Add(BaseMap.MapSize.Medium);
		List<MobProperties> zoneMobs_2 = new List<MobProperties>();
		zoneMobs_2.Add(new MobProperties(0, 100, 10));
		zoneMobs_2.Add(new MobProperties(0, 100, 10));
		AllZones.Add(new ZoneProperties(zoneTiles_2, zoneMobs_2));
		#endregion
		#region Zone 3
		List<BaseMap.MapSize> zoneTiles_3 = new List<BaseMap.MapSize>();
		zoneTiles_3.Add(BaseMap.MapSize.Small);
		zoneTiles_3.Add(BaseMap.MapSize.Medium);
		zoneTiles_3.Add(BaseMap.MapSize.Medium);
		zoneTiles_3.Add(BaseMap.MapSize.Medium);
		List<MobProperties> zoneMobs_3 = new List<MobProperties>();
		zoneMobs_3.Add(new MobProperties(0, 100, 10));
		zoneMobs_3.Add(new MobProperties(0, 100, 10));
		AllZones.Add(new ZoneProperties(zoneTiles_3, zoneMobs_3));
		#endregion
		#region Zone 4
		List<BaseMap.MapSize> zoneTiles_4 = new List<BaseMap.MapSize>();
		zoneTiles_4.Add(BaseMap.MapSize.Small);
		zoneTiles_4.Add(BaseMap.MapSize.Medium);
		zoneTiles_4.Add(BaseMap.MapSize.Medium);
		zoneTiles_4.Add(BaseMap.MapSize.Medium);
		List<MobProperties> zoneMobs_4 = new List<MobProperties>();
		zoneMobs_4.Add(new MobProperties(0, 100, 10));
		zoneMobs_4.Add(new MobProperties(0, 100, 10));
		AllZones.Add(new ZoneProperties(zoneTiles_4, zoneMobs_4));
		#endregion
		#region Zone 5
		List<BaseMap.MapSize> zoneTiles_5 = new List<BaseMap.MapSize>();
		zoneTiles_5.Add(BaseMap.MapSize.Small);
		zoneTiles_5.Add(BaseMap.MapSize.Medium);
		zoneTiles_5.Add(BaseMap.MapSize.Medium);
		zoneTiles_5.Add(BaseMap.MapSize.Medium);
		List<MobProperties> zoneMobs_5 = new List<MobProperties>();
		zoneMobs_5.Add(new MobProperties(0, 100, 10));
		zoneMobs_5.Add(new MobProperties(0, 100, 10));
		AllZones.Add(new ZoneProperties(zoneTiles_5, zoneMobs_5));
		#endregion
		#region Zone 6
		List<BaseMap.MapSize> zoneTiles_6 = new List<BaseMap.MapSize>();
		zoneTiles_6.Add(BaseMap.MapSize.Small);
		zoneTiles_6.Add(BaseMap.MapSize.Medium);
		zoneTiles_6.Add(BaseMap.MapSize.Medium);
		zoneTiles_6.Add(BaseMap.MapSize.Medium);
		List<MobProperties> zoneMobs_6 = new List<MobProperties>();
		zoneMobs_6.Add(new MobProperties(0, 100, 10));
		zoneMobs_6.Add(new MobProperties(0, 100, 10));
		AllZones.Add(new ZoneProperties(zoneTiles_6, zoneMobs_6));
		#endregion
	}
}
[System.Serializable]
public class ZoneData
{
	//In each zone there are map tiles
	//Each map tiles has its own MapaData variable
	public List<MapData> mapTiles = new List<MapData>();
	public ZoneData()
	{
		mapTiles = new List<MapData>();
	}
}
[System.Serializable]
public class ZoneProperties
{
	public List<BaseMap.MapSize> mapTiles = new List<BaseMap.MapSize>();
	public List<MobProperties> mobProperties = new List<MobProperties>();
	public ZoneProperties()
	{
		mapTiles.Add(BaseMap.MapSize.Medium);
		mobProperties.Add(new MobProperties());
	}
	public ZoneProperties(List<BaseMap.MapSize> tiles, List<MobProperties> mobs)
	{
		mapTiles = tiles;
		mobProperties = mobs;
	}
}
[System.Serializable]
public class MobProperties
{
	public int mobID = 0;
	public int spawnChance = 80;
	public int spawnCount = 10;
	public MobProperties()
	{
		mobID = 0;
		spawnChance = 80;
		spawnCount = 10;

	}
	public MobProperties(int mID, int sChance, int sCount)
	{
		mobID = mID;
		spawnChance = sChance;
		spawnCount = sCount;

	}

}
