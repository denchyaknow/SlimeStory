﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialMaker : MonoBehaviour {
	[Header("Resources/Effects/")]
    public string ResourcePathOfTextures;
    public string ResourcePathOfMaterials;
	public string AssetNamePrefix;
    public Shader AssetShader;
    public List<Texture2D> textures = new List<Texture2D>();
    //public Texture2D getPathOfThis;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
