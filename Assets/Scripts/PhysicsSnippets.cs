﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsSnippets : MonoBehaviour
{

	// Use this for initialization
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

	}

	//Pretty solid way of getting a hit during a tween
	//myBody.DOMove(targetPos, mobStats.attackDuration* 0.8f, false).SetEase(moveEase).OnUpdate(() =>
	//			{
	//	float dis = Vector3.Distance(transform.position, targetPos);
	//	if(dis < mobStats.attackRadius)
	//	{
	// NEW SYSTEM
	//		ContactFilter2D filter = new ContactFilter2D();
	//		filter.useLayerMask = true;
	//		filter.SetLayerMask(GameManager._Instance.inputManager.whatIsPlayer);
	//		int hits = Physics2D.GetContacts(myCollider, filter, capsuleHits.ToArray());
	//		if(hits > 0)
	//		{
	//			Debug.Log("CONTACT Hits-" + hits.ToString() + " Array-" + capsuleHits.Count + " [0]-" + capsuleHits[0].name);
	//			int hitID = capsuleHits[0].transform.root.gameObject.GetInstanceID();
	//			if(lastHitSlime != hitID)
	//			{
	//				lastHitSlime = hitID;
	//				capsuleHits[0].transform.GetComponent<BaseSlime>().TakeDamage(mobStats.damage);
	//			}
	//		}
	// OLD SYSTEM
	//		Collider2D hit = Physics2D.OverlapCircle(targetPos, mobStats.attackRadius, GameManager._Instance.inputManager.whatIsPlayer);
	//		if(hit && lastHitSlime != hit.transform.root.gameObject.GetInstanceID() && !isMobDead)
	//		{
	//			lastHitSlime = hit.transform.root.gameObject.GetInstanceID();
	//			hit.transform.root.GetComponent<BaseSlime>().TakeDamage(mobStats.damage);
	//		} else if(isMobDead)
	//		{
	//			DOTween.Pause(transform);
	//		}

	//	}
	//});
}
