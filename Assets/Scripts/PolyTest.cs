﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.Extensions;
using DG.Tweening;
public class PolyTest : MonoBehaviour {

    
    public int sides = 5;
    public float[] values = new float[5];
    //public float[] temp = new float[5];
    public float[] times = new float[5];
    public UIPolygon poly;
    public int[] stats = new int[5] { 1, 2, 3, 4, 5 };
    public Ease statEase;
    public float time = 1;
    public float delay = 1;
    public bool test = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void LateUpdate ()
    {
        if(test)
        {
            test = false;
            float[] temp = new float[sides + 1];
            if (temp.Length != sides + 1)
            {

                temp = new float[sides + 1];
                for(int i = 0; i < temp.Length; i++)
                {
                    temp[i] = 0.2f;
                }
            }
            else
            {
                for (int i = 0;i < temp.Length; i++)
                {
                    temp[i] = 0.2f;
                }
            }
        times = new float[sides + 1];
        values = new float[sides + 1];
        values[0] = (float)(stats[0]+51) / (306f);
        values[1] = (float)(stats[1]+51) / (306f);
        values[2] = (float)(stats[2]+51) / (306f);
        values[3] = (float)(stats[3]+51) / (306f);
        values[4] = (float)(stats[4]+51) / (306f);
            values[5] = values[0];
            poly.DrawPolygon(sides, temp);
            poly.VerticesDistances[0] = poly.VerticesDistances[poly.VerticesDistances.Length -1];
            poly.SetAllDirty();
            //DOTween.To(() => temp[4], x => temp[4] = x, values[4], time / 5).SetEase(statEase).SetUpdate(UpdateType.Late).OnUpdate(() =>
            //  {
            //      poly.DrawPolygon(sides, temp);
            //      poly.SetAllDirty();
            //  });
            //DOTween.To(() => temp[3], x => temp[3] = x, values[3], time / 5).SetDelay((delay / 4)).SetEase(statEase).SetUpdate(UpdateType.Late).OnUpdate(() =>
            //{
            //    poly.DrawPolygon(sides, temp);
            //    poly.SetAllDirty();
            //});
            //DOTween.To(() => temp[2], x => temp[2] = x, values[2], time / 5).SetDelay((delay / 4) * 2).SetEase(statEase).SetUpdate(UpdateType.Late).OnUpdate(() =>
            //{
            //    poly.DrawPolygon(sides, temp);
            //    poly.SetAllDirty();
            //});
            //DOTween.To(() => temp[1], x => temp[1] = x, values[1], time / 5).SetDelay((delay / 4) * 3).SetEase(statEase).SetUpdate(UpdateType.Late).OnUpdate(() =>
            //{
            //    poly.DrawPolygon(sides, temp);
            //    poly.SetAllDirty();
            //});
            //DOTween.To(() => temp[0], x => temp[0] = x, values[0], time / 5).SetDelay((delay)).SetEase(statEase).SetUpdate(UpdateType.Late).OnUpdate(() =>
            //{
            //    poly.DrawPolygon(sides, temp);
            //    poly.SetAllDirty();
            //});

            //DOTween.To(() => temp[4], x => temp[4] = x, values[4], time / 5).SetEase(statEase).SetUpdate(UpdateType.Late).OnUpdate(() =>
            //{
            //    poly.DrawPolygon(sides, temp);
            //    poly.SetAllDirty();
            //}).OnComplete(()=>
            //{
            //    DOTween.To(() => temp[3], x => temp[3] = x, values[3], time/5).SetEase(statEase).SetUpdate(UpdateType.Late).OnUpdate(() =>
            //    {
            //        poly.DrawPolygon(sides, temp);
            //        poly.SetAllDirty();
            //    }).OnComplete(() =>
            //    {
            //        DOTween.To(() => temp[2], x => temp[2] = x, values[2], time/5).SetEase(statEase).SetUpdate(UpdateType.Late).OnUpdate(() =>
            //        {
            //            poly.DrawPolygon(sides, temp);
            //            poly.SetAllDirty();
            //        }).OnComplete(() =>
            //        {
            //            DOTween.To(() => temp[1], x => temp[1] = x, values[1], time/5).SetEase(statEase).SetUpdate(UpdateType.Late).OnUpdate(() =>
            //            {
            //                poly.DrawPolygon(sides, temp);
            //                poly.SetAllDirty();
            //            }).OnComplete(() =>
            //            {
            //                DOTween.To(() => temp[0], x => temp[0] = x, values[0], time/5).SetEase(statEase).SetUpdate(UpdateType.Late).OnUpdate(() =>
            //                {
            //                    poly.DrawPolygon(sides, temp);
            //                    poly.SetAllDirty();
            //                }).OnComplete(() =>
            //                {
            //                    //poly.DrawPolygon(sides, temp);
            //                    //poly.SetAllDirty();
            //                });
            //            });
            //        });
            //    });
            //});

        //for (int i = 0; i < temp.Length -1; i++)
        //{
        //        int index = i;
        //    DOTween.To(() => temp[index], x => temp[index] = x, values[index], 1).SetEase(statEase).SetUpdate(UpdateType.Late).OnUpdate(()=> 
        //    {
        //        if(index == 0)
        //        {

        //        poly.DrawPolygon(sides, temp);
        //        poly.SetAllDirty();
        //        }
        //    });

        //}
        //for (int i = 0; i < values.Length; i++)
        //{
        //    float stat = (float)stats[i] / (255f);
        //    values[i] = stat;
        //}
        }
        
	}
}
