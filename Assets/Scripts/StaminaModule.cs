﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class StaminaModule : MonoBehaviour
{
    public SpriteRenderer[] bar;
    public float speed = 1f;
    public float count
    {
        get
        {
            float num = bar[1].size.x;
            return num;
        }
        set
        {
            bar[1].size = new Vector2(value, 1);
        }
    }
    private bool filling = false;
    private bool startFilling = false;
    private bool isVisible = false;
    public bool test = false;
    private Vector3 startScale;
	private Vector3 startPos;
	// Use this for initialization
	void Start ()
    {
        bar[0].color = new Color(255, 255, 255, 0);
        bar[1].color = new Color(255, 255, 255, 0);
        isVisible = false;
		startPos = transform.localPosition;
        startScale = transform.localScale;
	}

    // Update is called once per frame
    private void LateUpdate()
    {
        if (transform.localScale != startScale)
        {
            transform.localScale = startScale;
        }
    }
    void Update ()
    {
        if(test)
        {
            test = false;
            RemoveCounter(1f);
        }
        if(transform.eulerAngles != Vector3.zero)
        {
            transform.eulerAngles = Vector3.zero;
        }
        if(!isVisible && count < 10f)
        {
            isVisible = true;
            FadeIn();
        } else if(isVisible && count >= 10f)
        {
            count = 10f;
            isVisible = false;
            startFilling = false;
            FadeOut();
        }
        if(startFilling && !filling && count < 10f)
        {
            filling = true;
            StartCoroutine(Fill());
        }
        if(!filling && !startFilling && count < 10f)
        {
            filling = true;
            StartCoroutine(StartFilling());
        }
    }
    IEnumerator StartFilling()
    {
       
        yield return new WaitForSeconds(1f);
        filling = false;
        startFilling = true;
    }
    IEnumerator Fill()
    {

        yield return new WaitForSeconds(Time.deltaTime);
        count += 0.1f * speed;
        filling = false;

    }
    public void RemoveCounter(float amount)
    {
        StopAllCoroutines();
        startFilling = false;
        filling = false;
        count -= amount;
    }
    void FadeIn()
    {
        bar[0].DOFade(255, 1f).SetEase(Ease.InOutExpo);
        bar[1].DOFade(255, 1f).SetEase(Ease.InOutExpo);
    }
    void FadeOut()
    {
        bar[0].DOFade(0, 1f).SetEase(Ease.InOutExpo);
        bar[1].DOFade(0, 1f).SetEase(Ease.InOutExpo);
    }
    public void ConnectModule(Transform target)
    {
        transform.SetParent(target);
		transform.localPosition = startPos;
    }
    public void DisconnectModule()
    {
		startPos = transform.localPosition;
        transform.SetParent(GameManager._Instance.garbageCan);
    }
}
