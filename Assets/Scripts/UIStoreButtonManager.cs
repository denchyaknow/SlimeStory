﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class UIStoreButtonManager : MonoBehaviour {
	public List<UIStoreButton> listButtons = new List<UIStoreButton>();
	public UIStoreButton buttCurrent;
	public UIStoreButton buttNext;
	// Use this for initialization
	void Awake ()
	{
		listButtons.Clear();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(buttNext != null)
		{
			if(buttNext != buttCurrent)
			{
				buttCurrent = buttNext;
				buttNext.ShowBuyButtons();
				buttNext = null;
			} else
			{
				buttNext.HideBuyButtons();
				buttNext = null;

				buttCurrent = null;

			}
			for(int i = 0; i < listButtons.Count; ++i)
			{
				if(listButtons[i] != buttCurrent)
				{
					listButtons[i].HideBuyButtons();
				}
			}
		}
	}
	public void ResetAllButtons()
	{
		for(int i = 0; i < listButtons.Count; ++i)
		{
			DOTween.Kill(listButtons[i].transform);
			listButtons[i].buttCoin.transform.localScale = new Vector3(0, 1, 1);
			listButtons[i].buttGem.transform.localScale = new Vector3(0, 1, 1);
		}
	}
}
