﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToyBehavior : StateMachineBehaviour
{

	private BaseSlime slime;
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		if (slime == null)
			slime = animator.GetComponent<BaseSlime>();
		slime.ToyBehaviorEnter();
	}

	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		if (slime == null)
			slime = animator.GetComponent<BaseSlime>();
		slime.ToyBehaviorUpdate();
	}

	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		if (slime == null)
			slime = animator.GetComponent<BaseSlime>();
		slime.ToyBehaviorExit();
	}

}
