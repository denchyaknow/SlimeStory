﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Rendering;
using DG.Tweening;
[System.Serializable]
public class BaseUpgrade : BaseObject
{

    public UpgradeData upgradeData;
	// Use this for initialization
	public override void Start ()
    {
        base.Start();
	}
	
	// Update is called once per frame
	public override void Update ()
    {
        base.Update();	
	}
    public void UpgradeSlime(BaseSlime slime)
    {
        Transform target = slime.transform;
        float jump = Random.Range(2.0f, 3.5f);
        float dis = Vector2.Distance(transform.position, target.position);
        float time = 18f / (dis * 10);
        float finTime = time / 8;
        float curTime = time;
        bool fading = false;
        BaseSlime targetSlime = slime;
		Vector2 center = GameManager._Instance.inputManager.GetScreenCenterPosition();
		if(transform.position.y > center.y);
			jump = -jump;
        transform.DOMove(target.position, time, false).SetEase(Ease.Linear).OnUpdate(() =>
        {
            float mag = Vector2.Distance(transform.position, target.position);
            curTime -= Time.deltaTime;
            if (curTime < finTime && !fading)
            {
                fading = true;
                myRenderer.DOFade(0, 0.4f);
            }
        }).OnComplete(()=>
		{
			if(!fading)
			{
				fading = true;
				myRenderer.DOFade(0, 0.4f);
			}
			targetSlime.AddUpgrade(upgradeData);
		});
        myRenderer.transform.DOLocalJump(Vector3.zero, jump, 1, time, false).SetEase(Ease.Linear);


}
	public override void TouchFunction(int _id, Vector3 _currentPos)
	{
		base.TouchFunction(_id, _currentPos);
		
	}
}
