﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class TrailModule : MonoBehaviour {
    private Material mat;
    private ParticleSystem sys;
    private ParticleSystem.TextureSheetAnimationModule anim;
    private SpriteRenderer sprite;
    private string keyword = "_MainTex";
    private Texture lastTex;
    public bool useInstancing = false;
	public bool reverseFlip = false;
    public bool testMat = false;
	private float x1;
	private float y1;
	private float x2;
	private float y2;
	private Vector3 scale = new Vector2(); 
	void Start ()
    {
        sys = transform.GetComponent<ParticleSystem>();
        anim = sys.textureSheetAnimation;
        sprite = transform.GetComponentInParent<SpriteRenderer>();
	    if(useInstancing)
        {
            mat = new Material(sys.GetComponent<Renderer>().material);
            GetComponent<Renderer>().material = mat;
        } else
        {
            mat = sys.GetComponent<Renderer>().sharedMaterial;
        }
        lastTex = sprite.sprite.texture;
        mat.shader.hideFlags = HideFlags.None;
        mat.SetTexture(keyword, lastTex);
        
        sys.GetComponent<ParticleSystemRenderer>().material = mat;
		//InitializeTrail();
    }

    private void LateUpdate()
    {
        if (sprite.sprite.texture != lastTex)
        {
            lastTex = sprite.sprite.texture;
        }
		x1 = lastTex.width;
		y1 = lastTex.height;
		x2 = sprite.sprite.rect.width;
		y2 = sprite.sprite.rect.height;
		x1 = x1 / x2;
		y1 = y1 / y2;
		x1 = 1f / x1;
		y1 = 1f / y1;
		scale = new Vector2(x1, -y1);
		mat.SetTextureScale(keyword, scale);
		mat.SetTexture(keyword, lastTex);
		int flipTrue = reverseFlip ? 0 : 1;
		int flipFalse = reverseFlip ? 1 : 0;
        anim.flipU = sprite.flipX ? flipTrue : flipFalse;
        Vector2 offset = sprite.sprite.uv[0];
        mat.SetTextureOffset(keyword, offset);
    }
    void Update ()
    {
		//if(playTrail)
		//{
		//	playTrail = false;
		//	Play();
		//}
	}
	public int trailLength = 20;
	private int cachedTrailLength;
	public float trailRate = 0.2f;
	public int trailDensity = 1;
	private ParticleSystem[] trail;
	private Material[] trailMats;
	public bool playTrail = false;
	public void Play(float time)
	{
		ParticleSystem trailPieceSys = sys;
		ParticleSystem.TextureSheetAnimationModule trailAnim = trailPieceSys.textureSheetAnimation;
		ParticleSystem.MainModule main = sys.main;
		main.loop = true;
		x1 = lastTex.width;
		y1 = lastTex.height;
		x2 = sprite.sprite.rect.width;
		y2 = sprite.sprite.rect.height;
		x1 = x1 / x2;
		y1 = y1 / y2;
		x1 = 1f / x1;
		y1 = 1f / y1;
		scale = new Vector2(x1, -y1);
		mat.SetTextureScale(keyword, scale);
		mat.SetTexture(keyword, lastTex);
		int flipTrue = reverseFlip ? 0 : 1;
		int flipFalse = reverseFlip ? 1 : 0;
		trailAnim.flipU = sprite.flipX ? flipTrue : flipFalse;
		Vector2 offset = sprite.sprite.uv[0];
		mat.SetTextureOffset(keyword, offset);
		sys.Play();
		float timer = 0;
		DOTween.To(() => timer, x => timer = x, 1, time).OnComplete(()=>
		{
			main.loop = false;
		});
		//if (cachedTrailLength != trailLength)
		//{
		//	cachedTrailLength = trailLength;
		//	InitializeTrail();
		//}
		//if(trail.Length > 0)
		//	StartCoroutine(PlayTrail());
		
	}
	
	IEnumerator PlayTrail()
	{
		for(int i = 0; i < trail.Length; i++)
		{
			yield return new WaitForSeconds(trailRate);

			ParticleSystem trailPieceSys = trail[i];
			ParticleSystem.TextureSheetAnimationModule trailAnim = trailPieceSys.textureSheetAnimation;
			x1 = lastTex.width;
			y1 = lastTex.height;
			x2 = sprite.sprite.rect.width;
			y2 = sprite.sprite.rect.height;
			x1 = x1 / x2;
			y1 = y1 / y2;
			x1 = 1f / x1;
			y1 = 1f / y1;
			scale = new Vector2(x1, -y1);
			trailMats[i].SetTextureScale(keyword, scale);
			trailMats[i].SetTexture(keyword, lastTex);
			int flipTrue = reverseFlip ? 0 : 1;
			int flipFalse = reverseFlip ? 1 : 0;
			trailAnim.flipU = sprite.flipX ? flipTrue : flipFalse;
			Vector2 offset = sprite.sprite.uv[0];
			trailMats[i].SetTextureOffset(keyword, offset);
			trail[i].Emit(trailDensity);

		}
	}
	void InitializeTrail()
	{
		trail = new ParticleSystem[trailLength];
		trailMats = new Material[trailLength];
		if(transform.childCount > 1)
		{
			for(int i = 1; i < transform.childCount; i++)
			{
				DestroyImmediate(transform.GetChild(i).gameObject);
			}
		}
		for(int i = 0; i < trailLength; i++)
		{
			//make new trail piece and parent it to the head
			GameObject trailPiece = Instantiate(sys.gameObject);
			trailPiece.name = "TrailPiece(" + i.ToString() + ")";
			trailPiece.transform.SetParent(transform);
			trailPiece.transform.localPosition = Vector3.zero;
			//CopyComponent<ParticleSystem>(sys, trailPiece);
			ParticleSystem trailPieceSys = trailPiece.GetComponent<ParticleSystem>();
			ParticleSystem.MainModule trailPieceMain = trailPieceSys.main;
			
			ParticleSystem.TextureSheetAnimationModule trailAnim = trailPieceSys.textureSheetAnimation;
			trail[i] = trailPiece.GetComponent<ParticleSystem>();
			trailMats[i] = new Material(sys.GetComponent<Renderer>().material);
			trailPieceSys.GetComponent<Renderer>().material = trailMats[i];
			trailPieceMain.loop = false;

			x1 = lastTex.width;
			y1 = lastTex.height;
			x2 = sprite.sprite.rect.width;
			y2 = sprite.sprite.rect.height;
			x1 = x1 / x2;
			y1 = y1 / y2;
			x1 = 1f / x1;
			y1 = 1f / y1;
			scale = new Vector2(x1, -y1);
			trailMats[i].SetTextureScale(keyword, scale);
			trailMats[i].SetTexture(keyword, lastTex);
			int flipTrue = reverseFlip ? 0 : 1;
			int flipFalse = reverseFlip ? 1 : 0;
			trailAnim.flipU = sprite.flipX ? flipTrue : flipFalse;
			trailAnim.flipV = 1;
			Vector2 offset = sprite.sprite.uv[0];
			trailMats[i].SetTextureOffset(keyword, offset);
		}
	}
	//T CopyComponent<T>(T original, GameObject destination) where T : Component
	//{
	//	System.Type type = original.GetType();
	//	var dst = destination.GetComponent(type) as T;
	//	if(!dst)
	//		dst = destination.AddComponent(type) as T;
	//	var fields = type.GetFields();
	//	foreach(var field in fields)
	//	{
	//		if(field.IsStatic)
	//			continue;
	//		field.SetValue(dst, field.GetValue(original));
	//	}
	//	var props = type.GetProperties();
	//	foreach(var prop in props)
	//	{
	//		if((!prop.CanWrite || !prop.CanWrite || prop.Name == "name"))
	//			continue;
	//		Debug.Log("Setting:" + prop.Name);
	//		if(prop.Name != "randomSeed" && prop.Name != "useAutoRandomSeed")
	//			prop.SetValue(dst, prop.GetValue(original, null), null);
	//	}
	//	return dst as T;
	//}
	
	private void OnDrawGizmosSelected()
	{
		if(testMat)
		{
			if(sprite == null)
			sprite = transform.GetComponentInParent<SpriteRenderer>();
			if(sys == null)
				sys = transform.GetComponent<ParticleSystem>();

			anim = sys.textureSheetAnimation;

			
			mat = sys.GetComponent<Renderer>().sharedMaterial;
			lastTex = sprite.sprite.texture;
			mat.shader.hideFlags = HideFlags.None;

			GetComponent<ParticleSystemRenderer>().material = mat;
			if(lastTex != sprite.sprite.texture)
			{
				lastTex = sprite.sprite.texture;
			}
			x1 = lastTex.width;
			y1 = lastTex.height;
			x2 = sprite.sprite.rect.width;
			y2 = sprite.sprite.rect.height;
			x1 = x1 / x2;
			y1 = y1 / y2;
			x1 = 1f / x1;
			y1 = 1f / y1;
			//I found out that the texture for the mat does not become visible unless i use a new vector2 instead of a cached one with the same set values
			//this works
			//Vector2 scale = new Vector2(x1, -y1);
			//this also works
			scale = new Vector2(x1, -y1);
			//this does not work
			//scale.x = x1;
			//this does not work
			//scale.y = y1;
			mat.SetTextureScale(keyword, scale);
			mat.SetTexture(keyword, lastTex);
			
			int flipTrue = reverseFlip ? 0 : 1;
			int flipFalse = reverseFlip ? 1 : 0;
			anim.flipU = sprite.flipX ? flipTrue : flipFalse;
			Vector2 offset = sprite.sprite.uv[0];
			mat.SetTextureOffset(keyword, offset);
		}
	}
}
