﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerData
{
	public string version = "";
	public List<SlimeData> slimeData = new List<SlimeData>();
	public List<PoopData> slimePoop = new List<PoopData>();
	public List<FoodData> slimeFood = new List<FoodData>();
	public List<FoodData> slimeFoodStorage = new List<FoodData>();
	public List<DecoData> playPenDeco = new List<DecoData>();
	public List<int> playPenDecoPurchased = new List<int>();
	public List<int> playPenMaps = new List<int>();

	public int slimeLeader = 0;
	public int[] slimeParty = new int[] { };
	public int slimePartyMax = 2;
	public int coinCount = 50;
	public float coinMobBonusMulti = 1f;
	public int playPenMapIndex = 0;


	public int combatMapIndex = 0;
	public int combatMapHighest = 0;
	public PlayerData()
	{
		slimeData = new List<SlimeData>();
		slimePoop = new List<PoopData>();
		slimeFood = new List<FoodData>();
		slimeFoodStorage = new List<FoodData>();
		playPenDeco = new List<DecoData>();
		playPenDecoPurchased = new List<int>();
		playPenMaps = new List<int>();
		//coinCount = 99999;

	}
}
[System.Serializable]
public class PoopData
{
	public int pooID = 0;
	public int pooType = 0;
	public float x = 0;
	public float y = 0;
	public float z = 0;
	public string description = string.Empty;

	public PoopData(int _ID, int type, float posX, float posY, float posZ)
	{
		pooID = _ID;
		pooType = type;
		x = posX;
		y = posY;
		z = posZ;
	}
	public PoopData()
	{
		pooID = 0;
		pooType = 0;
	}
}
[System.Serializable]
public class FoodData
{
	public string name = string.Empty;
	public int instanceID;
	public int inStorage = 0;
	public float x = 0;
	public float y = 0;
	public float z = 0;
	public int index = 0;
	public int biteCount = 0;
	public int healthPerBite = 1;
	public int hungerPerBite = 2;
	public int goldCost = 5;
	public string description = string.Empty;

	public FoodData(int _ID, float posX, float posY, float posZ,
		int _foodIndex, int _biteCount, int hpBite, int hgBite, int gdCost, string desc)
	{
		instanceID = _ID;
		x = posX;
		y = posY;
		z = posZ;
		index = _foodIndex;
		biteCount = _biteCount;
		healthPerBite = hpBite;
		hungerPerBite = hgBite;
		goldCost = gdCost;
		description = desc;
	}
	public FoodData()
	{
		instanceID = -1;
	}

}
[System.Serializable]
public class DecoData
{
	public string name = string.Empty;
	public int decoID = 0;
	public int decoValue = 100;
	public float x = 0;
	public float y = 0;
	public float z = 0;
	public float angle = 0;
	public int goldCost = 5;
	public string description = string.Empty;
	public DecoData(int _ID, string newName, int value, float posX, float posY, float posZ, float rot)
	{
		decoID = _ID;
		name = newName;
		decoValue = value;
		x = posX;
		y = posY;
		z = posZ;
		angle = rot;
	}
}
[System.Serializable]
public class UpgradeData
{
	public enum EffectType
	{
		ADD_STR = 0,
		ADD_MAG = 1,
		ADD_AGI = 2,
		ADD_SPR = 3,
		ADD_DEF = 4,
		ADD_MAXHP = 5
	}
	public string name = string.Empty;
	public int upgradeID = 0;
	public EffectType upgradeEffect;
	public float upgradeValue = 0.1f;
	public int happinessValue = 1;
	public int hungerValue = 1;
	public int goldCost = 10;
	public string description = string.Empty;
	public UpgradeData(string newName, int id, int gold, float power, int happy, int hunger)
	{
		name = newName;
		upgradeID = id;
		upgradeValue = power;
		happinessValue = happy;
		hungerValue = hunger;
		goldCost = gold;
	}
}