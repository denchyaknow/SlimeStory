﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
[RequireComponent(typeof(BoxCollider2D))]
public class MapTrigger : MonoBehaviour {
	public LayerMask whatIsHitbox;
	public UnityEvent onTriggered;
	public virtual void OnEnable()
	{
		transform.GetComponent<BoxCollider2D>().size = new Vector2(5, 10);
		transform.GetComponent<BoxCollider2D>().isTrigger = true;

	}
	public virtual void OnDisable()
	{
		onTriggered.RemoveAllListeners();
	}
	public virtual void Start () {
		
	}
	
	// Update is called once per frame
	public virtual void Update () {
		
	}
	
	public virtual void OnTriggerEnter2D(Collider2D collision)
	{
		//Debug.Log("Trigger hit by " + collision.gameObject.name);
	}
	public virtual void OnTriggerStay2D(Collider2D collision)
	{

	}
	public virtual void OnTriggerExit2D(Collider2D collision)
	{

	}
	public virtual void OnDrawGizmosSelected()
	{
		transform.GetComponent<BoxCollider2D>().size = new Vector2(5, 10);
		transform.GetComponent<BoxCollider2D>().isTrigger = true;
	}
}
