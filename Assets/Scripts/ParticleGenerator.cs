﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleGenerator : MonoBehaviour {

	//public int TextureOverride = -1;
	public bool UseAllTextures;

	[HideInInspector] public int NumOfPartcleSystems = 1;
	public Transform ParentSamples;
	public Transform ParentGenerated;
	public Shader[] Shaders;
	//[HideInInspector] public bool ShowTextureSelection = false;
	//[HideInInspector] public int GridPreviewCount = 10;
	[HideInInspector] public ParticleSystem[] SampledSystems;
	[HideInInspector]public Texture2D[] Textures;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}

