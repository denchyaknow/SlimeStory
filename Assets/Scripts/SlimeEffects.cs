﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class SlimeEffects : MonoBehaviour {
	[HideInInspector] public Transform onAttackParent;
	[HideInInspector] public Transform onWalkParent;
	[HideInInspector] public Transform onPetParent;
	[HideInInspector] public Transform onHitParent;
	[HideInInspector] public Transform onDeathParent;
	[HideInInspector] public Transform onSpawnParent;
	[HideInInspector] public Transform onNoClipParent;
	private ParticleSystem[] onAttackParticles;
	private TrailModule onAttackTrail;
	private ParticleSystem[] onWalkParticles;
	private ParticleSystem[] onPetParticles;
	private ParticleSystem[] onHitParticles;
	private ParticleSystem[] onDeathParticles;
	private ParticleSystem[] onSpawnParticles;
	private ParticleSystem[] onNoClipParticles;

	private BaseSlime attachedSlime;

	[Header("Petting Tween")]
	public float onPetScaleTime = 0.5f;
	public float onPetElasticity = 1f;
	public int onPetVibration = 10;
	public Vector3 onPetScaleVector = new Vector3(1, 1, 1);
	public Ease onPetScaleEase = Ease.InOutExpo;

	[Header("Hit Tween")]
	public float onHitScaleTime = 0.2f;
	public float onHitElasticity = 2f;
	public int onHitVibration = 2;
	public Vector3 onHitScaleVector = new Vector3(2, 2, 0);
	public Ease onHitScaleEase = Ease.Linear;

	private void Awake()
	{
		if(onAttackParent != null)
		{
			onAttackParticles = onAttackParent.GetComponentsInChildren<ParticleSystem>();
		}
		if(onWalkParent != null)
		{
			onWalkParticles = onWalkParent.GetComponentsInChildren<ParticleSystem>();
		}
		if(onHitParent != null)
		{
			onHitParticles = onHitParent.GetComponentsInChildren<ParticleSystem>();
		}
		if(onDeathParent != null)
		{
			onDeathParticles = onDeathParent.GetComponentsInChildren<ParticleSystem>();
		}
		if(onSpawnParent != null)
		{
			onSpawnParticles = onSpawnParent.GetComponentsInChildren<ParticleSystem>();
		}

	}
	// Use this for initialization
	void Start()
	{
		if(attachedSlime == null)
		{
			attachedSlime = transform.GetComponentInParent<BaseSlime>();
			attachedSlime.myEffects = this;
			attachedSlime.OnAttack += PlayAttack;
			attachedSlime.OnWalk += PlayWalk;
			attachedSlime.OnPet += PlayPet;
			attachedSlime.OnHit += PlayHit;
			attachedSlime.OnDeath += PlayDeath;
			attachedSlime.OnSpawn += PlaySpawn;
		}
	}

	// Update is called once per frame
	void Update()
	{

	}
	public void PlayAttack()
	{
		if(onAttackParticles == null || onAttackParticles.Length == 0)
			return;
		for(int i = 0; i < onAttackParticles.Length; i++)
		{
			if (!onAttackParticles[i].transform.GetComponent<TrailModule>())
				onAttackParticles[i].Play(true);
		}
		if(onAttackTrail == null && onAttackParent.GetComponentInChildren<TrailModule>())
			onAttackTrail = onAttackParent.GetComponentInChildren<TrailModule>();
		if(onAttackTrail != null)
			onAttackTrail.Play(0.6f);
	}
	public void PlayWalk()
	{
		if(onWalkParticles == null || onWalkParticles.Length == 0)
			return;
		for(int i = 0; i < onWalkParticles.Length; i++)
		{
			onWalkParticles[i].Play(true);
		}
	}
	public void PlayPet()
	{
		if(onPetParticles == null || onPetParticles.Length == 0)
			return;
		for(int i = 0; i < onPetParticles.Length; i++)
		{
			onPetParticles[i].Play(true);
		}
		DOTween.Kill(gameObject.GetInstanceID().ToString() + "onPet");
		attachedSlime.myRenderer.transform.DOPunchScale(onPetScaleVector, onPetScaleTime, 10, 1).SetEase(onPetScaleEase).SetId(gameObject.GetInstanceID().ToString() + "onPet").OnComplete(()=>
		{
			attachedSlime.myRenderer.transform.localScale = new Vector3(1, 1, 1);
		});
	}
	public void PlayHit()
	{
		if(onHitParticles == null || onHitParticles.Length == 0)
			return;
		for(int i = 0; i < onHitParticles.Length; i++)
		{
			onHitParticles[i].Play(true);
		}
		DOTween.Kill(gameObject.GetInstanceID().ToString() + "onHit");
		attachedSlime.myRenderer.transform.DOPunchScale(onHitScaleVector, onHitScaleTime, onHitVibration, onPetElasticity).SetEase(onHitScaleEase).SetId(gameObject.GetInstanceID().ToString() + "onHit").OnComplete(()=>
		{
			attachedSlime.myRenderer.transform.localScale = new Vector3(1, 1, 1);
		});
	}

	public void PlayDeath()
	{
		if(onDeathParticles == null || onDeathParticles.Length == 0)
			return;
		for(int i = 0; i < onDeathParticles.Length; i++)
		{
			onDeathParticles[i].Play(true);
		}
	}

	public void PlaySpawn()
	{
		if(onSpawnParticles == null || onSpawnParticles.Length == 0)
			return;
		for(int i = 0; i < onSpawnParticles.Length; i++)
		{
			onSpawnParticles[i].Play(true);
		}
	}
	public void PlayNoClip(float time)
	{
		if (onNoClipParticles == null || onNoClipParticles.Length == 0)
			return;
		for (int i = 0; i < onNoClipParticles.Length; i++)
		{
			ParticleSystem.MainModule main = onNoClipParticles[i].main;
			main.loop = true;
			onNoClipParticles[i].Play();
		}
		float timer = 0;
		DOTween.To(() => timer, x => timer = x, 1, time / 2).OnComplete(() =>
					 {
						 for (int i = 0; i < onNoClipParticles.Length; i++)
						 {
							 ParticleSystem.MainModule main = onNoClipParticles[i].main;
							 main.loop = false;
						 }
					 });
	}
}
