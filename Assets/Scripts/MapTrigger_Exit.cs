﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapTrigger_Exit : MapTrigger {

	public enum ExitType
	{
		ToLeftMap,
		ToRightMap,
		ToPlaypen
	}
	public ExitType exitType;
	private float timer = 2;
	private bool timerTicking = false;
	private bool eventTriggered = false;
	public override void OnEnable()
	{
		base.OnEnable();
	}
	public override void OnDisable()
	{
		base.OnDisable();
	}
	public override void Start () {
		base.Start();
	}

	public override void Update () {
		base.Update();
		if(timerTicking)
		{
			timer -= Time.deltaTime;
		} else
		{
			timer = 2;
		}
	}
	public void ExitMap()
	{
		switch(exitType)
		{
			case ExitType.ToLeftMap:

				break;
			case ExitType.ToRightMap:
				//if(GameManager._Instance.combatManager != null)
					//GameManager._Instance.combatManager.PlayerGoToEndNode();

				break;
			case ExitType.ToPlaypen:

				break;
		}
	}
	public override void OnTriggerEnter2D(Collider2D collision)
	{
		base.OnTriggerEnter2D(collision);
		timerTicking = true;
	}
	public override void OnTriggerStay2D(Collider2D collision)
	{
		base.OnTriggerStay2D(collision);
		if(timer < 0 && !eventTriggered)
		{
			eventTriggered = true;
			//string layerName = LayerMask.LayerToName(collision.gameObject.layer);
			//Debug.Log("Exit Layer:" + collision.gameObject.layer + " / " + LayerMask.LayerToName(collision.gameObject.layer) + " / " + LayerMask.NameToLayer(layerName));
			//Debug.Log("Exit Map triggered by:" + collision.gameObject.name + "/" + collision.transform.parent.parent.gameObject.name + " / " + collision.transform.root.gameObject.name);
			//ExitMap();
		}
	}
	public override void OnTriggerExit2D(Collider2D collision)
	{
		base.OnTriggerEnter2D(collision);
		timerTicking = false;
	}
}
