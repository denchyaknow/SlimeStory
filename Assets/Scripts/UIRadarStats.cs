﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI.Extensions;
using DG.Tweening;
public class UIRadarStats : MonoBehaviour
{
	private int sides = 5;
	private float rot;
	private float[] values = new float[6];
	private float[] statArray = new float[6];
	public Ease statEase;
	public float time = 1;
	public float delay = 1;
	public bool test = false;
	private UIPolygon poly;
	public delegate void Delegate();
	public Delegate OnResetComplete;
	public bool update = false;
	public float updateRate = 1f;
	public int[] currentStats;
	private int[] cachedStats;
	private List<int> quedStats = new List<int>();
	private float lastUpdate;
	private void OnEnable()
	{
		lastUpdate = Time.timeSinceLevelLoad;
	}
	private void OnDisable()
	{
		HideStats();
		OnResetComplete = null;
	}
	void Awake()
	{
		poly = transform.GetComponent<UIPolygon>();
	}
	private void LateUpdate()
	{
		if (test)
		{
			test = false;
			ShowStats(GameManager._Instance.GetSelectedSlime().slimeStats);
		}
		if (Time.timeSinceLevelLoad < lastUpdate || !update)
			return;
		lastUpdate = Time.timeSinceLevelLoad + updateRate;

		if (currentStats != null && currentStats.Length > 0)
		{
			if (cachedStats == null || cachedStats.Length != currentStats.Length)
			{//When we first intialize the stats array or if the stats array length changes, restructure the cache
				cachedStats = new int[currentStats.Length];
				for (int i = 0; i < currentStats.Length; i++)
				{
					int index = i;
					cachedStats[i] = currentStats[i];
					quedStats.Add(index);
				}
			}
			else
			{//Otherwise lets check if render updates are needed for stat values and take note of which stats need to update (for fancy animations and what not)
				for (int i = 0; i < currentStats.Length; i++)
				{
					if (cachedStats[i] != currentStats[i])
					{
						int index = i;
						cachedStats[i] = currentStats[i];
						quedStats.Add(index);
					}
				}
			}
			if (quedStats.Count > 0)
				UpdateStats();

		}

	}
	public void ClearStats()
	{
		for (int i = 0; i < currentStats.Length; i++)
		{
			currentStats[i] = 1;
			cachedStats[i] = 1;
			quedStats.Add(i);
		}
		UpdateStats();
	}
	public void UpdateStats()
	{
		DOTween.Kill(this);
		sides = currentStats.Length;
		rot = poly.rotation;
		if (statArray.Length != sides + 1)
		{
			statArray = new float[sides + 1];
			for (int i = 0; i < statArray.Length; i++)
			{
				statArray[i] = 0.1f;
			}
		}
		else
		{
			float[] tempArray = new float[statArray.Length];
			for (int i = 0; i < statArray.Length; i++)
				tempArray[i] = statArray[i];//This code is cancer :(
			statArray = new float[sides + 1];
			for (int i = 0; i < statArray.Length; i++)
			{
				if (i < tempArray.Length)
					statArray[i] = tempArray[i];
				else
					statArray[i] = 0.1f;
			}
		}
		poly.DrawPolygon(sides, statArray, rot);
		poly.SetAllDirty();
		while (quedStats.Count > 0)
		{
			ChangeValue(quedStats[0]);
			quedStats.Remove(quedStats[0]);
		}
	
	}
	public void ChangeValue(int _index)
	{
		int index = _index;
		float oldValue = statArray[index];
		float newValue = (float)(currentStats[index] + 51) / 306f;
		DOTween.To(() => oldValue, x => oldValue = x, newValue, time)
			.SetId(this)
			.SetDelay(delay * quedStats.Count)
			.SetEase(statEase)
			.SetUpdate(UpdateType.Late)
			.OnUpdate(() =>
			{
				statArray[index] = oldValue;
				
				poly.DrawPolygon(sides, statArray, rot);
				poly.SetAllDirty();
				
			}).OnStart(()=>
			{
				
			}).OnComplete(()=>
			{
				statArray[index] = newValue;
				
					poly.DrawPolygon(sides, statArray, rot);
					poly.SetAllDirty();
				
			});//Damn this is sexy
	}
	public void HideStats()
	{
		rot = poly.rotation;
		if (statArray.Length != sides + 1)
		{

			statArray = new float[sides + 1];
			for (int i = 0; i < statArray.Length; i++)
			{
				statArray[i] = 0.1f;
			}
		}
		else
		{
			for (int i = 0; i < statArray.Length; i++)
			{
				statArray[i] = 0.1f;
			}
		}
		poly.DrawPolygon(sides, statArray, rot);
		poly.SetAllDirty();
	}
	public void ResetStats()
	{
		DOTween.Kill(this);
		sides = poly.sides;
		rot = poly.rotation;
		values = new float[sides];
		statArray = new float[sides + 1];
		for(int i = 0; i < values.Length; i++)
		{
			statArray[i] = values[i];
		}
		for (int i = 0; i < values.Length; i++)
		{
			values[i] = 0.1f;
		}
		for (int j = 0; j < values.Length; j++)
		{
			int index = j;
			if(index == (values.Length -1))
			{
				DOTween.To(() => statArray[index], x => statArray[index] = x, values[index], time / 5)
					.SetId(this)
					.SetDelay((delay / 5) * (values.Length-index))
					.SetEase(statEase)
					.SetUpdate(UpdateType.Late)
					.OnUpdate(() =>
					{
						poly.DrawPolygon(sides, statArray, rot);
						poly.SetAllDirty();
					})
					.OnComplete(()=> 
					{
						if(OnResetComplete != null)
							OnResetComplete();
						poly.DrawPolygon(sides, statArray, rot);
						poly.SetAllDirty();
					});
			} else
			{
				DOTween.To(() => statArray[index], x => statArray[index] = x, values[index], time / 5)
					.SetId(this)
					.SetDelay((delay / 5) * (values.Length - index))
					.SetEase(statEase);
			}
		}
		poly.DrawPolygon(sides, statArray, rot);
		poly.SetAllDirty();
	}
	public void ShowStats(SlimeData _stats)
	{
		sides = poly.sides;
		rot = poly.rotation;
		if (statArray.Length != sides + 1)
		{
			statArray = new float[sides + 1];
			for (int i = 0; i < statArray.Length; i++)
			{
				statArray[i] = 0.1f;
			}
		}
		else
		{
			for (int i = 0; i < statArray.Length; i++)
			{
				statArray[i] = 0.1f;
			}
		}
		poly.DrawPolygon(sides, statArray, rot);
		poly.SetAllDirty();
		values = new float[sides];
		values[0] = (float)(_stats.defense + _stats.defenseAddative + 51) / (306f);
		values[1] = (float)(_stats.strength + _stats.strengthAddative + 51) / (306f);
		values[2] = (float)(_stats.agility + _stats.agilityAddative + 51) / (306f);
		values[3] = (float)(_stats.spirit + _stats.spiritAddative + 51) / (306f);
		values[4] = (float)(_stats.magic + _stats.magicAddative + 51) / (306f);
		DOTween.To(() => statArray[4], x => statArray[4] = x, values[4], time / 5).SetId(this).SetDelay((delay / 5) * 3f).SetEase(statEase).SetUpdate(UpdateType.Late).OnUpdate(() =>
		 {
			 poly.DrawPolygon(sides, statArray, rot);
			 poly.SetAllDirty();
		 });
		DOTween.To(() => statArray[3], x => statArray[3] = x, values[3], time / 5).SetId(this).SetDelay((delay / 5) * 4.5f).SetEase(statEase).SetUpdate(UpdateType.Late).OnUpdate(() =>
		{
			poly.DrawPolygon(sides, statArray, rot);
			poly.SetAllDirty();
		});
		DOTween.To(() => statArray[2], x => statArray[2] = x, values[2], time / 5).SetId(this).SetDelay((delay / 5) * 2.25f).SetEase(statEase).SetUpdate(UpdateType.Late).OnUpdate(() =>
		{
			poly.DrawPolygon(sides, statArray, rot);
			poly.SetAllDirty();
		});
		DOTween.To(() => statArray[1], x => statArray[1] = x, values[1], time / 5).SetId(this).SetDelay((delay / 5) * 3.75f).SetEase(statEase).SetUpdate(UpdateType.Late).OnUpdate(() =>
		{
			poly.DrawPolygon(sides, statArray, rot);
			poly.SetAllDirty();
		});
		DOTween.To(() => statArray[0], x => statArray[0] = x, values[0], time / 5).SetId(this).SetDelay((delay / 5) * 1.5f).SetEase(statEase).SetUpdate(UpdateType.Late).OnUpdate(() =>
		{
			poly.DrawPolygon(sides, statArray, rot);
			poly.SetAllDirty();
		});
	}
}
