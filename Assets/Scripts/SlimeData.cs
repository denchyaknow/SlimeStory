﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class SlimeData
{

	[HideInInspector] public float penPosX = 0;
	[HideInInspector] public float penPosY = 0;
	public string name;
	public int classType = 0;
	public int goldCost = 10;
	public string description = string.Empty;
	public int level = 1;

	public int currentExp = 0;
	public int maxExp = 100;
	public int currentHealth = 100;
	public int maxHealth = 100;
	public int maxHealthAddative = 100;
	public int currentHappiness = 50;
	public int maxHappiness = 100;
	public int currentHunger = 0;
	public int maxHunger = 100;
	public int index = 0;
	public int spriteSheetIndex = 0;
	public int currentCoin = 5;
	public float currentScale = 1;
	[Header("ClassGrowth")]//0 = str, 1 = mag, 2 = agi, 3 = spr, 4 = def
	public int[] growth = new int[5] { 4, 4, 4, 4, 4 };//0-S, 1-A, 2-B, 3-C, 4-D
	[Header("ClassStats")]
	public int strength = 1;
	public int strengthAddative = 0;
	public int magic = 1;
	public int magicAddative = 0;
	public int agility = 1;
	public int agilityAddative = 0;
	public int spirit = 1;
	public int spiritAddative = 0;
	public int defense = 1;
	public int defenseAddative = 0;

	[Header("ClassParameters")]
	public int damage = 1;
	public int attackType = 0;
	public float attackCD = 0.2f;
	public float attackAutoCD = 0.7f;
	public float attackDelay = 0.6f;
	[Tooltip("Used in BaseSlime.ProcessAttack() Invoked via delegate OnAttack")]
	public float attackDuration = 0.15f;
	public float attackDistance = 5;
	public float attackRadius = 0.8f;
	public int attackHitCount = 1;
	public float attackHitRate = 0.1f;
	public float attackCost = 0.25f;
	public float moveDistance = 5f;
	public float moveCD = 0.5f;

	public int GetHealth()
	{
		int totalDef = defense + defenseAddative;
		int hp = baseHealth + totalDef;
		for (int i = 0; i < level; ++i)
		{
			hp += GameManager._Instance.ScaledHP(baseHealth + totalDef, lvlHealthRate, lvlHealthFlat, i);
		}
		hp += maxHealthAddative;
		return hp;

	}
	public float GetHealthPercent()
	{
		float healthPercent = (float)currentHealth / (float)(GetHealth());
		return healthPercent;
	}
	public float GetHappinessPercent()
	{
		float happyPercent = (float)currentHappiness / (float)maxHappiness;
		return happyPercent;
	}
	public float GetHungerPercent()
	{
		float hungerPercent = (float)currentHunger / (float)maxHunger;
		return hungerPercent;
	}
	public int GetStrength()
	{
		return strength + strengthAddative;
	}
	public int GetMagic()
	{
		return magic + magicAddative;
	}
	public int GetAgility()
	{
		return agility + agilityAddative;
	}
	public float GetAttackCDReduction()
	{
		float totalReduc = 0;
		float bonusReduc = 0;
		int baseReduc = agility + agilityAddative;
		int softCappedReduc = 0;
		if (baseReduc > 150)
		{
			softCappedReduc = baseReduc - 150;
			bonusReduc = (float)softCappedReduc / 420f;
			bonusReduc = bonusReduc / 4f;

		}
		totalReduc = baseReduc / 420f;
		totalReduc += bonusReduc;
		totalReduc = attackCD * totalReduc;

		return totalReduc;
	}
	public int GetSpirit()
	{
		return spirit + spiritAddative;
	}
	public int GetDefense()
	{
		return defense + defenseAddative;
	}
	public virtual float GetDamageReduction()
	{
		float totalReduc = 0;
		float bonusReduc = 0;
		int baseReduc = defense + defenseAddative;
		int softCappedReduc = 0;
		if (baseReduc > 150)
		{
			softCappedReduc = baseReduc - 150;
			bonusReduc = (float)softCappedReduc / 420F;
			bonusReduc = bonusReduc / 4f;
		}
		totalReduc = baseReduc / 420f;
		totalReduc += bonusReduc;

		return totalReduc;
	}
	public int GetDamage()
	{

		int totalStr = strength + strengthAddative;
		int totalMag = magic + magicAddative;
		int totalAdd = attackType == 0 ? totalStr + (totalMag / 4) : totalMag + (totalStr / 4);
		int damage = baseDamage + totalAdd;
		for (int i = 0; i < level; ++i)
		{
			damage += (GameManager._Instance.ScaledDamage(baseDamage + totalAdd, lvlDamageRate, lvlDamageFlat, i));
		}
		return damage;

	}
	public int GetExperience()
	{
		int exp = baseExp + strength + magic + agility + spirit + defense;
		for (int i = 0; i < level; ++i)
		{
			exp += GameManager._Instance.ScaledEXP(baseExp + strength + magic + agility + spirit + defense, lvlExpRate, lvlExpFlat, i);
		}
		return exp;
	}
	public int GetCoin()
	{
		int coin = 0;
		for (int i = 0; i < GameManager._Instance.playerData.combatMapIndex; ++i)
		{
			coin += GameManager._Instance.ScaledCoin(currentHealth, lvlCoinRate, lvlCoinFlat, i, 1f);
		}
		return coin;
	}
	public float GetScale()
	{
		float scale = 1;
		for (int i = 0; i < level; ++i)
		{
			scale += 0.01f * GameManager._Instance.ScaledTransform(i, baseScale, lvlScaleRate, lvlScaleFlat);
		}
		if (scale > 4f)
			scale = 4f;
		return scale;
	}
	[Header("BaseStats")]
	public int baseHealth = 50;
	public int baseDamage = 5;
	public int baseExp = 100;
	public float baseScale = 1f;

	public float lvlHealthRate = 1.123f;
	public float lvlHealthFlat = 1.003f;
	public float lvlExpRate = 10f;
	public float lvlExpFlat = 50f;
	public float lvlCoinRate = 0.008f;
	public float lvlCoinFlat = 0.0002f;
	public float lvlDamageRate = 1.11234f;
	public float lvlDamageFlat = 1.003f;

	public float lvlScaleRate = 0.08f;
	public float lvlScaleFlat = 0.08f;

	public List<UpgradeData> upgrades = new List<UpgradeData>();
	public SlimeData(string _name, int _level,
		int _currentExp, int _maxExp,
		int _currentHealth, int _maxHealth,
		int _currentHappiness, int _maxHappiness,
		int _currentHunger, int _maxHunger,
		//, int _currentPoop, int _maxPoop,
		int _index
		, float attCD, float attAutoCD)
	{
		level = _level;
		currentExp = _currentExp;
		maxExp = _maxExp;
		currentHealth = _currentHealth;
		maxHealth = _maxHealth;
		currentHappiness = _currentHappiness;
		maxHappiness = _maxHappiness;
		currentHunger = _currentHunger;
		maxHunger = _maxHunger;
		//currentPoop = _currentPoop;
		//maxPoop = _maxPoop;
		index = _index;
		growth = new int[5] { 4, 4, 4, 4, 4 };

		attackCD = attCD;
		attackAutoCD = attAutoCD;
	}
	public SlimeData()
	{
		level = 1;
		index = -1;
		growth = new int[5] { 4, 4, 4, 4, 4 };
	}
}