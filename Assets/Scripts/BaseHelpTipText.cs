﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BaseHelpTipTrigger))]
public class BaseHelpTipText : MonoBehaviour {

	public string helpText = string.Empty;
	public int lines = 0;
	[Header("Slime Stats")]
	public bool showSlimeData = false;
	public SlimeData slimeData
	{
		get
		{
			return sD;
		}
		set
		{
			sD = value;
		}
	}
	private SlimeData sD = null;
	public FoodData foodData
	{
		get
		{
			return fD;
		}
		set
		{
			fD = value;
		}
	}
	private FoodData fD = null;
	public DecoData decoData
	{
		get
		{
			return dD;
		}
		set
		{
			dD = value;
		}
	}
	private DecoData dD = null;
	public UpgradeData upgradeData
	{
		get
		{
			return uD;
		}
		set
		{
			uD = value;
		}
	}
	private UpgradeData uD = null;
	
}
