﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Shoot : MonoBehaviour
{
 
	public float velocity = 10f;
    public float drag = 0.01f;
	public Vector3 radius = new Vector3(1, 1, 1);
	public int numOfParticles = 10;
	public float gizmoRadius = 0.2f;
	[SerializeField]
	private ParticleSystem particleSystem;
	private ParticleSystem.Particle[] particles = new ParticleSystem.Particle[0];
    private readonly Vector3 _gravity = Vector3.down * 10;
	private Vector3 lastHitPoint;
	private Vector3[] hitPoints;
	void Start ()
	{
        particleSystem = transform.GetComponentInChildren<ParticleSystem>();
	}
	void Update ()
	{
        bool shoot = false;
		if(Input.GetMouseButtonDown(0))
		{
            shoot = ShootParticleSplash(Input.mousePosition, radius, velocity, numOfParticles);
        }
		Debug.Log(shoot?"Hit":"Miss");
	}
    void LateUpdate() 
    {
        int alive = particleSystem.GetParticles(particles);
        if (alive > 0)
        {
            for (int i = 0; i < alive; ++i)
            {
                Vector3 angle = (hitPoints[i] - particleSystem.transform.position).normalized;
                Vector3 vel = angle * velocity;
                vel += _gravity * Time.deltaTime;
                vel -= vel.sqrMagnitude * (vel.normalized * drag);
                particles[i].position += vel * Time.deltaTime;
            }
            particleSystem.SetParticles(particles, alive);
        }
    }
	public bool ShootParticleSplash(Vector3 pos, Vector3 radius, float velocity, int numParticles)
	{
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(pos);
        if (Physics.Raycast(ray, out hit))
        {
            lastHitPoint = hit.point;
			particleSystem.Clear();
			particleSystem.time = 0;
			particleSystem.Play();
			if (particles == null || particles.Length < numParticles)
                particles = new ParticleSystem.Particle[numParticles];
           else
               particleSystem.GetParticles(particles);
            hitPoints = new Vector3[numOfParticles];
            for (int i =  0; i <  particles.Length; ++i)
            {
                Vector3 dir;
                Vector2 randomPoint = Random.insideUnitCircle * radius.magnitude;
                Vector3 mrRandom = lastHitPoint + hit.transform.TransformDirection(new Vector3(randomPoint.x, 0, randomPoint.y ));
                hitPoints[i] = mrRandom;
                dir = (hitPoints[i] - transform.position).normalized;
                float life = particleSystem.main.startLifetime.constant;
                particles[i].remainingLifetime = life;
                particles[i].position = Vector3.zero;
            }
            particleSystem.SetParticles(particles, numOfParticles);
            //Debug.Log("Hit" + hit.transform.gameObject.name + hit.transform.position);
            return true;
        }
		return false;
	}
	private void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(lastHitPoint, gizmoRadius);
		Gizmos.color = Color.yellow;
		Gizmos.DrawWireSphere(lastHitPoint, radius.magnitude);
		if(hitPoints != null && hitPoints.Length > 0)
		{
			Gizmos.color = Color.cyan;
			for(int i = 0; i < hitPoints.Length; ++i)
			{
				Gizmos.DrawLine(particleSystem.transform.position, hitPoints[i]);
			}
		}
		if(particles != null && particles.Length > 0)
		{
			for(int i = 0; i < particles.Length; ++i)
			{
				Gizmos.color = Color.gray;
				Gizmos.DrawLine(particleSystem.transform.TransformPoint(particles[i].position), hitPoints[i]);
				Gizmos.DrawSphere(particleSystem.transform.TransformPoint(particles[i].position), gizmoRadius / 2);
			}
		}
	}
}
