﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class MobData
{

	public int classType = 0;
	public string description = string.Empty;

	//public int spriteSheetIndex = 0;
	public int baseHealth = 50;
	public int baseDamage = 5;
	public int baseExp = 100;
    public float baseScale = 0.5f;
    public float moveSpeed = 0.3f;
    public float moveRadius = 4f;
    public float moveCD = 5f;

	public int healthMax = 10;
	public int healthCurrent = 10;
	public int damage = 5;
	public int attackType = 0;
	public float attackDuration = 0.4f;
	public float attackDelay = 1.2f;
	public float attackCD = 2f;
	public float attackRadius = 0.2f;
	public int expValue = 1;
	public int coinValue = 1;
    public float currentScale = 1;

    public float lvlHealthRate = 1.123f;
    public float lvlHealthFlat = 1.003f;
    public float lvlExpRate = 10f;
    public float lvlExpFlat = 50f;
    public float lvlCoinRate = 5f;
    public float lvlCoinFlat = 5f;
    public float lvlDamageRate = 1.11234f;
    public float lvlDamageFlat = 1.003f;
    public float lvlScaleRate = 0.08f;
    public float lvlScaleFlat = 0.08f;
    public MobData(int hp, int exp, int coin, float scale)
	{
		healthMax = hp;
		healthCurrent = hp;
		expValue = exp;
		coinValue = coin;
        currentScale = scale;
	}
	public MobData()
	{
		healthMax = 10;
		healthCurrent = 10;
		expValue = 1;
		coinValue = 1;
	}
}
