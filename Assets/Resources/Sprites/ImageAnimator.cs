﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ImageAnimator : MonoBehaviour {
    public Sprite[] frames = new Sprite[0];
    public float frameTime = 0.2f;
    private bool animating = false;
    public bool start = false;
    public bool stop = false;
	public bool onShot = false;
	public float delay = 0;
	private float curTime = 0;
    private int frame = 0;
    private SpriteRenderer mySprite;
    private Image myImage;
	// Use this for initialization
	void Start ()
    {
        if (transform.GetComponent<Image>() != null)
        {
            myImage = transform.GetComponent < Image>();

        }
        if (transform.GetComponent<SpriteRenderer>() != null)
        {
            mySprite = transform.GetComponent<SpriteRenderer>();
        }
		curTime = Time.timeSinceLevelLoad + delay;
	}
	
	// Update is called once per frame
	void Update ()
    {
		
        if(stop)
        {
            StopAllCoroutines();
            animating = false;
            start = false;
            if (mySprite != null && mySprite.sprite != frames[frames.Length -1])
                mySprite.sprite = frames[frames.Length - 1];
            if (myImage != null && myImage.sprite != frames[frames.Length - 1])
                myImage.sprite = frames[frames.Length - 1];
        }
        if(!animating && start)
        {
			if(Time.timeSinceLevelLoad < curTime)
				return;
			animating = true;
            StartCoroutine(Animate());
        }
	}
    IEnumerator Animate()
    {
        frame++;
		if(frame > frames.Length - 1)
		{
			if(!onShot)
				frame = 0;
		}
        yield return new WaitForSeconds(frameTime);
        if (mySprite != null)
            mySprite.sprite = frames[frame];
        if(myImage != null)
            myImage.sprite = frames[frame];
		if(!onShot)
			animating = false;
		else if(frame != frames.Length -1)
			animating = false;
    }
    public void StartAnimation()
    {
        start = true;
        stop = false;
    }
    public void StopAnimation()
    {
        stop = true;
      
    }
}
