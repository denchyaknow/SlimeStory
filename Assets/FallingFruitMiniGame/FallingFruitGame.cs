﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FallingFruitGame : MonoBehaviour
{

    public Transform[] fruits;

    public int goldCount;
    float timer = 30;

    public Text timerText;
    public Text goldText;
    public Text finalGoldText;

    public float fallDelay;

    float timeToFall;
    float randomPos;

    Vector3 topRightScreen;
    Vector3 topLeftScreen;

    public Image afterGameMenuPopup;

	int randomFruitIndex;

    // Use this for initialization
    void Start() {

        afterGameMenuPopup.gameObject.SetActive(false);

        goldCount = 0;

        timerText.text = timer.ToString();

        topRightScreen = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, Camera.main.pixelHeight, 0));

        timeToFall = fallDelay;

    }

    // Update is called once per frame
    void Update() {

        randomFruitIndex = Random.Range(0, 3);

        goldText.text = goldCount.ToString();

        if (timer > 0) {
            timer -= Time.deltaTime;

            timerText.text = timer.ToString();

            randomPos = Random.Range(topRightScreen.z, topRightScreen.x);

            if (timeToFall > 0) {
                timeToFall--;
            } else {
                timeToFall = fallDelay;
                SpawnFruit();
            }
        }

        if (timer <= 0) {
            timerText.text = "0";
            goldText.text = goldCount.ToString();
            afterGameMenuPopup.gameObject.SetActive(true);
            finalGoldText.text = goldCount.ToString();
        }



    }

    void SpawnFruit() {

        Instantiate(fruits[randomFruitIndex], new Vector3(randomPos, 6, 0), Quaternion.identity);
    }

    public void PlayAgain() {
        SceneManager.LoadScene("FallingFruitGame");
    }

	public void BackToPlaypen()
	{
		SceneManager.LoadScene("Playpen");
	}





}
