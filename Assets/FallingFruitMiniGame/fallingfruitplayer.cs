﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fallingfruitplayer : MonoBehaviour {

    public float speed;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        float moveX = this.transform.position.x + Input.acceleration.x * speed * Time.deltaTime;
        if (this.gameObject.transform.position.x > -6.8f || this.gameObject.transform.position.x > 6.8f) {
            this.gameObject.transform.position = new Vector3(moveX, -4, 0);
        }

            //Debug Stuff Remove in Final
            if (Input.GetKey("a")) {
                this.gameObject.transform.position += Vector3.left;
            } else if (Input.GetKey("d")) {
                this.gameObject.transform.position += Vector3.right;
            }
        

    }
}
