﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
namespace Denchyaknow
{
	public class UniMob_Lvl : MonoBehaviour
	{
		public TextMesh lvlText
		{
			get
			{
				if(lT == null)
					lT = transform.GetComponentInChildren<TextMesh>();
				return lT;
			}
		}
		private TextMesh lT;
		private int lvl = -1;
		public UniMob_Base attachedUniMob;
		private UniMob_Base cachedMob;
		// Use this for initialization
		void Start()
		{

		}
		private void OnEnable()
		{
			lvlText.color = Color.white;
			lvlText.gameObject.SetActive(true);
			cachedMob = null;
		}
		// Update is called once per frame
		void Update()
		{
			if(cachedMob == null)
			{
				if(attachedUniMob == null)
					return;
				cachedMob = attachedUniMob;
				attachedUniMob.OnDeath += FadeOut;
			}
			if(lvl != attachedUniMob.mobLevel)
			{
				lvl = attachedUniMob.mobLevel;
				lvlText.text = "Lv. " + lvl.ToString();
			}
		}
		public void FadeOut()
		{
			float timer = lvlText.color.a;
			DOTween.To(() => timer, x => timer = x, 0, 0.8f).OnUpdate(()=>
			{
				Color col = lvlText.color;
				col.a = timer;
				lvlText.color = col;
			}).OnComplete(()=>
			{
				lvlText.gameObject.SetActive(false);
			});
		}
	}
}
