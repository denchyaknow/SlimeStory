﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
namespace Denchyaknow
{

	public class UniMob_Spawner : MonoBehaviour
	{
		public bool spawnAuto = true;//Start spawning when ready?

		public int spawnLevel = 0;//set level of the mobs spawned to this
		public int spawnCount = 1;//How many to spawn per wave
		public int spawnWaves = 1;//how many waves of spawn Count
		public Transform spawnWaypoints;//A single transform to hold many other transforms, that are waypoints
		public List<Transform> spawnedMobs = new List<Transform>();
		private bool spawnReady = false;
		public List<UniMobSpawnable> spawnables = new List<UniMobSpawnable>();
		// Use this for initialization
		void Start()
		{
			UniMob_Base[] preSpawnedMobs = GameObject.FindObjectsOfType<UniMob_Base>();

		}

		// Update is called once per frame
		void Update()
		{
			if(spawnAuto && spawnReady)
			{

			}
		}
	}

	[System.Serializable]
	public class UniMobSpawnable
	{
		public GameObject spawnable;
		public float spawnDelay = 0;
	}
}
