﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
namespace Denchyaknow
{
	[CustomEditor(typeof(UniMob_Base), true)]
	[CanEditMultipleObjects]
	public class UniMobs_Editor : Editor
	{
		GUIStyle headerStyleBig;
		GUIStyle headerStyleSmall;
		GUIStyle buttonStyleBig;
		GUIStyle enumStyleBig;
		GUIStyle enumStyleBackground;
		GUIStyle enumStyleBorder;
		SerializedProperty agroRadius;
		SerializedProperty spawnDelay;
		SerializedProperty moveRate;
		SerializedProperty moveForce;
		SerializedProperty attackType;
		SerializedProperty attackRange;
		SerializedProperty attackCooldown;
		SerializedProperty attackChargeTime;
		SerializedProperty attackLifeTime;
		SerializedProperty attackMoveForce;
		SerializedProperty attackHitForce;
		BaseAttack baseAttack;
		public float scrollableSectionHeight = 150;
		SerializedProperty mobHealth;
		public bool showHealth;
		public bool[] shownHealth = new bool[0];
		public Vector2 scrollHealth = new Vector2();
		SerializedProperty mobDamage;
		public bool showDamage;
		public bool[] shownDamage = new bool[0];
		public Vector2 scrollDamage = new Vector2();
		SerializedProperty mobCoin;
		public bool showCoin;
		public bool[] shownCoin = new bool[0];
		public Vector2 scrollCoin = new Vector2();
		SerializedProperty mobExp;
		public bool showExp;
		public bool[] shownExp = new bool[0];
		public Vector2 scrollExp = new Vector2();
		SerializedProperty mobScale;
		public bool showScale;
		public bool[] shownScale = new bool[0];
		public Vector2 scrollScale = new Vector2();
		SerializedProperty mobGodMode;
		SerializedProperty mobFreeWill;
		SerializedProperty mobFlipToVelocity;
		SerializedProperty mobFlipToTarget;
		public int mobFlipOption = 0;
		private void OnEnable()
		{
			agroRadius = serializedObject.FindProperty("agroRadius");
			spawnDelay = serializedObject.FindProperty("spawnDelay");
			moveRate = serializedObject.FindProperty("moveRate");
			moveForce = serializedObject.FindProperty("moveForce");

			attackType = serializedObject.FindProperty("attackType");
			attackRange = serializedObject.FindProperty("attackRange");
			attackCooldown = serializedObject.FindProperty("attackCooldown");
			attackChargeTime = serializedObject.FindProperty("attackDelay");
			attackLifeTime = serializedObject.FindProperty("attackTime");
			attackMoveForce = serializedObject.FindProperty("attackMoveForce");
			attackHitForce = serializedObject.FindProperty("attackHitForce");

			mobHealth = serializedObject.FindProperty("mobBaseHealth");
			mobDamage = serializedObject.FindProperty("mobBaseDamage");
			mobCoin = serializedObject.FindProperty("mobBaseCoin");
			mobExp = serializedObject.FindProperty("mobBaseExp");
			mobScale = serializedObject.FindProperty("mobBaseScale");

			mobGodMode = serializedObject.FindProperty("mob_GodMode");
			mobFreeWill = serializedObject.FindProperty("mob_FreeWill");
			mobFlipToVelocity = serializedObject.FindProperty("mob_SpriteFlipToVelocity");
			mobFlipToTarget = serializedObject.FindProperty("mob_SpriteFlipToTarget");

			mobFlipOption = 0;
			if (mobFlipToVelocity.boolValue && !mobFlipToTarget.boolValue)
				mobFlipOption = 1;
			else if (!mobFlipToVelocity.boolValue && mobFlipToTarget.boolValue)
				mobFlipOption = 2;
			else if (mobFlipToVelocity.boolValue && mobFlipToTarget.boolValue)
				mobFlipOption = 3;
		}
		public override void OnInspectorGUI()
		{
			#region Styles
			headerStyleBig = new GUIStyle(GUI.skin.label);
			headerStyleSmall = new GUIStyle(GUI.skin.label);
			buttonStyleBig = new GUIStyle(GUI.skin.button);
			enumStyleBig = new GUIStyle(GUI.skin.box);
			enumStyleBackground = new GUIStyle(GUI.skin.box);
			enumStyleBorder = new GUIStyle(GUI.skin.button);
			headerStyleBig.alignment = TextAnchor.MiddleLeft;
			headerStyleBig.fontStyle = FontStyle.Bold;
			headerStyleBig.fontSize = 18;
			headerStyleSmall.alignment = TextAnchor.MiddleCenter;
			headerStyleSmall.fontStyle = FontStyle.Bold;
			headerStyleSmall.fontSize = 14;
			headerStyleSmall.normal.textColor = Color.black;
			buttonStyleBig.alignment = TextAnchor.MiddleCenter;
			buttonStyleBig.fontStyle = FontStyle.Bold;
			buttonStyleBig.fontSize = 14;
			buttonStyleBig.normal.textColor = Color.black;
			buttonStyleBig.active.textColor = Color.magenta;
			enumStyleBig.alignment = TextAnchor.MiddleCenter;
			enumStyleBig.fontStyle = FontStyle.Bold;
			enumStyleBig.fontSize = 14;
			enumStyleBig.normal = GUI.skin.button.normal;
			enumStyleBig.normal.textColor = Color.black;
			enumStyleBig.active.textColor = Color.magenta;
			enumStyleBackground.alignment = TextAnchor.MiddleCenter;
			enumStyleBackground.fontStyle = FontStyle.Bold;
			enumStyleBackground.fontSize = 14;
			enumStyleBackground.border = GUI.skin.box.border;
			enumStyleBackground.normal = GUI.skin.button.normal;
			enumStyleBackground.normal.textColor = Color.black;
			enumStyleBackground.active.textColor = Color.magenta;
			enumStyleBorder.alignment = TextAnchor.MiddleCenter;
			enumStyleBorder.fontStyle = FontStyle.Bold;
			enumStyleBorder.fontSize = 14;
			enumStyleBorder.border = GUI.skin.box.border;
			enumStyleBorder.normal = GUI.skin.box.normal;
			enumStyleBorder.normal.textColor = Color.black;
			enumStyleBorder.active.textColor = Color.magenta;
#if UNITY_PRO_LICENSE
			headerStyleSmall.normal.textColor = Color.white;
			buttonStyleBig.normal.textColor = Color.white;
			enumStyleBig.normal.textColor = Color.white;
			enumStyleBackground.normal.textColor = Color.white;
			enumStyleBorder.normal.textColor = Color.white;
#endif
			#endregion
			UniMob_Base baseMob = (UniMob_Base)target;
			serializedObject.Update();
			#region Name Properties
			using (var vScope = new GUILayout.VerticalScope(enumStyleBorder))
			{
				SerializedProperty mobName = serializedObject.FindProperty("mob_Name");
				GUILayout.Label("Name", headerStyleBig);
			
				
				//string mobName = baseMob.mob_Name;
				if (mobName.stringValue == string.Empty)
					mobName.stringValue = UniMob_Properties.RandomNames[Random.Range(0, UniMob_Properties.RandomNames.Length)];
				using (var scope = new GUILayout.HorizontalScope(GUILayout.ExpandWidth(true)))
				{
					EditorGUILayout.PropertyField(mobName, new GUIContent(""));
					if (GUILayout.Button("Random Rename"))
					{
						mobName.stringValue = UniMob_Properties.RandomNames[Random.Range(0, UniMob_Properties.RandomNames.Length)];
						baseMob.mob_Name = mobName.stringValue;
						Object prefabMob = PrefabUtility.GetPrefabParent(baseMob.gameObject);
						AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(prefabMob), "(UniMob)Lv_" + baseMob.mobLevel.ToString("D3") + "_" + mobName.stringValue);
						PrefabUtility.ReplacePrefab(baseMob.gameObject, prefabMob, ReplacePrefabOptions.Default);
						baseMob.gameObject.name = "(UniMob)Lv_" + baseMob.mobLevel.ToString("D3") + "_" + mobName.stringValue;
						serializedObject.ApplyModifiedProperties();
					}
					GUILayout.FlexibleSpace();
					if (GUILayout.Button("<< Lvl"))
					{
						baseMob.mobLevel--;
						if (baseMob.mobLevel < 0)
							baseMob.mobLevel = 0;
						if (mobName.stringValue == string.Empty)
						{
							mobName.stringValue = UniMob_Properties.RandomNames[Random.Range(0, UniMob_Properties.RandomNames.Length)];
							baseMob.mob_Name = mobName.stringValue;
						}
						baseMob.gameObject.name = "(UniMob)Lv_" + baseMob.mobLevel.ToString("D3") + "_" + baseMob.mob_Name;

					}
					GUILayout.TextField(baseMob.mobLevel.ToString("D3"), GUILayout.ExpandWidth(true));
					if (GUILayout.Button("Lvl >>"))
					{
						baseMob.mobLevel++;
						if (mobName.stringValue == string.Empty)
						{
							mobName.stringValue = UniMob_Properties.RandomNames[Random.Range(0, UniMob_Properties.RandomNames.Length)];
							baseMob.mob_Name = mobName.stringValue;
						}
						baseMob.gameObject.name = "(UniMob)Lv_" + baseMob.mobLevel.ToString("D3") + "_" + baseMob.mob_Name;

					}

				}
				using (var vBack = new GUILayout.VerticalScope(enumStyleBackground))
				{
					baseMob.SetStats();
					using (var hBack = new GUILayout.HorizontalScope(headerStyleSmall))
					{
						GUILayout.FlexibleSpace();
						GUILayout.Label("Health: " + baseMob.mobHealth.ToString() + " / " + baseMob.mobHealthMax.ToString());
						GUILayout.FlexibleSpace();
						GUILayout.Label("Damage: " + baseMob.mobDamage.ToString());
						GUILayout.FlexibleSpace();

					}
					using (var hBack = new GUILayout.HorizontalScope(headerStyleSmall))
					{
						GUILayout.FlexibleSpace();
						GUILayout.Label("Coin Drop: " + baseMob.mobCoin.ToString());
						GUILayout.FlexibleSpace();
						GUILayout.Label("Experience: " + baseMob.mobExp.ToString());
						GUILayout.FlexibleSpace();
						GUILayout.Label("Scale: " + baseMob.mobScale.ToString());
						GUILayout.FlexibleSpace();

					}
				}
			}
			#endregion
			GUILayout.Space(10);
			#region Behaviour Properties
			using (var vScope = new GUILayout.VerticalScope(enumStyleBorder))
			{
				GUILayout.Label("Behaviour Properties", headerStyleBig);
				using (var hScope = new GUILayout.HorizontalScope(GUILayout.Height(60)))
				{
					if (GUILayout.Button("Free Will:\n" + mobFreeWill.boolValue, buttonStyleBig, GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true)))
					{
						mobFreeWill.boolValue = !mobFreeWill.boolValue;
					}
					if (GUILayout.Button("God Mode:\n" + mobGodMode.boolValue, buttonStyleBig, GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true)))
					{
						mobGodMode.boolValue = !mobGodMode.boolValue;
					}
					string flipTxt = string.Empty;
					switch (mobFlipOption)
					{
						case 0://both disabled
							mobFlipToTarget.boolValue = false;
							mobFlipToVelocity.boolValue = false;
							flipTxt = "None";
							break;
						case 1://velocity
							mobFlipToTarget.boolValue = false;
							mobFlipToVelocity.boolValue = true;
							flipTxt = "Velocity";
							break;
						case 2://Target
							mobFlipToTarget.boolValue = true;
							mobFlipToVelocity.boolValue = false;
							flipTxt = "Target";
							break;
						case 3://Both enabled
							mobFlipToTarget.boolValue = true;
							mobFlipToVelocity.boolValue = true;
							flipTxt = "Velocity\n& Target";
							break;
					}
					if (GUILayout.Button("Flip to:\n" + flipTxt, buttonStyleBig, GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true)))
					{
						mobFlipOption++;
						if (mobFlipOption > 3)
							mobFlipOption = 0;
					}
				}
				using (var hScope = new GUILayout.HorizontalScope(enumStyleBackground))
				{
					using (var vSubScope = new GUILayout.VerticalScope(GUILayout.ExpandWidth(true)))
					{

						float fValue = (Mathf.RoundToInt(agroRadius.floatValue * 100)) * 0.01f;
						ProgressBar(agroRadius.floatValue / 2f, "Agro Radius: " + fValue);
						//GUILayout.Label("Agro Radius: " + fValue, headerStyleSmall, GUILayout.Width(100), GUILayout.ExpandWidth(true));
						agroRadius.floatValue = GUILayout.HorizontalSlider(fValue, 0f, 10f, GUILayout.ExpandWidth(true));
					}
					GUILayout.Space(10);
					using (var vSubScope = new GUILayout.VerticalScope(GUILayout.ExpandWidth(true)))
					{
						float fValue = (Mathf.RoundToInt(spawnDelay.floatValue * 100)) * 0.01f;
						ProgressBar(spawnDelay.floatValue / 2f, "Spawn Delay: " + fValue);
						//GUILayout.Label("Spawn Delay: " + fValue, headerStyleSmall, GUILayout.Width(100), GUILayout.ExpandWidth(true));
						spawnDelay.floatValue = GUILayout.HorizontalSlider(fValue, 0f, 2f, GUILayout.ExpandWidth(true));
					}
				}
				using (var hScope = new GUILayout.HorizontalScope(enumStyleBackground))
				{
					using (var vSlider = new GUILayout.VerticalScope(GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true)))
					{
						float fValue = (Mathf.RoundToInt(moveRate.floatValue * 100)) * 0.01f;
						if (!moveRate.hasMultipleDifferentValues)
							ProgressBar(moveRate.floatValue / 2f, "Move Rate: " + fValue);
						moveRate.floatValue = GUILayout.HorizontalSlider(fValue, 0f, 2f);
					}
					using (var vSlider = new GUILayout.VerticalScope(GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true)))
					{
						float fValue = (Mathf.RoundToInt(moveForce.floatValue * 100)) * 0.01f;
						if (!moveForce.hasMultipleDifferentValues)
							ProgressBar(moveForce.floatValue / 20f, "Move Force: " + fValue);
						moveForce.floatValue = GUILayout.HorizontalSlider(fValue, 0f, 20f);
					}
				}
			}
			#endregion
			GUILayout.Space(10);
			#region AttackProperties
			using (var vBorder = new GUILayout.VerticalScope(enumStyleBorder))
			{
				GUILayout.Label("Attack Properties", headerStyleBig);
				using (var hScope = new GUILayout.HorizontalScope())
				{
					//GUILayout.Space(30);
					using (var hsScope = new GUILayout.HorizontalScope(enumStyleBackground))
					{
						GUILayout.Label("ID", headerStyleSmall);
						GameObject[] allAttacks = Resources.LoadAll<GameObject>("GameObjects/Attacks/");
						string[] allAttackNames = new string[allAttacks.Length];
						for (int i = 0; i < allAttacks.Length; i++)
						{
							allAttackNames[i] = allAttacks[i].name;
						}
						baseMob.attackID = EditorGUILayout.Popup(baseMob.attackID, allAttackNames, enumStyleBig, GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
						GUILayout.Label("Type", headerStyleSmall);
						baseMob.attackType = (UniMob_Properties.UniMob_AttackType)EditorGUILayout.EnumPopup((UniMob_Properties.UniMob_AttackType)attackType.enumValueIndex, enumStyleBig, GUILayout.ExpandHeight(true));
					}
				}
				using (var vBack = new GUILayout.VerticalScope(enumStyleBackground))
				{
					using (var vSection = new GUILayout.VerticalScope(GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true)))
					{
						string txt = string.Empty;
						int attackID = baseMob.attackID;
						baseAttack = Resources.LoadAll<GameObject>("GameObjects/Attacks/")[attackID].GetComponent<BaseAttack>();
						if (baseAttack != null)
						{
							if (baseAttack.speedBased)
								txt = "TravelSpeed: ";
							else
								txt = "LifeTime: ";
						}
						GUILayout.Label(txt + "/CastTime/Cooldown", headerStyleSmall);
						using (var scope = new GUILayout.HorizontalScope(GUILayout.ExpandWidth(true)))
						{
							using (var vSlider = new GUILayout.VerticalScope(GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true)))
							{
								float fValue = (Mathf.RoundToInt(attackLifeTime.floatValue * 100)) * 0.01f;
								if (!attackLifeTime.hasMultipleDifferentValues)
									ProgressBar(attackLifeTime.floatValue / 20f, txt + fValue);
								attackLifeTime.floatValue = GUILayout.HorizontalSlider(fValue, 0f, 20f);
							}
							using (var vSlider = new GUILayout.VerticalScope(GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true)))
							{
								float fValue = (Mathf.RoundToInt(attackChargeTime.floatValue * 100)) * 0.01f;
								if (!attackChargeTime.hasMultipleDifferentValues)
									ProgressBar(attackChargeTime.floatValue / 10f, "CastTime: " + fValue);
								attackChargeTime.floatValue = GUILayout.HorizontalSlider(fValue, 0f, 10f);
							}
							using (var vSlider = new GUILayout.VerticalScope(GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true)))
							{
								float fValue = (Mathf.RoundToInt(attackCooldown.floatValue * 100)) * 0.01f;
								if (!attackCooldown.hasMultipleDifferentValues)
									ProgressBar(attackCooldown.floatValue / 5f, "Cooldown: " + fValue);
								attackCooldown.floatValue = GUILayout.HorizontalSlider(fValue, 0.2f, 5f);
							}
						}
					}
					using (var vSection = new GUILayout.VerticalScope(GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true)))
					{
						GUILayout.Label("Range/Dash/Knockback", headerStyleSmall);
						using (var scope = new GUILayout.HorizontalScope(GUILayout.ExpandWidth(true)))
						{
							using (var vSlider = new GUILayout.VerticalScope(GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true)))
							{
								float fValue = (Mathf.RoundToInt(attackRange.floatValue * 100)) * 0.01f;
								if (!attackRange.hasMultipleDifferentValues)
									ProgressBar(attackRange.floatValue / 10f, "Range: " + fValue);
								attackRange.floatValue = GUILayout.HorizontalSlider(fValue, 0, 10f);
							}
							using (var vSlider0 = new GUILayout.VerticalScope(GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true)))
							{
								float fValue = (Mathf.RoundToInt(attackMoveForce.floatValue * 100)) * 0.01f;
								if (!attackMoveForce.hasMultipleDifferentValues)
									ProgressBar(attackMoveForce.floatValue / 20f, "DashVelocity: " + fValue);
								attackMoveForce.floatValue = GUILayout.HorizontalSlider(fValue, 0, 20f);
							}
							using (var vSlider1 = new GUILayout.VerticalScope(GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true)))
							{
								float fValue = (Mathf.RoundToInt(attackHitForce.floatValue * 100)) * 0.01f;
								if (!attackHitForce.hasMultipleDifferentValues)
									ProgressBar(attackHitForce.floatValue / 500f, "Knockback: " + fValue);
								attackHitForce.floatValue = GUILayout.HorizontalSlider(fValue, 0.1f, 500f);
							}
						}
					}
				}
			}
			#endregion
			GUILayout.Space(10);
			#region StatProperties

			using (var vBorder = new GUILayout.VerticalScope(enumStyleBorder))
			{
				GUILayout.Label("Stat Properties", headerStyleBig);
				using (var vBack = new GUILayout.VerticalScope(enumStyleBackground))
				{
					using (var toggleHealth = new EditorGUILayout.ToggleGroupScope((showHealth ? "Changing " : "Change ") + "Health Modifier: " + mobHealth.floatValue.ToString(), showHealth))
					{
						showHealth = toggleHealth.enabled;
						if (showHealth)
						{
							ShowHealthProperties();
						}
					}
					GUILayout.Label("_________________________________________________________", GUILayout.ExpandWidth(false));
					using (var toggleDamage = new EditorGUILayout.ToggleGroupScope((showDamage ? "Changing " : "Change ") + "Damage Modifier: " + mobDamage.floatValue.ToString(), showDamage))
					{
						showDamage = toggleDamage.enabled;
						if (showDamage)
						{
							ShowDamageProperties();
						}
					}
					GUILayout.Label("_________________________________________________________", GUILayout.ExpandWidth(false));
					using (var toggleCoin = new EditorGUILayout.ToggleGroupScope((showCoin ? "Changing " : "Change ") + "Coin Modifier: " + mobCoin.floatValue.ToString(), showCoin))
					{
						showCoin = toggleCoin.enabled;
						if (showCoin)
						{
							ShowCoinProperties();
						}
					}
					GUILayout.Label("_________________________________________________________", GUILayout.ExpandWidth(false));
					using (var toggleExp = new EditorGUILayout.ToggleGroupScope((showExp ? "Changing " : "Change ") + "Exp Modifier: " + mobExp.floatValue.ToString(), showExp))
					{
						showExp = toggleExp.enabled;
						if (showExp)
						{
							ShowExpProperties();
						}
					}
					GUILayout.Label("_________________________________________________________", GUILayout.ExpandWidth(false));
					using (var toggleScale = new EditorGUILayout.ToggleGroupScope((showScale ? "Changing " : "Change ") + "Scale Modifier: " + mobScale.floatValue.ToString(), showScale))
					{
						showScale = toggleScale.enabled;
						if (showScale)
						{
							ShowScaleProperties();
						}
					}
				}
			}
			#endregion
			serializedObject.ApplyModifiedProperties();
			base.OnInspectorGUI();
		}
		void ShowHealthProperties()
		{
			using (var header = new GUILayout.HorizontalScope())
			{
				float fValue = (Mathf.RoundToInt(mobHealth.floatValue * 100)) * 0.01f;
				mobHealth.floatValue = GUILayout.HorizontalSlider(fValue, -20f, 100f, GUILayout.ExpandWidth(true));
				mobHealth.floatValue = EditorGUILayout.FloatField(mobHealth.floatValue, GUILayout.Width(20), GUILayout.MaxWidth(70));
			}
			using (var header = new GUILayout.HorizontalScope())
			{
				bool oneIsCollapsed = false;
				foreach (bool shown in shownHealth)
				{
					if (shown)
					{
						oneIsCollapsed = true;
						break;
					}
				}
				if (GUILayout.Button((oneIsCollapsed ? "Uncollapse" : "Collapse") + " All", GUILayout.ExpandWidth(true)))
				{
					if (oneIsCollapsed)
					{
						for (int i = 0; i < shownHealth.Length; i++)
						{
							shownHealth[i] = false;
						}
					}
					else
					{
						for (int i = 0; i < shownHealth.Length; i++)
						{
							shownHealth[i] = true;
						}
					}
				}
				GUILayout.Label("Actual Health per Lvl | Diff | Base Health per Lvl", GUILayout.ExpandWidth(true));
			}
			using (var scrollGrp = new GUILayout.VerticalScope(GUILayout.Height(scrollableSectionHeight), GUILayout.ExpandWidth(true)))
			{
				scrollHealth = GUILayout.BeginScrollView(scrollHealth, false, false);
				int count = 1;
				int max = 20;
				int maxPerSection = 10;
				if (shownHealth.Length < max * maxPerSection)
					shownHealth = new bool[max * maxPerSection];
				for (int j = 1; j < max; j++)
				{
					int section = j * maxPerSection;
					float highestVal = UniMob_Properties.GetMobHealth(section);
					float highestCompareVal = UniMob_Properties.GetMobHealth(section, mobHealth.floatValue);
					int start = section;
					int end = start;
					if (GUILayout.Button((shownHealth[j] ? "Hide" : "Show") + " Lvls: " + (start - maxPerSection).ToString() + "/" + (start - 1).ToString()))
					{
						shownHealth[j] = !shownHealth[j];
					}
					if (shownHealth[j])
					{
						for (int i = start - maxPerSection; i < end; i++)
						{
							int lvl = i;
							using (var horizontalBars = new GUILayout.HorizontalScope(GUILayout.ExpandWidth(true)))
							{
								float compareVal = (float)(UniMob_Properties.GetMobHealth(lvl, mobHealth.floatValue));
								float keyVal = (float)(UniMob_Properties.GetMobHealth(lvl));
								ProgressBar(compareVal / highestVal, lvl.ToString() + "[" + compareVal + "/" + highestVal + "]");
								GUILayout.Label("|" + (compareVal - keyVal) + "|", headerStyleSmall, GUILayout.ExpandWidth(false), GUILayout.MinWidth(45));
								ProgressBar(keyVal / highestVal, lvl.ToString() + "[" + keyVal + "/" + highestVal + "]");
							}
							count++;
						}
					}
				}
				GUILayout.EndScrollView();
			}
		}
		void ShowDamageProperties()
		{
			using (var header = new GUILayout.HorizontalScope())
			{
				float fValue = (Mathf.RoundToInt(mobDamage.floatValue * 100)) * 0.01f;
				mobDamage.floatValue = GUILayout.HorizontalSlider(fValue, -3f, 10f, GUILayout.ExpandWidth(true));
				mobDamage.floatValue = EditorGUILayout.FloatField(mobDamage.floatValue, GUILayout.Width(20), GUILayout.MaxWidth(70));
			}
			using (var header = new GUILayout.HorizontalScope())
			{
				bool oneIsCollapsed = false;
				foreach (bool shown in shownDamage)
				{
					if (shown)
					{
						oneIsCollapsed = true;
						break;
					}
				}
				if (GUILayout.Button((oneIsCollapsed ? "Uncollapse" : "Collapse") + " All", GUILayout.ExpandWidth(true)))
				{
					if (oneIsCollapsed)
					{
						for (int i = 0; i < shownDamage.Length; i++)
						{
							shownDamage[i] = false;
						}
					}
					else
					{
						for (int i = 0; i < shownDamage.Length; i++)
						{
							shownDamage[i] = true;
						}
					}
				}
				GUILayout.Label("Actual Damage per Lvl | Diff | Base Damage per Lvl", GUILayout.ExpandWidth(true));
			}
			using (var scrollGrp = new GUILayout.VerticalScope(GUILayout.Height(scrollableSectionHeight), GUILayout.ExpandWidth(true)))
			{
				scrollDamage = GUILayout.BeginScrollView(scrollDamage, false, false);
				int count = 1;
				int max = 20;
				int maxPerSection = 10;
				if (shownDamage.Length < max * maxPerSection)
					shownDamage = new bool[max * maxPerSection];
				for (int j = 1; j < max; j++)
				{
					int section = j * maxPerSection;
					float highestVal = UniMob_Properties.GetMobDamage(section);
					float highestCompareVal = UniMob_Properties.GetMobDamage(section, mobDamage.floatValue);
					int start = section;
					int end = start;
					if (GUILayout.Button((shownDamage[j] ? "Hide" : "Show") + " Lvls: " + (start - maxPerSection).ToString() + "/" + (start - 1).ToString()))
					{
						shownDamage[j] = !shownDamage[j];
					}
					if (shownDamage[j])
					{
						for (int i = start - maxPerSection; i < end; i++)
						{
							int lvl = i;
							using (var horizontalBars = new GUILayout.HorizontalScope(GUILayout.ExpandWidth(true)))
							{
								float compareVal = (float)(UniMob_Properties.GetMobDamage(lvl, mobDamage.floatValue));
								float keyVal = (float)(UniMob_Properties.GetMobDamage(lvl));
								ProgressBar(compareVal / highestVal, lvl.ToString() + "[" + compareVal + "/" + highestVal + "]");
								GUILayout.Label("|" + (compareVal - keyVal) + "|", headerStyleSmall, GUILayout.ExpandWidth(false), GUILayout.MinWidth(45));
								ProgressBar(keyVal / highestVal, lvl.ToString() + "[" + keyVal + "/" + highestVal + "]");
							}
							count++;
						}
					}
				}
				GUILayout.EndScrollView();
			}
		}
		void ShowCoinProperties()
		{
			using (var header = new GUILayout.HorizontalScope())
			{
				float fValue = (Mathf.RoundToInt(mobCoin.floatValue * 100)) * 0.01f;
				mobCoin.floatValue = GUILayout.HorizontalSlider(fValue, -20f, 50f, GUILayout.ExpandWidth(true));
				mobCoin.floatValue = EditorGUILayout.FloatField(mobCoin.floatValue, GUILayout.Width(20), GUILayout.MaxWidth(70));
			}
			using (var header = new GUILayout.HorizontalScope())
			{
				bool oneIsCollapsed = false;
				foreach (bool shown in shownCoin)
				{
					if (shown)
					{
						oneIsCollapsed = true;
						break;
					}
				}
				if (GUILayout.Button((oneIsCollapsed ? "Uncollapse" : "Collapse") + " All", GUILayout.ExpandWidth(true)))
				{
					if (oneIsCollapsed)
					{
						for (int i = 0; i < shownCoin.Length; i++)
						{
							shownCoin[i] = false;
						}
					}
					else
					{
						for (int i = 0; i < shownCoin.Length; i++)
						{
							shownCoin[i] = true;
						}
					}
				}
				GUILayout.Label("Actual Coin per Lvl | Diff | Base Coin per Lvl", GUILayout.ExpandWidth(true));
			}
			using (var scrollGrp = new GUILayout.VerticalScope(GUILayout.Height(scrollableSectionHeight), GUILayout.ExpandWidth(true)))
			{
				scrollCoin = GUILayout.BeginScrollView(scrollCoin, false, false);
				int count = 1;
				int max = 20;
				int maxPerSection = 10;
				if (shownCoin.Length < max * maxPerSection)
					shownCoin = new bool[max * maxPerSection];
				for (int j = 1; j < max; j++)
				{
					int section = j * maxPerSection;
					float highestVal = UniMob_Properties.GetMobCoin(section);
					float highestCompareVal = UniMob_Properties.GetMobCoin(section, mobCoin.floatValue);
					int start = section;
					int end = start;
					if (GUILayout.Button((shownCoin[j] ? "Hide" : "Show") + " Lvls: " + (start - maxPerSection).ToString() + "/" + (start - 1).ToString()))
					{
						shownCoin[j] = !shownCoin[j];
					}
					if (shownCoin[j])
					{
						for (int i = start - maxPerSection; i < end; i++)
						{
							int lvl = i;
							using (var horizontalBars = new GUILayout.HorizontalScope(GUILayout.ExpandWidth(true)))
							{
								float compareVal = (float)(UniMob_Properties.GetMobCoin(lvl, mobCoin.floatValue));
								float keyVal = (float)(UniMob_Properties.GetMobCoin(lvl));
								ProgressBar(compareVal / highestVal, lvl.ToString() + "[" + compareVal + "/" + highestVal + "]");
								GUILayout.Label("|" + (compareVal - keyVal) + "|", headerStyleSmall, GUILayout.ExpandWidth(false), GUILayout.MinWidth(45));
								ProgressBar(keyVal / highestVal, lvl.ToString() + "[" + keyVal + "/" + highestVal + "]");
							}
							count++;
						}
					}
				}
				GUILayout.EndScrollView();
			}
		}
		void ShowExpProperties()
		{
			using (var header = new GUILayout.HorizontalScope())
			{
				float fValue = (Mathf.RoundToInt(mobExp.floatValue * 100)) * 0.01f;
				mobExp.floatValue = GUILayout.HorizontalSlider(fValue, -20f, 500f, GUILayout.ExpandWidth(true));
				mobExp.floatValue = EditorGUILayout.FloatField(mobExp.floatValue, GUILayout.Width(20), GUILayout.MaxWidth(70));
			}
			using (var header = new GUILayout.HorizontalScope())
			{
				bool oneIsCollapsed = false;
				foreach (bool shown in shownExp)
				{
					if (shown)
					{
						oneIsCollapsed = true;
						break;
					}
				}
				if (GUILayout.Button((oneIsCollapsed ? "Uncollapse" : "Collapse") + " All", GUILayout.ExpandWidth(true)))
				{
					if (oneIsCollapsed)
					{
						for (int i = 0; i < shownExp.Length; i++)
						{
							shownExp[i] = false;
						}
					}
					else
					{
						for (int i = 0; i < shownExp.Length; i++)
						{
							shownExp[i] = true;
						}
					}
				}
				GUILayout.Label("Actual Exp per Lvl | Diff | Base Exp per Lvl", GUILayout.ExpandWidth(true));
			}
			using (var scrollGrp = new GUILayout.VerticalScope(GUILayout.Height(scrollableSectionHeight), GUILayout.ExpandWidth(true)))
			{
				scrollExp = GUILayout.BeginScrollView(scrollExp, false, false);
				int count = 1;
				int max = 20;
				int maxPerSection = 10;
				if (shownExp.Length < max * maxPerSection)
					shownExp = new bool[max * maxPerSection];
				for (int j = 1; j < max; j++)
				{
					int section = j * maxPerSection;
					float highestVal = UniMob_Properties.GetMobExperience(section);
					float highestCompareVal = UniMob_Properties.GetMobExperience(section, mobExp.floatValue);
					int start = section;
					int end = start;
					if (GUILayout.Button((shownExp[j] ? "Hide" : "Show") + " Lvls: " + (start - maxPerSection).ToString() + "/" + (start - 1).ToString()))
					{
						shownExp[j] = !shownExp[j];
					}
					if (shownExp[j])
					{
						for (int i = start - maxPerSection; i < end; i++)
						{
							int lvl = i;
							using (var horizontalBars = new GUILayout.HorizontalScope(GUILayout.ExpandWidth(true)))
							{
								float compareVal = (float)(UniMob_Properties.GetMobExperience(lvl, mobExp.floatValue));
								float keyVal = (float)(UniMob_Properties.GetMobExperience(lvl));
								ProgressBar(compareVal / highestVal, lvl.ToString() + "[" + compareVal + "/" + highestVal + "]");
								GUILayout.Label("|" + (compareVal - keyVal) + "|", headerStyleSmall, GUILayout.ExpandWidth(false), GUILayout.MinWidth(45));
								ProgressBar(keyVal / highestVal, lvl.ToString() + "[" + keyVal + "/" + highestVal + "]");
							}
							count++;
						}
					}
				}
				GUILayout.EndScrollView();
			}
		}
		void ShowScaleProperties()
		{
			using (var header = new GUILayout.HorizontalScope())
			{
				float fValue = (Mathf.RoundToInt(mobScale.floatValue * 100)) * 0.01f;
				mobScale.floatValue = GUILayout.HorizontalSlider(fValue, 0f, 3f, GUILayout.ExpandWidth(true));
				mobScale.floatValue = EditorGUILayout.FloatField(mobScale.floatValue, GUILayout.Width(20), GUILayout.MaxWidth(70));
			}
			using (var header = new GUILayout.HorizontalScope())
			{
				bool oneIsCollapsed = false;
				foreach (bool shown in shownScale)
				{
					if (shown)
					{
						oneIsCollapsed = true;
						break;
					}
				}
				if (GUILayout.Button((oneIsCollapsed ? "Uncollapse" : "Collapse") + " All", GUILayout.ExpandWidth(true)))
				{
					if (oneIsCollapsed)
					{
						for (int i = 0; i < shownScale.Length; i++)
						{
							shownScale[i] = false;
						}
					}
					else
					{
						for (int i = 0; i < shownScale.Length; i++)
						{
							shownScale[i] = true;
						}
					}
				}
				GUILayout.Label("Actual Scale per Lvl | Diff | Base Scale per Lvl", GUILayout.ExpandWidth(true));
			}
			using (var scrollGrp = new GUILayout.VerticalScope(GUILayout.Height(scrollableSectionHeight), GUILayout.ExpandWidth(true)))
			{
				scrollScale = GUILayout.BeginScrollView(scrollScale, false, false);
				int count = 1;
				int max = 20;
				int maxPerSection = 10;
				if (shownScale.Length < max * maxPerSection)
					shownScale = new bool[max * maxPerSection];
				for (int j = 1; j < max; j++)
				{
					int section = j * maxPerSection;
					float highestVal = UniMob_Properties.GetMobScale(section);
					float highestCompareVal = UniMob_Properties.GetMobScale(section, mobScale.floatValue);
					int start = section;
					int end = start;
					if (GUILayout.Button((shownScale[j] ? "Hide" : "Show") + " Lvls: " + (start - maxPerSection).ToString() + "/" + (start - 1).ToString()))
					{
						shownScale[j] = !shownScale[j];
					}
					if (shownScale[j])
					{
						for (int i = start - maxPerSection; i < end; i++)
						{
							int lvl = i;
							using (var horizontalBars = new GUILayout.HorizontalScope(GUILayout.ExpandWidth(true)))
							{
								float compareVal = (float)(UniMob_Properties.GetMobScale(lvl, mobScale.floatValue));
								float keyVal = (float)(UniMob_Properties.GetMobScale(lvl));
								ProgressBar(compareVal / highestVal, lvl.ToString() + "[" + compareVal + "/" + highestVal + "]");
								GUILayout.Label("|" + (compareVal - keyVal) + "|", headerStyleSmall, GUILayout.ExpandWidth(false), GUILayout.MinWidth(45));
								ProgressBar(keyVal / highestVal, lvl.ToString() + "[" + keyVal + "/" + highestVal + "]");
							}
							count++;
						}
					}
				}
				GUILayout.EndScrollView();
			}
		}
		void ScaleGraph()
		{
			int propValue = mobHealth.intValue;
			AnimationCurve curve = new AnimationCurve();
			for (int i = 0; i < 200; i++)
			{
				int lvl = i;
				int keyVal = UniMob_Properties.GetMobHealth(lvl);

				curve.AddKey((float)lvl, (float)keyVal);
			}
			Rect rect = GUILayoutUtility.GetRect(18, 18, "TextField", GUILayout.ExpandHeight(true));

			EditorGUI.CurveField(rect, curve);
		}
		void ProgressBar(float value, string label)
		{
			Rect rect = GUILayoutUtility.GetRect(18, 18, "TextField", GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));
			EditorGUI.ProgressBar(rect, value, string.Empty);
			float fValue = Mathf.RoundToInt(value * 100);
			fValue *= 0.01f;
			headerStyleSmall.fontSize = 12;
			GUI.Label(rect, label, headerStyleSmall);
			headerStyleSmall.fontSize = 14;
		}
	}

	[System.Serializable]
	public class SpriteArray
	{
		public Sprite[] sprites;
		public SpriteArray()
		{
			sprites = null;
		}
	}
	
	public class UniMobs_Window : EditorWindow
	{
		string mobListHeader;
		public Rect headerRect = new Rect();
		public string namePrefix = "UniMob_";
		public Texture2D[] allTextures;
		public bool showMobMenu = false;
		private Vector2 scrollPosition = Vector2.zero;
		private List<SpriteArray> arrayList = new List<SpriteArray>();
		private float lastFrameTime = 0;
		private float frameTime = 0.12f;
		private int page = 0;
		private int mobsPerColumn = 4;
		private int mobsPerRow = 4;
		private int pages = 10;
		private int frame = 0;
		private float m_LastEditorUpdateTime;
		void OnEnable()
		{
			allTextures = Resources.LoadAll<Texture2D>("Sprites/Enemies/Face/");
			m_LastEditorUpdateTime = Time.realtimeSinceStartup;
			EditorApplication.update += OnEditorUpdate;
			if (allTextures != null && allTextures.Length > 0)
			{
				for (int i = 0; i < allTextures.Length; i++)
				{
					string sheetName = allTextures[i].name;
					SpriteArray sArray = new SpriteArray();
					sArray.sprites = Resources.LoadAll<Sprite>("Sprites/Enemies/Attack/" + sheetName);
					arrayList.Add(sArray);
				}
			}
			mobsPerRow = UniMob_Properties.UniMobsPerRow;
			mobsPerColumn = UniMob_Properties.UniMobsPerColumn;
			page = 0;
			pages = Mathf.RoundToInt(allTextures.Length - 1) / ((mobsPerColumn) * mobsPerRow);
		}
		void OnDisable()
		{
			EditorApplication.update -= OnEditorUpdate;
		}
		void OnEditorUpdate()
		{
			m_LastEditorUpdateTime = Time.realtimeSinceStartup;
			if (lastFrameTime < m_LastEditorUpdateTime)
			{
				lastFrameTime = m_LastEditorUpdateTime + 0.1f;
				frame++;
				Repaint();
			}
		}
		[MenuItem("Window/Uni_Mobs")]
		static void ShowWindow()
		{
			UniMobs_Window window = (UniMobs_Window)EditorWindow.GetWindow(typeof(UniMobs_Window));
			window.titleContent.text = "UniMobs";
			Vector2 maxSize = UniMob_Properties.Button_Med;
			maxSize.x *= UniMob_Properties.UniMobsPerRow;
			maxSize.y *= UniMob_Properties.UniMobsPerColumn;
			Vector2 minSize = UniMob_Properties.Button_Med;
			maxSize.x *= UniMob_Properties.UniMobsPerRow;
			maxSize.y *= UniMob_Properties.UniMobsPerColumn;
			window.minSize = minSize;
			window.maxSize = maxSize;
			window.Show();
		}
		UniMob_Base GetSelectedUniMob()
		{
			if (Selection.activeTransform != null)
			{
				if (Selection.activeTransform.GetComponent<UniMob_Base>())
				{
					return Selection.activeTransform.GetComponent<UniMob_Base>();
				}
				if (Selection.activeTransform.GetComponentInChildren<UniMob_Base>() != null)
				{
					return Selection.activeTransform.GetComponentInChildren<UniMob_Base>();
				}
				if (Selection.activeTransform.GetComponentInParent<UniMob_Base>() != null)
				{
					return Selection.activeTransform.GetComponentInParent<UniMob_Base>();
				}
			}
			return null;
		}
		private void OnGUI()
		{
			GUILayoutOption[] layout = new GUILayoutOption[3]{
			GUILayout.MaxHeight(UniMob_Properties.Button_Med.y * UniMob_Properties.UniMobsPerColumn),
			GUILayout.MinWidth(UniMob_Properties.Button_Med.x * UniMob_Properties.UniMobsPerRow * 1.78f),
			GUILayout.ExpandWidth(true)
			};
			UniMob_Base baseMob = null;

			if (GetSelectedUniMob() == null)
			{
				using (var title = new EditorGUILayout.HorizontalScope())
				{
					GUILayout.FlexibleSpace();
					GUILayout.Label("Choose a Universal Mob to edit", EditorStyles.boldLabel);
					GUILayout.FlexibleSpace();
				}
				if (GUILayout.Button("Or spawn a new Universal Mob", GUILayout.ExpandWidth(true), GUILayout.Height(UniMob_Properties.Button_Row.y)))
				{
					int MrRandom = Random.Range(0, allTextures.Length);
					GameObject mob = (GameObject)Instantiate(AssetDatabase.LoadAssetAtPath("Assets/UNI_MOBS/Editor/UniMob_Template.prefab", typeof(GameObject)));
					baseMob = (UniMob_Base)mob.GetComponent<UniMob_Base>();
					baseMob.mob_Name = UniMob_Properties.RandomNames[Random.Range(0, UniMob_Properties.RandomNames.Length)];
					baseMob.gameObject.name = "(UniMob)Lv_" + baseMob.mobLevel.ToString("D3") + "_" + baseMob.mob_Name;
					Selection.activeTransform = baseMob.transform;
					Sprite[] tmp0 = Resources.LoadAll<Sprite>("Sprites/Enemies/Walk/" + allTextures[MrRandom].name);
					baseMob.walkSprites = tmp0;
					Sprite[] tmp1 = Resources.LoadAll<Sprite>("Sprites/Enemies/Attack/" + allTextures[MrRandom].name);
					baseMob.attackSprites = tmp1;
					baseMob.mob_SpriteRenderer.sprite = tmp1[Random.Range(0, tmp1.Length - 1)];
					baseMob.faceSprite = Resources.Load<Sprite>("Sprites/Enemies/Face/" + allTextures[MrRandom].name);
					string localPath = UniMob_Properties.UniMobsPrefabFolder + mob.name + ".prefab";
					Object prefab = PrefabUtility.CreateEmptyPrefab(localPath);
					PrefabUtility.ReplacePrefab(baseMob.gameObject, prefab, ReplacePrefabOptions.ConnectToPrefab);
					Selection.activeTransform = baseMob.transform;
				}
			}
			else
			{
				baseMob = GetSelectedUniMob();
				using (var rootRect = new EditorGUILayout.VerticalScope(layout))
				{
					using (var title = new EditorGUILayout.HorizontalScope())
					{
						GUILayout.FlexibleSpace();
						GUILayout.Label("Editing: Lv" + baseMob.mobLevel.ToString("D3") + "_" + baseMob.mob_Name, EditorStyles.boldLabel);
						GUILayout.FlexibleSpace();
					}
					if (allTextures.Length == 0 || allTextures == null)
						return;
					int id = 0;
					if (!baseMob)
						return;
					UniMob_Base uniMob = baseMob;
					scrollPosition = GUILayout.BeginScrollView(scrollPosition, false, false);
					int row = 0;
					int mobsPerPage = mobsPerRow * mobsPerColumn;
					for (int i = page * mobsPerPage; i <= (page * mobsPerPage) + mobsPerColumn - 1; i++)
					{
						string sheetName = string.Empty;
						using (var rowOfMobs = new EditorGUILayout.HorizontalScope(GUILayout.Width(UniMob_Properties.Button_Med.x * mobsPerRow), GUILayout.Height(UniMob_Properties.Button_Med.y + UniMob_Properties.Button_Row.y)))
						{
							GUILayout.FlexibleSpace();
							for (int j = 0; j <= mobsPerRow - 1; j++)
							{
								int index = i + j + (row * mobsPerRow);
								if (index < allTextures.Length)
								{
									sheetName = allTextures[index].name;
									if (frame > arrayList[index].sprites.Length - 1)
									{
										frame = 0;
									}
									using (var mobWindow = new GUILayout.VerticalScope(GUILayout.Width(UniMob_Properties.Button_Med.x), GUILayout.Height(UniMob_Properties.Button_Med.y + UniMob_Properties.Button_Row.y)))
									{
										using (var mobPic = new GUILayout.HorizontalScope(GUILayout.Width(UniMob_Properties.Button_Med.x), GUILayout.Height(UniMob_Properties.Button_Med.y)))
										{
											GUILayout.Label(index.ToString(), GUILayout.ExpandWidth(true));
											Rect c = arrayList[index].sprites[frame].rect;
											float spriteW = c.width;
											float spriteH = c.height;
											Rect rect = GUILayoutUtility.GetRect(spriteW, spriteH);
											if (Event.current.type == EventType.Repaint)
											{
												var tex = arrayList[index].sprites[0].texture;
												c.xMin /= tex.width;
												c.xMax /= tex.width;
												c.yMin /= tex.height;
												c.yMax /= tex.height;
												GUI.DrawTextureWithTexCoords(rect, tex, c);
											}
											GUILayout.FlexibleSpace();
										}
										using (var botButtons = new GUILayout.HorizontalScope(GUILayout.ExpandWidth(true), GUILayout.Height(UniMob_Properties.Button_Row.y)))
										{
											GUILayout.FlexibleSpace();
											if (GUILayout.Button("Reskin", GUILayout.ExpandWidth(true), GUILayout.Height(UniMob_Properties.Button_Row.y)))
											{
												Sprite[] tmp0 = Resources.LoadAll<Sprite>("Sprites/Enemies/Walk/" + sheetName);
												uniMob.walkSprites = tmp0;
												Sprite[] tmp1 = Resources.LoadAll<Sprite>("Sprites/Enemies/Attack/" + sheetName);
												uniMob.attackSprites = tmp1;
												uniMob.mob_SpriteRenderer.sprite = tmp1[Random.Range(0, tmp1.Length - 1)];
												uniMob.faceSprite = Resources.Load<Sprite>("Sprites/Enemies/Face/" + sheetName);
												Object prefab = PrefabUtility.GetPrefabParent(uniMob.gameObject);
												PrefabUtility.ReplacePrefab(uniMob.gameObject, prefab, ReplacePrefabOptions.Default);
											}
											if (GUILayout.Button("Spawn", GUILayout.ExpandWidth(true), GUILayout.Height(UniMob_Properties.Button_Row.y)))
											{
												Transform parent = Selection.activeGameObject.transform.parent;
												Vector3 pos = Selection.activeGameObject.transform.position;
												pos.x += 1f;
												GameObject mob = (GameObject)Instantiate(AssetDatabase.LoadAssetAtPath("Assets/UNI_MOBS/Editor/UniMob_Template.prefab", typeof(GameObject)));
												mob.transform.SetParent(parent);
												mob.transform.position = pos;
												uniMob = (UniMob_Base)mob.GetComponent<UniMob_Base>();
												uniMob.mob_Name = UniMob_Properties.RandomNames[Random.Range(0, UniMob_Properties.RandomNames.Length)];
												uniMob.gameObject.name = "(UniMob)Lv_" + baseMob.mobLevel.ToString("D3") + "_" + baseMob.mob_Name;
												Selection.activeTransform = baseMob.transform;
												Sprite[] tmp0 = Resources.LoadAll<Sprite>("Sprites/Enemies/Walk/" + sheetName);
												uniMob.walkSprites = tmp0;
												Sprite[] tmp1 = Resources.LoadAll<Sprite>("Sprites/Enemies/Attack/" + sheetName);
												uniMob.attackSprites = tmp1;
												uniMob.mob_SpriteRenderer.sprite = tmp1[Random.Range(0, tmp1.Length - 1)];
												uniMob.faceSprite = Resources.Load<Sprite>("Sprites/Enemies/Face/" + sheetName);
												string localPath = UniMob_Properties.UniMobsPrefabFolder + mob.name + ".prefab";
												Object prefab = PrefabUtility.CreateEmptyPrefab(localPath);
												PrefabUtility.ReplacePrefab(mob, prefab, ReplacePrefabOptions.ConnectToPrefab);
											}
											GUILayout.FlexibleSpace();
										}
									}
								}
							}
							row++;
							GUILayout.FlexibleSpace();
						}
					}
					GUILayout.EndScrollView();
					using (var scrollRectTitle = new EditorGUILayout.HorizontalScope("navigation", GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true)))
					{
						GUILayout.FlexibleSpace();
						if (GUILayout.Button("|<", GUILayout.ExpandWidth(true)))
						{
							page = 0;
						}
						if (GUILayout.Button("<<", GUILayout.ExpandWidth(true)))
						{
							page--;
							if (page < 0)
								page = pages;
						}
						GUILayout.Label("Page " + (page + 1), GUILayout.ExpandWidth(true));
						if (GUILayout.Button(">>", GUILayout.ExpandWidth(true)))
						{
							page++;
							if (page > pages)
								page = 0;
						}
						if (GUILayout.Button(">|", GUILayout.ExpandWidth(true)))
						{
							page = pages;
						}
						GUILayout.FlexibleSpace();
					}
					if (GUILayout.Button("Save selected prefab(s)", GUILayout.MinHeight(16), GUILayout.MaxHeight(32)))
					{
						string name = uniMob.faceSprite.name;
						Sprite[] tmp0 = Resources.LoadAll<Sprite>("Sprites/Enemies/Walk/" + name);
						uniMob.walkSprites = tmp0;
						Sprite[] tmp1 = Resources.LoadAll<Sprite>("Sprites/Enemies/Attack/" + name);
						uniMob.attackSprites = tmp1;
						uniMob.mob_SpriteRenderer.sprite = tmp1[Random.Range(0, tmp1.Length - 1)];
						uniMob.faceSprite = Resources.Load<Sprite>("Sprites/Enemies/Face/" + name);
						GameObject[] selection = Selection.gameObjects;
						foreach (GameObject go in selection)
						{
							string localPath = UniMob_Properties.UniMobsPrefabFolder + "(UniMob)_Lv" + uniMob.mobLevel.ToString("D3") + "_" + uniMob.mob_Name + ".prefab";
							while (AssetDatabase.LoadAssetAtPath(localPath, typeof(GameObject)))
							{
								localPath = UniMob_Properties.UniMobsPrefabFolder + "(UniMob)_Lv" + uniMob.mobLevel.ToString("D3") + "_" + uniMob.mob_Name + ".prefab";
								if (AssetDatabase.LoadAssetAtPath(localPath, typeof(GameObject)))
									break;
							}
							Object prefabMob = PrefabUtility.GetPrefabParent(go);
							if (prefabMob != null)
							{
								AssetDatabase.RenameAsset(AssetDatabase.GetAssetPath(prefabMob), go.name);
								PrefabUtility.SetPropertyModifications(prefabMob, PrefabUtility.GetPropertyModifications(PrefabUtility.GetPrefabObject(go)));
								PrefabUtility.ReplacePrefab(go, prefabMob, ReplacePrefabOptions.Default);
							}
							else
							{
								Object prefab = PrefabUtility.CreateEmptyPrefab(localPath);
								PrefabUtility.ReplacePrefab(go, prefab, ReplacePrefabOptions.ConnectToPrefab);
							}
						}
					}
					GUILayout.Space(12f);
				}
			}
		}
	}
}
