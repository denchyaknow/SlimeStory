﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Denchyaknow;
using DG.Tweening;
public class UniMob_Chat : MonoBehaviour {
	public static float LastTimePlayed = 0;
	public string[] chatOnSpawn =  null;
	public string[] chatOnAttack = null;
	public string[] chatOnDeath =  null;
	private float msgDelay = 5f;
	public bool testChat = false;
	private UniMob_Base mob
	{
		get
		{
			return transform.GetComponentInParent<UniMob_Base>();
		}
	}
	// Use this for initialization
	void Start ()
	{
		if (mob != null)
		{
			if (chatOnSpawn != null && chatOnSpawn.Length > 0)
			{
				mob.OnSpawn += PlaySpawnChat;
			}
			if (chatOnAttack != null && chatOnAttack.Length > 0)
			{
				mob.OnAttack += PlayAttackChat;
			}
			if (chatOnDeath != null && chatOnDeath.Length > 0)
			{
				mob.OnDeath += PlayDeathChat;
			}
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (testChat)
		{
			testChat = false;
			PlaySpawnChat();
		}
	}
	private bool PlayMessage(int max)
	{
		bool play = false;
		int chance = Random.Range(0,max * 14);
		int goal = max * 10;
		if (chance > goal)
			play = true;
		return play;
	}
	public void PlaySpawnChat()
	{
		int id = Random.Range(0, chatOnSpawn.Length - 1);
		bool msgPlaying = GameManager._Instance.messageManager.playingMessage;
		if (Time.timeSinceLevelLoad < LastTimePlayed)
			return;
		if (msgPlaying || !PlayMessage(chatOnSpawn.Length))
			return;
		LastTimePlayed = Time.timeSinceLevelLoad + msgDelay;
		MessageObj msg = new MessageObj((gameObject.name + "_" + gameObject.GetInstanceID()),chatOnSpawn[id],string.Empty,string.Empty,BaseToolTip.ToopTipType.Bubble,transform);
		bool played = GameManager._Instance.messageManager.SetNextMessage(msg);
		//Debug.Log("Played message =" + played);
	}
	public void PlayAttackChat()
	{
	}
	public void PlayDeathChat()
	{
	}
}
