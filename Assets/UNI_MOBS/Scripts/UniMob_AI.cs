﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
namespace Denchyaknow
{
	public class UniMob_AI : UniMob_Base
	{
		[HideInInspector] public bool canMove = false;
		public float moveRate = 0.5f;
		public float moveForce = 10f;
		private bool isIdling = false;
		private Vector2 moveTimeMinMax = new Vector2(1.6f, 2.4f);
		private float nextMoveTime = 0;
		private float lastMoveTime = 0;
		private Vector2 moveDisMinMax = new Vector2(1f, 2f);//mob will move a distance magnitude between the x and y of this vector.
		private float moveDisCached = 0;//the current distance the mob will move until it gets a new random distance
		private Vector2 lastMovePos = Vector2.zero;
		private Vector2 moveDirUpdateMinMax = new Vector2(2.4f,3.4f);//the time it takes to get a new random dir, value is between x and y
		private float lastMoveDirUpdate = 0;//last time mobs dir was updated
		private float nextMoveDirUpdate = 0;//next move update

		private Vector2 lastMoveDir = new Vector2();
		private Vector2 nextMoveDir
		{
			get
			{
				if (moveDirCache == 0)
					moveDirCache = Random.Range(0, 100) > 50 ? 1 : -1;
				Vector2 ranAngle = Vector2.right;
				ranAngle.x += Random.Range(moveDirXMinMax.x, moveDirXMinMax.y);
				ranAngle.y += Random.Range(moveDirYMinMax.x, moveDirYMinMax.y);
				Debug.DrawLine(transform.position, transform.position + (new Vector3(ranAngle.x * moveDirCache, ranAngle.y,0)), Color.gray);
				//Vector2 dir = (Random.insideUnitCircle * 5).normalized;
				return ranAngle;
			}
		}
		private Vector2 moveDirXMinMax = new Vector2(4.8f, 7.5f);
		private Vector2 moveDirYMinMax = new Vector2(-2.3f, 2.3f);
		private int moveDirCache = 0;
		public bool testMove = false;

		public override void OnEnable()
		{
			base.OnEnable();
			OnIdle += ProcessMovement;
			OnWalk += ProcessMovement;
		}
		public override void OnDisable()
		{
			base.OnDisable();
			OnIdle -= ProcessMovement;
			OnWalk -= ProcessMovement;

		}
		public override void Start ()
		{
			base.Start();

		}
		public override void Update ()
		{
			base.Update();

		}
		void UpdateDistance(bool forceUpdate = false)
		{
			float curDis = Vector2.Distance(lastMovePos, transform.position);
			if (curDis > moveDisCached || forceUpdate )
			{
				lastMovePos = transform.position;
				moveDisCached = Random.Range(moveDisMinMax.x, moveDisMinMax.y);
				isIdling = true;
				moveDirCache *= -1;
			}
		}
		void UpdateDirection(bool forceUpdate = false)
		{
			if (Time.timeSinceLevelLoad - lastMoveDirUpdate > 0 || forceUpdate)
			{
				nextMoveDirUpdate = Random.Range(moveDirUpdateMinMax.x, moveDirUpdateMinMax.y);
				lastMoveDirUpdate = Time.timeSinceLevelLoad + nextMoveDirUpdate;
				lastMoveDir = nextMoveDir;
			}

		}
		void ProcessMovement()
		{
			if(!mob_FreeWill || GameManager._Instance.gamePaused)
				return;
			if(closestAgroTarget == null)
			{
				MoveRandomly();
			} else
			{
				MoveAroundTarget();
			}
		}
		public override void MoveToPosition(Vector3 pos)
		{
			base.MoveToPosition(pos);
			StartCoroutine(MoveTo(pos));
		}
		public IEnumerator MoveTo(Vector3 pos)
		{
			Vector2 moveDir = Vector2.zero;
			while (Vector3.Distance(pos, transform.position) < 0.5f)
			{
				yield return new WaitForSeconds(0.5f);
				moveDir = (pos - transform.position).normalized;
				mob_Rigidbody.velocity = moveDir * moveForce;
			}
		}
		void MoveAroundTarget()
		{
			if((Time.timeSinceLevelLoad - lastMoveTime) < moveRate)
			{
				return;
			}
			lastMoveTime = Time.timeSinceLevelLoad;
			
			Vector2 movePos = GetMovePosition(closestAgroTarget);
			Vector2 moveDir = (movePos - (Vector2)transform.position).normalized;
			
			mob_Rigidbody.velocity = moveDir * moveForce;

		}
		Vector2 GetMovePosition(Transform target)
		{
			Vector2 moveRandom = Vector2.zero;
			Vector2 mobPos = Vector2.zero;
			Vector2 movePos = Vector2.zero;
			Vector2 moveOffset = Vector2.zero;
			Vector2 dir = (closestAgroTarget.position - transform.position).normalized;
			int dirI = 0;
			if(dir.x < 0)
				dirI = 1;
			if(dir.x > 0)
				dirI = -1;
			switch(attackType)
			{
				case UniMob_Properties.UniMob_AttackType.Melee:
					moveRandom = (Random.insideUnitCircle * (attackRange * 0.3f));
					mobPos = transform.position;
					moveOffset = (Vector2)closestAgroTarget.position + (Vector2.right * dirI * (attackRange * 0.8f));//get center point away from player

					movePos = (moveRandom + moveOffset);
					break;
				case UniMob_Properties.UniMob_AttackType.Ranged:
					moveRandom = (Random.insideUnitCircle * (attackRange * 0.8f));
					mobPos = transform.position;
					moveOffset = (Vector2)closestAgroTarget.position + (Vector2.right * dirI * attackRange);//get center point away from player
					Vector2 randomOffset = moveOffset + (Random.insideUnitCircle * (attackRange / 2));//get random point via offset

					float yDisFromOffset = randomOffset.y - moveOffset.y;
					float yMin = -attackRange;
					float yMax = attackRange;
					if(yDisFromOffset < 0)
						yMax += (-2 * yDisFromOffset);
					else if(yDisFromOffset > 0)
						yMin += (2 * yDisFromOffset);
					randomOffset.y += Random.Range(yMin, yMax);
					movePos = randomOffset;
					break;
			}
			return movePos;
		}
		void MoveRandomly()
		{
			UpdateDistance();
			UpdateDirection();
			if (isIdling)
			{
				if (Time.timeSinceLevelLoad - lastMoveTime > 0)
				{
					nextMoveTime = Random.Range(moveTimeMinMax.x, moveTimeMinMax.y);
					lastMoveTime = Time.timeSinceLevelLoad + nextMoveTime;
					isIdling = false;
				}
				return;
			}
			Vector2 curDir = lastMoveDir;
			curDir.x *= moveDirCache;
			if ((mob_Rigidbody.velocity.x < -0.2f && curDir.x > 0.2f) || (mob_Rigidbody.velocity.x > 0.2f && curDir.x < -0.2f))
			{
				curDir.x *= -1;
				moveDirCache *= -1;
			}
			mob_Rigidbody.velocity = Vector2.Lerp(mob_Rigidbody.velocity, curDir * moveForce, Time.smoothDeltaTime * moveRate);
		}
		public override void OnDrawGizmosSelected()
		{
			base.OnDrawGizmosSelected();
			Gizmos.color = Color.red;
			Vector2 moveRandom = Vector2.zero;
			Vector2 mobPos = Vector2.zero;
			Vector2 movePos = Vector2.zero;
			Vector2 moveDir = Vector2.zero;
			Vector2 moveOffset = Vector2.zero;
			Vector3 gPos;
			if(closestAgroTarget != null)
			{
				gPos = closestAgroTarget.position;

			} else
			{
				gPos = transform.position;

			}
			Vector2 dir = closestAgroTarget != null ? (Vector2)((closestAgroTarget.position - transform.position).normalized) : Vector2.right;
					
			int dirI = 0;
			if(dir.x < 0)
				dirI = 1;
			if(dir.x > 0)
				dirI = -1;
			switch(attackType)
			{
				case UniMob_Properties.UniMob_AttackType.Melee:
					moveRandom = (Random.insideUnitCircle * (attackRange * 0.3f));
					mobPos = transform.position;
					moveOffset = (Vector2)gPos + (Vector2.right * dirI * (attackRange * 0.8f));//get center point away from player

					movePos = (moveRandom + moveOffset);
					Gizmos.DrawLine(movePos, (Vector2)gPos);
					Gizmos.DrawLine(movePos, moveOffset);
					Gizmos.DrawWireSphere(moveOffset, 0.2f);
					break;
				case UniMob_Properties.UniMob_AttackType.Ranged:
					moveRandom = (Random.insideUnitCircle * (attackRange * 0.8f));
					mobPos = transform.position;
					moveOffset = (Vector2)gPos + (Vector2.right * dirI * attackRange);//get center point away from player
					Gizmos.DrawWireSphere(moveOffset, attackRange / 2);
					Gizmos.DrawLine(moveOffset, gPos);
					Vector2 randomOffset = moveOffset + (Random.insideUnitCircle * (attackRange / 2));//get random point via offset
					Gizmos.DrawLine(moveOffset, randomOffset);
					Gizmos.DrawWireSphere(randomOffset, 0.2f);
					Vector3 newRandomOffset = randomOffset;
					float yDisFromOffset = newRandomOffset.y - moveOffset.y;
					float yMin = -attackRange;
					float yMax = attackRange;
					if(yDisFromOffset < 0)
						yMax += (-2 * yDisFromOffset);
					else if(yDisFromOffset > 0)
						yMin += (2 * yDisFromOffset);
					newRandomOffset.y += Random.Range(yMin, yMax);
					movePos = newRandomOffset;
					Gizmos.DrawLine(randomOffset, newRandomOffset);
					Gizmos.DrawWireSphere(newRandomOffset, 0.2f);
					break;
			}
			
		}
	}
}
