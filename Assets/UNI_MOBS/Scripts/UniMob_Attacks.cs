﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Denchyaknow
{

	public class UniMob_Attacks : UniMob_AI
	{
		
		public List <GameObject> attacks = new List<GameObject>();
		private BaseAttack baseAttack = null;
		public override void OnEnable()
		{
			OnAttack += ProcessAttack;
			base.OnEnable();
			//OnDeath += DisableAllAttacks;
		}
		public override void OnDisable()
		{
			OnAttack -= ProcessAttack;
			base.OnDisable();
			//OnDeath -= DisableAllAttacks;

		}
		public override void Start()
		{
			base.Start();

		}
		public override void Update()
		{
			base.Update();
	
		}
		//void DisableAllAttacks()
		//{
		//	if(attacks.Count > 0)
		//	{
		//		foreach(GameObject att in attacks)
		//		{
		//			att.transform.GetComponent<BaseAttack>().OnParentDeath();
		//		}
		//	}
		//}
		void ProcessAttack()
		{
			if(!isAttacking)
				return;
			else
				isAttacking = false;
			//lastMoveTime = Time.timeSinceLevelLoad + Random.Range(0,moveRate) + (Random.Range(0,attackDelay));
			baseAttack = ReloadedAttack();
			Vector2 moveDir = (closestAgroTarget.position - transform.position).normalized;
			float _angle = Mathf.Atan2(moveDir.y, moveDir.x) * Mathf.Rad2Deg * -1;
			attackRotation = (Quaternion.AngleAxis(_angle, Vector3.forward)).eulerAngles;//rotate transform towards target

			switch((int)attackType)
			{
				case 0:
					mob_Rigidbody.velocity = attackMoveForce * moveDir;
					baseAttack.Fire(closestAgroTarget.position);
					break;
				case 1:
					baseAttack.Fire(closestAgroTarget.position);

					break;
			}

		}
		public BaseAttack ReloadedAttack()
		{
			BaseAttack baseAtt = null;
			//BaseMob baseMob = transform.GetComponent<BaseMob>();
			if((GameManager._Instance != null ? (!GameManager._Instance.DoNotPool) : false) && attacks.Count > 0)
			{

				for(int i = 0; i < attacks.Count; ++i)
				{
					if(attacks[i] == null)
					{
						attacks.Remove(attacks[i]);
					} else if(!attacks[i].activeInHierarchy)
					{
						attacks[i].SetActive(true);
						baseAtt = attacks[i].GetComponent<BaseAttack>();
						break;
					}
				}
			} else if((GameManager._Instance != null ? (GameManager._Instance.DoNotPool) : false) && attacks.Count > 0)
			{
				for(int i = 0; i < attacks.Count; i++)
				{
					if(attacks[i] != null)
						Destroy(attacks[i].gameObject);
				}
				attacks.Clear();
			}
			if(baseAtt == null)
			{
				int classType = attackID;
				
				baseAtt = Instantiate(GameManager._Instance.database.Attacks[classType], hidden).GetComponent<BaseAttack>();
				if((GameManager._Instance != null?(!GameManager._Instance.DoNotPool): false))
					attacks.Add(baseAtt.gameObject);
			}

			baseAtt.transform.SetParent(hidden);
			baseAtt.transform.localPosition = Vector3.zero;
			baseAtt.parent = hidden;
			baseAtt.gameObject.SetActive(true);
			baseAtt.Setup(this);
			baseAtt.enabled = true;
			return baseAtt;
		}
	}
}
