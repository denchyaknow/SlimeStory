﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.Rendering;
namespace Denchyaknow
{
	[System.Serializable]
	public class UniMob_Base : MonoBehaviour
	{
		public enum BehaviourType
		{
			Aggressive
		}
		[HideInInspector] public string mob_Name;
		public BehaviourType mob_Behaviour;
		[HideInInspector] public bool mob_FreeWill = true;// let mob do what ever it wants?
		[HideInInspector] public bool mob_GodMode = false;// Duh
		[HideInInspector] public bool mob_SpriteFlipToVelocity = true;// should the mobs sprite flip to the direction its walking?
		[HideInInspector] public bool mob_SpriteFlipToTarget = true;//Overrides fliptovelocity, when a target is locked on it will flip to that always
		private bool mobDied = false;
		#region Stats
		
		[HideInInspector] public int mobLevel = 0;
		[HideInInspector] public int mobExp = 0;
		[HideInInspector] public int mobHealth = 0;
		[HideInInspector] public int mobHealthMax = 0;
		[HideInInspector] public int mobCoin = 0;
		[HideInInspector] public int mobDamage = 0;
		[HideInInspector] public float mobScale = 0;
		//these are the base stats of the mob, if they are all set to 0 the mob will be as strong as the default scale that mobs level up at
		[HideInInspector] public float mobBaseHealth = 0;
		[HideInInspector] public float mobBaseExp = 0;
		[HideInInspector] public float mobBaseCoin = 0;
		[HideInInspector] public float mobBaseDamage = 0;
		[HideInInspector] public float mobBaseScale = 0;

		#endregion
		[HideInInspector] public float spawnDelay = 0.8f;
		[HideInInspector] public float agroRadius = 10f;// The radius at which the mob would see the player during a patrol
		[HideInInspector] public float attackRange = 5f;// The radius at which the mob would launch an attack on the player
		[HideInInspector] public float attackCooldown = 1;
		[HideInInspector] public int attackID = 0;
		[HideInInspector] public float attackDelay = 0;
		[HideInInspector] public float attackTime = 1;
		[HideInInspector] public float attackMoveForce = 5;
		[HideInInspector] public float attackHitForce = 200;
		[HideInInspector] public UniMob_Properties.UniMob_AttackType attackType;
		[HideInInspector] public Vector3 attackRotation = new Vector3();
		public UniMob_Spawner spawner;
		public LayerMask whatIsPlayer;
		public LayerMask whatIsWall;
		[HideInInspector] public bool isAttacking = false;
		
		private float lastAttackTime;

		private Collider2D[] agroTargets = null;
		private float lastHitTime;
		private float lastScanTime;
		private Vector3 previousPosition;//Used to get velocity
		[HideInInspector] public Transform closestAgroTarget = null;
		[HideInInspector] public Vector2 direction = Vector2.zero;
		[HideInInspector] public int agroDirection = 0;
		[HideInInspector] public float magnitude = 0;
		public Vector2 velocity
		{
			get
			{
				Vector3 vel = (transform.position - previousPosition) / Time.deltaTime;
				return vel;
			}
		}
		private int walkFrame = 0;
		private int idleFrame = 0;
		private int attackFrame = 0;
		private int[] idleFrames;
		private int[] attackFrames;

		public Delegate OnIdle;
		public Delegate OnWalk;
		public Delegate OnAttack;
		public Delegate OnHit;
		public Delegate OnDeath;
		public Delegate OnSpawn;
		public delegate void Delegate();
		private Delegate OnState;

		[Header("Important Refferences")]
		public UniMob_Lvl lvlTxt;
		public UniMob_HpBar hpBar;
		public Transform hp_Bar;
		private SpriteRenderer hp_Fill;
		private float hp_FillSize;
		private float hp_FadeTime = 0.8f;//0 = Always showing, else fade over time hp bar
		public Rigidbody2D mob_Rigidbody;
		public Collider2D mob_HitBox;
		public SpriteRenderer mob_SpriteRenderer;
		public Transform mob_EffectsParent;
		public Transform hidden;

		[HideInInspector] public Sprite[] attackSprites;
		[HideInInspector] public Sprite[] walkSprites;
		[HideInInspector] public Sprite faceSprite;
		private float frameRate = 0.08f;
		private float lastFrameTime;
		private SortingGroup mob_SortingGrp;
		private bool sortingSynced = false;
		public int sortingOrder
		{
			get
			{
				int pos = -(int)Camera.main.WorldToScreenPoint(transform.position).y;
				return pos;
			}
		}
		//public bool testDeath = false;
		public virtual void Awake()
		{
			InitializeEffects();
			hpBar = transform.GetComponentInChildren<UniMob_HpBar>();
			hpBar.attachedUniMob = this;
			lvlTxt = transform.GetComponentInChildren<UniMob_Lvl>();
			lvlTxt.attachedUniMob = this;
			mob_SortingGrp = transform.GetComponent<SortingGroup>();

		}
		public virtual void Start()
		{
			lastHitTime = 0;
			lastFrameTime = 0;
			lastAttackTime = 0;
			lastScanTime = 0;
			
			idleFrames = UniMob_Properties.GetIdleFrames();
			attackFrames = UniMob_Properties.GetAttackFrames(attackType);
		}
		public void LateUpdate()
		{
			previousPosition = transform.position;
			if (mob_SortingGrp != null && GameManager._Instance.combatManager.playerSlime != null)
			{
				if (sortingSynced == false)
				{
					sortingSynced = true;
					mob_SortingGrp.sortingLayerID = GameManager._Instance.combatManager.playerSlime.mySortingGroup.sortingLayerID;
				}
				mob_SortingGrp.sortingOrder = sortingOrder;
			}
		}
		public virtual void Update()
		{
			
			if(mobDied || GameManager._Instance.gamePaused)
				return;
			if (OnState == null)
			{
				OnState = OnIdle;
				magnitude = velocity.magnitude;
				direction = velocity.normalized;
				if (magnitude > 0.12f)//Mob is walking
				{
					OnState = OnWalk;
				}
			}
			//At a fixed rate scan for group of players, might be overkill but hey its automatic and dynamic
			if (Time.timeSinceLevelLoad > lastScanTime)
			{
				lastScanTime = Time.timeSinceLevelLoad + 1.5f;
				agroTargets = Physics2D.OverlapCircleAll(transform.position, agroRadius, whatIsPlayer);

				//target closest or with most agro
			}
			//Getting Closest target
			float distance = 0;
			if (agroTargets != null && agroTargets.Length > 0)
			{
				distance = Vector3.Distance(transform.position, agroTargets[0].transform.position);
				if (distance < agroRadius)
					closestAgroTarget = agroTargets[0].transform;
				else
					distance = agroRadius;
				for(int i = 0; i < agroTargets.Length; i++)
				{
					if (agroTargets[i] != null)
					{
					float tryDis = Vector3.Distance(transform.position, agroTargets[i].transform.position);

					if(tryDis < distance)
						closestAgroTarget = agroTargets[i].transform;
					}
				}
			}
			else
			{
				closestAgroTarget = null;
			}
			if (closestAgroTarget == transform)
				closestAgroTarget = null;
			if(closestAgroTarget != null && !GameManager._Instance.combatManager.playerSlime.isDead)
			{
				
				agroDirection = (closestAgroTarget.position - transform.position).x > 0? 1 : -1;
				switch (mob_Behaviour)// Might add in other behaviours like 
				{
					case BehaviourType.Aggressive:
						float moddedAttackRange = (attackType == UniMob_Properties.UniMob_AttackType.Ranged)?(attackRange * 2):attackRange;
						float moddedDistance = distance;
						switch(attackType)
						{
							case UniMob_Properties.UniMob_AttackType.Melee:
								float vDis;
								
								vDis = transform.position.y - closestAgroTarget.position.y;
								if(vDis < 0)
									vDis *= -1;//we need a positive value to add to distance
								moddedDistance += vDis;
								
								break;
							case UniMob_Properties.UniMob_AttackType.Ranged:

								break;
						}
					
					
						if (moddedDistance < moddedAttackRange && Time.timeSinceLevelLoad > lastAttackTime && mob_FreeWill && !GameManager._Instance.gamePaused)
						{
							lastAttackTime = Time.timeSinceLevelLoad + attackCooldown;
							isAttacking = true;
							OnState = OnAttack;
						}
						break;
				}

			}


			if (OnState != null && lastHitTime < Time.timeSinceLevelLoad)
			{
				OnState();
			}
		}
		public virtual void OnEnable()
		{
			OnWalk += PlayWalkAnimation;
			OnIdle += PlayIdleAnimation;
			OnAttack += PlayAttackAnimation;
			OnDeath += ProcessDeath;
			OnDeath += ProcessRewards;
			OnDeath += RemoveFromCombat;
			OnState = OnIdle;
			OnSpawn += FinishSpawn;
			ProcessSpawn();

		}
		public virtual void OnDisable()
		{
			OnWalk -= PlayWalkAnimation;
			OnIdle -= PlayIdleAnimation;
			OnAttack -= PlayAttackAnimation;
			OnDeath -= ProcessDeath;
			OnDeath -= ProcessRewards;
			OnDeath -= RemoveFromCombat;
			OnSpawn -= FinishSpawn;
			OnState = null;

		}
		public void RemoveFromCombat()
		{
			if (GameManager._Instance.combatManager != null && GameManager._Instance.combatManager.enemies.Contains(this))
			{
				GameManager._Instance.combatManager.enemies.Remove(this);
			}
		}
		public virtual void ProcessSpawn()
		{
			mobDied = true;
			SetStats();
			foreach(SpriteRenderer sprite in transform.GetComponentsInChildren<SpriteRenderer>())
			{
				sprite.enabled = false;
			}
			foreach(Collider2D collider in transform.GetComponentsInChildren<Collider2D>())
			{
				collider.enabled = false;
			}
			StartCoroutine(DelayedSpawn());
		}
		private IEnumerator DelayedSpawn()
		{
			yield return new WaitForSeconds(spawnDelay);
			OnSpawn();
		}
		public void FinishSpawn()
		{
			mobDied = false;
			//PlayEffects(4);
			foreach (SpriteRenderer sprite in transform.GetComponentsInChildren<SpriteRenderer>())
			{
				sprite.enabled = true;
			}
			foreach (Collider2D collider in transform.GetComponentsInChildren<Collider2D>())
			{
				collider.enabled = true;
			}
			if (GameManager._Instance.combatManager != null && !GameManager._Instance.combatManager.enemies.Contains(this))
			{
				GameManager._Instance.combatManager.enemies.Add(this);
			}
		}
		public virtual void ProcessHit()
		{
			PlayEffects(2);
		}
		public virtual void ProcessDeath()
		{
			float timer = 255f;
			if(GameManager._Instance.combatManager != null)
				GameManager._Instance.combatManager.AddExperience(this);
			//PlayEffects(3);
			GameManager._Instance.combatManager.KillAMob(this);
			//StartCoroutine(DelayedDeath());
		
		}
		public void DisablePhysics()
		{
			foreach (Collider2D collider in transform.GetComponentsInChildren<Collider2D>())
			{
				collider.enabled = false;
			}
		}
		public void DisableRenderers()
		{
			foreach (SpriteRenderer sprite in transform.GetComponentsInChildren<SpriteRenderer>())
			{
				sprite.enabled = false;
			}
		}
		public void DisableTweeners()
		{
			transform.GetComponentInChildren<UniMob_HpBar>().enabled = false;
		}
		private IEnumerator DelayedDeath()
		{
			
			yield return new WaitForSeconds(2f);
			
			yield return new WaitForSeconds(3f);
			Destroy(gameObject);
		}
		public virtual void ProcessRewards()
		{
			int coin = mobCoin;
			GameManager._Instance.coinManager.SpawnCoin(transform.position, coin);
		}
		//public Vector3 baseCoin = UniMob_Properties.UniMobBaseCoin;
		public virtual void SetStats()
		{
			mobHealth = UniMob_Properties.GetMobHealth(mobLevel, mobBaseHealth);
			mobHealthMax = UniMob_Properties.GetMobHealth(mobLevel, mobBaseHealth);
			mobDamage = UniMob_Properties.GetMobDamage(mobLevel, mobBaseDamage);
			mobExp = UniMob_Properties.GetMobExperience(mobLevel, mobBaseExp);
			//mobCoin = UniMob_Properties.GetMobCoin(mobLevel, baseCoin.y, baseCoin.z, baseCoin.x);
			mobCoin = UniMob_Properties.GetMobCoin(mobLevel, mobBaseCoin);
			mobScale = UniMob_Properties.GetMobScale(mobLevel, mobBaseScale);
		}
		public virtual void MoveToPosition(Vector3 pos)
		{

		}
		public virtual void TakeDamage(int _amount)
		{
			lastHitTime = Time.timeSinceLevelLoad + 0.25f;
			mobHealth -= _amount;
			if (mobHealth <= 0 && !mobDied)
			{
				mobDied = true;
				OnDeath();
				if(GameManager._Instance != null && GameManager._Instance.soundManager != null)
					GameManager._Instance.soundManager.PlayMobDeathScream();
			}
			else
			{
				OnHit();
				if (GameManager._Instance != null && GameManager._Instance.soundManager != null)
					GameManager._Instance.soundManager.PlayMobOnHit();
			}
			if (GameManager._Instance != null && GameManager._Instance.uiManager != null)
			{
					GameManager._Instance.uiManager.LaunchCounter(transform.position, _amount, Color.red);
					GameManager._Instance.uiManager.ShakeCamera();
			}
		}
		
		private int fxIndex = 0;
		public void InitializeEffects()
		{
			if (mob_EffectsParent == null)
			{
				Debug.Log("Slime has no particle parent for effects assign to mob_EffectsParent transform and make sure it has childrne with effects");
				return;
			}
			foreach (ParticleSystem pSystem in mob_EffectsParent.GetComponentsInChildren<ParticleSystem>())
			{
				ParticleSystem.MainModule pMain = pSystem.main;
				pMain.loop = false;
				pMain.playOnAwake = false;
				pSystem.time = 0;
				pSystem.Stop(true);
			}
		}
		/// <summary>
		/// 0 = OnAttack, 1 = OnWalk, 2 = OnHit, 3 = OnDeath, 4 = OnSpawn 
		/// </summary>
		/// <param name="cIndex"></param>
		public void PlayEffects(int cIndex)
		{
			// Register your effect, Make a transform, name it fxNameOfEffect
			// Get the Child Index of the new effects transform, Call PlayEffects(childIndex) anywhere
			// Once you add a child transform you cannot remove it just turn off emission in 
			mob_EffectsParent.localScale = new Vector3((velocity.x > 0? -1:1), 1, 1);
			int childCount = mob_EffectsParent.childCount;
			if (childCount > 0)
			{
				for (int i = 0; i < childCount; ++i)
				{
					if (i == cIndex)
					{
						int childParticleCount = mob_EffectsParent.GetChild(i).transform.childCount;
						if (childParticleCount > 0)
						{
							foreach(ParticleSystem pSys in mob_EffectsParent.GetChild(i).transform.GetComponentsInChildren<ParticleSystem>())
							{
								if(pSys.gameObject.activeInHierarchy)
								{
									ParticleSystem.MainModule pMain = pSys.main;
									pMain.loop = false;
									pSys.time = 0;
									pSys.Play(true);
								}
							}
							fxIndex = cIndex;
						} else
						{
							Debug.Log("No Particles for effect: " + mob_EffectsParent.GetChild(i).gameObject.name);
						}
						break;

					}
				}
			}
		}
#region Animations
		void PlayIdleAnimation()
		{
			if (attackSprites == null || attackSprites.Length == 0)
			{
				Debug.Log("Link to Idle sequence is lost, Please reinstantiate prefab or reimport package");
				return;
			}
			if (idleFrame >= idleFrames.Length)
				idleFrame = 0;

			if (Time.timeSinceLevelLoad - frameRate > lastFrameTime)
			{
				lastFrameTime = Time.timeSinceLevelLoad;
				float xDir = 0;
				if (mob_SpriteFlipToTarget && closestAgroTarget != null)
				{
					Vector2 trgDir = agroTargets[0].transform.position - transform.position;
					xDir = trgDir.x;
				}
				else if (mob_SpriteFlipToVelocity)
				{
					xDir = velocity.x;
				}
				if(xDir > 0)
					mob_SpriteRenderer.flipX = true;
				else if(xDir < 0)
					mob_SpriteRenderer.flipX = false;
					mob_SpriteRenderer.sprite = attackSprites[idleFrames[idleFrame]];
				//myRenderer.flipX = dir == 1 ? false : true;

				idleFrame++;
			}

			OnState = null;
		}
		void PlayWalkAnimation()
		{
			if (walkSprites == null || walkSprites.Length == 0)
			{
				Debug.Log("Link to Walk sequence is lost, Please reinstantiate prefab or reimport package");
				return;
			}
			if (Time.timeSinceLevelLoad - frameRate > lastFrameTime)
			{
				lastFrameTime = Time.timeSinceLevelLoad;
				int start = 0;
				int end = 2;
				if (walkFrame > end)
					walkFrame = start;
				float xDir = 0;
					//mob_SpriteRenderer.flipX = true;
				if (mob_SpriteFlipToTarget && closestAgroTarget != null)
				{
					Vector2 trgDir = agroTargets[0].transform.position - transform.position;
					xDir = trgDir.x;
				} else if (mob_SpriteFlipToVelocity)
				{
					xDir = velocity.x;
				}
				//if(xDir > 0)
				//{
				//	start = 0;
				//	end = 2;
				//} else if(xDir < 0)
				//{
				//	start = 3;
				//	end = 5;
				//}
				
				
				if (xDir > 0)
					mob_SpriteRenderer.flipX = true;
				else if (xDir < 0)
					mob_SpriteRenderer.flipX = false;
				mob_SpriteRenderer.sprite = walkSprites[walkFrame];
				PlayEffects(1);

				walkFrame++;
			}
			OnState = null;
		}
		void PlayAttackAnimation()
		{
			if (attackSprites == null || attackSprites.Length == 0)
			{
				Debug.Log("Link to Attack sequence is lost, Please reinstantiate prefab or reimport package");
				return;
			}
			if (attackFrame >= attackFrames.Length)
			{
				OnState = null;
				attackFrame = 0;
				return;
			}
			if (Time.timeSinceLevelLoad - frameRate > lastFrameTime)
			{
				lastFrameTime = Time.timeSinceLevelLoad;
				float xDir = 0;
				if (mob_SpriteFlipToTarget && closestAgroTarget != null)
				{
					Vector2 trgDir = agroTargets[0].transform.position - transform.position;
					xDir = trgDir.x;
				}
				else if (mob_SpriteFlipToVelocity && velocity.normalized.magnitude > 0.3f)
				{
					xDir = velocity.x;
				}
				if(xDir > 0)
					mob_SpriteRenderer.flipX = true;
				else if(xDir < 0)
					mob_SpriteRenderer.flipX = false;
				mob_SpriteRenderer.sprite = attackSprites[attackFrames[attackFrame]];
				PlayEffects(0);
				attackFrame++;
			}

		}
#endregion

		public virtual void OnDrawGizmosSelected()
		{
			Gizmos.color = agroTargets == null ? Color.red : Color.green;
			Gizmos.DrawWireSphere(transform.position, agroRadius);
			Gizmos.color = Color.magenta;
			Gizmos.DrawWireSphere(transform.position, attackRange);
			
		}
	}

}
