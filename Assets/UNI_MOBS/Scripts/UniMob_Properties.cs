﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Denchyaknow
{

	[System.Serializable]
	public static class UniMob_Properties
	{
		//public Rect editorWindowRect = new Rect();
		public const string UniMobsRootFolder = "Assets/UNI_MOBS/";
		public const string UniMobsPrefabFolder = "Assets/Resources/GameObjects/Enemies/";
		public static bool UniMob_HpBarHides = true;
		public enum UniMob_AttackType
		{
			Melee = 0,
			Ranged = 1

		}
		public static int[] idleFrames = new int[4] { 0, 1, 2, 1 };

		public static int[] GetIdleFrames()
		{
			return idleFrames;
		}

		public static int[] attackPunchFrames = new int[5] { 2, 3, 4, 5, 5 };
		public static int[] attackSpellFrames = new int[6] { 18, 19, 20, 21, 22, 23 };

		public static int[] GetAttackFrames(UniMob_AttackType type)
		{
			switch ((int)type)
			{
				case 0:
					return attackPunchFrames;
				case 1:
					return attackSpellFrames;
			}
			return null;
		}
		public static Vector2 Window_Bordr = new Vector2(24, 16);
		public static Vector2 Window_Min = new Vector2(232, 384);
		public static Vector2 Window_Max = new Vector2(484, 712);
		public static Vector2 Button_Row = new Vector2(32, 24);
		public static Vector2 Button_Large = new Vector2(128, 128);
		public static Vector2 Button_Med = new Vector2(64, 64);
		public static Vector2 Button_Small = new Vector2(32, 32);
		public static Vector2 Button_Smallest = new Vector2(16, 16);
		public static int UniMobsPerRow = 3;
		public static int UniMobsPerColumn = 6;

		public static Vector3 UniMobBaseHealth = new Vector3(25f, 1.0456f, 1.02543f);
		public static Vector3 UniMobBaseExp = new Vector3(25f, 1.041f, 1.002f);
		public static Vector3 UniMobBaseDamage = new Vector3(4f, 1.0456f, 1.005f);
		public static Vector3 UniMobBaseCoin = new Vector3(0.005f, 0.015f, 0.0005f);
		public static Vector3 UniMobBaseScale = new Vector3(0.5f, 0.08f, 0.08f);

		
		public static int GetMobHealth(int level, float modBase = 0)
		{
			int hp = (int)(UniMobBaseHealth.x + modBase);
			for (int i = 0; i < level; ++i)
			{
				hp += ScaledHP((int)(UniMobBaseHealth.x + modBase), UniMobBaseHealth.y, UniMobBaseHealth.z, level);
			}
			return hp;

		}
		public static int GetMobExperience(int level, float modBase = 0)
		{
			int exp = (int)(UniMobBaseExp.x + modBase);
			for(int i = 0; i < level; ++i)
			{
				exp += ScaledEXP(UniMobBaseExp.x + modBase, UniMobBaseExp.y, UniMobBaseExp.z, level);
			}
			return exp;
		}
		public static int GetMobDamage(int level, float modBase = 0)
		{
			int damage = (int)(UniMobBaseDamage.x + modBase);
			for(int i = 0; i < level; ++i)
			{
				damage += ScaledDamage(UniMobBaseDamage.x + modBase, UniMobBaseDamage.y, UniMobBaseDamage.z, level);
			}
			return damage;
		}
		public static int GetMobCoin(int level, float modBase = 0)
		{
			
			int coin = Random.Range((int)(level * 0.4f), (int)(level * 1.4f));
			if (coin <= 0)
				coin = 1;
			//int hp = GetMobHealth(level, + modBase);
			//int coin = ScaledCoin(hp, UniMobBaseCoin.y, UniMobBaseCoin.z, level, UniMobBaseCoin.x);
			//for(int i = 0; i < level; ++i)
			//{
			//	coin += ScaledCoin(hp, UniMobBaseCoin.y, UniMobBaseCoin.z, level, UniMobBaseCoin.x);
			//}
			return coin;
		}
		//public static int GetMobCoin(int level, float baseY, float baseZ, float baseX)
		//{
		//	int hp = GetMobHealth(level);
		//	int coin = ScaledCoin(hp, baseY, baseZ, level, baseX);
		//	for (int i = 0; i < level; ++i)
		//	{
		//		coin += ScaledCoin(hp, baseY, baseZ, level, baseX);
		//	}
		//	return coin;
		//}
		public static float GetMobScale(int level, float modBase = 0)
		{
			float scale = UniMobBaseScale.x + modBase;
			for(int i = 0; i < level; ++i)
			{
				scale += 0.01f * ScaledTransform(level, UniMobBaseScale.x + modBase, UniMobBaseScale.y, UniMobBaseScale.z);
			}
			if(scale > 4f)
				scale = 4f;
			return scale;
		}

		public static int ScaledHP(float baseHP, float rate, float flat, int stage)
		{
			//b * (1 + (level - 1) * r) + (f * (level - 1))
			float hp = (baseHP * (Mathf.Pow(rate, (Mathf.Min(stage, 115))) * (Mathf.Pow(flat, (Mathf.Max((stage - 115), 0))))));
			return (int)hp;
		}
		public static int ScaledDamage(float baseDM, float rate, float flat, int stage)
		{
			float damage = (baseDM * (Mathf.Pow(rate, (Mathf.Min(stage, 115))) * (Mathf.Pow(flat, (Mathf.Max((stage - 115), 0))))));
			return (int)damage;
		}
		public static int ScaledEXP(float baseEX, float rate, float flat, int stage)
		{
			//b * (1 + (level - 1) * r) + (f * (level - 1))
			//float exp = _base * (1 + (_level - 1) * _rate) + (_flat * (_level - 1
			float exp = (baseEX * (Mathf.Pow(rate, (Mathf.Min(stage, 115))) * (Mathf.Pow(flat, (Mathf.Max((stage - 115), 0))))));
			return (int)exp;

		}
		public static int ScaledCoin(int baseCO, float rate, float flat, int stage, float bonus)
		{
			//b * (1 + (level - 1) * r) + (f * (level - 1))
			float coin = baseCO * rate + flat * Mathf.Min(stage, 150) * bonus;
			return (int)coin;

		}
		public static float ScaledTransform(int _level, float _base, float _rate, float _flat)
		{
			//b * (1 + (level - 1) * r) + (f * (level - 1))

			float xyz = _base * (1 + (_level - 1) * _rate) + (_flat * (_level - 1));

			return xyz;
		}
		public static string[] RandomNames =
{
"Bibble",
"doodle sack",
"Chaos Magician",
"Mystic Corruptor",
"grave Demonologist",
"Chaos Templar",
"Castle Slave",
"Heresy Witch",
"Dream Sacrificer",
"Agony Warlock",
"Heresy Evoker",
"Spirit Zealot",
"Royal Corruptor",
"Sorcerous Sinner",
"Royal Cultist",
"Murder Enchanter",
"Sorcerous Mutilator",
"Plague Prophet ",
"Spell Executioner ",
"Holy Gravedigger ",
"Ghost Gravedigger ",
"Dream Heretic ",
"Royal Blasphemer ",
"Bloody Astrologer ",
"Heresy Gladiator ",
"Apocalypse Shaman ",
"Royal Corruptor ",
"Ghost Slaughterer ",
"Devil Mage ",
"Sorcerous Blasphemer ",
"Corrupt Elementalist",
"Medical Gravedigger ",
"Mathematical Witch ",
"Necromantic Prophet ",
"Holy Pharmacist ",
"Horse Surgeon ",
"Heresy Mutant ",
"Advertising Torturer ",
"Horror Nurse ",
"Turbine Blasphemer ",
"Unholy Lawyer ",
"Chemical Dungeoneer ",
"Accounting Invoker ",
"Interfaced Cowboy ",
"Malice Rancher ",
"Pain Rancher ",
"Malice Witch ",
"Wired Warlock ",
"Graviton Cultist ",
"Vampiric Biologist ",
"Murder Banker ",
"Alchemical Doctor ",
"Plague Pioneer ",
"Scroll Librarian ",
"Grenade Torturer ",
"Spiritual Slave ",
"Automotive Driver ",
"Investment Mutilator ",
"Mystic Chemist ",
"Heresy Linguist ",
"Castle Homesteader ",
"Burglar Fop ",
"Chimneysweep Alchemist ",
"Dietician Warlock ",
"Duelist Chimneysweep ",
"Housekeeper Mercenary ",
"Jockey Prophet ",
"Milkmaid Soceress ",
"Oracle Bartender ",
"Paladin Landscaper ",
"Priest Jockey ",
"Sales Associate Soldier ",
"Soceress Nanny ",
"Soldier Butler ",
"Warlock Laundryman ",
"Windowwasher Mentalist ",
"Adventurer Gigolo ",
"Archer Gigolo ",
"Barbarian Watchmaker ",
"Chimneysweep Bard ",
"Halberdier Panhandler ",
"Magic User Florist ",
"Martial Artist Waiter ",
"Monk Babysitter ",
"Ninja Dietician ",
"Receptionist Martial Artist ",
"Salesman Priest ",
"Warrior Watchmaker ",
"Watchmaker Conjurer ",
"Weaver Mystic ",
"Witch Banker "

};
		public static string[] RandomAttackNames = 
		{
"Atomo Meteor ",
"Charming Annihilation ",
"Chronal Ballad ",
"Cut Peace ",
"Ebony Illusion ",
"Gadget Spirit ",
"Geologic Dart ",
"Healer Song ",
"Invulnerable Shriek ",
"Lovely Barrage ",
"Magic Sprout ",
"Music Rush ",
"Mystical Scream ",
"Paragon Apocalypse ",
"Posedion Illumination ",
"Sonic Frenzy ",
"Spy Penetrator ",
"Stigmata Stream ",
"Transition Slash ",
"Witchcraft Cremator ",
"Burning Detection ",
"Earthquake Flare ",
"False Touch ",
"Fatal Contemplation ",
"Germ Carapace ",
"Illumination Mandala ",
"Necro Shriek ",
"Orbiting Talisman ",
"Pierce Ballad ",
"Prayer Seeker ",
"Pulse Veil ",
"Ranged Spin ",
"Remote Slicer ",
"Romantic Rescue ",
"Sanctum Petrification ",
"Sentinel Slasher ",
"Shear Flight ",
"Sickness View ",
"Wire Beam ",
"Zephyr Glow ",
"Alchemist Burner ",
"Alteration Phantom ",
"Bacteria Divination ",
"Berserk Slayer ",
"Burning Fissure ",
"Crushing Chill ",
"Eclipse Snare ",
"Evolution Kill ",
"Finder Net ",
"Flickering Flare ",
"Illusionist Rhapsody ",
"Past Blast ",
"Seeker Lance ",
"Slayer Talisman ",
"Spiralling Sanctum ",
"Thought Seeking ",
"Timber Laser ",
"Tornado Awareness ",
"Tremor Warp ",
"Vision Flamer ",
"Aero Bullet ",
"Beauty Volcano ",
"Blazing Wall ",
"Brute Splash ",
"Dying Grenade ",
"Ember Avalanche ",
"Health Director ",
"Invulnerable Explosion ",
"Killer Enchantment ",
"Mountain Rule ",
"Oscillating Defender ",
"Pace Corruption ",
"Pacifist Dirge ",
"Pulsing Shell ",
"Quick Mash ",
"Roto Slash ",
"Shadowy Inhumation ",
"Soothing Zephyr ",
"Thundering Sorcery ",
"View Twist ",
"Accelerated Splash ",
"Arrow Bashing ",
"Change Modification ",
"Erasing Whirlpool ",
"Explosion Track ",
"Frost Fusion ",
"Geological Death ",
"Hammering Cage ",
"Heroic Slice ",
"Influence Persuer ",
"Net Change ",
"Orchestral Sentinel ",
"Paragon Programmer ",
"Pierce Alchemy ",
"Seducer Flame ",
"Shade Rhapsody ",
"Shaft Seduction ",
"Siege Rescue ",
"Unity Shock ",
"Wrestler Carol ",
"Anti Shock ",
"Beauty Repair ",
"Blood Sensor ",
"Chase Shell ",
"Cyber Rebirth ",
"Cycle Shocker ",
"Ebony Hunter ",
"Erasing Carapace ",
"Holy Maelstrom ",
"King Gale ",
"Lovely Grave ",
"Obliterator Curse ",
"Past Therapy ",
"Pinnacle Converter ",
"Refrain Bolt ",
"Regal Subjugation ",
"Sickness Spark ",
"Velocity Influence ",
"Vibrating Way ",
"Year Director "
		};
	}
	[System.Serializable]
	public class SpriteArray
	{
		public Sprite[] sprites;
		public SpriteArray()
		{
			sprites = null;
		}
	}

}
