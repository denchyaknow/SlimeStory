﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Denchyaknow
{
	//This class is attached to the main sprite of any UniMob, it has a ton of childed effects that are used through-out this script. 
	public class UniMob_Effects : MonoBehaviour
	{
		[HideInInspector] public Transform onAttackParent;
		[HideInInspector] public Transform onWalkParent;
		[HideInInspector] public Transform onHitParent;
		[HideInInspector] public Transform onDeathParent;
		[HideInInspector] public Transform onSpawnParent;
		private ParticleSystem[] onAttackParticles;
		private ParticleSystem[] onWalkParticles;
		private ParticleSystem[] onHitParticles;
		private ParticleSystem[] onDeathParticles;
		private ParticleSystem[] onSpawnParticles;
		private UniMob_Base attachedUniMob;
		// Use this for initialization
		private void OnEnable()
		{
			if(onAttackParent != null)
			{
				onAttackParticles = onAttackParent.GetComponentsInChildren<ParticleSystem>();
			}
			if(onWalkParent != null)
			{
				onWalkParticles = onWalkParent.GetComponentsInChildren<ParticleSystem>();
			}
			if(onHitParent != null)
			{
				onHitParticles = onHitParent.GetComponentsInChildren<ParticleSystem>();
			}
			if(onDeathParent != null)
			{
				onDeathParticles = onDeathParent.GetComponentsInChildren<ParticleSystem>();
			}
			if(onSpawnParent != null)
			{
				onSpawnParticles = onSpawnParent.GetComponentsInChildren<ParticleSystem>();
			}
			if(attachedUniMob == null)
			{
				attachedUniMob = transform.GetComponentInParent<UniMob_Base>();
			}
			attachedUniMob.OnAttack += PlayAttack;
			attachedUniMob.OnWalk += PlayWalk;
			attachedUniMob.OnHit += PlayHit;
			attachedUniMob.OnDeath += PlayDeath;
			attachedUniMob.OnSpawn += PlaySpawn;
		}
		private void OnDisable()
		{
			attachedUniMob.OnAttack -= PlayAttack;
			attachedUniMob.OnWalk -= PlayWalk;
			attachedUniMob.OnHit -= PlayHit;
			attachedUniMob.OnDeath -= PlayDeath;
			attachedUniMob.OnSpawn -= PlaySpawn;
		}
		private void Awake()
		{
			

		}
		void Start()
		{
			
		}

		// Update is called once per frame
		void Update()
		{

		}
		public void PlayAttack()
		{
			if (onAttackParticles == null || onAttackParticles.Length == 0) return;
			for (int i = 0; i < onAttackParticles.Length; i++)
			{
				onAttackParticles[i].Play(true);
			}
		}
		public void PlayWalk()
		{
			if (onWalkParticles == null || onWalkParticles.Length == 0) return;
			for (int i = 0; i < onWalkParticles.Length; i++)
			{
				onWalkParticles[i].Play(true);
			}
		}

		public void PlayHit()
		{
			if (onHitParticles == null || onHitParticles.Length == 0) return;
			for(int i = 0; i < onHitParticles.Length; i++)
			{
				onHitParticles[i].Play(true);
			}
		}

		public void PlayDeath()
		{
			if (onDeathParticles == null || onDeathParticles.Length == 0) return;
			for (int i = 0; i < onDeathParticles.Length; i++)
			{
				onDeathParticles[i].Play(true);
			}
		}

		public void PlaySpawn()
		{
			if (onSpawnParticles == null || onSpawnParticles.Length == 0) return;
			for (int i = 0; i < onSpawnParticles.Length; i++)
			{
				onSpawnParticles[i].Play(true);
			}
		}

	}
}
