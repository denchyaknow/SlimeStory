﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
namespace Denchyaknow
{
	//This class is part of a prefab that is childed to the root of any given UniMob
	public class UniMob_HpBar : MonoBehaviour
	{
		public static string TweenID = "HPBAR_MOB_";
		private float lastCurrent = 0;
		private float lastMax = 0;
		public float hpCurrent = 0;
		public float hpMax = 0;
		public float hpPercent = 0;
		public SpriteRenderer hpBackground;
		public SpriteRenderer hpFill;
		public float hpFadeRate = 1f;
		public float hpFadeTime = 1f;
		public float hpFadeDelay = 0.7f;
		private float lastFadeTime = 0;
		private float hpFillSize = 0;
		public UniMob_Base attachedUniMob
		{
			get
			{
				if (uniMob == null)
					uniMob = transform.GetComponentInParent<UniMob_Base>();
				return uniMob;
			}
			set
			{
				uniMob = value;
			}
		}
		private UniMob_Base uniMob;
		// Use this for initialization
		void Awake()
		{
			
				SpriteRenderer[] renders = transform.GetComponentsInChildren<SpriteRenderer>();
				if (renders.Length > 1)
				{
					hpBackground = renders[0];
					hpFill = renders[1];
					hpFillSize = hpFill.size.x;
				} else
				{
					//Debug.Log("UniMob Hpbar is broken");
					gameObject.SetActive(false);
				}
			
		}
		private void OnDisable()
		{
			DOTween.Kill(UniMob_HpBar.TweenID + gameObject.GetInstanceID());

		}
		// Update is called once per frame
		void LateUpdate()
		{
			if ((hpCurrent != uniMob.mobHealth ) || (hpMax != uniMob.mobHealthMax))
			{
				hpCurrent = uniMob.mobHealth >= 0 ? uniMob.mobHealth : 0;
				lastCurrent = hpCurrent;
				hpMax = uniMob.mobHealthMax;
				lastMax = hpMax;
				hpPercent = ((float)hpCurrent) / (float)hpMax;
				
				hpFill.size = new Vector2(hpPercent * hpFillSize, hpFill.size.y);
				hpFill.color = Color.white;
				hpBackground.color = Color.black;
				Color fillColor = hpFill.color;
				DOTween.Kill(UniMob_HpBar.TweenID + gameObject.GetInstanceID());
				DOTween.ToAlpha(() => fillColor, x => fillColor = x, 0, hpFadeTime).SetDelay(hpFadeDelay).SetId(UniMob_HpBar.TweenID + gameObject.GetInstanceID()).OnUpdate(() =>
				{
					hpFill.color = fillColor;
					Color backColor = hpBackground.color;
					backColor.a = fillColor.a;
					hpBackground.color = backColor;
				});
			}
			//if (UniMob_Properties.UniMob_HpBarHides || lastCurrent <= 0)
			//{
			//	if(Time.timeSinceLevelLoad > lastFadeTime)
			//	{
			//		//lastFadeTime = Time.timeSinceLevelLoad + fad;
			//	Color fillColor = hpFill.color;
			//	DOTween.ToAlpha(() => fillColor, x => fillColor = x, 0, hpFadeTime).SetId(transform).OnUpdate(() =>
			//		{
			//			hpFill.color = fillColor;
			//			hpBackground.color = fillColor;
			//		});
			//	}
			//	//hpFill.color = Color.Lerp(hpFill.color, Color.clear, Time.deltaTime * hpFadeRate);
			//	//hpBackground.color = Color.Lerp(hpBackground.color, Color.clear, Time.deltaTime * hpFadeRate);

			//}
		}
		
	}
}
